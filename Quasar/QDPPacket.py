"""
A module containing the base QDP Packet class
"""
from Quasar import CRC
from isti.utils import structWrapper
from isti.utils.GetSet import GetSet

ipPacketHeader = '>BBBHHHHBBHLL'
udpPacketHeader = ipPacketHeader + 'HHHH'
qdpPacketHeader = '>LBBHHH'


class QDPPacket(GetSet):
    """
    The base class for all QDP command packets
    """

    def __init__(self, packet_bytes=None, deviceType='Q330'):
        """
        Set the packet header fields up, and call setPacketBytes if
        we've been initialized with a string of bytes
        """
        self._deviceType = deviceType
        self.setQDPCRC(0 & 0xFFFFFFFF)
        # it's up to the actual command packet to set this
        # self.QDPCommand = 0
        self.setQDPVersion(2)
        self.setQDPLength(0)
        self.setQDPSequenceNumber(0)
        self.setQDPAcknowledgeNumber(0)
        self.initialize()
        if packet_bytes:
            self.setPacketBytes(packet_bytes)

    def initialize(self):
        """
        set all fields in our 'Fields' list to have a value of zero.
        """
        for field in self.Fields:
            self.set_attr(field, 0)

    def unpackQDPHeader(self, headerBytes):
        """
        unpack the QDP Header and set up internal data members
        representing each.
        """
        parts = structWrapper.unpack(qdpPacketHeader, headerBytes)
        self.setQDPCRC(parts[0] & 0xFFFFFFFF)
        # self.setQDPCommand(parts[1])
        self.setQDPVersion(parts[2])
        self.setQDPLength(parts[3])
        self.setQDPSequenceNumber(parts[4])
        self.setQDPAcknowledgeNumber(parts[5])

    def packQDPHeader(self):
        """
        Create and return a 12 byte QDP Header from our QDP header
        data members
        """
        return structWrapper.pack(
            qdpPacketHeader,
            self.getQDPCRC() & 0xFFFFFFFF,
            self.getQDPCommand(),
            self.getQDPVersion(),
            self.getQDPLength(),
            self.getQDPSequenceNumber(),
            self.getQDPAcknowledgeNumber()
        )

    def getPacketBytes(self):
        """
        Create and return a string of bytes that represents the packet.
        This string of bytes is suitable to put on the wire.
        """
        # some final setup first
        dataBytes = self.packData()
        if dataBytes:
            self.setQDPLength(len(dataBytes))
            return self.packQDPHeader() + dataBytes
        self.setQDPLength(0)
        return self.packQDPHeader()

    def setPacketBytes(self, packet_bytes):
        """
        Trigger unpacking of our header and our data members, based on
        a string of bytes being passed in
        """
        headerSize = structWrapper.calcsize(qdpPacketHeader)
        self.unpackQDPHeader(packet_bytes[0:headerSize])
        self.unpackData(packet_bytes[headerSize:])

    def computeCRC(self):
        """
        Compute our CRC and set our CRC data member to the value
        """
        packet_bytes = self.getPacketBytes()[4:]
        dataLen = len(self.getPacketBytes()) - 4
        self.setQDPCRC(CRC.computeCRC(packet_bytes, dataLen))

    def unpackData(self, packet_bytes):
        """
        Unpack a string of bytes, and set up all of our data members.
        Which members, and what they look like, are defined by self.Fields
        and self.FieldDefinition respectivly.  self.Fields is a list of the
        member names, and self.FieldDefinition is a struct module format
        string with a definition for each item in self.Fields.
        """
        # Sometimes we get "extra stuff" back.
        fieldData = structWrapper.unpack(self.FieldDefinition, packet_bytes[0:structWrapper.calcsize(self.FieldDefinition)])

        for i in range(0, len(self.Fields)):
            self.__dict__[self.Fields[i]] = fieldData[i]

    def packData(self):
        """
        Create and return a string of bytes that represent our data members.
        This is the reverse process of unpackData()
        """
        args = [self.FieldDefinition]
        # print ()
        # print(self.__class__.__name__)
        # print(self.FieldDefinition)
        # print(self.Fields)
        for field in self.Fields:
            args.append(self.__dict__[field])
        # print(args)

        return structWrapper.pack(*args)

    def prepareForSending(self):
        """
        Stub function.  This is called before sending, for any
        last minute cleanup, or preperation.
        """
        pass # pylint disable: unnecessary-pass

    def __str__(self):
        """
        Return a human readable string representing this packet instance.
        Each field in self.Fields is printed by calling the
        packet's str<FieldName> method if it exists.  If not Python's
        "%%s" is used
        """
        ret = [f'{self.__class__.__name__} ({self.getSourceIPAddressInfo()})']
        for field in self.getFields():
            strFunc = self.getStrFunction(field)
            ret.append(f'   {field}: {strFunc()}')
        return '\n'.join(ret)

    def asHex(self):
        """
        Return the packet's bytes (header + data) in hex form.
        This form is 8 bytes per line, each one in a 2 char hex
        format.  Handy for debugging.
        """
        packet_bytes = self.getPacketBytes()
        curPos = 0
        ret = ''
        for byte in packet_bytes:
            if not curPos % 8:
                ret += '\n'
            ret += f'{ord(byte):02x} '
            curPos += 1
        return ret

    def T(self, condition, a, b):
        if condition:
            return a
        return b
