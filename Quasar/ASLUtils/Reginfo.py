# This module was imported by an old version of CnC's QComm.py, but it now
# appears to be unused. Because of it appearing to be an example script for
# how to use Quasar, it may be best to keep it in the repository.

##
# This script demonstrates querying a Q330 for it's status.  This
# differs from the getStatus example in that it uses a config
# file to map tag IDs to IP addresses
#
# The root of the Qu@sar directory must be in the PYTHONPATH
# for this to work properly
#
# Please edit the IP_ADDRESS, PORT_NUMBER and AUTH_CODE variables below
# to reflect the Q330 we would like to talk to
#
##


import os

import Quasar.Site.Config
##
# Import the modules that we'll be needing for this script
##
from Quasar.Q330 import Q330, QDP_C1_CERR

REG_FILE = "register"

    
# Here is the main block of the script
def getreginfo(Rqtag=None):
    # make sure it's a valid looking tag
    try:
        int(Rqtag)
    except:
        print('Invalid tag "%s"' %Rqtag)
        return None

    # make sure the file exists
    if not os.path.isfile(REG_FILE):
        print('Config file register not found')
        return None

    # try to load the config file
    try:
        siteConfig = Quasar.Site.Config.Config(REG_FILE)
    except:
        print('Config file invalid or not readable')
        return None

    # make sure the tag that was requested exists
    deviceInfoMap = siteConfig.getDeviceMap()
    if Rqtag not in list(deviceInfoMap.keys()):
        print('Requested tag (%s) not in the config file.' %Rqtag)
        print('Config contains: %s' %(list(deviceInfoMap.keys())))
        return None

    deviceInfo = siteConfig.getDeviceInfoByTag(Rqtag)

    # Previous comments here suggested that a finally block existed here that
    # would deregister this script from the Q330. It has been removed, either
    # because it wasn't actually necessary or as a result of an automated
    # conversion from python 2 to 3.
    # No more information was available.
    try:
        # Here we're creating a Q330 instance with the information we
        # got from the config file
        q330 = Q330(eval('0x%s' %deviceInfo['Serial']), deviceInfo['IP'], deviceInfo['BasePort'])

        # set the authcode to the supplied value
        q330.setAuthCode(int(deviceInfo['AuthCode']))
            
        # Print the serial number in hex form
        print("Unit serial number is %x" %q330.getSerialNumber())
        return q330
    except QDP_C1_CERR as e:
        print(e)
    return None



