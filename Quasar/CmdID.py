from enum import Enum
"""
All of the QDP Command IDs for the QDP packets
"""


class CmdID(Enum):
    C1_RQSRV  = 0x10
    C1_SRVRSP = 0x11
    C1_DSRV   = 0x12
    C1_SAUTH  = 0x13
    C1_POLLSN = 0x14
    C1_SPHY   = 0x15
    C1_RQPHY  = 0x16
    C1_SLOG   = 0x17
    C1_RQLOG  = 0x18
    C1_CTRL   = 0x19
    C1_SGLOB  = 0x1A
    C1_RQGLOB = 0x1B
    C1_RQFIX  = 0x1C
    C1_SMAN   = 0x1D
    C1_RQMAN  = 0x1E
    C1_RQSTAT = 0x1F
    C1_WSTAT  = 0x20
    C1_VCO    = 0x21
    C1_PULSE  = 0x22
    C1_QCAL   = 0x23
    C1_STOP   = 0x24
    C1_RQRT   = 0x25
    C1_MRT    = 0x26
    C1_RQTHN  = 0x27
    C1_RQGID  = 0x28
    C1_SCNP   = 0x29
    C1_SRTC   = 0x2A
    C1_SOUT   = 0x2B
    C1_SSPP   = 0x2C
    C1_RQSPP  = 0x2D
    C1_SSC    = 0x2E
    C1_RQSC   = 0x2F
    C1_UMSG   = 0x30
    C1_SPWR   = 0x31
    C1_RQPWR  = 0x32
    C1_WEB    = 0x33
    C1_RQFLGS = 0x34
    C1_RQDCP  = 0x35
    C1_RQDEV  = 0x36
    C1_SDEV   = 0x37
    C1_PING   = 0x38
    C1_SCOM   = 0x39
    C1_RQCOM  = 0x3A
    C1_SMEM   = 0x40
    C1_RQMEM  = 0x41
    C1_ERASE  = 0x42
    C1_RQMOD  = 0x43
    C1_RQFREE = 0x44
    C2_SPHY   = 0x50
    C2_RQPHY  = 0x51
    C2_SGPS   = 0x52
    C2_RQGPS  = 0x53
    C2_SWIN   = 0x54
    C2_RQWIN  = 0x55
    C2_SAMASS = 0x57
    C2_RQAMASS= 0x58
    C2_SBPWR  = 0x59
    C2_BRDY   = 0x5A
    C2_BOFF   = 0x5B
    C2_BRESP  = 0x5C
    C2_REGCHK = 0x5D
    C2_INST   = 0x5E
    C2_RQQV   = 0x5F
    C2_RQMD5  = 0x60
    C2_SNAPT  = 0x61
    #C2_DEP    = 0x69 # Overwritten by TERC
    #C2_DEPR   = 0x6A # Overwritten by TERR
    # added by Kanglin
    C2_TERC   = 0x69
    C2_TERR   = 0x6A
    #added by JDEdwards
    C2_RQEPD  = 0x70
    C2_RQEPCFG= 0x71
    C2_SEPCFG = 0x72
    C2_EPTUN  = 0x73
    C2_RQUMSGS= 0x74

    C1_CACK   = 0xA0
    C1_SRVCH  = 0xA1
    C1_CERR   = 0xA2
    C1_MYSN   = 0xA3
    C1_PHY    = 0xA4
    C1_LOG    = 0xA5
    C1_GLOB   = 0xA6
    C1_FIX    = 0xA7
    C1_MAN    = 0xA8
    C1_STAT   = 0xA9
    C1_RT     = 0xAA
    C1_THN    = 0xAB
    C1_GID    = 0xAC
    C1_RCNP   = 0xAD
    C1_SPP    = 0xAE
    C1_SC     = 0xAF
    C1_PWR    = 0xB0
    C1_FLGS   = 0xB1
    C1_DCP    = 0xB2
    C1_DEV    = 0xB3
    C1_COM    = 0xB4
    C1_MEM    = 0xB8
    C1_MOD    = 0xB9
    C1_FREE   = 0xBA
    C2_PHY    = 0xC0
    C2_GPS    = 0xC1
    C2_WIN    = 0xC2
    C2_AMASS  = 0xC4
    C2_POC    = 0xC5
    C2_BACK   = 0xC6
    C2_VACK   = 0xC7
    C2_BCMD   = 0xC8
    C2_REGRESP= 0xC9
    C2_QV     = 0xCA
    C2_MD5    = 0xCB
    #added by JDEdwards
    C2_EPD    = 0xD0
    C2_EPCFG  = 0xD1
    C2_EPRESP = 0xD2
    C2_UMSGS  = 0xD3

    # added by Kanglin
    C3_ANNC   = 0
    C3_RQANNC = 1
    C3_SANNC  = 2
    #added by JDEdwards
    C3_BCFG   = 3
    C3_RQBCFG = 4
    C3_SBCFG  = 5

    # baler specific
    RL_CMD = 0xBF
