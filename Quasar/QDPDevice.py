"""
This module contains a base class for anything that can talk QDP
"""

import socket
import time

from Quasar import Cmds
from Quasar.CmdID import CmdID
from Quasar.Commands import c1_cerr
from Quasar.Commands.rl_cmd import RLCMD_ERROR, RLCMD_ACK, RLCMD_DATA
from isti.utils import structWrapper
from isti.utils.GetSet import GetSet


class QDP_C1_CERR(Exception):
    """
    An exception class to be raised when a
    c1_err is returned
    """

    def __init__(self, errCode, errMsg):
        Exception.__init__(self, errMsg)
        self.setErrorCode(errCode)

    def __str__(self):
        return c1_cerr.codeToMessage(self.errorCode)

    def setErrorCode(self, num):
        self.errorCode = num

    def getErrorCode(self):
        return self.errorCode


class TimeoutException(Exception):
    """
    An exception used for simulating a socket timeout
    in pre 2.3 versions of python
    """


class QDPDevice(GetSet):
    """
    The base class for any other classes that speak QDP.
    """

    def __init__(self):
        self.connection = None
        self.seqNum = -1
        self.setDeviceType("Generic QDP Device")
        self.setBroadcastIP('255.255.255.255')
        self.commandsSent = 0
        self.packetsReceived = 0
        self.txBytes = 0
        self.rxBytes = 0
        self.numRetries = 3
        self.skipSwappedBytesInPortHack = 0
        self.maxTries = 1
        self.bind_address = None

    def getMaxTries(self):
        return self.maxTries

    def setMaxTries(self, tries):
        if type(tries) != int:
            raise TypeError("Invalid type for maxTries")
        self.maxTries = tries

    def incrementBytesSent(self, toAdd):
        self.txBytes += toAdd

    def incrementBytesReceived(self, toAdd):
        self.rxBytes += toAdd

    def getBytesReceived(self):
        return self.rxBytes

    def getBytesSent(self):
        return self.txBytes

    def clearByteCounters(self):
        self.rxBytes = 0
        self.txBytes = 0

    def getNextSequenceNumber(self):
        """
        Get the next unused sequence number, for QDP use
        """
        if self.seqNum == 65536:
            self.seqNum = 0

        self.seqNum += 1
        return self.seqNum

    def setReceiveTimeout(self, newTimeoutValue):
        """
        Set the number of seconds we should wait for a packet to arrive, before we
        give up
        """
        self.receiveTimeout = newTimeoutValue
        if self.connection is not None:
            # self.closeConnection()
            # self.connection = None
            self.connection.settimeout(newTimeoutValue)

    def setNumRetries(self, newNumRetries):
        """
        How many times should we retry a command before giving up
        """
        self.numRetries = newNumRetries

    def getNumRetries(self):
        return self.numRetries

    def getReceiveTimeout(self):
        """
        return the receive timeout, or the default (20) if one has not been set
        """
        try:
            return self.receiveTimeout
        except:
            return 20

    def openConnection(self):
        """
        Prepare our socket connection.
        """
        self.connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # try to bind to address if specified
        if self.bind_address is not None:
            self.connection.bind(self.bind_address)

        # only set the socket timeout if we're running a socket API that has the
        # ability to do so
        if 'settimeout' in dir(self.connection):
            self.connection.settimeout(self.getReceiveTimeout())

    def setBindAddress(self, bind_address):
        self.bind_address = bind_address
        if self.connection is not None:
            self.connection.bind(self.bind_address)

    def getBindAddress(self):
        return self.bind_address

    def closeConnection(self):
        """
        Finalize, and close our socket connection.
        """
        self.connection.close()
        self.connection = None

    def getPortForCommand(self, cmd):
        """
        What port do we use to send a particular command packet?
        """
        return self.getBasePort()

    def getVersionTuple(self):
        return 1, 0, 'generic'

    def sendCommand(self, cmd, receiveMultiple=0, preservePortForBroadcast=0, skipReceive=0, singleSend=0):
        original_timeout = self.getReceiveTimeout()
        tries = self.maxTries
        while 1:
            try:
                result = self._sendCommand(cmd, receiveMultiple, preservePortForBroadcast, skipReceive, singleSend)
                self.setReceiveTimeout(original_timeout)
                return result
            except TimeoutException as e:
                tries -= 1
                if tries <= 0:
                    self.setReceiveTimeout(original_timeout)
                    raise
                last_timeout = self.getReceiveTimeout()
                self.setReceiveTimeout(last_timeout + (last_timeout / 2))
            except:
                self.setReceiveTimeout(original_timeout)
                raise

    def _sendCommand(self, cmd, receiveMultiple=0, preservePortForBroadcast=0, skipReceive=0, singleSend=0):
        """
        Send cmd onto the wire.  The packet sent to the IP address and port
        that has been set by the setIPAddress() and setBasePort() methods.

        receiveMultiple - If non-zero, a list of responses (possibly containing
        a single response) will be returned
        """
        if not self.connection:
            self.openConnection()

        cmd.setQDPSequenceNumber(self.getNextSequenceNumber())
        cmd.getPacketBytes()
        cmd.computeCRC()

        if cmd.getIsBroadcast() and self.getIPAddress() == '255.255.255.255':
            if 'SO_BROADCAST' in dir(socket):
                self.connection.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            host = self.getBroadcastIP()  # '255.255.255.255'
            if preservePortForBroadcast == 0:
                port = 65535
            else:
                port = self.getPortForCommand(cmd)
        else:
            host = self.getIPAddress()
            port = self.getPortForCommand(cmd)

        cmd.prepareForSending()
        bytesToSend = cmd.getPacketBytes()

        # this is being injected here because the Q330 seems to want it
        time.sleep(.05)

        recvConnection = None
        alreadySent = None
        recvConnectionListenPort = 0

        ##
        # Below is a hack for the "replying on byteswapped port number"
        # bug in early Baler releases
        ##
        if self.getDeviceType() == 'Baler14' and \
                cmd.getQDPCommand() == CmdID.C2_BCMD:
            major, minor, mod = self.getVersionTuple()
            if major == 1 and ((minor < 40) or
                               (minor == 40 and mod in ('RC1',))):
                recvConnection = socket.socket(socket.AF_INET,
                                               socket.SOCK_DGRAM)
                try:
                    recvConnection.bind(
                        (self.connection.getsockname()[0],
                         socket.ntohs(self.connection.getsockname()[1])))
                    recvConnectionListenPort = \
                        socket.ntohs(self.connection.getsockname()[1])
                except:
                    try:
                        self.connection.sendto(bytesToSend, 0, (host, port))
                    except:
                        self.connection.sendto(bytesToSend, (host, port))
                    alreadySent = 1
                    recvConnection.bind(
                        (self.connection.getsockname()[0], socket.ntohs(self.connection.getsockname()[1])))

        if not alreadySent:
            try:
                self.connection.sendto(bytesToSend, 0, (host, port))
            except:
                self.connection.sendto(bytesToSend, (host, port))

        self.incrementBytesSent(len(bytesToSend))

        if skipReceive != 0:
            return None
        ##
        # Here we do different things if we're expecting to receive multiple items or
        # not
        ##
        if receiveMultiple == 0:
            ##
            # We want a single response
            ##
            rsp = cmd

            while rsp == cmd or \
                    rsp.getQDPAcknowledgeNumber() != cmd.getQDPSequenceNumber():
                # result & addr should get defined
                # by the time they are read from
                result = ''
                addr = (None, None)
                gotData = 0
                attempts = 0

                # we're going to re-try 3 times,
                # and give up if we havent heard back
                while gotData == 0 and attempts < self.numRetries:
                    if recvConnection and recvConnectionListenPort == 0 and self.connection.getsockname()[1] != 0:
                        recvConnection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        recvConnection.bind(
                            (self.connection.getsockname()[0], socket.ntohs(self.connection.getsockname()[1])))

                    attempts = attempts + 1
                    if 'timeout' in socket.__dict__:
                        if recvConnection:
                            recvConnection.settimeout(self.getReceiveTimeout())
                        else:
                            self.connection.settimeout(self.getReceiveTimeout())
                        try:
                            if recvConnection:
                                result, addr = recvConnection.recvfrom(2048)
                            else:
                                result, addr = self.connection.recvfrom(2048)
                            gotData = 1
                        except socket.error as e:
                            pass
                    else:
                        timeoutVal = self.getReceiveTimeout()
                        startTime = time.time()
                        if recvConnection:
                            recvConnection.setblocking(False)
                        else:
                            self.connection.setblocking(False)
                        while (time.time() - startTime) < timeoutVal:
                            try:
                                if recvConnection:
                                    result, addr = recvConnection.recvfrom(2048)
                                else:
                                    result, addr = self.connection.recvfrom(2048)
                                gotData = 1
                            except socket.error as e:
                                pass
                            if gotData == 1:
                                if recvConnection:
                                    recvConnection.setblocking(True)
                                else:
                                    self.connection.setblocking(True)
                                break
                        if recvConnection:
                            recvConnection.setblocking(True)
                        else:
                            self.connection.setblocking(True)

                    # resend, if we got nothing    
                    if gotData == 0 and not singleSend:
                        try:
                            self.connection.sendto(bytesToSend, 0, (host, port))
                        except:
                            self.connection.sendto(bytesToSend, (host, port))
                        self.incrementBytesSent(len(bytesToSend))

                if not gotData:
                    raise TimeoutException('recv timed out')

                self.incrementBytesReceived(len(result))

                # decide what type of packet we received
                aaa = result[4:5]
                cid = structWrapper.unpack('B', result[4:5])[0]
                # return the received packet class
                rsp = Cmds.cmdToClass[cid](result)
                rsp.setSourceIPAddressInfo(addr)

            if (isinstance(rsp, Cmds.c1_cerr)) or (
                    isinstance(rsp, Cmds.rl_cmd) and
                    rsp.getCommandType() == RLCMD_ERROR):
                # this is an error
                raise QDP_C1_CERR(rsp.getErrorCode(), rsp.getErrorText())

            if recvConnection:
                recvConnection.close()
                del recvConnection
        else:
            ##
            # we want multiple responses
            ##
            # do this once outside the loop, so that we can be sure the timeout period is waited
            # if there's nothing on the line
            result = 'foof'
            addr = (None, None)
            rsp = []
            iteration = 0
            timeoutVal = self.getReceiveTimeout()
            while (result not in (None, '')):
                ## take into account pythons without settimeout()
                if 'timeout' in socket.__dict__:
                    ## This python has timeout
                    try:
                        result, addr = self.connection.recvfrom(2048)
                    except socket.timeout as e:
                        ##
                        # Only rethrow the exception if we timed out the first time.  This
                        # is really the only time we should do so here
                        ##
                        if iteration == 0:
                            raise TimeoutException('recv timed out')
                        else:
                            break
                else:
                    ## this python has no timeout
                    startTime = time.time()
                    try:
                        self.connection.setblocking(0)
                        gotData = 0
                        while (time.time() - startTime) < timeoutVal:
                            try:
                                result, addr = self.connection.recvfrom(2048)
                                gotData = 1
                                break
                            except socket.error as e:
                                pass
                        if not gotData:
                            self.connection.setblocking(1)
                            raise TimeoutException('recv timed out')
                    except TimeoutException as e:
                        if iteration == 0:
                            self.connection.setblocking(1)
                            raise e
                        else:
                            break

                    self.setblocking(1)

                cid = structWrapper.unpack('B', result[4:5])[0]
                responsePacket = Cmds.cmdToClass[cid](result)
                responsePacket.setSourceIPAddressInfo(addr)

                ##
                # Communicating with the baler is somewhat different.  In the case of the baler,
                # RL_CMD responses to a read request need to be ACK'd one at a time.  We'll
                # do this here.  Also, duplicated 
                #
                # We do our special stuff if we're a baler, we got an RL_CMD and it's an RLCMD_DATA packet
                ##
                if self.getDeviceType() == 'Baler14' and responsePacket.getQDPCommand() == CmdID.RL_CMD and responsePacket.getCommandType() == RLCMD_DATA:
                    # first, lets ack
                    rlcmd = Cmds.rl_cmd()
                    rlcmd.setCommandType(RLCMD_ACK)
                    rlcmd.setBlockNumber(responsePacket.getBlockNumber())
                    self.sendCommand(rlcmd, skipReceive=1)

                    # now, lets append this packet to the list if we havent gotten it yet
                    alreadyReceived = 0
                    for received in rsp:
                        if received.getBlockNumber() == responsePacket.getBlockNumber():
                            alreadyReceived = 1
                            break
                    if alreadyReceived == 0:
                        rsp.append(responsePacket)
                else:
                    if responsePacket.getQDPAcknowledgeNumber() == cmd.getQDPSequenceNumber():
                        rsp.append(responsePacket)

                ##
                # after we're gotten the first packet that is a response to the initial
                # command, drop the timeout to almost nothing, because data is flowing
                ##
                if iteration == 0 and len(rsp) != 0:
                    iteration += 1
                    try:
                        self.connection.settimeout(1)
                    except:
                        # settimeout only exists in python2.3 and higher
                        timeoutVal = 1

            ##
            # put the timeout back to the way we found it
            ##
            try:
                self.connection.settimeout(self.getReceiveTimeout())
            except:
                pass

        return rsp

    # This method was only used in a now-deleted module and can probably be
    # safely removed if (unlikely) errors prevent addr from being assigned
    # a value
    def receiveResponse(self, sentCommand=None, receiveMultiple=False,
                        auxReceiveSocket=None, recvSize=2048):
        ##
        # Here we do different things if we're expecting to receive
        # multiple items or not
        ##
        #

        # neither of these values were defined elsewhere in this method.
        # these lines were copied and pasted from _sendCommand
        host = self.getIPAddress()
        port = self.getPortForCommand(sentCommand)


        recvConnection = self.connection
        if auxReceiveSocket:
            recvConnection = auxReceiveSocket
        if not receiveMultiple:
            ##
            # We want a single response
            ##
            rsp = sentCommand

            while not rsp or rsp == sentCommand or (
                    sentCommand and rsp.getQDPAcknowledgeNumber() != sentCommand.getQDPSequenceNumber()):
                result = ''
                gotData = 0
                attempts = 0

                # we're going to re-try 3 times, and give up if we havent heard back
                while gotData == 0 and attempts < 3:
                    if auxReceiveSocket and self.connection.getsockname()[1] != 0:
                        recvConnection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        recvConnection.bind(
                            (self.connection.getsockname()[0], socket.ntohs(self.connection.getsockname()[1])))

                    attempts = attempts + 1
                    if 'timeout' in socket.__dict__:
                        recvConnection.settimeout(self.getReceiveTimeout())
                        try:
                            result, addr = recvConnection.recvfrom(recvSize)
                            gotData = 1
                        except socket.error as e:
                            pass
                    else:
                        timeoutVal = self.getReceiveTimeout()
                        startTime = time.time()
                        recvConnection.setblocking(False)
                        while (time.time() - startTime) < timeoutVal:
                            try:
                                result, addr = recvConnection.recvfrom(recvSize)
                                gotData = 1
                            except socket.error as e:
                                pass
                            if gotData == 1:
                                recvConnection.setblocking(True)
                                break
                        recvConnection.setblocking(True)

                    # resend, if we got nothing    
                    if gotData == 0 and sentCommand:
                        try:
                            self.connection.sendto(sentCommand.getPacketBytes(), 0, (host, port))
                        except:
                            self.connection.sendto(sentCommand.getPacketBytes(), (host, port))
                        self.incrementBytesSent(len(sentCommand.getPacketBytes()))

                if not gotData:
                    raise TimeoutException('recv timed out')

                self.incrementBytesReceived(len(result))

                print("[QDPDevice Debug]> Data:", result)  # Added JDE
                # decide what type of packet we received
                cid = structWrapper.unpack('B', result[4])[0]
                # return the received packet class
                rsp = Cmds.cmdToClass[cid](result)
                rsp.setSourceIPAddressInfo(addr)

            if (isinstance(rsp, Cmds.c1_cerr)) or (
                    isinstance(rsp, Cmds.rl_cmd) and rsp.getCommandType() == RLCMD_ERROR):
                # this is an error
                raise QDP_C1_CERR(rsp.getErrorCode(), rsp.getErrorText())

            # if recvConnection:
            #    recvConnection.close()
            #    del recvConnection

        else:
            ##
            # we want multiple responses
            ##
            # do this once outside the loop, so that we can be sure the timeout period is waited
            # if there's nothing on the line
            result = 'foof'
            addr = (None, None)
            rsp = []
            iteration = 0
            timeoutVal = self.getReceiveTimeout()
            while (result not in (None, '')):
                ## take into account pythons without settimeout()
                if 'timeout' in socket.__dict__:
                    ## This python has timeout
                    try:
                        result, addr = recvConnection.recvfrom(recvSize)
                    except socket.timeout as e:
                        ##
                        # Only rethrow the exception if we timed out the first time.  This
                        # is really the only time we should do so here
                        ##
                        if iteration == 0:
                            raise TimeoutException('recv timed out')
                        else:
                            break
                else:
                    ## this python has no timeout
                    startTime = time.time()
                    try:
                        recvConnection.setblocking(0)
                        gotData = 0
                        while (time.time() - startTime) < timeoutVal:
                            try:
                                result, addr = recvConnection.recvfrom(recvSize)
                                gotData = 1
                                break
                            except socket.error as e:
                                pass
                        if not gotData:
                            recvConnection.setblocking(1)
                            raise TimeoutException('recv timed out')
                    except TimeoutException as e:
                        if iteration == 0:
                            recvConnection.setblocking(1)
                            raise e
                        else:
                            break

                    self.setblocking(1)

                cid = structWrapper.unpack('B', result[4])[0]
                responsePacket = Cmds.cmdToClass[cid](result)
                responsePacket.setSourceIPAddressInfo(addr)

                ##
                # Communicating with the baler is somewhat different.  In the case of the baler,
                # RL_CMD responses to a read request need to be ACK'd one at a time.  We'll
                # do this here.  Also, duplicated 
                #
                # We do our special stuff if we're a baler, we got an RL_CMD and it's an RLCMD_DATA packet
                ##
                if self.getDeviceType() == 'Baler14' and responsePacket.getQDPCommand() == CmdID.RL_CMD and responsePacket.getCommandType() == RLCMD_DATA:
                    # first, lets ack
                    rlcmd = Cmds.rl_cmd()
                    rlcmd.setCommandType(RLCMD_ACK)
                    rlcmd.setBlockNumber(responsePacket.getBlockNumber())
                    self.sendCommand(rlcmd, skipReceive=1)

                    # now, lets append this packet to the list if we havent gotten it yet
                    alreadyReceived = 0
                    for received in rsp:
                        if received.getBlockNumber() == responsePacket.getBlockNumber():
                            alreadyReceived = 1
                            break
                    if alreadyReceived == 0:
                        rsp.append(responsePacket)
                else:
                    if sentCommand:
                        if responsePacket.getQDPAcknowledgeNumber() == \
                                sentCommand.getQDPSequenceNumber():
                            rsp.append(responsePacket)
                    else:
                        rsp.append(responsePacket)

                ##
                # after we're gotten the first packet that is a response to the initial
                # command, drop the timeout to almost nothing, because data is flowing
                ##
                if iteration == 0 and len(rsp) != 0:
                    iteration += 1
                    try:
                        recvConnection.settimeout(1)
                    except:
                        # settimeout only exists in python2.3 and higher
                        timeoutVal = 1

            ##
            # put the timeout back to the way we found it
            ##
            try:
                recvConnection.settimeout(self.getReceiveTimeout())
            except:
                pass

        return rsp
