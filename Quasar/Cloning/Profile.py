"""
Module to manage configuration profiles
With the exception of profile management related items, everything
needed for a cloning system is contained between here and in the
Q330 class
"""

import pickle
import sys
import time

from Quasar.QDPDevice import QDP_C1_CERR, TimeoutException

from Quasar.Cmds import c1_rqcom, c1_rqman, c1_rqphy, c1_rqlog, c1_rqglob, \
    c1_rqspp, c1_rqsc, c1_rqpwr, c1_rqfix, c1_sphy, c1_scom, c1_slog, \
    c1_sglob, c1_sspp, c1_ssc, c1_spwr, c2_rqgps, c2_rqamass, c2_rqwin, \
    c2_rqphy, c2_rqepcfg, c2_swin, c2_sgps, c2_sphy, c2_sepcfg, c2_samass, \
    c3_rqannc, c3_rqbcfg, c3_sannc, c3_sbcfg
from Quasar.Tokens import TokenStructs, Token, MEM_TYPE_DP_1, MEM_TYPE_DP_2, \
    MEM_TYPE_DP_3, MEM_TYPE_DP_4
from isti.utils.GetSet import GetSet


class Q330TypeMismatchException(Exception):
    pass


# it is not clear to me why a dictionary needs an index in each value
# given that index values don't seem to be used. If an ordering is used I would
# suggest that an order based on the alphabetical order of keys is more
# appropriate. the function to match an index to a K-V pair is not used anywhere
# Also, apparently the second item in the tuple is a callable function?
section_map = {
    'C1_MAN': (0, "ManArea", "Manufacturer's Area", ('C1_RQMAN',), ('C1_SMAN',)),
    'C1_MEM_1': (1, "TokenSet1", 'Data Port 1 Tokens', ('C1_RQMEM',), ('C1_SMEM',)),
    'C1_MEM_2': (2, "TokenSet2", 'Data Port 2 Tokens', ('C1_RQMEM',), ('C1_SMEM',)),
    'C1_MEM_3': (3, "TokenSet3", 'Data Port 3 Tokens', ('C1_RQMEM',), ('C1_SMEM',)),
    'C1_MEM_4': (4, "TokenSet4", 'Data Port 4 Tokens', ('C1_RQMEM',), ('C1_SMEM',)),
    'C1_MEM_W': (5, "WebPage", 'Web Page', ('C1_RQMEM',), ('C1_SMEM',)),
    'C1_PHY': (6, "Phy", 'Physical Interfaces', ('C1_RQPHY',), ('C1_SPHY',)),
    'C1_LOG_1': (7, "Log1", 'Data Port 1 Programming', ('C1_RQLOG',), ('C1_SLOG',)),
    'C1_LOG_2': (8, "Log2", 'Data Port 2 Programming', ('C1_RQLOG',), ('C1_SLOG',)),
    'C1_LOG_3': (9, "Log3", 'Data Port 3 Programming', ('C1_RQLOG',), ('C1_SLOG',)),
    'C1_LOG_4': (10, "Log4", 'Data Port 4 Programming', ('C1_RQLOG',), ('C1_SLOG',)),
    'C1_GLOB': (11, "Glob", 'Global Programming', ('C1_RQGLOB',), ('C1_SGLOB',)),
    'C1_SPP': (12, "Spp", 'Slave Processor Parameters', ('C1_RQSPP',), ('C1_SSPP',)),
    'C1_SC': (13, "Sc", 'Sensor Control Mapping', ('C1_RQSC',), ('C1_SSC',)),
    'C1_PWR': (14, "Pwr", 'Power Programming', ('C1_RQPWR',), ('C1_SPWR',)),

    'C2_GPS': (15, "Gps", 'GPS Programming', ('C2_RQGPS',), ('C2_SGPS',)),
    'C2_AMASS': (16, "Amass", 'Automatic Mass Center Programming', ('C2_RQAMASS',), ('C2_SAMASS',)),
    'C2_WIN': (17, "Win", 'Recording Window Programming', ('C2_RQWIN',), ('C2_SWIN',)),
    'C2_PHY_SP1': (18, "PhySer1", 'Serial Port 1 Programming', ('C2_RQPHY',), ('C2_SPHY',)),
    'C2_PHY_SP2': (19, "PhySer2", 'Serial Port 2 Programming', ('C2_RQPHY',), ('C2_SPHY',)),
    'C2_PHY_SP3': (20, "PhySer3", 'Serial Port 3 Programming', ('C2_RQPHY',), ('C2_SPHY',)),
    'C2_PHY_ETH': (21, "PhyEther", 'Ethernet Port Programming', ('C2_RQPHY',), ('C2_SPHY',)),
    'C2_EPCFG': (22, "EpCfg", 'Environmental Processer Programming', ('C2_RQEPCFG',), ('C2_SEPCFG',)),

    'C3_ANNC': (23, "AnncStruct", 'Announce Structure Programming', ('C3_RQANNC',), ('C3_SANNC',)),
    'C3_BCFG': (24, "BalerCfg", 'Baler Configuration', ('C3_RQBCFG',), ('C3_SBCFG',)),
    'C1_COM': (25, "Com", 'Communications', ('C1_RQCOM',), ('C1_SCOM',)),
}


# getSectionFromIndex and getIndexFromSection are not used in Quasar or CnC
def getSectionFromIndex(index):
    for k, v in section_map:
        if v[0] == index:
            return k
    return None


def getIndexFromSection(section):
    if section in section_map:
        return section_map[section][0]
    return None


MAX_DESC_LEN = max(list(map(len, [v[2] for v in list(section_map.values())])))
MAX_RQST_LEN = max(list(map(len, [v[3][0] for v in list(section_map.values())])))
MAX_RESP_LEN = max(list(map(len, [v[4][0] for v in list(section_map.values())])))
MAX_CMD_LEN = max(MAX_RQST_LEN, MAX_RESP_LEN)


def writeProfileToFile(prof, fileName=None):
    """
    Write a profile object to a file.  <profile> is a Profile instance,
    fileName is the name of the file to write to.  None results in the
    file being named <the_profiles_profile_name>.q330.
    """
    if fileName is None:
        fileName = '%s.q330' % prof.getProfileName()
    pickle.dump(prof, open(fileName, 'wb'))


def getProfileFromFile(filename):
    """
    Read a profile from a file, and return a Profile class
    """
    return pickle.load(open(filename, 'rb'))


class Profile(GetSet):
    """
    A class representing the "essence" of being a particular Q330.  All of the
    config structs and all the token memory is contained here.
    """

    # IMPORTANT NOTE: Getter and setter methods defined inside the class like
    # _setQ330Section are defined not in relation to this class but the Q330
    # itself. Calling 'get' on a Q330 field sets the __dict__ in this class to
    # the result of the command called on the Q330. Calling 'set' on a field
    # will change the value on the Q330 to that stored in __dict__.

    # TODO: need to find some way to verify that what is stored in these
    #  dict entries is actually a callable function or not. This is relevant
    #  in two sections -- listFileContents and diff

    def __init__(self, profileName=None, verbosity=0, tries=1):
        if profileName is not None:
            self.setProfileName(profileName)
        self.verbosity = verbosity
        self.tries = tries
        self.subset = None

        # Controlled items must be set to True in order to modify the Q330
        # configuration. They are set to true here such that Quasar's default
        # behavior is to overwrite the Q330's configuration in spite of the
        # fact that these are known to cause issues from time to time.
        # This gives the developer control over whether or not they wish to
        # disable these actions by default. For example, the CnC suite
        # disables these actions unless special flags are supplied which
        # are associated with each action.
        #
        # The one exception is the Ethernet MAC Address, because we really
        # shouldn't change this unless we are really sure we want to.

        # controlled parameters
        self.write_base_port = True
        self.write_ethernet_ip_address = True

        # Q330 Only
        self.write_ethernet_mac_address = False

        # Q335 Only
        self.write_ethernet_netmask = True
        self.write_ethernet_gateway_address = True
        self.write_baler_base_port = True

        # controlled sections
        self.write_slave_processor_parameters = True
        self.write_web_page = True

        for key, value in list(section_map.items()):
            self.__dict__["IndexOf" + value[1]] = value[0]

    def _getQ330Section(self, q330, section):
        if section == 'C1_FIX':
            pass
        elif self.subset is not None:
            idx = section_map[section][0]
            if idx not in self.subset:
                return
        tries = self.tries
        count = 0
        backoff = 1.0
        max_backoff = 60.0
        while tries > 0:
            tries -= 1
            count += 1
            if section == 'C1_FIX':
                description = 'Fixed Values after Reboot'
                cmd_info = ('C1_RQFIX',)
                idx_str = '      '
            else:
                idx, _, description, cmd_info, _ = section_map[section]
                idx_str = '% 3d : ' % idx
            try:
                ci_str = str("(%s)" % cmd_info[0]).ljust(MAX_CMD_LEN + 2)
                print("  try #%d: Get %s%s %s" %
                      (count, idx_str, description.ljust(MAX_DESC_LEN),
                       ci_str), end=' ')
                sys.stdout.flush()
                self._getSectionByID(q330, section)
                print("[Success]")
                tries = 0
            except QDP_C1_CERR as e:
                code = e.getErrorCode()
                print("[FAILED]")
                print("    %s received C1_CERR [%d]: %s" %
                      (cmd_info[0], code, str(e)))
                if code in (8, 12):
                    if tries > 0:
                        time.sleep(backoff)
                        backoff = backoff * 2
                        if backoff > max_backoff:
                            backoff = max_backoff
                        continue
                raise
            except TimeoutException as e:
                print("[FAILED]")
                print(f"    {cmd_info[0]} timed out while reading: {str(e)}")
                if tries > 0:
                    time.sleep(backoff)
                    backoff = backoff * 2
                    if backoff > max_backoff:
                        backoff = max_backoff
                    continue
                raise

    def _getSectionByID(self, q330, section):
        if section == 'C1_MEM_1':
            self.setTokenSet1(q330.getTokenSet(MEM_TYPE_DP_1))
        elif section == 'C1_MEM_2':
            self.setTokenSet2(q330.getTokenSet(MEM_TYPE_DP_2))
        elif section == 'C1_MEM_3':
            self.setTokenSet3(q330.getTokenSet(MEM_TYPE_DP_3))
        elif section == 'C1_MEM_4':
            self.setTokenSet4(q330.getTokenSet(MEM_TYPE_DP_4))
        elif section == 'C1_MEM_W':
            self.setWebPage(q330.getWebPage())
        elif section == 'C1_MAN':
            self.setMan(q330.sendCommand(c1_rqman()))
        elif section == 'C1_PHY':
            self.setPhy(q330.sendCommand(c1_rqphy()))
        elif section == 'C1_COM':
            self.setCom(q330.sendCommand(c1_rqcom()))
        elif section == 'C1_LOG_1':
            rqlog1 = c1_rqlog()
            rqlog1.setDataPortNumber(0)
            self.setLog1(q330.sendCommand(rqlog1))
        elif section == 'C1_LOG_2':
            rqlog2 = c1_rqlog()
            rqlog2.setDataPortNumber(1)
            self.setLog2(q330.sendCommand(rqlog2))
        elif section == 'C1_LOG_3':
            rqlog3 = c1_rqlog()
            rqlog3.setDataPortNumber(2)
            self.setLog3(q330.sendCommand(rqlog3))
        elif section == 'C1_LOG_4':
            rqlog4 = c1_rqlog()
            rqlog4.setDataPortNumber(3)
            self.setLog4(q330.sendCommand(rqlog4))
        elif section == 'C1_GLOB':
            self.setGlob(q330.sendCommand(c1_rqglob()))
        elif section == 'C1_SPP':
            self.setSpp(q330.sendCommand(c1_rqspp()))
        elif section == 'C1_SC':
            self.setSc(q330.sendCommand(c1_rqsc()))
        elif section == 'C1_PWR':
            # DEPRECATED as of version 13
            self.setPwr(q330.sendCommand(c1_rqpwr()))
        elif section == 'C2_GPS':
            self.setGps(q330.sendCommand(c2_rqgps()))
        elif section == 'C2_AMASS':
            self.setAmass(q330.sendCommand(c2_rqamass()))
        elif section == 'C2_WIN':
            self.setWin(q330.sendCommand(c2_rqwin()))
        elif section == 'C2_PHY_SP1':
            rqphySer1 = c2_rqphy()
            rqphySer1.setPhysicalInterfaceNumber(0)
            self.setPhySer1(q330.sendCommand(rqphySer1))
        elif section == 'C2_PHY_SP2':
            rqphySer2 = c2_rqphy()
            rqphySer2.setPhysicalInterfaceNumber(1)
            self.setPhySer2(q330.sendCommand(rqphySer2))
        elif section == 'C2_PHY_SP3':
            rqphySer3 = c2_rqphy()
            rqphySer3.setPhysicalInterfaceNumber(2)
            self.setPhySer3(q330.sendCommand(rqphySer3))
        elif section == 'C2_PHY_ETH':
            rqphyEther = c2_rqphy()
            rqphyEther.setPhysicalInterfaceNumber(3)
            self.setPhyEther(q330.sendCommand(rqphyEther))
        elif section == 'C2_EPCFG':
            self.setEpCfg(q330.sendCommand(c2_rqepcfg()))
        elif section == 'C3_ANNC':
            self.setAnncStruct(q330.sendCommand(c3_rqannc()))
        elif section == 'C3_BCFG':
            self.setBalerCfg(q330.sendCommand(c3_rqbcfg()))
        elif section == 'C1_FIX':
            self.setFixedValues(q330.sendCommand(c1_rqfix()))

    def _setQ330Section(self, q330, section):
        if self.subset is not None:
            idx = section_map[section][0]
            if idx not in self.subset:
                return
        tries = self.tries
        count = 0
        backoff = 1.0
        max_backoff = 60.0
        while tries > 0:
            tries -= 1
            count += 1
            idx, _, description, _, cmd_info = section_map[section]
            try:
                ci_str = str("(%s)" % cmd_info[0]).ljust(MAX_CMD_LEN + 2)
                print("  try #%d: Set % 3d : %s %s" %
                      (count, idx, description.ljust(MAX_DESC_LEN), ci_str),
                      end=' ')
                sys.stdout.flush()
                if (section == 'C1_MEM_W') and not self.write_web_page:
                    print("[SKIP]")
                    return
                if (section == 'C1_SPP') and not \
                        self.write_slave_processor_parameters:
                    print("[SKIP]")
                    return
                self._setSectionByID(q330, section)
                print("[Success]")
                tries = 0
            except QDP_C1_CERR as e:
                code = e.getErrorCode()
                print("[FAILED]")
                print("    %s received C1_CERR: %s" % (cmd_info[0], str(e)))
                if code in (8, 12):
                    if tries > 0:
                        time.sleep(backoff)
                        backoff = backoff * 2
                        if backoff > max_backoff:
                            backoff = max_backoff
                        continue
                raise

    def _setSectionByID(self, q330, section):
        if section == 'C1_MEM_1':
            q330.setTokenSet(self.getTokenSet1(), MEM_TYPE_DP_1)
        elif section == 'C1_MEM_2':
            q330.setTokenSet(self.getTokenSet2(), MEM_TYPE_DP_2)
        elif section == 'C1_MEM_3':
            q330.setTokenSet(self.getTokenSet3(), MEM_TYPE_DP_3)
        elif section == 'C1_MEM_4':
            q330.setTokenSet(self.getTokenSet4(), MEM_TYPE_DP_4)
        elif section == 'C1_MEM_W':
            q330.setWebPage(self.getWebPage())
        # elif section == 'C1_MAN': # We do not set the manufacturer's area
        elif section == 'C1_PHY':
            fixed = self.getFixedValues()
            # newPhy = self.getPhy()
            q330Phy = q330.sendCommand(c1_rqphy())
            # Our serial number must match, otherwise the Q330 will
            # refuse to write this section
            if fixed and (fixed.getSystemSerialNumber() !=
                          self.getPhy().getSerialNumber()):
                self.getPhy().setSerialNumber(fixed.getSystemSerialNumber())
            if not self.write_base_port:
                self.getPhy().setBasePort(q330Phy.getBasePort())
            if not self.write_ethernet_ip_address:
                self.getPhy().setEthernetIPAddress(
                    q330Phy.getEthernetIPAddress())
            if not self.write_ethernet_mac_address:
                self.getPhy().setEthernetMACAddressHigh32(
                    q330Phy.getEthernetMACAddressHigh32())
                self.getPhy().setEthernetMACAddressLow16(
                    q330Phy.getEthernetMACAddressLow16())
            q330.sendCommand(c1_sphy(self.getPhy().getPacketBytes()))
        elif section == 'C1_COM':
            fixed = self.getFixedValues()
            # newCom = self.getCom()
            q330Com = q330.sendCommand(c1_rqcom())
            # Our serial number must match, otherwise the Q330 will
            # refuse to write this section
            if fixed and (fixed.getSystemSerialNumber() !=
                          self.getCom().getSerialNumber()):
                self.getCom().setSerialNumber(fixed.getSystemSerialNumber())
            if not self.write_base_port:
                self.getCom().setBasePort(q330Com.getBasePort())
            if not self.write_ethernet_ip_address:
                self.getCom().setEthernetAddress(q330Com.getEthernetAddress())
            if not self.write_ethernet_netmask:
                self.getCom().setNetmask(q330Com.getNetmask())
            if not self.write_ethernet_gateway_address:
                self.getCom().setGatewayAddress(q330Com.getGatewayAddress())
            q330.sendCommand(c1_scom(self.getCom().getPacketBytes()))
        elif section == 'C1_LOG_1':
            q330.sendCommand(c1_slog(self.getLog1().getPacketBytes()))
        elif section == 'C1_LOG_2':
            q330.sendCommand(c1_slog(self.getLog2().getPacketBytes()))
        elif section == 'C1_LOG_3':
            q330.sendCommand(c1_slog(self.getLog3().getPacketBytes()))
        elif section == 'C1_LOG_4':
            q330.sendCommand(c1_slog(self.getLog4().getPacketBytes()))
        elif section == 'C1_GLOB':
            q330.sendCommand(c1_sglob(self.getGlob().getPacketBytes()))
        elif section == 'C1_SPP':
            q330.sendCommand(c1_sspp(self.getSpp().getPacketBytes()))
        elif section == 'C1_SC':
            q330.sendCommand(c1_ssc(self.getSc().getPacketBytes()))
        elif section == 'C1_PWR':
            q330.sendCommand(c1_spwr(self.getPwr().getPacketBytes()))
        elif section == 'C2_GPS':
            q330.sendCommand(c2_sgps(self.getGps().getPacketBytes()))
        elif section == 'C2_AMASS':
            q330.sendCommand(c2_samass(self.getAmass().getPacketBytes()))
        elif section == 'C2_WIN':
            q330.sendCommand(c2_swin(self.getWin().getPacketBytes()))
        elif section == 'C2_PHY_SP1':
            q330.sendCommand(c2_sphy(self.getPhySer1().getPacketBytes()))
        elif section == 'C2_PHY_SP2':
            q330.sendCommand(c2_sphy(self.getPhySer2().getPacketBytes()))
        elif section == 'C2_PHY_SP3':
            q330.sendCommand(c2_sphy(self.getPhySer3().getPacketBytes()))
        elif section == 'C2_PHY_ETH':
            q330.sendCommand(c2_sphy(self.getPhyEther().getPacketBytes()))
        elif section == 'C2_EPCFG':
            q330.sendCommand(c2_sepcfg(self.getEpCfg().getPacketBytes()))
        elif section == 'C3_ANNC':
            q330.sendCommand(c3_sannc(self.getAnncStruct().getPacketBytes()))
        elif section == 'C3_BCFG':
            q330.sendCommand(c3_sbcfg(self.getBalerCfg().getPacketBytes()))

    def readFromQ330(self, q330):
        # Fixed values used for building clone (not restored)
        self._getQ330Section(q330, 'C1_FIX')
        version = self.getFixedValues().getSystemVersion()
        system_type = "Q330"
        if self.getFixedValues().getFlags() & 0x0100:
            system_type = "Q335"

        self._getQ330Section(q330, 'C1_MAN')
        self._getQ330Section(q330, 'C1_MEM_1')
        self._getQ330Section(q330, 'C1_MEM_2')
        self._getQ330Section(q330, 'C1_MEM_3')
        self._getQ330Section(q330, 'C1_MEM_4')
        self._getQ330Section(q330, 'C1_MEM_W')
        if system_type == "Q330":
            self._getQ330Section(q330, 'C1_PHY')
        self._getQ330Section(q330, 'C1_LOG_1')
        self._getQ330Section(q330, 'C1_LOG_2')
        self._getQ330Section(q330, 'C1_LOG_3')
        self._getQ330Section(q330, 'C1_LOG_4')
        self._getQ330Section(q330, 'C1_GLOB')
        self._getQ330Section(q330, 'C1_SPP')
        self._getQ330Section(q330, 'C1_SC')
        if system_type == "Q330":
            self._getQ330Section(q330, 'C1_PWR')
        self._getQ330Section(q330, 'C2_GPS')
        self._getQ330Section(q330, 'C2_AMASS')
        if system_type == "Q330":
            self._getQ330Section(q330, 'C2_WIN')
            self._getQ330Section(q330, 'C2_PHY_SP1')
            self._getQ330Section(q330, 'C2_PHY_SP2')
            self._getQ330Section(q330, 'C2_PHY_SP3')
            self._getQ330Section(q330, 'C2_PHY_ETH')
            if version >= 0x0178:
                self._getQ330Section(q330, 'C2_EPCFG')
            else:
                print(f"  firmware {version / 256}.{version % 256}"
                      f" doesn't support Environmental Processor (C2_EPCFG)")
        self._getQ330Section(q330, 'C3_ANNC')
        if version >= 0x016c:
            self._getQ330Section(q330, 'C3_BCFG')
        else:
            print(f"  firmware {version / 256}.{version % 256}"
                  f" doesn't support Baler Configuration (C3_BCFG)")

        if system_type == "Q335":
            self._getQ330Section(q330, 'C1_COM')

    def listFileContents(self):
        print("Clone Sections:")
        keys = [t[0] for t in sorted(list(section_map.items()),
                                     key=lambda a: a[1][0])]
        for key in keys:
            # example of this tuple:
            # (0, "ManArea", "Manufacturer's Area", ('C1_RQMAN',), ('C1_SMAN',)
            index, func_name, desc, req, resp = section_map[key]
            # func_name, the second field, is the value being assigned to the
            # dictionary (see _getSectionByID for where/how these are used)
            print("  % 3d : %s" % (index, desc.ljust(MAX_DESC_LEN)), end=' ')
            if self.get_attr(func_name)() is not None:
                if key == 'C1_MEM_W':
                    length = len(self.get_attr(func_name)())
                elif key[0:7] == 'C1_MEM_':
                    length = sum(map(len,
                                     [c.getDataBytes() for c in
                                      self.get_attr(func_name).getChunkList()]))
                else:
                    length = len(self.get_attr(func_name)().getPacketBytes())
                print("(%d bytes)" % length)
            else:
                print("--")

    def writeToQ330(self, q330):
        cfg_fixed = self.getFixedValues()
        self._getQ330Section(q330, 'C1_FIX')
        # TODO: Do not throw exception if only compatible sections are
        #       selected, just provide a warning.
        #       Better yet, throw an exception for incompatible sections,
        #       throw a different exception for Q330 type mismatch,
        #       and allow a flag to supress type mismatches exceptions,
        #       and another flag to skip incompatabile sections.
        if (cfg_fixed.getFlags() & 0x0100) != \
                (self.getFixedValues().getFlags() & 0x0100):
            raise Q330TypeMismatchException()

        version = self.getFixedValues().getSystemVersion()
        system_type = "Q330"
        if self.getFixedValues().getFlags() & 0x0100:
            system_type = "Q335"

        if self.getTokenSet1() is not None:
            self._setQ330Section(q330, 'C1_MEM_1')
        if self.getTokenSet2() is not None:
            self._setQ330Section(q330, 'C1_MEM_2')
        if self.getTokenSet3() is not None:
            self._setQ330Section(q330, 'C1_MEM_3')
        if self.getTokenSet4() is not None:
            self._setQ330Section(q330, 'C1_MEM_4')
        if self.getWebPage() is not None:
            self._setQ330Section(q330, 'C1_MEM_W')
        if system_type == "Q330":
            if self.getPhy() is not None:
                self._setQ330Section(q330, 'C1_PHY')
        if self.getLog1() is not None:
            self._setQ330Section(q330, 'C1_LOG_1')
        if self.getLog2() is not None:
            self._setQ330Section(q330, 'C1_LOG_2')
        if self.getLog3() is not None:
            self._setQ330Section(q330, 'C1_LOG_3')
        if self.getLog4() is not None:
            self._setQ330Section(q330, 'C1_LOG_4')
        if self.getGlob() is not None:
            self._setQ330Section(q330, 'C1_GLOB')
        if self.getSpp() is not None:
            self._setQ330Section(q330, 'C1_SPP')
        if self.getSc() is not None:
            self._setQ330Section(q330, 'C1_SC')
        if system_type == "Q330":
            if self.getPwr() is not None:
                self._setQ330Section(q330, 'C1_PWR')
        if self.getGps() is not None:
            self._setQ330Section(q330, 'C2_GPS')
        if self.getAmass() is not None:
            self._setQ330Section(q330, 'C2_AMASS')
        if system_type == "Q330":
            if self.getWin() is not None:
                self._setQ330Section(q330, 'C2_WIN')
            if self.getPhySer1() is not None:
                self._setQ330Section(q330, 'C2_PHY_SP1')
            if self.getPhySer2() is not None:
                self._setQ330Section(q330, 'C2_PHY_SP2')
            if self.getPhySer3() is not None:
                self._setQ330Section(q330, 'C2_PHY_SP3')
            if self.getPhyEther() is not None:
                self._setQ330Section(q330, 'C2_PHY_ETH')
            if version < 0x0178:
                print(f"  firmware {version / 256}.{version % 256}"
                      " doesn't support Environmental Processor (C2_EPCFG)")
            elif self.getEpCfg() is not None:
                self._setQ330Section(q330, 'C2_EPCFG')

        if self.getAnncStruct() is not None:
            self._setQ330Section(q330, 'C3_ANNC')
        if version < 0x016c:
            print(f"  firmware {version / 256}.{version % 256}"
                  " doesn't support Baler Configuration (C3_BCFG)")
        elif self.getBalerCfg() is not None:
            self._setQ330Section(q330, 'C3_BCFG')

        if system_type == "Q335":
            if self.getCom() is not None:
                self._setQ330Section(q330, 'C1_COM')

    def diff(self, profile):
        if (profile.getFixedValues() is not None) and (self.getFixedValues() is not None):
            if (profile.getFixedValues().getFlags() & 0x0100) != \
                    (self.getFixedValues().getFlags() & 0x0100):
                raise Q330TypeMismatchException()
        keys = [t[0] for t in sorted(list(section_map.items()),
                                     key=lambda a: a[1][0])]
        for key in keys:
            index, func_name, desc, req, resp = section_map[key]
            print("< % 3d : %s" % (index, desc.ljust(MAX_DESC_LEN)), end=' ')
            if self.__dict__[func_name] is not None:
                function = self.__dict__[func_name]
                if key == 'C1_MEM_W':
                    length = len(function())
                elif key[0:7] == 'C1_MEM_':
                    length = sum(map(len, [c.getDataBytes() for c in
                                           function().getChunkList()]))
                else:
                    length = len(function().getPacketBytes())
                print("(%d bytes)" % length)
            else:
                print("--")

            print("> % 3d : %s" % (index, desc.ljust(MAX_DESC_LEN)), end=' ')
            if profile.get_attr(func_name)() is not None:
                if key == 'C1_MEM_W':
                    length = len(profile.get_attr(func_name)())
                elif key[0:7] == 'C1_MEM_':
                    length = sum(map(len,
                                     [c.getDataBytes() for c in
                                      profile.get_attr(func_name)().getChunkList()]))
                else:
                    length = len(profile.get_attr(func_name)().getPacketBytes())
                print("(%d bytes)" % length)
            else:
                print("--")

            if self.__dict__[func_name]() and profile.get_attr(func_name)():
                part1 = self.__dict__[func_name]()
                part2 = profile.get_attr(func_name)()
                if key == 'C1_MEM_W':
                    if part1 != part2:
                        print("Web Pages Differ")
                elif key[0:7] == 'C1_MEM_':
                    tokens1 = {}
                    for token in part1.getTokens():
                        token_id, length = Token.getTokenIDAndLength(
                            token.getTokenBytes())
                        if token_id not in tokens1:
                            tokens1[token_id] = {}
                        if token_id == 192:
                            aaa = 3
                        struct = TokenStructs.TokenStruct(token.getTokenBytes())
                        tokens1[token_id][struct.get_identity()] = struct

                    tokens2 = {}
                    for token in part2.getTokens():
                        token_id, length = Token.getTokenIDAndLength(
                            token.getTokenBytes())
                        if token_id not in tokens2:
                            tokens2[token_id] = {}
                        struct = TokenStructs.TokenStruct(token.getTokenBytes())
                        tokens2[token_id][struct.get_identity()] = struct

                    ids = []
                    ids.extend(list(tokens1.keys()))
                    ids.extend(list(tokens2.keys()))
                    for token_id in set(sorted(ids)):
                        if token_id not in tokens1:
                            print("Token %d >" % token_id)
                            continue
                        elif token_id not in tokens2:
                            print("Token %d <" % token_id)
                            continue

                        structs1 = tokens1[token_id]
                        structs2 = tokens2[token_id]

                        identities = []
                        identities.extend(list(structs1.keys()))
                        identities.extend(list(structs2.keys()))

                        for identity in set(sorted(identities)):
                            print("Token %d - '%s': " % (token_id, identity), end=' ')
                            if identity not in structs1:
                                print(">")
                                continue
                            elif identity not in structs2:
                                print("<")
                                continue
                            print()

                            struct1 = structs1[identity]
                            struct2 = structs2[identity]

                            fn = []
                            fn.extend(struct1.Fields)
                            fn.extend(struct2.Fields)
                            for name in set(sorted(fn)):
                                value1 = struct1.get_attr(name)
                                value2 = struct2.get_attr(name)
                                if value1 != value2:
                                    print(f"  {name}: {str(value1)} |"
                                          f" {str(value2)}")
                else:
                    fn = []
                    fn.extend(part1.Fields)
                    fn.extend(part2.Fields)
                    for name in set(sorted(fn)):
                        value1 = part1.get_attr(name)
                        value2 = part2.get_attr(name)
                        if value1 != value2:
                            print(f"  {name}: {str(value1)} |"
                                  f" {str(value2)}")

    def __repr__(self):
        """
        print ourselves
        """
        ret = []
        for item in list(self.__dict__.keys()):
            ret.append(f'----------\n{item}\n----------\n{self.__dict__[item]}')
        return '\n'.join(ret)
