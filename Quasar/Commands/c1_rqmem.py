from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqmem(QDPPacket):
    """
    Represents a request for memory contents based on start address and range
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQMEM)
        self.Fields = [
            'StartingAddress',
            'ByteCount',
            'MemoryType',
            'PasswordHigh32',
            'PasswordMedHigh32',
            'PasswordMedLow32',
            'PasswordLow32'
            ]
        self.FieldDefinition = '>LHHLLLL'
        QDPPacket.__init__(self, byte_array, deviceType)

    def setPassword(self, password):
        h,mh,ml,l = password
        self.setPasswordHigh32(h)
        self.setPasswordMedHigh32(mh)
        self.setPasswordMedLow32(ml)
        self.setPasswordLow32(l)
