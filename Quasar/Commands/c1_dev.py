from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c1_dev(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_DEV)
        self.Fields = []
        self.FieldDefinition = '>'
        self._numDevs = 0

        devStruct = '>HHHHQHH'

        if byte_array:
            numDevs = (len(byte_array) - 12) / structWrapper.calcsize(devStruct)
            self._numDevs = numDevs
        
            for dev in range(1, numDevs+1):
                self.Fields.append('PortNumber_%d' %dev)
                self.Fields.append('UnitID_%d' %dev)
                self.Fields.append('Version_%d' %dev)
                self.Fields.append('Options_%d' %dev)
                self.Fields.append('SerialNumber_%d' %dev)
                self.Fields.append('DeviceStaticStorage_%d' %dev)
                self.Fields.append('SecondsSinceHeard_%d' %dev)
                self.FieldDefinition = self.FieldDefinition + devStruct[1:]
            
        QDPPacket.__init__(self, byte_array, deviceType)

    def strUnitID(self, index=0):
        v = self.__dict__["UnitID_%d" % index]
        if v == 13:
            return 'SP1320 power supply'
        elif v in (15, 17, 19):
            return 'Unknown power supply'
        elif v == 20:
            return 'Black and white camera'
        elif v == 33:
            return 'QCAL330 Calibrator'
        elif v == 99:
            return 'WHOI Controller'
        else:
            return "Unrecognized [%d]" % v

    def getNumDevs(self):
        return self._numDevs
        
