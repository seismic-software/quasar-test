from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqqv(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQQV)
        self.Fields = [
            'LowestSequenceNumber',
            'ChannelMap'
            ]
        self.FieldDefinition = '>LH'
        QDPPacket.__init__(self, byte_array, deviceType)
            
