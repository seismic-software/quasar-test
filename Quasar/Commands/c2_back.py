from Quasar.CmdID import CmdID
from Quasar.Commands.c2_poc import c2_poc


class c2_back(c2_poc):
    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_poc.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C2_BACK)
        self.FieldDefinition = '>QLLLHHHHHHQ'
