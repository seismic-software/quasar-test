from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_md5(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_MD5)
        self.Fields = [
            'MD5Result'
            ]
        self.FieldDefinition = '>16s'
        QDPPacket.__init__(self, byte_array, deviceType)
