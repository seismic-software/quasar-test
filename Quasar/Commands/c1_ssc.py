from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitIndexToInteger, bitIndexListToInteger


class c1_ssc(QDPPacket):
    """
    Represents a request to set the Q330 sensor status bits
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SSC)
        self.Fields = [
            'SensorOutput1Definition',
            'SensorOutput2Definition',
            'SensorOutput3Definition',
            'SensorOutput4Definition',
            'SensorOutput5Definition',
            'SensorOutput6Definition',
            'SensorOutput7Definition',
            'SensorOutput8Definition'
            ]
        self.FieldDefinition = '>LLLLLLLL'
        QDPPacket.__init__(self, byte_array, deviceType)

    def strSensorOutput1Definition(self):
        return self.getSensorOutputString(self.getSensorOutput1Definition())

    def strSensorOutput2Definition(self):
        return self.getSensorOutputString(self.getSensorOutput2Definition())

    def strSensorOutput3Definition(self):
        return self.getSensorOutputString(self.getSensorOutput3Definition())

    def strSensorOutput4Definition(self):
        return self.getSensorOutputString(self.getSensorOutput4Definition())

    def strSensorOutput5Definition(self):
        return self.getSensorOutputString(self.getSensorOutput5Definition())

    def strSensorOutput6Definition(self):
        return self.getSensorOutputString(self.getSensorOutput6Definition())

    def strSensorOutput7Definition(self):
        return self.getSensorOutputString(self.getSensorOutput7Definition())

    def strSensorOutput8Definition(self):
        return self.getSensorOutputString(self.getSensorOutput8Definition())
    
    def getSensorOutputString(self, outputDef):
        v = outputDef & bitIndexListToInteger([0, 1, 2, 3, 4, 5, 6, 7])
        bit8 = outputDef & bitIndexToInteger(8)
        values = [
            'Not doing calibration or recentering',
            'Sensor A calibration',
            'Sensor A centering',
            'Sensor A capacitive coupling',
            'Sensor B calibration',
            'Sensor B centering',
            'Sensor B capacitive coupling',
            'Sensor A lock',
            'Sensor A unlock',
            'Sensor A aux1',
            'Sensor A aux2',
            'Sensor B lock',
            'Sensor B unlock',
            'Sensor B aux1',
            'Sensor B aux2'
            ]
        if bit8:
            highLow = 'active high'
        else:
            highLow = 'active low'
        return f'{values[v]} ({highLow})'
