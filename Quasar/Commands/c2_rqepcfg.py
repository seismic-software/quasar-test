from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqepcfg(QDPPacket):
    """
    Represents a request to the Q330 for environmental processor information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQEPCFG)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
