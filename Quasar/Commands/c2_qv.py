from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c2_qv(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_QV)
        self.Fields = [
            'StartingSequenceNumber',
            'SecondsCount',
            'NumberOfEntries',
            'ActualChannelMask',
            'Spare'
            ]
        self.FieldDefinition = '>LHHHH'

        if byte_array:
            numEntries = structWrapper.unpack('>H', byte_array[18:20])[0]
            for i in range(0, numEntries):
                self.Fields.append('Channel_%d' %i)
                self.Fields.append('ShiftCount_%d' %i)
                self.Fields.append('SecondsOffset_%d' %i)
                self.Fields.append('Spare_%d' %i)
                self.Fields.append('StartingValue_%d' %i)
                self.Fields.append('Differences_%d' %i)
                self.FieldDefinition = self.FieldDefinition + 'HHHHl40s'

        QDPPacket.__init__(self, byte_array, deviceType)
