from Quasar.CmdID import CmdID
from Quasar.Commands import c2_dep
from Quasar.QDPPacket import QDPPacket


class c2_depr(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        tmpDep = c2_dep.c2_dep(deviceType=deviceType)
        self.setQDPCommand(CmdID.C2_DEPR)
        self.Fields = [
            'SubCommand',
            'SubResponse',
            'RemainderOfParameters'
            ]
        self.FieldDefinition = tmpDep.FieldDefinition
        QDPPacket.__init__(self, byte_array, deviceType)
