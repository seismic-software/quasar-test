from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_umsg(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_UMSG)
        self.Fields = [
            'Ignored',
            'UserMessage'
            ]
        self.FieldDefinition = '>L80p'
        QDPPacket.__init__(self, byte_array, deviceType)

    def setUserMessage(self, msg):
        if len(msg) > 79:
            self.__dict__['UserMessage'] = msg[:79]
        else:
            self.__dict__['UserMessage'] = msg
