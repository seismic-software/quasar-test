from Quasar.CmdID import CmdID
from Quasar.Commands.c1_spwr import c1_spwr


class c1_pwr(c1_spwr):
    """
    Represents a response from a Q330 for a power programming request.
    This field is deprecated.
    (see c1_spwr for field details)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_spwr.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_PWR)
