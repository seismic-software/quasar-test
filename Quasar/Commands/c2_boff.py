from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_boff(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_BOFF)
        self.Fields = [
            'PowerOffDelay'
            ]
        self.FieldDefinition = '>H'
        QDPPacket.__init__(self, byte_array, deviceType)
                                                                                            
