import struct

from Quasar.CmdID import CmdID
from Quasar.Commands.c2_terc import c2_terc


class c3_sbcfg(c2_terc):
    """
    Represents a request to the Q330 to set baler configuration information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        if byte_array is None:
            byte_array = []
        c2_terc.__init__(self, byte_array, deviceType)
        self.Fields.extend([
            'BlockSize',
            'PhysicalInterfaceNumber',
            'BalerType',
            'Version',
            ])
        self.FieldDefinition += 'HHHH'

        if len(byte_array) > 24:
            block_size = struct.unpack('>H', byte_array[16:18])[0]
            if (block_size > 12):
                self.Fields.append('BalerConfigurationString')
                self.FieldDefinition += "%ds" % (block_size - 12,)
        c2_terc.__finish_init__(self, byte_array)
        self.setSubCommand(CmdID.C3_SBCFG)
        self.setSpare(0)
