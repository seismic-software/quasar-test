import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket, qdpPacketHeader
from isti.utils import structWrapper


class c1_free(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_FREE)
        self.AllFields = {
            0 : [
                'StartingByteAddressForFreeFlash',
                'SizeInBytesOfFreeFlash'
                ],
            1 : [
                'ResumeBlock',
                'NumberofEntries',
                'MoreRequestsRequired',
                'Spare',
                'BitmapofEmptyBlocks_0_31',
                'BitmapofEmptyBlocks_31_63',
                'BitmapofEmptyBlocks_64_95',
                'BitmapofEmptyBlocks_96_127',
                'BitmapofEmptyBlocks_128_159',
                'BitmapofEmptyBlocks_160_191',
                'BitmapofEmptyBlocks_192_223',
                'BitmapofEmptyBlocks_224_255'
                ]
            }
        freeStructs = {
           0 : '>LL',
           1 : '>4H8L',
        }

        type_field = 0
        if byte_array:  # if we received no data, we can't construct anything
            headerSize = structWrapper.calcsize(qdpPacketHeader)
            data = byte_array[headerSize:]
            if len(data) >= 40:
                type_field = 1
            if type_field not in list(self.AllFields.keys()):
                type_field = 0
            self.Fields = self.AllFields[type_field]
            self.FieldDefinition = freeStructs[type_field]

            if type_field == 1:
                (numEntries,) = struct.unpack('>H', data[2:4])
                if numEntries:
                    for index in range(0,numEntries):
                        addition = []
                        addition.append( "Module_%d_FlashBlockNumber" % index )
                        addition.append( "Module_%d_LengthofModule" % index )
                        addition.append( "Module_%d_ModuleRevision" % index )
                        addition.append( "Module_%d_OverlayNumber" % index )
                        self.Fields.append(addition)
                        self.FieldDefinition += '4H'

        QDPPacket.__init__(self, byte_array, deviceType)

