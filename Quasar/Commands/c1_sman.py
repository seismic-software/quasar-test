
from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitIndexToInteger


class c1_sman(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SMAN)
        self.Fields = [
            'PasswordHigh32',
            'PasswordMidHigh32',
            'PasswordMidLow32',
            'PasswordLow32',
            'QAPCHP1To3Type',
            'QAPCHP1To3Version',
            'QAPCHP4To6Type',
            'QAPCHP4To6Version',
            'QAPCHP1To3SerialNumber',
            'QAPCHP4To6SerialNumber',
            'QAPCHPChannel1ExpectedCountsWithReferenceApplied',
            'QAPCHPChannel2ExpectedCountsWithReferenceApplied',
            'QAPCHPChannel3ExpectedCountsWithReferenceApplied',
            'QAPCHPChannel4ExpectedCountsWithReferenceApplied',
            'QAPCHPChannel5ExpectedCountsWithReferenceApplied',
            'QAPCHPChannel6ExpectedCountsWithReferenceApplied',
            'BornOnTime',
            'PacketMemoryInstalledInBytes',
            'ClockType',
            'ModelNumber',
            'DefaultCalibratorOffset',
            'Flags',
            'KMIPropertyTag',
            'MaximumPowerOnSeconds'
            ]
        self.FieldDefinition = '>LLLLHHHH10LHHHHLL'
        if deviceType == 'Q335':
            self.Fields = [
                'PasswordHigh64',
                'PasswordLow64',
                'FrontEndBoard1Type',
                'FrontEndBoard1Version',
                'FrontEndBoard2Type',
                'FrontEndBoard2Version',
                'FrontEndBoard1SerialNumber',
                'FrontEndBoard2SerialNumber',
                'FrontEndBoard1ToneDAC1A',
                'FrontEndBoard1ToneDAC1B',
                'FrontEndBoard1ToneDAC2A',
                'FrontEndBoard1ToneDAC2B',
                'FrontEndBoard1ToneDAC3A',
                'FrontEndBoard1ToneDAC3B',
                'FrontEndBoard2ToneDAC1A',
                'FrontEndBoard2ToneDAC1B',
                'FrontEndBoard2ToneDAC2A',
                'FrontEndBoard2ToneDAC2B',
                'FrontEndBoard2ToneDAC3A',
                'FrontEndBoard2ToneDAC3B',
                'BornOnTime',
                'PacketMemoryInstalledInBytes',
                'ClockType',
                'ModelNumber',
                'DefaultCalibratorOffset',
                'Flags',
                'KMIPropertyTag',
                'MaximumPowerOnSeconds',
                'CalibrationMonitorToneDAC',
                'CoreProcessorVCOOffset',
                'FrontEndBoard1VCOOffset',
                'FrontEndBoard2VCOOffset',
                ]
            self.FieldDefinition = '>QQHHHHLLHHHHHHHHHHHHLLHHHHLLHHHH'
        QDPPacket.__init__(self, byte_array, deviceType)

    def strClockType(self):
        if self.getClockType() == 0:
            return 'No clock'
        if self.getClockType() == 1:
            return 'Motorola M12'
        return 'Unknown clock type'

    def strFlags(self):
        flags = self.getFlags()
        ret = ['']
        if flags & bitIndexToInteger(0):
            ret.append('Clear internal counters')
        if self._deviceType == 'Q330':
            if flags & bitIndexToInteger(3):
                ret.append('Enable 26 bit output on channels 1-3')
            if flags & bitIndexToInteger(4):
                ret.append('Enable 26 bit output on channels 4-6')
            if flags & bitIndexToInteger(5):
                ret.append('Enable NTP operation')
            if flags & bitIndexToInteger(6):
                ret.append('Q330 has an integrated Baler 44 (Q330S)')

            val = flags >> 8
            ret.append('Power supply voltage: %f' % (5.45 + (val*.05)))
        return '\n      '.join(ret)
                   
    def setPassword(self, password):
        h, mh, ml, l = password
        self.setPasswordHigh32(h)
        self.setPasswordMedHigh32(mh)
        self.setPasswordMedLow32(ml)
        self.setPasswordLow32(l)
