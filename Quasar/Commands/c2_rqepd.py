from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqepd(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQEPD)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
