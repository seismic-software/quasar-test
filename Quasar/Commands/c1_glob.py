from Quasar.CmdID import CmdID
from Quasar.Commands.c1_sglob import c1_sglob


class c1_glob(c1_sglob):
    """
    Represents the response from a Q330 for a global programming request
    (rqglob)
    """
    
    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_sglob.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_GLOB)
