from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_eptun(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_EPTUN)
        self.Fields = [
            'Command',
            'Interface',
            'Length',
            'Payload',
            ]
        self.FieldDefinition = '>BBH'
        QDPPacket.__init__(self, byte_array, deviceType)

    def prepareForSending(self):
        length = 0
        if self.getPayload():
            try:
                length = len(self.getPayload)
            except:
                pass
        if length:
            self.FieldDefinition += f"{length + 4:d}s"
        else:
            self.Fields = self.Fields[:-1]

