from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqman(QDPPacket):
    """
    Represents a request for manufacturer's area
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQMAN)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
