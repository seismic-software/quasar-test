import struct
from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket

BCMD_PING = 0
BCMD_SETCONFIG = 1
BCMD_ALLOC_DELETE = 2
BCMD_GETALLOC = 3
BCMD_DEALLOC = 4
BCMD_SETCLOCK = 5
BCMD_SETLOOPBACK = 6

BCMD_DELETE_DATA = 1
BCMD_DELETE_Q330_SERIAL = 2
BCMD_DELETE_Q330_SPEED_INTERFACE = 4
BCMD_DELETE_Q330_DATAPORT = 8
BCMD_DELETE_NETWORK_STATION = 16
BCMD_DELETE_PERMINANT_IP = 32
BCMD_REWRITE_BALER_INI = 64
BCMD_DELETE_NON_BALER_PARAMS = 128
class c2_bcmd(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_BCMD)
        self.Fields = [
            'BalerCommand',
            'SequencingField'
            ]
        self._bcmdStruct = '>HH'

        fieldDef = self._bcmdStruct
        if byte_array is not None:
            cmdInfo = struct.unpack(self._bcmdStruct, byte_array[12:16])[0]
            if cmdInfo in (BCMD_PING, BCMD_DEALLOC, BCMD_GETALLOC):
                pass
            elif cmdInfo in (BCMD_ALLOC_DELETE, BCMD_SETLOOPBACK):
                fieldDef = fieldDef + 'H'
                self.Fields.append('CommandSpecificInformation')
            elif cmdInfo == BCMD_SETCLOCK:
                fieldDef = fieldDef + 'L'
                self.Fields.append('CommandSpecificInformation')
            else:
                print('Unsupported bcmd command')
            
        self.FieldDefinition = fieldDef
        QDPPacket.__init__(self, byte_array, deviceType)

    def setMode(self, mode):

        self.Fields = [
            'BalerCommand',
            'SequencingField'
            ]
            
        fieldDef = self._bcmdStruct
        cmdInfo = mode
        if cmdInfo in (BCMD_PING, BCMD_DEALLOC, BCMD_GETALLOC):
            pass
        elif cmdInfo in (BCMD_ALLOC_DELETE, BCMD_SETLOOPBACK):
            fieldDef = fieldDef + 'H'
            self.Fields.append('CommandSpecificInformation')
        elif cmdInfo == BCMD_SETCLOCK:
            fieldDef = fieldDef + 'L'
            self.Fields.append('CommandSpecificInformation')
        else:
            print('Unsupported bcmd command')
            
        self.FieldDefinition = fieldDef
        self.setBalerCommand(mode)

