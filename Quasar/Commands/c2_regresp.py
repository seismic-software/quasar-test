from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_regresp(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_REGRESP)
        self.Fields = [
            'MagicNumber'
            ]
        self.FieldDefinition = '>L'
        QDPPacket.__init__(self, byte_array, deviceType)
