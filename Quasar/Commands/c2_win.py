from Quasar.CmdID import CmdID
from Quasar.Commands.c2_swin import c2_swin


class c2_win(c2_swin):
    """
    Represents a response from the Q330 to a windowing data request (rqwin)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_swin.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C2_WIN)
