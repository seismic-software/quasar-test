from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket

class c1_pollsn(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.FieldDefinition = '>HH'
        self.Fields = [
            'SerialNumberMask',
            'SerialNumberMatch'
            ]
        self.setIsBroadcast(1)

        self.setQDPCommand(CmdID.C1_POLLSN)

        QDPPacket.__init__(self, byte_array, deviceType)


