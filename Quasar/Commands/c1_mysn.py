from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_mysn(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_MYSN)
        self.Fields = [
            'SerialNumber',
            'KMIPropertyTag',
            'UserTag'
            ]
        self.FieldDefinition = '>QLL'

        QDPPacket.__init__(self, byte_array, deviceType)

    
    def strSerialNumber(self):
        return '%x' %self.getSerialNumber()
