from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_sauth(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SAUTH)
        self.Fields = [
            'SerialNumber',
            'DataPort1AuthorizationCode',
            'DataPort2AuthorizationCode',
            'DataPort3AuthorizationCode',
            'DataPort4AuthorizationCode',
            'ConfigurationPortAuthorizationCode',
            'SpecialFunctionsPortAuthorizationCode'
            ]
        self.FieldDefinition = '>QQQQQQQ'

        QDPPacket.__init__(self, byte_array, deviceType)
        
