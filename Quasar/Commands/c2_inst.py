from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_inst(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_INST)
        self.Fields = [
            'DPIPAddress',
            'RouterIPAddress',
            'TimeoutInMinutes',
            'Spare1',
            'Flags',
            'DPUDPPort'
            ]
        self.FieldDefinition = '>LLHHHH'
        QDPPacket.__init__(self, byte_array, deviceType)
