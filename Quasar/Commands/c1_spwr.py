from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_spwr(QDPPacket):
    """
    Represents a request to set power programming details
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SPWR)
        self.Fields = [
            'Q330PowerOn',
            'Q330PowerOff',
            'LDSBPowerOn',
            'LDSBPowerOff',
            'Aux1PowerOn',
            'Aux1PowerOff',
            'Aux2PowerOn',
            'Aux2PowerOff',
            'Aux3PowerOn',
            'Aux3PowerOff',
            'Aux4PowerOn',
            'Aux4PowerOff',
            'Aux5PowerOn',
            'Aux5PowerOff',
            'Aux6PowerOn',
            'Aux6PowerOff',
            'OnePercentCapacityChargeThreshold',
            'MinimumBatteryTemperature',
            'MaximumBatteryTemperature',
            'Spare'
            ]
        self.FieldDefinition = '>16BLBBH'
        QDPPacket.__init__(self, byte_array, deviceType)
