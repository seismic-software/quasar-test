from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_stop(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_STOP)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
