import socket
import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_scom(QDPPacket):
    """
    Represents a request to set COM values on the Q330.
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SCOM)
        self.Fields = [
            'SerialNumber',
            'StructureVersion',
            'CurrentActiveStructureLength',
            'MTU',
            'BasePort',
            'EthernetAddress',
            'Netmask',
            'GatewayAddress',
            'PowerCyclingFlags',
            'DataPort1EmptyTimeout',
            'DataPort1NotYetEmptyTimeout',
            'DataPort2EmptyTimeout',
            'DataPort2NotYetEmptyTimeout',
            'DataPort3EmptyTimeout',
            'DataPort3NotYetEmptyTimeout',
            'DataPort4EmptyTimeout',
            'DataPort4NotYetEmptyTimeout',
            'DataPort1TriggerPercent',
            'DataPort2TriggerPercent',
            'DataPort3TriggerPercent',
            'DataPort4TriggerPercent',
            'MinimumPowerOffTime',
            'WhiteOrBlackListOptions',
            'MaximumPowerOffTime',
            'BalerMinimumPercentage',
            'WhiteOrBlackListRange0',
            'WhiteOrBlackListRange1',
            'WhiteOrBlackListRange2',
            'WhiteOrBlackListRange3',
            'WhiteOrBlackListRange4',
            'WhiteOrBlackListRange5',
            'WhiteOrBlackListRange6',
            'WhiteOrBlackListRange7',
            'EthernetFlags',
            'MinimumOnTime',
            'BalerBasePort',
            'CommunicationsHoldTime',
            'Spare1',
            'Reserved',
        ]
        self.FieldDefinition = '>Q4H4L16H16LL4H360s'
        super().__init__(byte_array, deviceType)

    def strEthernetAddress(self, pad=""):
        config = self.getEthernetFlags() & 0x00000003
        ip = socket.inet_ntoa(struct.pack('>L', self.getEthernetAddress()))
        if config == 0:
            ip = "Using default IP 192.168.0.50 [cfg=%s]" % ip
        elif config == 2:
            ip = "Using DHCP [cfg=%s]" % ip
        return pad + ip

    def strNetmask(self, pad=""):
        config = (self.getEthernetFlags() & 0x0000000C) >> 2
        ip = socket.inet_ntoa(struct.pack('>L', self.getNetmask()))
        if config == 0:
            ip = "Using default netmask 255.255.255.0 [cfg=%s]" % ip
        elif config == 2:
            ip = "Using DHCP [cfg=%s]" % ip
        return pad + ip

    def strGatewayAddress(self, pad=""):
        config = (self.getEthernetFlags() & 0x00000030) >> 4
        ip = socket.inet_ntoa(struct.pack('>L', self.getGatewayAddress()))
        if config == 0:
            ip = "Using default gateway 192.168.0.1 [cfg=%s]" % ip
        elif config == 2:
            ip = "Using DHCP [cfg=%s]" % ip
        return pad + ip

    def strPowerCyclingFlags(self, pad=""):
        results = []
        # this should be handled by GetSet -- not sure why PyCharm has an issue
        flags = self.getPowerCyclingFlags()
        results.append("Power on at %02d:00 UTC" % (flags & 0x00ffffff,))
        results.append("Continuous Power [%s]" %
                       (self.T((flags & 0x01000000) == 0, "Disabled",
                               "Enabled")))
        results.append("Power on After Maximum Off Time [%s]" %
                       (self.T((flags & 0x02000000) == 0, "Disabled",
                               "Enabled")))
        results.append("Baler Minimum %% Field [%s]" % (
            self.T((flags & 0x02000000) == 0, "Inactive", "Active")))
        return "\n".join([pad + r for r in results])

    def strWhiteOrBlackListRange0(self, pad=""):
        return self.makeWhiteOrBlackListString(0, pad)

    def strWhiteOrBlackListRange1(self, pad=""):
        return self.makeWhiteOrBlackListString(1, pad)

    def strWhiteOrBlackListRange2(self, pad=""):
        return self.makeWhiteOrBlackListString(2, pad)

    def strWhiteOrBlackListRange3(self, pad=""):
        return self.makeWhiteOrBlackListString(3, pad)

    def strWhiteOrBlackListRange4(self, pad=""):
        return self.makeWhiteOrBlackListString(4, pad)

    def strWhiteOrBlackListRange5(self, pad=""):
        return self.makeWhiteOrBlackListString(5, pad)

    def strWhiteOrBlackListRange6(self, pad=""):
        return self.makeWhiteOrBlackListString(6, pad)

    def strWhiteOrBlackListRange7(self, pad=""):
        return self.makeWhiteOrBlackListString(7, pad)

    def makeWhiteOrBlackListString(self, index, pad=""):
        resultStr = pad
        options = self.getWhiteOrBlackListOptions()
        if options & (1 << index):
            wb_range = self.__dict__["WhiteOrBlackListRange%d" % index]
            lowIP = socket.inet_ntoa(struct.pack('>L', (wb_range >> 32)))
            highIP = socket.inet_ntoa(struct.pack('>L', (wb_range & 0xffffffff)))
            listType = "Black"
            if options & 0x8000:
                listType = "White"
            resultStr += "%s-List %s - %s" % (listType, lowIP, highIP)
        return resultStr

    # There were THREE different versions of these string methods. Of them,
    # these ones are retained because it is the only set that doesn't
    # produce any kind of error messages in the IDE

    def strDataPort1TriggerPercent(self, pad=""):
        return "%s%d %%" % (pad, int(self.getDataPort1TriggerPercent() / 2.56))

    def strDataPort2TriggerPercent(self, pad=""):
        return "%s%d %%" % (pad, int(self.getDataPort2TriggerPercent() / 2.56))

    def strDataPort3TriggerPercent(self, pad=""):
        return "%s%d %%" % (pad, int(self.getDataPort3TriggerPercent() / 2.56))

    def strDataPort4TriggerPercent(self, pad=""):
        return "%s%d %%" % (pad, int(self.getDataPort4TriggerPercent() / 2.56))
