from Quasar.CmdID import CmdID
from Quasar.Commands.c1_sphy import c1_sphy


class c1_phy(c1_sphy):
    """
    Represents the data from a Q330 returned
    from a request for physical interfaces
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_sphy.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_PHY)
