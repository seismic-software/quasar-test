import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket

RLCMD_READ_REQUEST = 1
RLCMD_WRITE_REQUEST = 2
RLCMD_DATA = 3
RLCMD_ACK = 4
RLCMD_ERROR = 5
RLCMD_REBOOT = 6
RLCMD_LOGIN_REQUEST = 7
RLCMD_LOGIN_ACK = 8
RLCMD_RRQ_ACK = 9
RLCMD_DATA_ACK = 10
RLCMD_MD5_RES = 11
RLCMD_MD5_ACK = 12


class rl_cmd(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.RL_CMD)
        fieldDef = '>HH'
        self.Fields = [
            'BlockNumber',
            'CommandType',
            ]

        if byte_array is not None:
            cmdInfo = struct.unpack('>H', byte_array[14:16])[0]
            self.setCommandType(cmdInfo, byte_array)
        else:
            self.FieldDefinition = fieldDef

        # If this was a login ack and we weren't asked to request express mode,
        # or the baler does not support express mode, we need to add 2 byte_array
        if self.getCommandType() in (RLCMD_LOGIN_ACK, RLCMD_DATA_ACK):
            if len(byte_array) != struct.calcsize(self.FieldDefinition):
                byte_array += '\x00'
                byte_array += '\x00'

        QDPPacket.__init__(self, byte_array, deviceType)

    def setCommandType(self, newCmd, auxData=None):
        """
        Do all of the internal bookkeeping required to change command modes
        """
        if self.getCommandType() == newCmd:
            return
        else:
            fieldDef = '>HH'
            self.Fields = [
                'BlockNumber',
                'CommandType',
            ]
    
        if newCmd in (RLCMD_READ_REQUEST, RLCMD_WRITE_REQUEST):
            self.Fields.append('Filename')
            filenameLength = struct.unpack('B', auxData[16])[0]
            fieldDef = fieldDef + '%dp' %(filenameLength+1)
        elif newCmd == RLCMD_DATA:
            packetLength = struct.unpack('>H', auxData[6:8])[0]
            if packetLength > 4:
                self.Fields.append('Data')
                fieldDef = fieldDef + '%ds' %(packetLength-4)
        elif newCmd == RLCMD_ERROR:
            self.Fields.append('ErrorText')
            errorLength = struct.unpack('B', auxData[16])[0]
            fieldDef = fieldDef + '%dp' %(errorLength+1)
        elif newCmd in (RLCMD_LOGIN_REQUEST, RLCMD_LOGIN_ACK):
            self.Fields.append('ExpressMode')
            fieldDef  += 'H'
        elif newCmd == RLCMD_DATA_ACK:
            self.Fields.append('AcknowledgementMask')
            fieldDef += 'H'
        elif newCmd == RLCMD_MD5_RES:
            self.Fields.append('MD5Result')
            fieldDef += '16s'
            
        self.FieldDefinition = fieldDef            
        self.initialize()
        self.__dict__["CommandType"] = newCmd
