from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqmod(QDPPacket):
    """
    Represents a request to list running modules on a Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQMOD)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
