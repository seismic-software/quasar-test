from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqsrv(QDPPacket):
    """
    Represents a registration request to a Q330
    """
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQSRV)
        self.setIsBroadcast(1)
        self.Fields = [
            'SerialNumber'
            ]

        self.FieldDefinition = '>Q'

        QDPPacket.__init__(self, byte_array, deviceType)


