from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_gid(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_GID)
        self.Fields = [
            'ID1',
            'ID2',
            'ID3',
            'ID4',
            'ID5',
            'ID6',
            'ID7',
            'ID8',
            'ID9'
            ]
        self.FieldDefinition = '>32p32p32p32p32p32p32p32p32p'
        QDPPacket.__init__(self, byte_array, deviceType)
            
