import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_epresp(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_EPRESP)
        self.Fields = [
            'Command',
            'Interface',
            'Length',
            'Payload',
            ]
        self.FieldDefinition = '>BBH'
        length = struct.unpack('>H', byte_array[14:16])[0]
        if length > 4:
            self.FieldDefinition += "%ds" % (length - 4,)
        QDPPacket.__init__(self, byte_array, deviceType)
