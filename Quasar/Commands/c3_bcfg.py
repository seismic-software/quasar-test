import struct

from Quasar.QDPPacket import QDPPacket


class c3_bcfg(QDPPacket):
    """
    Represents the response from a Q330 to a request for baler configuration
    information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        super().__init__(deviceType)
        self.Fields = [
            'BlockSize',
            'PhysicalInterfaceNumber',
            'BalerType',
            'Version',
            ]
        self.FieldDefinition = 'HHHH'
        block_size = struct.unpack('>H', byte_array[16:18])[0]
        if (block_size > 12):
            self.Fields.append('BalerConfigurationString')
            self.FieldDefinition += "%ds" % (block_size - 12,)
