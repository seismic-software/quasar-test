from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqpwr(QDPPacket):
    """
    Represents a request for power programming info from Q330
    (i.e., which ports are/aren't receiving power)
    What fields are received are in c1_spwr
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQPWR)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
