from Quasar.CmdID import CmdID
from Quasar.Commands.c1_scom import c1_scom


class c1_com(c1_scom):
    """
    Represents the response from a Q330 to a COM request.
    COM was not documented in the list of QDP packets.
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_scom.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_COM)
