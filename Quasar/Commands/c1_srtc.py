from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_srtc(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SRTC)
        self.Fields = [
            'Seconds',
            'Minutes',
            'Hours',
            'Reserved',
            'Day',
            'Month',
            'Year',
            'Spare'
            ]
        self.FieldDefinition = '>BBBBBBBB'
        QDPPacket.__init__(self, byte_array, deviceType)
