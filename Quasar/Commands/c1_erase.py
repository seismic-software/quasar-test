from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_erase(QDPPacket):
    """
    Represents a request for the Q330 to erase some of its flash memory
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_ERASE)
        self.Fields = [
            'FirstSector',
            'LastSector'
            ]
        self.FieldDefinition = '>HH'

        QDPPacket.__init__(self, byte_array, deviceType)
