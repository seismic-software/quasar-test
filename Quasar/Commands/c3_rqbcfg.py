from Quasar.CmdID import CmdID
from Quasar.Commands.c2_terc import c2_terc


class c3_rqbcfg(c2_terc):
    """
    Represents a request to the Q330 for baler configuration information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_terc.__init__(self, byte_array, deviceType)
        c2_terc.__finish_init__(self, byte_array)
        self.setSubCommand(CmdID.C3_RQBCFG)
        self.setSpare(0)
