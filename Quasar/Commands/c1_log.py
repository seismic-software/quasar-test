from Quasar.CmdID import CmdID
from Quasar.Commands.c1_slog import c1_slog


class c1_log(c1_slog):
    """
    Represents the response from the Q330 for a data port (log) request
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_slog.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_LOG)
