import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket, qdpPacketHeader
from isti.utils import structWrapper

from .c1_stat import c1_stat


class c1_ping(QDPPacket):
    """
    Represents (unsurprisingly) a ping request to a Q330.
    """

    def __init__(self, byte_array=None, deviceType='Q330', type_value=0):
        self.setQDPCommand(CmdID.C1_PING)
        self.AllFields = {
            0 : [
                'Type',
                'ID',
                'OptionalData'
                ],
            1 : [
                'Type',
                'ID',
                'OptionalData'
                ],
            2 : [
                'Type',
                'UserMessageNumber',
                'StatusRequestBitmap'
                ],
            3 : [
                'Type',
                'UserMessageNumber',
                'DriftTolerance',
                'UserMessageCount',
                'TimeOfLastReboot',
                'Spare1',
                'Spare2',
                'StatusBitmap'
                ],
            4 : [
                'Type',
                'Ignored'
                ],
            5 : [
                'Type',
                'Ignore',
                'Version',
                'Flags',
                'KMIPropertyTag',
                'SerialNumber',
                'DataPort1PacketMemorySize',
                'DataPort2PacketMemorySize',
                'DataPort3PacketMemorySize',
                'DataPort4PacketMemorySize',
                'SerialInterface1MemoryTriggerLevel',
                'SerialInterface2MemoryTriggerLevel',
                'Reserved1',
                'EthernetInterfaceMemoryTriggerLevel',
                'SerialInterface1AdvancedFlags',
                'SerialInterface2AdvancedFlags',
                'Reserved2',
                'EthernetInterfaceAdvancedFlags',
                'SerialInterface1DataPortNumber',
                'SerialInterface2DataPortNumber',
                'Reserved3',
                'EthernetInterfaceDataPortNumber',
                'CalibrationErrorBitmap',
                'SystemSoftwareVersion'
                ]
            }
        if deviceType == 'Q335':
            self.AllFields = {
                0 : [
                    'Type',
                    'ID',
                    'OptionalData'
                    ],
                1 : [
                    'Type',
                    'ID',
                    'OptionalData'
                    ],
                2 : [
                    'Type',
                    'UserMessageNumber',
                    'StatusRequestBitmap'
                    ],
                3 : [
                    'Type',
                    'UserMessageNumber',
                    'DriftTolerance',
                    'UserMessageCount',
                    'TimeOfLastReboot',
                    'Spare1',
                    'Spare2',
                    'StatusBitmap'
                    ],
                4 : [
                    'Type',
                    'Ignored'
                    ],
                5 : [
                    'Type',
                    'Ignore',
                    'Version',
                    'Flags',
                    'KMIPropertyTag',
                    'SerialNumber',
                    'DataPort1PacketMemorySize',
                    'DataPort2PacketMemorySize',
                    'DataPort3PacketMemorySize',
                    'DataPort4PacketMemorySize',
                    'Spare1',
                    'Spare2',
                    'Reserved1',
                    'MemoryTriggerLevel',
                    'Spare3',
                    'Spare4',
                    'Reserved2',
                    'AdvancedFlags',
                    'Spare5',
                    'Spare6',
                    'Reserved3',
                    'DataPort4',
                    'Spare7',
                    'SystemSoftwareVersion'
                    ]
                }
        pingStructs = {
            0 : '>HHL',
            1 : '>HHL',
            2 : '>HHL',
            3 : '>4H4L',
            4 : '>HH',
            5 : '>4HLQ8L10H',
            }

        self.statusClass = None
        if byte_array:
            headerSize = structWrapper.calcsize(qdpPacketHeader)
            data = byte_array[headerSize:]
            type_value = struct.unpack('>H', data[0:2])[0]
            if type_value not in list(self.AllFields.keys()):
                type_value = 0
            if type_value == 3:
                self.statusClass = c1_stat(byte_array[20:])

        self.Fields = self.AllFields[type_value]
        self.FieldDefinition = pingStructs[type_value]

        QDPPacket.__init__(self, byte_array, deviceType)
        self.setType(type_value)

    def getStatusClass(self):
        return self.statusClass

    def __str__(self):
        result = QDPPacket.__str__(self)
        try:
            result += "\n" + self.statusClass.__str__()
        except:
            pass
        return result

