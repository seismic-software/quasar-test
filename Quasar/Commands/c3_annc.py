from Quasar.QDPPacket import QDPPacket


class c3_annc(QDPPacket):
    """
    Represents a response from the Q330 to a request for announce structure
    programming information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        super().__init__(byte_array, deviceType)
        self.Fields = [
            'NumberOfActiveEntries',
            'UnlockFlags',
            ]
        for i in range(1,7):
            self.Fields.extend([
                "DPIPAddress%d" % i,
                "RouterIPAddress%d" % i,
                "TimeoutInMinutes%d" % i,
                "ResumeTimeInMinutes%d" % i,
                "Flags%d" % i,
                "DPUPDPort%d" % i,
            ])
        self.FieldDefinition = 'HHLLHHHHLLHHHHLLHHHHLLHHHHLLHHHHLLHHHH'
