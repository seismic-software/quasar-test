from Quasar.CmdID import CmdID
from Quasar.Commands.c2_sphy2 import c2_sphy2


class c2_phy(c2_sphy2):
    """
    Represents a response from a Q330 to a physical interfaces request
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        # TODO: why is sphy2 used instead of sphy?
        c2_sphy2.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C2_PHY)
