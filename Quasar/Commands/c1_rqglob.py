from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqglob(QDPPacket):
    """
    Represents a request for the Q330's global programming data
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQGLOB)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
