from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_sphy(QDPPacket):
    """
    Represents a request to the Q330 to set physical interfaces.
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SPHY)
        self.Fields = [
            'PhysicalInterfaceNumber',
            'DataPortNumber',
            'ModemInitialization',
            'PhoneNumber',
            'DialOutUserName',
            'DialInUserName',
            'Spare0',
            'DialOutPassword',
            'DialInPassword',
            'Spare1',
            'MemoryTriggerLevel',
            'Flags',
            'RetryIntervalOrPowerOffTime',
            'Interval',
            'WebServerBPSLimit',
            'PointOfContactOrBalerIPAddress',
            'DialInIPSuggestionOrBalerSecondaryIPAddress',
            'PointOfContactPortNumber',
            'BalerRetries',
            'BalerRegistrationTimeout',
            'SerialBaudRate',
            'RoutedPacketTimeout',
            'SerialSensor'
            ]
        self.FieldDefinition = '>HH40p40p20p10pH20p10pHL4HLL6H'
        QDPPacket.__init__(self, byte_array, deviceType)
