#By Kanglin
from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_sphy2(QDPPacket):
    """
    This class appears to define the fields used in a c2_phy response,
    and is nearly identical to c2_sphy except for how some fields are keyed.
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SPHY)
        self.Fields = [
            'PhysicalInterfaceNumber',
            'DataPortNumber',
            'ModemInitialization',
            'PhoneNumber',
            'DialOutUserName',
            'DialInUserName',
            'Spare',
            'DialOutPassword',
            'DialInPassword',
            'Spare',
            'MemoryTriggerLevel',
            'Flags',
            'RetryIntervalOrPowerOffTime',
            'Interval',
            'WebServerBPSLimit',
            'PointOfContactOrBalerIPAddress',
            'BalerSecondaryIPAddress',
            'PointOfContactPortNumber',
            'BalerRetries',
            'BalerRegistrationTimeout',
            'SerialBaudRate',
            'RoutedPacketsTimeout',
            'Spare'
            ]
        self.FieldDefinition = '>HH40p40p20p10pH20p10pHLHHHHLLHHHHHH'
        QDPPacket.__init__(self, byte_array, deviceType)
