from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_scnp(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SCNP)
        self.Fields = [
            'CNPMessage',
            'CNPMessageContinued'
            ]
        self.FieldDefinition = '>LL'
        QDPPacket.__init__(self, byte_array, deviceType)
