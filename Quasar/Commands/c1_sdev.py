from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_sdev(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SDEV)
        self.Fields = [
            'PortNumber',
            'NewOptions'
            ]
        self.FieldDefinition = '>HH'
        QDPPacket.__init__(self, byte_array, deviceType)

    def setBit(self, bit):
        if -1 < bit < 16:
            bits = self.getNewOptions()
            if not bits:
                bits = 0
            bits |= 0x0001 << bit
            self.setNewOptions(bits)

    def setBitClearCrashCodes(self, bitMask):
        self.setBit(0)

    def setBitClearBadPacketMemoryMap(self, bitMask):
        self.setBit(1)
        
