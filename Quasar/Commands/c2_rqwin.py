from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqwin(QDPPacket):
    """
    Represents a request for window data from the Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQWIN)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
