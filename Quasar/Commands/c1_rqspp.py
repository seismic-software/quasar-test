from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqspp(QDPPacket):
    """
    Represents a request for the parameters of the Q330's secondary processor
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQSPP)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
