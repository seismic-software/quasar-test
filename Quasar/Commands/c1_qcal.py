from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitsToStrings


class c1_qcal(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_QCAL)
        self.Fields = [
            'StartingTime',
            'Waveform',
            'Amplitude',
            'DurationInSeconds',
            'SettlingTimeInSeconds',
            'CalibrationBitmap',
            'TrailerTimeInSeconds',
            'SensorControlBitmap',
            'MonitorChannelBitmap',
            'FrequencyDivider',
            'Spare',
            'CouplingBytes'
            ]
        self.FieldDefinition = '>LHHHHHHHHHH12s'
        QDPPacket.__init__(self, byte_array, deviceType)

    def strWaveform(self):
        v = self.getWaveform()
        ret = ['']
        whichWaveform = v & 0x07

        if self._deviceType == 'Q335':
            ret.append( [
                'Sine',
                'Red Noise',
                'White Noise',
                'Step',
                'Random Telegraph',
                '',
                '',
                'Timing Test',
                ][whichWaveform])
        else:
            ret.append( [
                'Sine',
                'Red Noise',
                'White Noise',
                'Step',
                'Random Telegraph',
                ][whichWaveform])

        
        if v & 0x40:
            ret.append('Negative step')
        else:
            ret.append('Positive step')

        if v & 0x80:
            ret.append('Automatic calibration')

        return '\n      '.join(ret)

    def strCalibrationBitmap(self):
        values = [
            'Channel 1',
            'Channel 2',
            'Channel 3',
            'Channel 4',
            'Channel 5',
            'Channel 6'
            ]
        return bitsToStrings(self.getCalibrationBitmap(), values)

    def strSensorControlBitmap(self):
        values = [
            'Channel 1',
            'Channel 2',
            'Channel 3',
            'Channel 4',
            'Channel 5',
            'Channel 6'
            ]
        return bitsToStrings(self.getCalibrationBitmap(), values)

    def strMonitorChannelBitmap(self):
        values = [
            'Channel 1',
            'Channel 2',
            'Channel 3',
            'Channel 4',
            'Channel 5',
            'Channel 6'
            ]
        return bitsToStrings(self.getCalibrationBitmap(), values)
            
        

        
        
