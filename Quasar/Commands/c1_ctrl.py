from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket

FLAGS_SAVE_TO_EEPROM = 0x0001  # bit  0
FLAGS_REBOOT = 0x0002  # bit  1
FLAGS_RESYNC = 0x0004  # bit  2
FLAGS_TURN_ON_GPS = 0x0008  # bit  3
FLAGS_TURN_OFF_GPS = 0x0010  # bit  4
FLAGS_COLD_START_GPS = 0x0020  # bit  5
FLAGS_REBOOT_EP_S1 = 0x0040  # bit  6
FLAGS_REBOOT_EP_S2 = 0x0080  # bit  7


class c1_ctrl(QDPPacket):
    """
    Represents various command operations (see const values above)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_CTRL)
        self.Fields = [
            'Flags'
        ]
        self.FieldDefinition = '>H'
        QDPPacket.__init__(self, byte_array, deviceType)

    def strFlags(self):
        flags = self.getFlags()
        ret = ['']
        if flags & FLAGS_SAVE_TO_EEPROM:
            ret.append('Save current programming to EEPROM')
        if flags & FLAGS_REBOOT:
            ret.append('Reboot')
        if flags & FLAGS_RESYNC:
            ret.append('Re-Sync')
        if flags & FLAGS_TURN_ON_GPS:
            ret.append('Turn on GPS')
        if flags & FLAGS_TURN_OFF_GPS:
            ret.append('Turn off GPS')
        if flags & FLAGS_COLD_START_GPS:
            ret.append('Cold-start GPS')
        if flags & FLAGS_REBOOT_EP_S1:
            ret.append('Reboot EnvProc on Serial 1')
        if flags & FLAGS_REBOOT_EP_S2:
            ret.append('Reboot EnvProc on Serial 2')

        return '\n      '.join(ret)
