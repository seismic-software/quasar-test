import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_bresp(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_BRESP)
        self.Fields = [
            'BalerResponse'
            ]

        brespStruct = ">H"

        fieldDef = brespStruct

        if byte_array is not None:
            cmdInfo = struct.unpack(brespStruct, byte_array[12:14])[0]
            if cmdInfo in (0, 2, 4, 6):
                pass
            elif cmdInfo == 3:
                fieldDef = fieldDef + 'H'
                packetSize = struct.unpack(">H", byte_array[6:8])[0]
                strSize = packetSize - 16
                fieldDef = fieldDef + '%ds' %strSize
                self.Fields.append('SequencingField')
                self.Fields.append('Info')
            elif cmdInfo == 5:
                fieldDef = fieldDef + 'HL'
                self.Fields.append('SequencingField')
                self.Fields.append('CommandSpecificInformation')
            else:
                print('Unsupported bcmd response')

        self.FieldDefinition = fieldDef
        QDPPacket.__init__(self, byte_array, deviceType)
