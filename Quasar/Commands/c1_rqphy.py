from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqphy(QDPPacket):
    """
    Represents a request to the Q330 to receive physical interface information
    (i.e., MAC and IP addresses)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQPHY)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
