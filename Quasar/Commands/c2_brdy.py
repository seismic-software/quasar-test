try:
    import warnings
    warnings.filterwarnings("ignore", r'.*', FutureWarning, 'c2_brdy', 0)
except:
    pass

import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket

BRDY_VPREF = 0xE5F40000
BRDY_AVAIL = 0xE5F30000

class c2_brdy(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_BRDY)
        self.Fields = [
            'Q330SerialNumber',
            'Network',
            'StationName',
            'Flags',
            'ModelNumber',
            'Version',
            'DiskSizeInBytes',
            'BalerSerialNumber'
            ]
        self.FieldDefinition = '<QH5sBHHLQ'
        QDPPacket.__init__(self, byte_array, deviceType)

        highByte, lowByte = struct.unpack(">LL", struct.pack("<Q", self.getQ330SerialNumber()))
        self.setVPREF(0)
        self.setAVAIL(0)
        self.setBalerTag(0)
        if highByte == BRDY_VPREF:
            self.setVPREF(1)
            self.setQ330SerialNumber(1)
            self.setBalerTag(lowByte)
        if highByte == BRDY_AVAIL:
            self.setAVAIL(1)
            self.setQ330SerialNumber(1)
            self.setBalerTag(lowByte)

