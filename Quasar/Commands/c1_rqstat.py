from Quasar import Status
from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqstat(QDPPacket):
    """
    Represents a status request to a Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQSTAT)
        self.Fields = [
            'RequestBitmap'
            ]
        self.FieldDefinition = '>L'

        QDPPacket.__init__(self, byte_array, deviceType)

    def strRequestBitmap(self):
        return Status.bitmapToString(self.getRequestBitmap())
            
