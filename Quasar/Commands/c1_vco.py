from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_vco(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_VCO)
        self.Fields = [
            'NewCurrentVCOValue',
            'PLLFlag'
            ]
        self.FieldDefinition = '>HH'
        QDPPacket.__init__(self, byte_array, deviceType)
