from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqflgs(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQFLGS)
        self.Fields = [
            'DataPortNumber'
            ]
        self.FieldDefinition = '>H'
        if deviceType == 'Q335':
            self.Fields = [
                'DataPortNumber',
                'Q335AwareFlag'
                ]
            self.FieldDefinition = '>HH'

        QDPPacket.__init__(self, byte_array, deviceType)

    def setQ335Aware(self, aware=True):
        if aware:
            self.setQ335AwareFlag(1)
        else:
            self.setQ335AwareFlag(0)
        
