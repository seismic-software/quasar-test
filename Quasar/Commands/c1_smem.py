from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c1_smem(QDPPacket):
    """
    Represents a command to set some memory in the Q330 to specific values
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SMEM)
        self.Fields = [
            'StartingAddress',
            'ByteCount',
            'MemoryType',
            'MemoryContents'
            ]
        self._memHeader = '>LHH'
        self.FieldDefinition = self._memHeader
        headerSize = structWrapper.calcsize(self._memHeader) + 12
        if byte_array:
            self.FieldDefinition = self._memHeader + ("%ds" %(len(byte_array)-headerSize))

            QDPPacket.__init__(self, byte_array, deviceType)
        else:
            QDPPacket.__init__(self, None, deviceType)

    def setMemoryContents(self, newcontents):
        self.FieldDefinition = self._memHeader + f'{len(newcontents):d}s'
        self.__dict__['MemoryContents'] = newcontents 

    def strMemoryContents(self):
        byte_array = self.getMemoryContents()
        string_out = ''
        i = 0
        for byte in byte_array:
            if i%16 == 0:
                string_out += '\n'
            string_out += f'{ord(byte):02x} '
            i+= 1
        return string_out


