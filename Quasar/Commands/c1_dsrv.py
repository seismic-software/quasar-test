from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_dsrv(QDPPacket):
    """
    Represents a deregistration request to a Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_DSRV)
        self.Fields = [
            'SerialNumber'
            ]
        self.FieldDefinition = '>Q'
#        self.setIsBroadcast(1)
        QDPPacket.__init__(self, byte_array, deviceType)

    def strSerialNumber(self):
        return '%x' %self.getSerialNumber()
