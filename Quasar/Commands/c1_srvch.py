import socket

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c1_srvch(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SRVCH)

        self.Fields = [
            'ChallengeValue',
            'ServerIPAddress',
            'ServerUDPPort',
            'RegistrationNumber'
            ]
        self.FieldDefinition = '>QLHH'

        QDPPacket.__init__(self, byte_array, deviceType)

    
    def strServerIPAddress(self):
        return socket.inet_ntoa(structWrapper.pack('>L', self.getServerIPAddress()))
