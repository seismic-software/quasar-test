from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_pulse(QDPPacket):
    """
    Represents a sensor control pulse (i.e., recenter, lock, unlock)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_PULSE)
        self.Fields = [
            'SensorControlBitmap',
            'Duration'
            ]
        self.FieldDefinition = '>HH'
        if deviceType == 'Q335':
            self.Fields = [
                'SensorControlBitmap',
                'Duration',
                'Spare1',
                'Flags',
                'NewSensorAOffTime',
                'NewSensorBOffTime',
                ]
            self.FieldDefinition = '>HHHHLL'
        QDPPacket.__init__(self, byte_array, deviceType)

    def setSensorAOffTime(self, off_time):
        self.setNewSensorAOffTime(off_time)
        self.setFlags(self.getFlags() | 0x01)

    def setSensorBOffTime(self, off_time):
        self.setNewSensorBOffTime(off_time)
        self.setFlags(self.getFlags() | 0x02)

