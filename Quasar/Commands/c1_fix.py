from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitIndexToInteger, bitIndexListToInteger


class c1_fix(QDPPacket):
    """
    Represents a response to a request for fixed values (see fields for what
    data is returned).
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_FIX)
        self.Fields = [
            'TimeOfLastReboot',
            'TotalNumberOfReboots',
            'BackupDataStructureBitmap',
            'DefaultDataStructureBitmap',
            'CalibratorType',
            'CalibratorVersion',
            'AuxType',
            'AuxVersion',
            'ClockType',
            'Flags',
            'SystemVersion',
            'SlaveProcessorVersion',
            'PLDVersion',
            'MemoryBlockSize',
            'KMIPropertyTag',
            'SystemSerialNumber',
            'AnalogMotherBoardSerialNumber',
            'Seismometer1SerialNumber',
            'Seismometer2SerialNumber',
            'QAPCHP1SerialNumber',
            'InternalDataMemorySize',
            'InternalDataMemoryUsed',
            'ExternalDataMemorySize',
            'FlashMemorySize',
            'ExternalDataMemoryUsed',
            'QAPCHP2SerialNumber',
            'DataPort1PacketMemorySize',
            'DataPort2PacketMemorySize',
            'DataPort3PacketMemorySize',
            'DataPort4PacketMemorySize',
            'Bit7Frequency',
            'Bit6Frequency',
            'Bit5Frequency',
            'Bit4Frequency',
            'Bit3Frequency',
            'Bit2Frequency',
            'Bit1Frequency',
            'Bit0Frequency',
            'Channels1To3Bit7FrequencyDelayInUSec',
            'Channels1To3Bit6FrequencyDelayInUSec',
            'Channels1To3Bit5FrequencyDelayInUSec',
            'Channels1To3Bit4FrequencyDelayInUSec',
            'Channels1To3Bit3FrequencyDelayInUSec',
            'Channels1To3Bit2FrequencyDelayInUSec',
            'Channels1To3Bit1FrequencyDelayInUSec',
            'Channels1To3Bit0FrequencyDelayInUSec',
            'Channels4To6Bit7FrequencyDelayInUSec',
            'Channels4To6Bit6FrequencyDelayInUSec',
            'Channels4To6Bit5FrequencyDelayInUSec',
            'Channels4To6Bit4FrequencyDelayInUSec',
            'Channels4To6Bit3FrequencyDelayInUSec',
            'Channels4To6Bit2FrequencyDelayInUSec',
            'Channels4To6Bit1FrequencyDelayInUSec',
            'Channels4To6Bit0FrequencyDelayInUSec'
            ]
        self.FieldDefinition = '>LLLL10HLQQQQ11L8B16L'
        if deviceType == 'Q335':
            self.Fields = [
                'TimeOfLastReboot',
                'TotalNumberOfReboots',
                'BackupDataStructureBitmap',
                'DefaultDataStructureBitmap',
                'CalibratorType',
                'CalibratorFE1Version',
                'Spare1',
                'Spare2',
                'ClockType',
                'Flags',
                'SystemBackEndVersion',
                'CoreProcessorVersion',
                'FE2Version',
                'Spare3',
                'KMIPropertyTag',
                'SystemSerialNumber',
                'Spare4',
                'Seismometer1SerialNumber',
                'Seismometer2SerialNumber',
                'FrontEndProcessor1SerialNumber',
                'Spare5',
                'Spare6',
                'Spare7',
                'Spare8',
                'Spare9',
                'FrontEndProcessor2SerialNumber',
                'DataPort1PacketMemorySize',
                'DataPort2PacketMemorySize',
                'DataPort3PacketMemorySize',
                'DataPort4PacketMemorySize',
                'Bit7Frequency',
                'Bit6Frequency',
                'Bit5Frequency',
                'Bit4Frequency',
                'Bit3Frequency',
                'Bit2Frequency',
                'Bit1Frequency',
                'Bit0Frequency',
                'Channels1To3Bit7FrequencyDelayInUSec',
                'Channels1To3Bit6FrequencyDelayInUSec',
                'Channels1To3Bit5FrequencyDelayInUSec',
                'Channels1To3Bit4FrequencyDelayInUSec',
                'Channels1To3Bit3FrequencyDelayInUSec',
                'Channels1To3Bit2FrequencyDelayInUSec',
                'Channels1To3Bit1FrequencyDelayInUSec',
                'Channels1To3Bit0FrequencyDelayInUSec',
                'Channels4To6Bit7FrequencyDelayInUSec',
                'Channels4To6Bit6FrequencyDelayInUSec',
                'Channels4To6Bit5FrequencyDelayInUSec',
                'Channels4To6Bit4FrequencyDelayInUSec',
                'Channels4To6Bit3FrequencyDelayInUSec',
                'Channels4To6Bit2FrequencyDelayInUSec',
                'Channels4To6Bit1FrequencyDelayInUSec',
                'Channels4To6Bit0FrequencyDelayInUSec'
                ]
        QDPPacket.__init__(self, byte_array, deviceType)

        self.dataStructureBits = [
            (2 ** 0,  'Slave Processor parameters'),
            (2 ** 1,  'Manufacturer\'s Area'),
            (2 ** 2,  'Authentication Code'),
            (2 ** 3,  'Physical Interfaces'),
            (2 ** 4,  'Data Port 1'),
            (2 ** 5,  'Data Port 2'),
            (2 ** 6,  'Data Port 3'),
            (2 ** 7,  'Data Port 4'),
            (2 ** 8,  'Global Programming'),
            (2 ** 9,  'Record Keeping (Internal data structure)'),
            (2 ** 10, 'Sensor Control Mapping'),
            (2 ** 11, 'Power Supply Parameters'),
            (2 ** 12, 'Advanced Physical on serial 1'),
            (2 ** 13, 'Advanced Physical on serial 2'),
            (2 ** 15, 'Advanced Physical on Ethernet'),
            (2 ** 16, 'GPS and PLL Parameters'),
            (2 ** 17, 'Automatic Re-center'),
            (2 ** 18, 'Recording Windows'),
            (2 ** 19, 'Announcements'),
            (2 ** 20, 'Baler Link'),
            (2 ** 21, 'Baler Configuration'),
            (2 ** 22, 'Environmental Processor Configuration'),
        ]
        if deviceType == 'Q335':
            self.dataStructureBits = [
                (2 ** 0,  'Slave Processor parameters'),
                (2 ** 1,  'Manufacturer\'s Area'),
                (2 ** 2,  'Authentication Code'),
                (2 ** 4,  'Data Port 1'),
                (2 ** 5,  'Data Port 2'),
                (2 ** 6,  'Data Port 3'),
                (2 ** 7,  'Data Port 4'),
                (2 ** 8,  'Global Programming'),
                (2 ** 9,  'Record Keeping (Internal data structure)'),
                (2 ** 10, 'Sensor Control Mapping'),
                (2 ** 16, 'GPS and PLL Parameters'),
                (2 ** 17, 'Automatic Re-center'),
                (2 ** 19, 'Announcements'),
                (2 ** 23, 'Communication Parameters'),
            ]

    def strBackupDataStructureBitmap(self):
        display_string = ''
        bitmap = self.getBackupDataStrucutreBitmap()
        if type(bitmap) in (int, int):
            display_string = "The following structures were corrupt in the Backup Data Structure:\n"
            for (bit, message) in self.dataStructureBits:
                if bitmap & bit:
                    display_string += message + "\n"

    def strDefaultDataStructureBitmap(self):
        display_string = ''
        bitmap = self.getDefaultDataStrucutreBitmap()
        if type(bitmap) in (int, int):
            display_string = "The following structures were corrupt in the Main Data Structure:\n"
            for (bit, message) in self.dataStructureBits:
                if bitmap & bit:
                    display_string += message + "\n"

    def strClockType(self):
        if self.getClockType() == 0:
            return 'No clock'
        if self.getClockType() == 1:
            return 'Motorola M12'
        return 'Unknown clock type'

    def strCalibratorType(self):
        if self.getCalibratorType() == 33:
            if self._deviceType == 'Q335':
                return 'QCal335'
            return 'QCal330'
        return 'Unknown calibrator type'

    def strCalibratorFE1Version(self):
        return self.getVersionString(self.getCalibratorFE1Version())

    def strCalibratorVersion(self):
        return self.getVersionString(self.getCalibratorVersion())

    def strSystemBackEndVersion(self):
        return self.getVersionString(self.getSystemBackEndVersion())

    def strAuxType(self):
        if self.getAuxType() == 32:
            return 'AuxAD'
        return 'Unknown auxiliary type'

    def strCoreProcessorVersion(self):
        return self.getVersionString(self.getCoreProcessorVersion())

    def strAuxVersion(self):
        return self.getVersionString(self.getAuxVersion())

    def strFE2Version(self):
        return self.getVersionString(self.getFE2Version())

    def strSystemVersion(self):
        return self.getVersionString(self.getSystemVersion())

    def strSlaveProcessorVersion(self):
        return self.getVersionString(self.getSlaveProcessorVersion())

    def strPLDVersion(self):
        return self.getVersionString(self.getPLDVersion())
    
    def strBit7Frequency(self):
        return self.getFrequencyString(self.getBit7Frequency())

    def strBit6Frequency(self):
        return self.getFrequencyString(self.getBit6Frequency())

    def strBit5Frequency(self):
        return self.getFrequencyString(self.getBit5Frequency())

    def strBit4Frequency(self):
        return self.getFrequencyString(self.getBit4Frequency())

    def strBit3Frequency(self):
        return self.getFrequencyString(self.getBit3Frequency())

    def strBit2Frequency(self):
        return self.getFrequencyString(self.getBit2Frequency())

    def strBit1Frequency(self):
        return self.getFrequencyString(self.getBit1Frequency())

    def strBit0Frequency(self):
        return self.getFrequencyString(self.getBit0Frequency())

    @staticmethod
    def getVersionString(val):
        return '%d.%d' %(val/256, val%256)

    def getFlagsString(self):
        flags = self.getFlags()
        ret = ['']
        if self._deviceType == 'Q335':
            if flags & bitIndexToInteger(0):
                ret.append('Back-end running in normal mode (ports on Ethernet interface)')
            else:
                ret.append('Back-end running in loop-back only mode')
        else:
            if flags & bitIndexToInteger(0):
                ret.append('Ethernet installed')
            else:
                ret.append('Ethernet not installed')
        if flags & bitIndexToInteger(1):
            ret.append('Q330 Software can process a Dynamic IP Address status request')
        if flags & bitIndexToInteger(2):
            ret.append('Q330 Software can process an Auxiliary Board status request')
        if flags & bitIndexToInteger(3):
            ret.append(f'{self._deviceType} Software can process expanded C1_WEB commands')
        if flags & bitIndexToInteger(4):
            ret.append('Q330 Software can process a Serial Sensor status request')
        if flags & bitIndexToInteger(5):
            ret.append('Q330 can report higher than 255ma supply current')
        if flags & bitIndexToInteger(6):
            ret.append('Q330 has at least one Environmental Processor configured')
        if flags & bitIndexToInteger(7):
            ret.append('Q330 is a Q330S (integrated Baler44 on Serial 2)')
        if flags & bitIndexToInteger(8):
            ret.append('Device is a Q335 (Q330S+ or newer)')

        return '\n      '.join(ret)

    @staticmethod
    def getFrequencyString(val):
        if val == 255:
            return '0.1Hz'
        if val & bitIndexToInteger(7):
            # MS bit is set, look at the low order 4 bits
            maskedVal = val & (bitIndexListToInteger([0, 1, 2, 3]))
            freqs = [
                'Unknown',
                'Unknown',
                'Unknown',
                'Unknown',
                '200Hz',
                '250Hz',
                '300Hz',
                '400Hz',
                '500Hz',
                '800Hz',
                '1000Hz'
                ]
            return freqs[maskedVal]
        else:
            return '%sHz' %val
            
