from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqphy(QDPPacket):
    """
    Represents a request to the Q330 for physical interface information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQPHY)
        self.Fields = [
            'PhysicalInterfaceNumber'
            ]
        self.FieldDefinition = '>H'
        QDPPacket.__init__(self, byte_array, deviceType)
