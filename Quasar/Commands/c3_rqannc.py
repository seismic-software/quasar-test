from Quasar.CmdID import CmdID
from Quasar.Commands.c2_terc import c2_terc


class c3_rqannc(c2_terc):
    """
    Represents a request to the Q330 for announce structure programming info
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_terc.__init__(self, byte_array, deviceType)
        c2_terc.__finish_init__(self, byte_array)
        self.setSubCommand(CmdID.C3_RQANNC)
        self.setSpare(0)
