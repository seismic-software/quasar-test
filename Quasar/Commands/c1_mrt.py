from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_mrt(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_MRT)
        self.Fields = [
            'IPAddress',
            'NewPhysicalInterface',
            'NewDataPort'
            ]
        self.FieldDefinition = '>LHH'
        QDPPacket.__init__(self, byte_array, deviceType)
