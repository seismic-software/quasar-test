from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_sepcfg(QDPPacket):
    """
    Represents a request to set fields related to the environmental processor
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SEPCFG)
        self.Fields = [
            'ReservedSerial1EP',
            'ReservedSerial2EP',
            'FlagsSerial1EP',
            'FlagsSerial2EP',
            'ChannelCount',
            'Spare',
            ]
        self.FieldDefinition = '>LLLLHH'
        QDPPacket.__init__(self, byte_array, deviceType)

    def addChannel(self, channel, output_mask):
        index = self.getChannelCount()

        field = "Channel_%d" % index
        self.Fields.append(field)
        setattr(self, field, channel)

        field = "OutputMask_%d" % index
        setattr(self, field, output_mask)
        self.Fields.append(field)

        self.FieldDefinition += 'HH'
        self.setChannelCount(self.getChannelCount()+1)
