from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_web(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_WEB)
        self.Fields = [
            'IPAndPortNumber',
            'StationName'
            ]
        self.FieldDefinition = '>24s8s'
        if (deviceType == 'Q335') or ((byte_array is not None) and (len(byte_array) > 32)):
            self.Fields = [
                'StationName',
                'DPAddressAndPortNumber'
            ]
            self.FieldDefinition = '>8s256s'

        QDPPacket.__init__(self, byte_array, deviceType)
