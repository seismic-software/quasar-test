from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_sbpwr(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SBPWR)
        self.Fields = [
            'PhysicalInterface',
            'FlagAndTimeout'
            ]
        self.FieldDefinition = '>HH'
        if deviceType == 'Q335':
            self.Fields = [
                'Spare1',
                'FlagAndTimeout'
                ]
        QDPPacket.__init__(self, byte_array, deviceType)
            
    def setBit(self, bit):
        if -1 < bit < 16:
            bits = self.getNewOptions()
            if not bits:
                bits = 0
            bits |= 0x0001 << bit
            self.setNewOptions(bits)

    def clearBit(self, bit):
        if -1 < bit < 16:
            bits = self.getNewOptions()
            if not bits:
                bits = 0
            bits &= ~(0x0001 << bit)
            self.setNewOptions(bits)
