from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_sout(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SOUT)
        self.Fields = [
            'NewBitmap'
            ]
        self.FieldDefinition = '>H'
        QDPPacket.__init__(self, byte_array, deviceType)
