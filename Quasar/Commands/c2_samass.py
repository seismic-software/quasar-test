from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_samass(QDPPacket):
    """
    Represents a request to the Q330 to set automated mass-centering details
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SAMASS)
        self.Fields = [
            'Tolerance1A',
            'Tolerance1B',
            'Tolerance1C',
            'MaximumTries1',
            'NormalInterval1',
            'SquelchInterval1',
            'SensorControlBitmap1',
            'Duration1',
            'Tolerance2A',
            'Tolerance2B',
            'Tolerance2C',
            'MaximumTries2',
            'NormalInterval2',
            'SquelchInterval2',
            'SensorControlBitmap2',
            'Duration2'
            ]
        self.FieldDefinition = '>16H'
        QDPPacket.__init__(self, byte_array, deviceType)
