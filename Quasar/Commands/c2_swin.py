from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c2_swin(QDPPacket):
    """
    Represents a request to the Q330 to set windowing data
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SWIN)
        self.Fields = [
            'RecordingFlags',
            'NumberOfWindows',
            ]
        self.FieldDefinition = '>HH'
        if byte_array:
            numWindows = structWrapper.unpack(">H", byte_array[14:16])[0]
            for i in range(1, numWindows+1):
                self.Fields.append('StartRecordingTime_%d' % i)
                self.Fields.append('StopRecordingTime_%d' % i)
                self.FieldDefinition = self.FieldDefinition + 'LL'

        QDPPacket.__init__(self, byte_array, deviceType)
