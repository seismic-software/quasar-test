
from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitIndexToInteger


class c1_slog(QDPPacket):
    """
    Represents a request to set data to a Q330 data port (logging)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SLOG)
        self.FieldDefinition = '>18HLHHHHL'
        self.Fields = [
            'DataPortNumber',
            'Flags',
            'PercentOfPacketBuffer',
            'MTU',
            'GroupCount',
            'MaximumResendTimeout',
            'GroupTimeout',
            'MinimumResendTimeout',
            'WindowSize',
            'DataSequenceNumber',
            'Channel1Freqs',
            'Channel2Freqs',
            'Channel3Freqs',
            'Channel4Freqs',
            'Channel5Freqs',
            'Channel6Freqs',
            'AcknowledgeCount',
            'AcknowledgeTimeout',
            'OldDataThresholdTime',
            'EthernetThrottle',
            'AlarmPercent',
            'AutomaticFilters',
            'ManualFilters',
            'Spare'
        ]
        if deviceType == 'Q335':
            self.Fields = [
                'DataPortNumber',
                'Flags',
                'PercentOfPacketBuffer',
                'Spare1',
                'GroupCount',
                'MaximumResendTimeout',
                'GroupTimeout',
                'MinimumResendTimeout',
                'WindowSize',
                'DataSequenceNumber',
                'Channel1Freqs',
                'Channel2Freqs',
                'Channel3Freqs',
                'Channel4Freqs',
                'Channel5Freqs',
                'Channel6Freqs',
                'AcknowledgeCount',
                'AcknowledgeTimeout',
                'OldDataThresholdTime',
                'EthernetThrottle',
                'Spare2',
                'Spare3',
                'ManualFilters',
                'Spare4'
            ]

        QDPPacket.__init__(self, byte_array, deviceType)

    def strDataPortNumber(self):
        portStrings = [
            'Data Port 1',
            'Data Port 2',
            'Data Port 3',
            'Data Port 4'
        ]
        return portStrings[self.getDataPortNumber()]

    def strFlags(self):
        flags = self.getFlags()
        ret = ['']

        if flags & bitIndexToInteger(0):
            ret.append('Fill mode enabled')
        else:
            ret.append('Fill mode disabled')

        if flags & bitIndexToInteger(1):
            ret.append('Flush packet buffer based on time')

        if flags & bitIndexToInteger(2):
            ret.append('Freeze data port output')

        if flags & bitIndexToInteger(3):
            ret.append('Freeze packet buffer input')

        if flags & bitIndexToInteger(4):
            ret.append('Keep oldest data in packet buffer')

        if flags & bitIndexToInteger(8):
            ret.append('Have DP piggyback status requests with DT_DACK packets')

        if flags & bitIndexToInteger(9):
            ret.append('Turn on "Communications Fault" LED if flush did not reduce buffer by 5%%')

        if flags & bitIndexToInteger(10):
            ret.append('Allow "Hot Swap" on this data port')

        if flags & bitIndexToInteger(11):
            ret.append('Flush sliding window buffer based on time')

        if flags & bitIndexToInteger(14):
            ret.append('Base-96 encode data packets')

        if flags & bitIndexToInteger(15):
            if self._deviceType == 'Q335':
                ret.append('Save changes in DATAFLASH')
            else:
                ret.append('Save changes in EEPROM')

        return '\n      '.join(ret)

    def strPercentOfPacketBuffer(self):
        return '%f%%' % (self.getPercentOfPacketBuffer() * (100.0 / 256.0))

    def strMaximumResendTimeout(self):
        return '%d ms' % (self.getMaximumResendTimeout() * 100)

    def strMinimumResendTimeout(self):
        return '%d ms' % (self.getMinimumResendTimeout() * 100)

    def strAcknowledgeTimeout(self):
        return '%d ms' % (self.getAcknowledgeTimeout() * 100)

    def strAlarmPercent(self):
        return '%f%%' % (self.getAlarmPercent() * (100.0 / 256.0))
