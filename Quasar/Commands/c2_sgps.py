from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_sgps(QDPPacket):
    """
    Represents a request to set GPS programming data
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_SGPS)
        self.Fields = [
            'TimingMode',
            'PowerCyclingMode',
            'GPSOffTime',
            'GPSResyncHour',
            'GPSMaximumOnTime',
            'PLLLockUSecs',
            'Spare1',
            'PLLUpdateIntervalInSeconds',
            'PLLFlags',
            'Pfrac',
            'VCOSlope',
            'VCOIntercept',
            'MaxInitialKMRMS',
            'InitialKMWeight',
            'KMWeight',#added here by Kanglin
            'BestVCOWeight',
            'KMDelta',
            'Spare2',
            'Spare3'
            ]
        self.FieldDefinition = '>HHHHHHLHHffffffffLL'
        if deviceType == 'Q335':
            self.Fields = [
                'Spare1',
                'PowerCyclingMode',
                'GPSOffTime',
                'GPSResyncHour',
                'GPSMaximumOnTime',
                'PLLLockUSecs',
                'Spare2',
                'PLLUpdateIntervalInSeconds',
                'PLLFlags',
                'Pfrac',
                'VCOSlope',
                'VCOIntercept',
                'MaxInitialKMRMS',
                'InitialKMWeight',
                'KMWeight',#added here by Kanglin
                'BestVCOWeight',
                'KMDelta',
                'Spare3',
                'Spare4'
                ]
        QDPPacket.__init__(self, byte_array, deviceType)
        
            
