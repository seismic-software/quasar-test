from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_regchk(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_REGCHK)
        self.Fields = [
            'IPAddress'
            ]
        self.FieldDefinition = '>L'
        QDPPacket.__init__(self, byte_array, deviceType)
        
