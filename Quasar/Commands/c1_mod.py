from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c1_mod(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_MOD)
        self.Fields = []
        self.FieldDefinition = '>'

        self.numModules = 0

        modStruct = '>12pHHL'

        if byte_array:
            self.numModules = (len(byte_array) - 12) / structWrapper.calcsize(modStruct)
            for i in range(1, self.numModules+1):
                self.Fields.append('ModuleName_%d' %i)
                self.Fields.append('Revision_%d' %i)
                self.Fields.append('OverlayNumber_%d' %i)
                self.Fields.append('ModuleCRC_%d' %i)
                self.FieldDefinition = self.FieldDefinition + modStruct[1:]
        QDPPacket.__init__(self, byte_array, deviceType)

    def __str__(self):
        result = " Name         | Revision | Overlay | CRC         \n"
        result += "------------------------------------------------\n"
        for i in range(1, self.numModules+1):
            name = self.__dict__["ModuleName_%d" % i]
            result += " " + name.strip().ljust(12) + " | "
            result += str("%d.%d" % divmod(self.__dict__["Revision_%d" % i], 16)).ljust(8) + " | "
            overlay = self.__dict["OverlayNumber_%d" % i]
            if overlay == 65535:
                result += "        | "
            else:
                result += str("0x%04x" % (overlay,)).rjust(7) + " | "
            result += "0x%08x" % self.__dict__["ModuleCRC_%d" % i]
            result += "\n"
        return result

