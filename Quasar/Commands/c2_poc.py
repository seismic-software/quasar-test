import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket, qdpPacketHeader
from isti.utils import structWrapper


class c2_poc(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_POC)
        self.setIsBroadcast(1)
        self.Fields = [
            'Q330SerialNumber',
            'Q330IPAddress',
            'PointOfContactIPBalerIPAddress',
            'BalerSecondaryIPAddressOrNetMask',
            'Q330BasePortNumber',
            'DataPortNumber',
            'WebServerBPSLimit',
            'Flags',
            'AccessTimeout',
            'SerialBaudRate',
            'BalerSerialNumber'
            ]
        if deviceType == 'Q335':
            self.Fields = [
                'Q330SerialNumber',
                'Q330IPAddress',
                'PointOfContactIPBalerIPAddress',
                'Spare1',
                'Q330BasePortNumber',
                'DataPortNumber',
                'Spare2',
                'Spare3',
                'Spare4',
                'Spare5',
                'Spare6'
                ]
        self.FieldDefinition = '>QLLLHHHHHHQ'

        if deviceType == 'Q330':
            header_size = structWrapper.calcsize(qdpPacketHeader)
            data = byte_array[header_size:]
            flags = struct.unpack('>H', data[26:28])[0]
            if flags & 0x08: # Following baler configuration block
                block_length = struct.unpack('>H', data[40:42])[0]
                self.Fields.extend([
                    'BlockSize',
                    'PhysicalInterfaceNumber',
                    'BalerType',
                    'Spare',
                    'ConfigurationString',
                ])
                self.FieldDefinition += 'HHHH'
                self.FieldDefinition += "%ds" % (block_length - 8,)

        QDPPacket.__init__(self, byte_array, deviceType)
