from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_cack(QDPPacket):
    """
    Represents a command acknowledgement response from a Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_CACK)
        self.Fields = []
        self.FieldDefinition = ''
        
        QDPPacket.__init__(self, byte_array, deviceType)
