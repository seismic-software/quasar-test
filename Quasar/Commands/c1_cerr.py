from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_cerr(QDPPacket):
    """
    Represents an error message sent from the Q330 in response to a command
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_CERR)
        self.Fields = [
            'ErrorCode'
            ]
        self.FieldDefinition = '>H'
        self.setErrorCode(-1)

        QDPPacket.__init__(self, byte_array, deviceType)

    def strErrorCode(self):
        return self.getErrorText()
    
    def getErrorText(self):
        codeToMessage(self.getErrorCode())

def codeToMessage(code):
    if code == -1:
        return 'No Error'
    elif code == 0:
        return 'No Permission - Invalid Password.'
    elif code == 1:
        return 'Too many configuration or special functions servers are registered.'
    elif code == 2:
        return 'You are not currently registered, or registration timed out.'
    elif code == 3:
        return 'Invalid registration response.'
    elif code == 4:
        return 'Parameter error.'
    elif code == 5:
        return 'Structure not valid.  Tried to read an EEPROM structure that is not valid.'
    elif code == 6:
        return 'Command only valid on configuration port.'
    elif code == 7:
        return 'Command only valid on special functions port.'
    elif code == 8:
        return 'Memory operation in progress.  Try again later.'
    elif code == 9:
        return 'Calibration in progress.  Try again later.'
    elif code == 10:
        return 'Data not yet available for QuickView.'
    elif code == 11:
        return 'Console (virtual) interface only.  Must use the front panel connector.'
    elif code == 12:
        return 'Flash write or erase error.'
    else:
        return 'Unknown error (%d)' %code

