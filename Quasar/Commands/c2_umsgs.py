import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_umsgs(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_UMSGS)
        self.Fields = [
            'Flags',
            'NumberValid',
            ]
        self.FieldDefinition = '>HH'

        num_valid = struct.unpack('>H', byte_array[14:16])[0]
        msg_index = 4
        for i in range (0, num_valid):
            self.Fields.append("SendersIPAddress_%d" % i)
            self.Fields.append("UserMessage_%d" % i)
            self.FieldDefinition += 'L80p'

        QDPPacket.__init__(self, byte_array, deviceType)
