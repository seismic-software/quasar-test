from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqfree(QDPPacket):
    """
    Represents a request for addresses of free memory on the Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQFREE)
        self.Fields = []
        self.FieldDefinition = ''

        QDPPacket.__init__(self, byte_array, deviceType)

    def addStartingFlashBlockNumber(self, value):
        self.Fields.append('StartingFlashBlockNumber')
        self.FieldDefinition = '>H'
        self.setStartingFlashBlockNumber(value)
