from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqcom(QDPPacket):
    """
    Represents a request for com values from the Q330
    These values (see c1_scom) represent values associated with IO ports on
    the hardware.
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQCOM)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
