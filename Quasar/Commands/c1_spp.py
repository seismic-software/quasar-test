from Quasar.CmdID import CmdID
from Quasar.Commands.c1_sspp import c1_sspp


class c1_spp(c1_sspp):
    """
    Represents a response from the Q330 for a request for the parameters of the
    secondary processor
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_sspp.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_SPP)
