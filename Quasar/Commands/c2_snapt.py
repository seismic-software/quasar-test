from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_snapt(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_FIX)
        self.Fields = [
            'NumberOfPortEntries',
            'ProtocolBitmap',
            'DPAdditional1Port',
            'DPAdditional2Port',
            'DPAdditional3Port',
            'DPAdditional4Port',
            'Spare'
            ]
        self.FieldDefinition = '>6HL'
        QDPPacket.__init__(self, byte_array, deviceType)

