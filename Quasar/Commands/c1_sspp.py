from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_sspp(QDPPacket):
    """
    Represents a request to set the parameters of the Q330's secondary processor
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SSPP)
        self.Fields = [
            'MaximumMainCurrent',
            'MinimumOffTime',
            'MinimumPSVoltage',
            'MaximumAntennaCurrent',
            'MinimumTemperature',
            'MaximumTemperature',
            'TemperatureHysteresis',
            'VoltageHysteresis',
            'DefaultVCOValue',
            'Spare'
            ]
        self.FieldDefinition = '>HHHHhHHHHH'
        if deviceType == 'Q335':
            self.Fields = [
                'MaximumMainCurrent',
                'MinimumOffTime',
                'MinimumPSVoltage',
                'MaximumAntennaCurrent',
                'MinimumTemperature',
                'MaximumTemperature',
                'TemperatureHysteresis',
                'VoltageHysteresis',
                'Spare1',
                'Spare2',
                ]
        QDPPacket.__init__(self, byte_array, deviceType)
