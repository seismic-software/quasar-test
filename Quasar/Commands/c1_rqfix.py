from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqfix(QDPPacket):
    """
    Represents a request for fixed values from the Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQFIX)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
