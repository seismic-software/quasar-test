
import random
import socket
import struct
import time

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket

try:
    import hashlib
    new_md5 = hashlib.md5
except:
    import md5
    new_md5 = md5.new


class c1_srvrsp(QDPPacket):
    """
    Represents a response from a Q330 to a registration request
    """
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setIsBroadcast(1)

        self.setQDPCommand(CmdID.C1_SRVRSP)
        self.Fields = [
            'SerialNumber',
            'ChallengeValue',
            'ServerIPAddress',
            'ServerUDPPort',
            'RegistrationNumber',
            'ServersRandomValue',
            'MD5Result'
        ]
        self.FieldDefinition = '>QQLHHQ16s'

        QDPPacket.__init__(self, byte_array, deviceType)

    ##
    # This function sets our MD5 result, and returns a tuple
    # of (binary, hexstring)
    ##
    def createDigest(self, authCode):
        m = new_md5()
        random.seed(time.time())
        rnum = random.randint(0, 9999999)
        # rnum = self.getServersRandomValue()
        arr = [
            '%016x' % self.getChallengeValue(),
            '%08x' % self.getServerIPAddress(),
            '%04x' % self.getServerUDPPort(),
            '%04x' % self.getRegistrationNumber(),
            '%016x' % authCode,
            '%016x' % self.getSerialNumber(),
            '%016x' % rnum
        ]
        self.setServersRandomValue(rnum)
        m.update(''.join(arr).encode('utf-8'))
        self.setMD5Result(m.digest())
        return m.digest(), m.hexdigest()

    def strServerIPAddress(self):
        return socket.inet_ntoa(struct.pack('>L', self.getServerIPAddress()))

    def strMD5Result(self):
        md5 = self.createDigest(0)
        return md5[1]
