from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_dep(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_DEP)
        self.Fields = [
            'SubCommand',
            'Spare',
            'RemainderOfParameters'
            ]
        self.FieldDefinition = '>HHL'
        QDPPacket.__init__(self, byte_array, deviceType)
