from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqsc(QDPPacket):
    """
    Represents a request for the sensor status bits
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQSC)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
