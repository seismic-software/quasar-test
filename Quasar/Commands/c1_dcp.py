from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_dcp(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_DCP)
        self.Fields = [
            'Channel1CountsWithGroundedInput',
            'Channel2CountsWithGroundedInput',
            'Channel3CountsWithGroundedInput',
            'Channel4CountsWithGroundedInput',
            'Channel5CountsWithGroundedInput',
            'Channel6CountsWithGroundedInput',
            'Channel1CountsWithReferenceInput',
            'Channel2CountsWithReferenceInput',
            'Channel3CountsWithReferenceInput',
            'Channel4CountsWithReferenceInput',
            'Channel5CountsWithReferenceInput',
            'Channel6CountsWithReferenceInput'
            ]
        self.FieldDefinition = '>LLLLLLLLLLLL'
        QDPPacket.__init__(self, byte_array, deviceType)
