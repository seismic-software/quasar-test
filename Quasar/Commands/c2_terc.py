from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_terc(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        super().__init__(byte_array, deviceType)
        self.setQDPCommand(CmdID.C2_TERC)
        self.Fields = [
            'SubCommand',
            'Spare',
            ]
        self.FieldDefinition = '>HH'
        self.__deviceType = deviceType

    def __finish_init__(self, byte_array):
        QDPPacket.__init__(self, byte_array, self.__deviceType)
