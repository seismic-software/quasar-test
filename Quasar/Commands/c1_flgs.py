from Quasar.CmdID import CmdID
from Quasar.Commands import c1_fix
from Quasar.Commands import c1_glob
from Quasar.Commands import c1_log
from Quasar.Commands import c1_sc
from Quasar.QDPPacket import QDPPacket


class c1_flgs(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_FLGS)
        tmpFix  = c1_fix.c1_fix(deviceType=deviceType)
        tmpGlob = c1_glob.c1_glob(deviceType=deviceType)
        tmpSc   = c1_sc.c1_sc(deviceType=deviceType)
        tmpLog  = c1_log.c1_log(deviceType=deviceType)
        self.Fields = [
            'OffsetToGlobalProgramming',
            'OffsetToSensorControl',
            'OffsetToDataPort',
            'Spare'
            ]
        self.Fields = self.Fields + tmpFix.getFields() +\
                                    tmpGlob.getFields() +\
                                    tmpSc.getFields() +\
                                    tmpLog.getFields()

        self.FieldDefinition = '>HHHH' + tmpFix.getFieldDefinition() +\
                                         tmpGlob.getFieldDefinition() +\
                                         tmpSc.getFieldDefinition() +\
                                         tmpLog.getFieldDefinition()

        QDPPacket.__init__(self, byte_array, deviceType)
