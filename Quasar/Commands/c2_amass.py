from Quasar.CmdID import CmdID
from Quasar.Commands.c2_samass import c2_samass


class c2_amass(c2_samass):
    """
    Represents the response to a request for automatic mass centering (rqamass)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_samass.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C2_AMASS)
        
