from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c1_thn(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_THN)
        self.Fields = []
        self.FieldDefinition = ''
        self._numThreads = 0
        threadStruct = '>12p'
        if byte_array:
            numThreads = (len(byte_array)-12) / structWrapper.calcsize(threadStruct)
            self._numThreads = numThreads
            for i in range(1, numThreads + 1):
                self.Fields.append('Thread_%d' % i)
                self.FieldDefinition = self.FieldDefinition + threadStruct[1:]
        QDPPacket.__init__(self, byte_array, deviceType)

    def getNumThreads(self):
        return self._numThreads
