
import socket
import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitIndexToInteger


class c1_sphy(QDPPacket):
    """
    Represents a request for the Q330 to set physical interface values
    (i.e., MAC addr., IP)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SPHY)
        self.Fields = [
            'SerialNumber',
            'SerialInterface1IPAddress',
            'SerialInterface2IPAddress',
            'SerialInterface3IPAddress',
            'EthernetIPAddress',
            'EthernetMACAddressHigh32',
            'EthernetMACAddressLow16',
            'BasePort',
            'SerialInterface1Baud',
            'SerialInterface1Flags',
            'SerialInterface2Baud',
            'SerialInterface2Flags',
            'SerialInterface3Baud',
            'SerialInterface3Flags',
            'Reserved',
            'EthernetFlags',
            'SerialInterface1Throttle',
            'SerialInterface2Throttle',
            'SerialInterface3Throttle',
            'Reserved'
        ]
        self.FieldDefinition = '>QLLLLL14H'

        QDPPacket.__init__(self, byte_array, deviceType)

    def strEthernetMACAddressHigh32(self):
        s = hex(self.getEthernetMACAddressHigh32() * 1)[2:-1]
        # temp changed by Kanglin right here
        if len(s) < 8:  # if less 8, add some '0's, e.g. 12 -> 00000012
            s = "%08x" % (eval('0x%s' % s))  # need 8 chars(s[0],s[1],...,s[7])
        # end change
        return '%c%c:%c%c:%c%c:%c%c' % (s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7])

    def strEthernetMACAddressLow16(self):
        s = hex(self.getEthernetMACAddressLow16() * 1)[2:-1]
        # temp changed by Kanglin right here
        if len(s) < 4:  # if less 4, add some '0' or '0's, e.g. 12->0012
            s = "%04x" % (eval('0x%s' % s))  # need 4 chars(s[0],s[1],s[2],s[3])
        # end change
        return '%c%c:%c%c' % (s[0], s[1], s[2], s[3])

    def strSerialInterface1IPAddress(self):
        return socket.inet_ntoa(struct.pack('>L', self.getSerialInterface1IPAddress()))

    def strSerialInterface2IPAddress(self):
        return socket.inet_ntoa(struct.pack('>L', self.getSerialInterface2IPAddress()))

    def strSerialInterface3IPAddress(self):
        return socket.inet_ntoa(struct.pack('>L', self.getSerialInterface3IPAddress()))

    def strEthernetIPAddress(self):
        return socket.inet_ntoa(struct.pack('>L', self.getEthernetIPAddress()))

    def strSerialInterface1Baud(self):
        return self.getBaudString(self.getSerialInterface1Baud())

    def strSerialInterface2Baud(self):
        return self.getBaudString(self.getSerialInterface2Baud())

    def strSerialInterface3Baud(self):
        return self.getBaudString(self.getSerialInterface3Baud())

    def strSerialInterface1Flags(self):
        return self.getSerialFlagsString(self.getSerialInterface1Flags())

    def strSerialInterface2Flags(self):
        return self.getSerialFlagsString(self.getSerialInterface2Flags())

    def strSerialInterface3Flags(self):
        return self.getSerialFlagsString(self.getSerialInterface3Flags())

    def strEthernetFlags(self):
        flags = self.getEthernetFlags()
        ret = ['']
        if flags & bitIndexToInteger(0):
            ret.append('Enabled')
        else:
            ret.append('Disabled')

        if flags & bitIndexToInteger(1):
            ret.append('Always On')
        else:
            ret.append('Link Detect Mode')

        if flags & bitIndexToInteger(2):
            ret.append('Full-Duplex')
        else:
            ret.append('Half-Duplex')

        if flags & bitIndexToInteger(3):
            ret.append('Reserved bit')

        if flags & bitIndexToInteger(4):
            ret.append('Block non registered pings')

        if flags & bitIndexToInteger(5):
            ret.append('Block non registered TCP')

        if flags & bitIndexToInteger(6):
            ret.append('Routing enabled')
        else:
            ret.append('Routing disabled')

        if flags & bitIndexToInteger(7):
            ret.append('Block non registered QDP pings')

        if flags & bitIndexToInteger(10):
            ret.append('Unlocked')

        return '\n      '.join(ret)

    def getBaudString(self, baudID):
        baudStrings = [
            'Unknown',
            '1200',
            '2400',
            '4800',
            '9600',
            '19200',
            '38400',
            '57600',
            '115200',
            '230400'
        ]
        if baudID > len(baudStrings):
            return 'Unknown'
        return baudStrings[baudID]

    def getSerialFlagsString(self, flags):
        ret = ['']
        if flags & bitIndexToInteger(0):
            ret.append('Enabled')
        else:
            ret.append('Disabled')

        if flags & bitIndexToInteger(1):
            ret.append('Reserved bit')

        if flags & bitIndexToInteger(2):
            ret.append('RTS/CTS')

        if flags & bitIndexToInteger(3):
            ret.append('Break detect')

        if flags & bitIndexToInteger(4):
            ret.append('Block non registered pings')

        if flags & bitIndexToInteger(5):
            ret.append('Block non registered TCP')

        if flags & bitIndexToInteger(6):
            ret.append('Routing enabled')
        else:
            ret.append('Routing disabled')

        if flags & bitIndexToInteger(7):
            ret.append('Block non registered QDP pings')

        bit8 = flags & bitIndexToInteger(8)
        bit9 = flags & bitIndexToInteger(9)

        if not bit8 and not bit9:
            ret.append('Always use programmed IP address')
        if not bit9 and bit8 == 1:
            ret.append('Use programmed IP address if BOOTP not successful')
        if bit9 == 1 and not bit8:
            ret.append('Use BOOTP if programmed IP address is zero')

        return '\n      '.join(ret)
