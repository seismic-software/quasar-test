from Quasar.CmdID import CmdID
from Quasar.Commands.c2_terc import c2_terc


class c3_sannc(c2_terc):
    """
    Represents a request to the Q330 to set parameters for
    announce structure programming
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_terc.__init__(self, byte_array, deviceType)
        self.Fields.extend([
            'NumberOfActiveEntries',
            'UnlockFlags'
            ])
        for i in range(1,7):
            self.Fields.extend([
                "DPIPAddress%d" % i,
                "RouterIPAddress%d" % i,
                "TimeoutInMinutes%d" % i,
                "ResumeTimeInMinutes%d" % i,
                "Flags%d" % i,
                "DPUPDPort%d" % i,
                ])
        self.FieldDefinition += 'HHLLHHHHLLHHHHLLHHHHLLHHHHLLHHHHLLHHHH'
        c2_terc.__finish_init__(self, byte_array)
        self.setSubCommand(CmdID.C3_SANNC)
        self.setSpare(0)
