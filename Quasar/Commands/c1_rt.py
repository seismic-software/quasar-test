from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper


class c1_rt(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RT)
        self.Fields = []
        self.FieldDefinition = '>'
        self._numEntries = 0
        rtStruct = '>LHHL'
        if byte_array:
            numEntries = (len(byte_array) - 12) / structWrapper.calcsize(rtStruct)
            self._numEntries = numEntries
            for i in range(1, numEntries+1):
                self.Fields.append("IPAddress_%d" % i)
                self.Fields.append("PhysicalInterface_%d" % i)
                self.Fields.append("DataPort_%d" % i)
                self.Fields.append("SecondsSinceHeard_%d" % i)
                
                self.FieldDefinition = self.FieldDefinition + rtStruct[1:]
        QDPPacket.__init__(self, byte_array, deviceType)

    def getNumEntries(self):
        return self._numEntries

