from Quasar.CmdID import CmdID
from Quasar.Commands.c1_ssc import c1_ssc


class c1_sc(c1_ssc):
    """
    Represents the response from a Q330 sensor status bits request
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_ssc.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_SC)
