from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqlog(QDPPacket):
    """
    Represents a request for the Q330 for data port contents (logging)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQLOG)
        self.Fields = [
            'DataPortNumber'
            ]
        self.FieldDefinition = '>H'
        QDPPacket.__init__(self, byte_array, deviceType)

    def strDataPortNumber(self):
        portMap = {
            -1 : 'All Ports',
            0  : 'Data Port 1',
            1  : 'Data Port 2',
            2  : 'Data Port 3',
            3  : 'Data Port 4'
            }
        return portMap[self.getDataPortNumber()]
