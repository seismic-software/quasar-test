import struct

from Quasar.CmdID import CmdID
from Quasar.Commands.c3_annc import c3_annc
from Quasar.Commands.c3_bcfg import c3_bcfg
from Quasar.QDPPacket import QDPPacket, qdpPacketHeader


class c2_terr(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_TERR)
        self.Fields = [
            'SubCommand',
            'Spare',
            ]
        self.FieldDefinition = '>HH'

        #print "C2_TERR Packet Bytes:",
        #for b in map(int, struct.unpack(">%dB" % len(byte_array), byte_array)):
        #    print hex(b),
        #print

        headerslen = struct.calcsize(qdpPacketHeader+'HH')
        headers = struct.unpack(qdpPacketHeader+'HH',
                                byte_array[0:headerslen])
        rsp_type = headers[-1]
        rq_type  = headers[-2]
        self._real_command = None
        # here we will look at the underlying values as the comparison will
        rq_cmd = CmdID(rq_type)
        rsp_cmd = CmdID(rsp_type)
        if rq_cmd == CmdID.C3_RQANNC and rsp_cmd == CmdID.C3_ANNC:
            sub_command = c3_annc(byte_array)
        elif rq_cmd == CmdID.C3_RQBCFG and rsp_cmd == CmdID.C3_BCFG:
            sub_command = c3_bcfg(byte_array)
        elif rq_cmd == CmdID.C3_SANNC and rsp_cmd == CmdID.C3_ANNC:
            sub_command = c3_annc(byte_array)
        elif rq_cmd == CmdID.C3_SBCFG and rsp_cmd == CmdID.C3_BCFG:
            sub_command = c3_bcfg(byte_array)
        else:
            raise ValueError("Unrecognized Tertiary Command Response (%d, %d)" % (rq_type, rsp_type))
        self.Fields.extend(sub_command.Fields)
        self.FieldDefinition += sub_command.FieldDefinition

        #print "Fields:", self.Fields, "[%d]" % len(self.Fields)
        #print "FieldDefinition:", self.FieldDefinition, "[%d]" % struct.calcsize(self.FieldDefinition)

        QDPPacket.__init__(self, byte_array, deviceType)
