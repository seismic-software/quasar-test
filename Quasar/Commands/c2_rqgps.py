from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqgps(QDPPacket):
    """
    Represents a request for GPS programming information from the Q330
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQGPS)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
