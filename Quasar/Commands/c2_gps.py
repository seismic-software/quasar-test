from Quasar.CmdID import CmdID
from Quasar.Commands.c2_sgps import c2_sgps


class c2_gps(c2_sgps):
    """
    Represents a response from the Q330 for a request for GPS programming info.
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c2_sgps.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C2_GPS)
