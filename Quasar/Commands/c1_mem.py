from Quasar.CmdID import CmdID
from Quasar.Commands.c1_smem import c1_smem



class c1_mem(c1_smem):
    """
    Represents a response from the Q330 on a request for data over memory range
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_smem.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_MEM)
        
