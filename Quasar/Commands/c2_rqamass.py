from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_rqamass(QDPPacket):
    """
    Represents a request for automatic mass centering information
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_RQAMASS)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
                           
