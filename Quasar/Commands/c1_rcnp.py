from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rcnp(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RCNP)
        self.Fields = [
            'CNPReplyMessageOrErrorCode',
            'CNPReplyMessageContinued'
            ]
        self.FieldDefinition = '>LL'
        QDPPacket.__init__(self, byte_array, deviceType)
