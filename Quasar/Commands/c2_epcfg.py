import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c2_epcfg(QDPPacket):
    """
    Represents the response from the Q330 for an environmental processor info
    request
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_EPCFG)
        self.Fields = [
            'ReservedSerial1EP',
            'ReservedSerial2EP',
            'FlagsSerial1EP',
            'FlagsSerial2EP',
            'ChannelCount',
            'Spare',
            ]
        self.FieldDefinition = '>LLLLHH'
        epdChanStruct = 'HH'

        channel_count = struct.unpack('>H', byte_array[28:30])[0]
        for i in range (1, channel_count+1):
            self.Fields.append("Channel_%d" % i)
            self.Fields.append("OutputMask_%d" % i)
            self.FieldDefinition += epdChanStruct

        QDPPacket.__init__(self, byte_array, deviceType)
