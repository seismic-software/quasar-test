from Quasar.CmdID import CmdID
from Quasar.Commands.c1_sman import c1_sman


class c1_man(c1_sman):
    def __init__(self, byte_array=None, deviceType='Q330'):
        c1_sman.__init__(self, byte_array, deviceType)
        self.setQDPCommand(CmdID.C1_MAN)

    def getPassword(self):
        return (self.getPasswordHigh32(),
                self.getPasswordMidHigh32(),
                self.getPasswordMidLow32(),
                self.getPasswordLow32())
