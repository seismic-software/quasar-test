
from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket
from isti.utils.Bits import bitIndexToInteger


class c1_sglob(QDPPacket):
    """
    Represents a request to set the Q330's global programming (see fields)
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_SGLOB)
        self.Fields = [
            'ClockTimeout',
            'InitialVCO',
            'GPSBackupPower',
            'AuxAndStatusSamplingRates',
            'GainBitmap',
            'FilterBitmap',
            'InputBitmap',
            'WebPort',
            'ServerTimeout',
            'DriftTolerance',
            'JumpFilter',
            'JumpThresholdInUSec',
            'CalibratorOffset',
            'SensorControlBitmap',
            'SamplingPhase',
            'GPSColdStartSeconds',
            'UserTag',
            'Channel1FreqBit0Scale',
            'Channel1FreqBit1Scale',
            'Channel1FreqBit2Scale',
            'Channel1FreqBit3Scale',
            'Channel1FreqBit4Scale',
            'Channel1FreqBit5Scale',
            'Channel1FreqBit6Scale',
            'Channel1FreqBit7Scale',
            'Channel2FreqBit0Scale',
            'Channel2FreqBit1Scale',
            'Channel2FreqBit2Scale',
            'Channel2FreqBit3Scale',
            'Channel2FreqBit4Scale',
            'Channel2FreqBit5Scale',
            'Channel2FreqBit6Scale',
            'Channel2FreqBit7Scale',
            'Channel3FreqBit0Scale',
            'Channel3FreqBit1Scale',
            'Channel3FreqBit2Scale',
            'Channel3FreqBit3Scale',
            'Channel3FreqBit4Scale',
            'Channel3FreqBit5Scale',
            'Channel3FreqBit6Scale',
            'Channel3FreqBit7Scale',
            'Channel4FreqBit0Scale',
            'Channel4FreqBit1Scale',
            'Channel4FreqBit2Scale',
            'Channel4FreqBit3Scale',
            'Channel4FreqBit4Scale',
            'Channel4FreqBit5Scale',
            'Channel4FreqBit6Scale',
            'Channel4FreqBit7Scale',
            'Channel5FreqBit0Scale',
            'Channel5FreqBit1Scale',
            'Channel5FreqBit2Scale',
            'Channel5FreqBit3Scale',
            'Channel5FreqBit4Scale',
            'Channel5FreqBit5Scale',
            'Channel5FreqBit6Scale',
            'Channel5FreqBit7Scale',
            'Channel6FreqBit0Scale',
            'Channel6FreqBit1Scale',
            'Channel6FreqBit2Scale',
            'Channel6FreqBit3Scale',
            'Channel6FreqBit4Scale',
            'Channel6FreqBit5Scale',
            'Channel6FreqBit6Scale',
            'Channel6FreqBit7Scale',
            'Channel1Offset',
            'Channel2Offset',
            'Channel3Offset',
            'Channel4Offset',
            'Channel5Offset',
            'Channel6Offset',
            'Channel1Gain',
            'Channel2Gain',
            'Channel3Gain',
            'Channel4Gain',
            'Channel5Gain',
            'Channel6Gain',
            'MessageBitmap'
        ]
        self.FieldDefinition = '>16HL60HL'
        if deviceType == 'Q335':
            self.Fields = [
                'ClockTimeout',
                'InitialVCO',
                'GPSBackupPower',
                'MiscFlags',
                'GainBitmap',
                'FilterBitmap',
                'PGAOrMaximumFrequencySelect',
                'WebPort',
                'ClientTimeout',
                'DriftTolerance',
                'JumpFilter',
                'JumpThresholdInUSec',
                'Spare1',
                'SensorControlBitmap',
                'SamplingPhase',
                'GPSColdStartSeconds',
                'UserTag',
                'Channel1FreqBit0Scale',
                'Channel1FreqBit1Scale',
                'Channel1FreqBit2Scale',
                'Channel1FreqBit3Scale',
                'Channel1FreqBit4Scale',
                'Channel1FreqBit5Scale',
                'Channel1FreqBit6Scale',
                'Channel1FreqBit7Scale',
                'Channel2FreqBit0Scale',
                'Channel2FreqBit1Scale',
                'Channel2FreqBit2Scale',
                'Channel2FreqBit3Scale',
                'Channel2FreqBit4Scale',
                'Channel2FreqBit5Scale',
                'Channel2FreqBit6Scale',
                'Channel2FreqBit7Scale',
                'Channel3FreqBit0Scale',
                'Channel3FreqBit1Scale',
                'Channel3FreqBit2Scale',
                'Channel3FreqBit3Scale',
                'Channel3FreqBit4Scale',
                'Channel3FreqBit5Scale',
                'Channel3FreqBit6Scale',
                'Channel3FreqBit7Scale',
                'Channel4FreqBit0Scale',
                'Channel4FreqBit1Scale',
                'Channel4FreqBit2Scale',
                'Channel4FreqBit3Scale',
                'Channel4FreqBit4Scale',
                'Channel4FreqBit5Scale',
                'Channel4FreqBit6Scale',
                'Channel4FreqBit7Scale',
                'Channel5FreqBit0Scale',
                'Channel5FreqBit1Scale',
                'Channel5FreqBit2Scale',
                'Channel5FreqBit3Scale',
                'Channel5FreqBit4Scale',
                'Channel5FreqBit5Scale',
                'Channel5FreqBit6Scale',
                'Channel5FreqBit7Scale',
                'Channel6FreqBit0Scale',
                'Channel6FreqBit1Scale',
                'Channel6FreqBit2Scale',
                'Channel6FreqBit3Scale',
                'Channel6FreqBit4Scale',
                'Channel6FreqBit5Scale',
                'Channel6FreqBit6Scale',
                'Channel6FreqBit7Scale',
                'Channel1Offset',
                'Channel2Offset',
                'Channel3Offset',
                'Channel4Offset',
                'Channel5Offset',
                'Channel6Offset',
                'Channel1Gain',
                'Channel2Gain',
                'Channel3Gain',
                'Channel4Gain',
                'Channel5Gain',
                'Channel6Gain',
                'SensorAConfiguration',
                'SensorBConfiguration'
            ]
            self.FieldDefinition = '>16HL62H'

        QDPPacket.__init__(self, byte_array, deviceType)

    def strAuxAndStatusSamplingRates(self):
        val = self.getAuxAndStatusSamplingRates()
        ret = ['']

        bit0 = val & bitIndexToInteger(0)
        bit1 = val & bitIndexToInteger(1)

        bit8 = val & bitIndexToInteger(8)
        bit9 = val & bitIndexToInteger(9)

        if bit0 == 0 and bit1 == 0:
            ret.append('Aux: Not reported')
        if bit1 == 0 and bit0 != 0:
            ret.append('Aux: 1Hz')
        if bit1 == 1 and bit0 == 0:
            ret.append('Aux: 0.1Hz')

        if bit9 == 0 and bit8 == 0:
            ret.append('Status: Not reported')
        if bit9 == 0 and bit8 != 0:
            ret.append('Status: 1Hz')
        if bit9 != 0 and bit8 == 0:
            ret.append('Status: 0.1Hz')

        return '\n     '.join(ret)

    def strMiscFlags(self):
        val = self.getMiscFlags()
        ret = ['']

        string = "Environmental Processor "
        if val & bitIndexToInteger(10):
            string += "Enabled"
        else:
            string += "Disabled"
        ret.append(string)

        string = "Fast Gain Range Test Mode in FEs is "
        if val & bitIndexToInteger(11):
            string += "Enabled"
        else:
            string += "Disabled"
        ret.append(string)

        return '\n     '.join(ret)
