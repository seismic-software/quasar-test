import socket

from Quasar import Status
from Quasar.CmdID import CmdID
from Quasar.Commands import c1_umsg
from Quasar.QDPPacket import QDPPacket
from isti.utils import structWrapper
from isti.utils.Bits import bitIndexToInteger, bitsToStrings


class c1_stat(QDPPacket):
    """
    Represents the response from a Q330 for a status request
    """

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_STAT)
        self.Fields = ['StatusBitmap']
        self.FieldDefinition = Status.structBitmap

        if byte_array is None:
            QDPPacket.__init__(self, None, deviceType)
            return

        # ==== TEST ====
        #indices = len(byte_array) / 4
        #for index in range(0, indices, 4):
        #    end = index + 4
        #    bitmap = structWrapper.unpack(Status.structBitmap, byte_array[index:end])[0]
        #    print "byte_array[%d]: %d" % (index, bitmap)
        # ==== TEST ====

        bitmap = structWrapper.unpack(Status.structBitmap, byte_array[12:16])[0]

        if bitmap & (1 << 31):
            tmpUmsg = c1_umsg.c1_umsg(deviceType)
            print("Got a user message!")
            self.Fields = self.Fields + Status.UserMessageFields[deviceType]
            self.FieldDefinition = self.FieldDefinition + tmpUmsg.FieldDefinition

        if bitmap & (1 << 30):
            print("Data port programming has changed!")

        if bitmap & (1 << 29):
            print("DP tokens have changed!")

        if (deviceType == 'Q330') and (bitmap & (1 << 28)):
            print("EP filter delays have changed!")
        
        statusFields = Status.StatusFields[deviceType]
        statusDefinitions = Status.StatusDefinitions[deviceType]
        statusKeys = Status.StatusKeys[deviceType]
        statusBits = Status.StatusBits[deviceType]
        for bit in list(statusKeys.keys()):
            mask = 1 << bit
            if bitmap & mask:
                key = statusKeys[bit]
                self.Fields = self.Fields + [key + "_" + k for k in statusFields[key][deviceType]]
                self.FieldDefinition = self.FieldDefinition + statusDefinitions[key]

                if bit == 4:
                    # Threads
                    # first, we need to figure out how many threads
                    pos = structWrapper.calcsize(self.FieldDefinition) + 12
                    numThreadsStr = byte_array[pos-12:pos-10]
                    numThreads = structWrapper.unpack(">H", numThreadsStr)[0]
                    for i in range(0, numThreads):
                        for field in Status.ThreadEntryFields[deviceType]:
                            self.Fields.append('%s_%d' %(field, i))
                        self.FieldDefinition = self.FieldDefinition + Status.structThreadEntry
                elif bit == 6:
                    # Satellites
                    pos = structWrapper.calcsize(self.FieldDefinition) + 12
                    numSatsStr = byte_array[pos-4:pos-2]
                    numSats = structWrapper.unpack(">H", numSatsStr)[0]
                    for i in range(0, numSats):
                        for field in Status.SatelliteEntryFields[deviceType]:
                            self.Fields.append("%s_%d" %(field, i))
                        self.FieldDefinition = self.FieldDefinition + Status.structSatelliteEntry
                elif bit == 7:
                    # ARP entries
                    pos = structWrapper.calcsize(self.FieldDefinition) + 12
                    numARPStr = byte_array[pos-4:pos-2]
                    numARP = structWrapper.unpack(">H", numARPStr)[0]
                    for i in range(0, numARP):
                        for field in Status.ARPEntryFields[deviceType]:
                            self.Fields.append("%s_%d" %(field, i))
                        self.FieldDefinition = self.FieldDefinition + Status.structARPEntry
                elif bit == 18:
                    # Auxiliary Board
                    pos = structWrapper.calcsize(self.FieldDefinition) + 12
                    numAux = int(structWrapper.unpack(">H", byte_array[pos-8:pos-6])[0] / 4) - 2
                    for i in range(0, numAux):
                        for field in Status.AuxBoardEntryFields[deviceType]:
                            self.Fields.append("%s_%d" %(field, i))
                        self.FieldDefinition = self.FieldDefinition + Status.structAuxBoardEntry
                elif bit == 19:
                    # Serial Sensor
                    pos = structWrapper.calcsize(self.FieldDefinition) + 12
                    numSubBlocks = structWrapper.unpack(">H", byte_array[pos-2:pos])[0]
                    totalDataSize = structWrapper.unpack(">H", byte_array[pos-4:pos-2])[0]

                    startPos = pos
                    for i in range(0, numSubBlocks):
                        subBlockSize = structWrapper.unpack(">H",byte_array[startPos:startPos+2])[0]
                        subRead = 0
                        for (field_size, field_code, field) in Status.SerialSensorEntryFields[deviceType]:
                            if subBlockSize - subRead < field_size:
                                break
                            self.Fields.append("%s_%d" %(field, i))
                            self.FieldDefinition += field_code
                            subRead += field_size
                        startPos += subBlockSize

                elif bit == 20:
                    # Environmental Processor
                    for i in range(0, 4):
                        for field in Status.EPSDIDeviceEntry[deviceType]:
                            self.Fields.append("%s_%d" % (field, i))
                        self.FieldDefinition += Status.structEnvironmentalProcessorSDI
                    self.Fields.extend(Status.EPADCDeviceEntry[deviceType])
                    self.FieldDefinition += Status.structEnvironmentalProcessorADC

                    # The following is incase there are multiple 
                    #self.Fields = []
                    #for j in range(0, 2):
                    #    prefix = "EP%d" % j
                    #    for field in Status.EnvironmentalProcessorFields[deviceType]:
                    #        self.Fields.append("%s_%s" % (prefix, field))
                    #    self.FieldDefinition += Status.structEnvironmentalProcessor
                    #    for i in range(0, 4):
                    #        for field in Status.EPSDIDeviceEntry:
                    #            self.Fields.append("%s_%s_%d" % (prefix,field, i))
                    #        self.FieldDefinition += Status.structEnvironmentalProcessorSDI
                    #    for field in Status.EPADCDeviceEntry:
                    #        self.Fields.append("%s_%s" % (prefix, field))
                    #    self.FieldDefinition += Status.structEnvironmentalProcessorADC

                elif bit == 21:
                    # Front-End entries
                    pos = structWrapper.calcsize(self.FieldDefinition) + 12
                    numFEStr = byte_array[pos-4:pos-2]
                    numFE = structWrapper.unpack(">H", numFEStr)[0]
                    for i in range(0, numFE):
                        for field in Status.FrontEndEntryFields[deviceType]:
                            self.Fields.append("%s_%d" %(field, i))
                        self.FieldDefinition = self.FieldDefinition + Status.structFrontEndEntry

        QDPPacket.__init__(self, byte_array, deviceType)

    def strFaultCode(self):
        code = self.getFaultCode()
        main = code >> 4
        sub  = code & 0x0f
        if main == 3:
            if   sub == 0: return "Error storing incoming data in packet memory"
            elif sub == 1: return "Checkum erro when retrieving data from packet memory for BE"
        elif main == 6:
            if   sub == 0: return "Data packet from FE is too large"
            elif sub == 1: return "Data packet being retrieved from packet memory for BE is too large"
        return "Un-Recognized Fault Code"

    def strIPAddress(self, index):
        value = self.__dict__['IPAddress_%d' %index]

        return socket.inet_ntoa(structWrapper.pack(">L", value))
    
    def strStatusBitmap(self):
        return Status.bitmapToString(self.getStatusBitmap())

    def strClockQuality(self):
        v = self.getClockQuality()
        #print v
        ret = ['']
        if not (v & bitIndexToInteger(0)):
            ret.append('Has never had lock')
        else:
            ret.append('Has had lock')
            if v & bitIndexToInteger(1):
                ret.append('Has 2D lock')
            if v & bitIndexToInteger(2):
                ret.append('Has 3D lock')
            if v & bitIndexToInteger(3):
                ret.append('Has 1D lock')
            if v & bitIndexToInteger(4):
                ret.append('Timemarks are currently frozen due to filtering')
            if v & bitIndexToInteger(5):
                ret.append('Speculative lock based on internal clock')

            bit6 = v & bitIndexToInteger(6)
            bit7 = v & bitIndexToInteger(7)

            if bit7 == 0 and bit6 == 0:
                ret.append('PLL not enabled')
            elif bit7 == 0 and bit6 == 1:
                ret.append('PLL hold')
            elif bit7 == 1 and bit6 == 0:
                ret.append('PLL tracking')
            else:
                ret.append('PLL locked')
            
        return '\n      '.join(ret)

    def strCalibratorStatus(self):
        values = [
            'Calibration enabled',
            'Calibration on',
            'Calibrator should be sending signal, but isn\'t'
            ]
        return bitsToStrings(self.getCalibratorStatus(), values)

    def strMiscInputs(self):
        values = [
            'AC OK: ON', 
            'Input Spare 1',
            'Input Spare 2',
            'Analog Fault',
            'Main Power',
            None,
            'Analog Power',
            'Trigger: on', 
            ]
        if self._deviceType == 'Q335':
            values = [
                'AC OK: ON', 
                'Input Spare 1',
                'Input Spare 2',
                None,
                None,
                None,
                None,
                'Trigger: ON', 
                'Magnetic Switch: ON', 
                ]
        return bitsToStrings(self.getMiscInputs(), values)

    def strChargingPhase(self):
        values = [
            'Not Charging',
            'Bulk',
            'Absorption',
            'Float'
            ]
        return values[self.getChargingPhase()]

    def strBatteryTemperature(self):
        return '%dC' %self.getBatteryTemperature()

    def strBatteryVoltage(self):
        return '%dmv' %self.getBatteryVoltage()

    def strInputVoltage(self):
        return '%dmv' %self.getInputVoltage()

    def strSeismometer1Temperature(self):
        v = self.getSeismometer1Temperature()
        if v == 666:
            return 'Not Available'
        else:
            return '%dC' %v

    def strSeismometer2Temperature(self):
        v = self.getSeismometer2Temperature()
        if v == 666:
            return 'Not Available'
        else:
            return '%dC' %v

    def strSensorATemperature(self):
        v = self.getSeismometer1Temperature()
        if v == 666:
            return 'Not Available'
        else:
            return '%dC' %v

    def strSensorBTemperature(self):
        v = self.getSeismometer2Temperature()
        if v == 666:
            return 'Not Available'
        else:
            return '%dC' %v

    def strSensorACurrent(self):
        return "%d ma" % (self.getSensorACurrent() * 5,)

    def strSensorBCurrent(self):
        return "%d ma" % (self.getSensorBCurrent() * 5,)

    def strState(self):
        st = self.getState()
        if st == 0x00:
            return 'PLL not enabled'
        if st == 0x40:
            return 'Hold'
        if st == 0x80:
            return 'Tracking'
        if st == 0xC0:
            return 'Locked'

    def strDataPortFlags(self):
        flags = self.getDataPortFlags()
        ret = ['']
        if flags & bitIndexToInteger(0):
            ret.append('Baler should disconnect and prepare to reconnect to a data vaccuum')
        if flags & bitIndexToInteger(1):
            if self._deviceType == 'Q335':
                ret.append('DP should stay powered on')
            else:
                ret.append('Baler should stay powered on')
        if flags & bitIndexToInteger(2):
            if self._deviceType == 'Q335':
                ret.append('DP should shut down immediately')
            else:
                ret.append('Baler should shut down immediately')
        if flags & bitIndexToInteger(15):
            ret.append('Packet memory reduced due to bad packet memory RAM')
        return '\n      '.join(ret)

    def strCurrentLinkStatus(self):
        values = [
            None,
            None,
            None,
            None,
            'Polarity OK',
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            'Link OK'
            ]
        return bitsToStrings(self.getCurrentLinkStatus(), values)

    def strFrontEndFlags(self):
        flags = self.getFrontEndFlags()
        ret = ['']
        if flags & bitIndexToInteger():
            ret.append('Sensor temperature and serial number are valid')
        return '\n      '.join(ret)

    def strGainAndCalibrationStatus(self):
        # for some reason this call was assigned to 'status' and not flags,
        # so 'flags' was not ever actually referenced
        flags = self.getGainAndCalibrationStatus()
        ret = ['']
        if flags & bitIndexToInteger(0):
            ret.append('Calibration is enabled')
        else:
            ret.append('Calibration is disabled')
        if flags & bitIndexToInteger(1):
            ret.append('Calibration signal is on')
        else:
            ret.append('Calibration signal is off')
        if flags & bitIndexToInteger(2):
            ret.append('Calibrator signal generation failure')
        for i in range(0, 3):
            value = (flags >> (i * 2)) & 0x03
            if value == 0:
                ret.append('Configured for High Voltage input')
            elif value == 1:
                ret.append('Configured for Low Voltage input, testing if high possible')
            elif value == 2:
                ret.append('Configured for Low Voltage input')
            elif value == 3:
                ret.append('Configured for Low Voltage input, but set high due to range')

        return '\n      '.join(ret)

