import struct

from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket, qdpPacketHeader
from isti.utils import structWrapper


class c2_epd(QDPPacket):
    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C2_EPD)
        self.Fields = [
            'ReservedSerial1EP',
            'ReservedSerial2EP',
            'ChannelCount',
            'Spare',
            ]
        self.FieldDefinition = '>LLHH'
        epdChanStruct = 'HH'

        header_size = structWrapper.calcsize(qdpPacketHeader)
        data = byte_array[header_size:]
        channel_count = struct.unpack('>H', data[20:22])[0]
        if channel_count > 0:
            for i in range (0, channel_count):
                self.Fields.append("Channel_%d" % i)
                self.Fields.append("FilterDelayHighByte_%d" % i)
                self.Fields.append("FilterDelayMidByte_%d" % i)
                self.Fields.append("FilterDelayLowByte_%d" % i)
                self.FieldDefinition += epdChanStruct

        QDPPacket.__init__(self, byte_array, deviceType)
