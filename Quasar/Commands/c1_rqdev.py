from Quasar.CmdID import CmdID
from Quasar.QDPPacket import QDPPacket


class c1_rqdev(QDPPacket):

    def __init__(self, byte_array=None, deviceType='Q330'):
        self.setQDPCommand(CmdID.C1_RQDEV)
        self.Fields = []
        self.FieldDefinition = ''
        QDPPacket.__init__(self, byte_array, deviceType)
