# This class represents a list of token chunks, from a single DP

import struct

from Quasar.Tokens.Token import TOKEN_ID_TO_NAME, getTokenIDAndLength, \
    TOKEN_ID_TO_CLASS
from Quasar.Commands.c1_mem import c1_mem
from Quasar.Tokens.TokenChunk import TokenChunk
from isti.utils.GetSet import GetSet


#
# Give a token name based on a token ID
#
def getTokenName(tokenID):
    return TOKEN_ID_TO_NAME[tokenID]


class TokenException(Exception):
    """
    Exception for token errors
    """
    pass


class TokenSet(GetSet):

    def __init__(self, chunks=None):
        if chunks is not None:
            self.setChunkList(chunks)
            self.setMemoryType(chunks[0].getMemMemoryType())
            self.parseTokens()

    def __repr__(self):
        ret = []
        self.parseTokens()
        for token in self.getTokens():
            ret.append('%s\n' % token)
        return '\n'.join(ret)

    def parseTokens(self):
        tokenList = []
        byte_array = b''
        for chunk in self.getChunkList():
            byte_array += chunk.getDataBytes()

        # now lets make our tokens
        nextTokenStart = 0
        while nextTokenStart < len(byte_array):
            tokenID, tokenLength = getTokenIDAndLength(
                byte_array[nextTokenStart:nextTokenStart + 3])
            tokenList.append(
                TOKEN_ID_TO_CLASS[tokenID](
                    byte_array[nextTokenStart:nextTokenStart + tokenLength + 1]))
            nextTokenStart += tokenLength + 1

        self.setTokens(tokenList)

    def rebuildChunks(self):
        # here we need to re-create the chunks
        if not self.getTokens():
            return ''

        tokenBytes = ''
        for token in self.getTokens():
            tokenBytes += token.getTokenBytes()

        chunkDataSize = 438
        startingPoint = 0
        # we have all the bytes, lets break them up into
        # chunks
        chunkBytes = []
        while startingPoint < len(tokenBytes):
            chunkBytes.append(
                tokenBytes[startingPoint:startingPoint + chunkDataSize])
            startingPoint += chunkDataSize

        chunks = []
        curAddress = 0
        currentSegmentNumber = 1
        for chunkData in chunkBytes:
            # we need to create the segment header
            segHeader = struct.pack(
                ">HH", currentSegmentNumber, len(chunkBytes))
            mem = c1_mem()
            # IDE claims that chunkData here is a str
            mem.setMemoryContents(segHeader + chunkData)
            mem.setStartingAddress(448 * (currentSegmentNumber - 1))
            mem.setByteCount(len(chunkData) + 4)
            mem.setMemoryType(self.getMemoryType())
            currentSegmentNumber += 1

            chunks.append(TokenChunk(mem))

        # set up our new chunk list

        # make a backup of the old one, in case the new doesn't validate
        oldChunks = self.getChunkList()

        # set the new one
        self.setChunkList(chunks)

        if not self.validate():
            self.setChunkList(oldChunks)
            raise TokenException("New token set is invalid. Rebuild aborted")

    def validate(self):
        byte_array = ''
        for chunk in self.getChunkList():
            byte_array += chunk.getDataBytes()

        # now that we have all of the token bytes, we can go through
        # the bytes, making sure that every time we skip ahead one
        # token, we're at a logical start of another token

        nextTokenStart = 0

        while nextTokenStart < len(byte_array):
            tokenID, tokenLength = getTokenIDAndLength(
                byte_array[nextTokenStart:nextTokenStart + 3])
            nextTokenStart += tokenLength + 1

        return 1


def hex_dump(byte_array):
    total = 0
    count = 0
    string = ''
    result = "%08x " % total
    for b in map(int, struct.unpack(">%dB" % len(byte_array), byte_array)):
        result += " %02x" % b
        if 31 < b < 127:
            string += chr(b)
        else:
            string += '.'
        count += 1
        total += 1
        if count == 8:
            result += " "
        if count == 16:
            result += "  " + string + "\n"
            result += "%08x " % total
            count = 0
            string = ''
    if len(string):
        while count < 16:
            count += 1
            result += "   "
        result += "  " + string + "\n"
    return result
