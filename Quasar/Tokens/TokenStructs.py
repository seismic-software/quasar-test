
import struct

from isti.utils import structWrapper
from isti.utils.GetSet import GetSet


class TokenStruct(GetSet):
    def __init__(self, byte_array=None):
        if byte_array:
            self.optionBitsMap = {}
            self.type = int(struct.unpack('>B', byte_array[0:1])[0])
            token_map = variableLengthTokenMap
            self.Fields = ['Type']
            self.FieldDefinition = ">B"
            if self.type >= 192:
                self.FieldDefinition += 'H'
                self.Fields.append('Length')
            elif self.type >= 128:
                self.FieldDefinition += 'B'
                self.Fields.append('Length')
                if self.type == 128:
                    self.optionBitsMap = bitMap128
                elif self.type == 135:
                    self.optionBitsMap = bitMap135
            else:
                token_map = fixedLengthTokenMap

            if self.type != 0:
                length = 0
                for (name, format_type, action) in token_map[self.type]:
                    if action == 'entry':
                        break
                    if action == 'pascal':
                        end = length + 1
                        str_len = int(struct.unpack(">B", byte_array[length:end])[0]) + 1
                        self.FieldDefinition += f"{str_len:d}{format_type}"
                        self.Fields.append(name)
                    elif action == 'default':
                        self.Fields.append(name)
                        self.FieldDefinition += format_type
                    length = struct.calcsize(self.FieldDefinition)
                self.generate_fields(byte_array)
                self.unpackData(byte_array)

    def get_identity(self):
        if self.type == 128:
            return "%s-%s" % (self.getLocationCode(), self.getSeedname())
        if self.type in (129, 130):
            return self.getFilterName()
        if self.type == 131:
            return self.getControlDetectorName()
        if self.type in (132, 133):
            return self.getDetectorName()
        if self.type == 192:
            return self.getCommEventName()
        if self.type == 193:
            return self.getDefaultEmailSenderAddress()
        return str(self.type)

    def generate_fields(self, byte_array):
        if byte_array:
            # -----------------
            if self.type == 128:
                option_bits = struct.unpack('>L', byte_array[10:14])[0]
                test_bit = lambda b: option_bits & (1 << b)
                indices = []
                if test_bit(3):  # Pre-Event Buffers
                    indices.append((6, None))
                if test_bit(4):  # Gap Threshold
                    indices.append((7, None))
                if test_bit(5):  # Calibration Delay
                    indices.append((8, None))
                if test_bit(6):  # Frame Count
                    indices.append((9, None))
                if test_bit(7):  # FIR Multiplier
                    indices.append((10, None))
                if test_bit(8):  # Averaging Paremeters
                    indices.append((11, None))
                    indices.append((12, None))
                if test_bit(9):  # Control Detector
                    indices.append((13, None))
                if test_bit(10):  # Decimation Entry
                    indices.append((14, None))
                    indices.append((15, None))
                for i in range(12, 20):
                    if test_bit(i):
                        indices.append((16, i - 11))
                        indices.append((17, i - 11))
                        indices.append((18, i - 11))

                for (index, field_index) in indices:
                    (name, format_type, action) = variableLengthTokenMap[self.type][index]
                    if field_index is not None:
                        self.Fields.append(f"{name}_{field_index:d}")
                    else:
                        self.Fields.append(name)
                    self.FieldDefinition += format_type
            # -------------------
            elif self.type == 129:
                stop_byte = struct.calcsize(self.FieldDefinition)
                num_sections = int(struct.unpack(self.FieldDefinition, byte_array[0:stop_byte])[4])
                for i in range(0, num_sections):
                    for (name, format_type, action) in variableLengthTokenMap[self.type]:
                        if action == 'entry':
                            self.Fields.append(f"{name}_{i:d}")
                            self.FieldDefinition += format_type
            # -------------------
            elif self.type == 131:
                run = True
                index = 0
                length = struct.calcsize(self.FieldDefinition)
                total_size = len(byte_array)
                while length < total_size:
                    item = struct.unpack('>B', byte_array[length:length + 1])[0]
                    for (name, format_type, action) in variableLengthTokenMap[self.type]:
                        if action == 'entry':
                            self.Fields.append(f"{name}_{index:d}")
                            self.FieldDefinition += format_type
                    index += 1
                    if ((item >> 6) == 3) and ((item & 0x3f) == 6):
                        run = False
                    length += 1
            # -----------------
            if self.type == 135:
                option_bits = struct.unpack('>L', byte_array[10:14])[0]
                test_bit = lambda b: option_bits & (1 << b)
                indices = []
                if test_bit(4):  # Gap Threshold
                    indices.append((6, None))
                if test_bit(6):  # Frame Count
                    indices.append((7, None))

                for (index, field_index) in indices:
                    (name, format_type, action) = variableLengthTokenMap[self.type][index]
                    if field_index is not None:
                        self.Fields.append("%s_%d" % (name, field_index))
                    else:
                        self.Fields.append(name)
                    self.FieldDefinition += format_type
            # -------------------
            elif self.type == 192:
                total_size = len(byte_array)
                for i in range(0, 32):
                    length = struct.calcsize(self.FieldDefinition)
                    if total_size > length:
                        str_len = int(struct.unpack('>B', byte_array[length + 1:length + 2])[0]) + 1
                        for (name, format_type, action) in variableLengthTokenMap[self.type]:
                            if action == 'entry':
                                self.Fields.append("%s_%d" % (name, i))
                                if format_type == 'p':
                                    self.FieldDefinition += f"{str_len:d}{format_type}"
                                else:
                                    self.FieldDefinition += format_type
                    else:
                        break
            # -------------------
            elif self.type == 193:
                total_size = len(byte_array)
                for i in range(0, 10):
                    length = struct.calcsize(self.FieldDefinition)
                    if total_size > length:
                        str_len = int(struct.unpack(">B", byte_array[length:])[0]) + 1
                        for (name, format_type, action) in variableLengthTokenMap[self.type]:
                            if action == 'entry':
                                self.Fields.append(f"{name}_{i:d}")
                                self.FieldDefinition += f"{str_len:d}{format_type}"
                    else:
                        break

            elif self.type == 194:
                # Maximum of 7700 bytes
                # (The limit is actually the total available space (8k) minus 
                #  the length of all the other tokens, so keep it as short as
                #  possible.)
                entry = variableLengthTokenMap[self.type][0]
                name, field_type, _ = entry
                self.Fields.append(name)
                self.FieldDefinition = field_type

    def initialize(self):
        for field in self.Fields:
            setattr(self, field, 0)

    def unpackData(self, byte_array):
        if self.FieldDefinition == 'R':
            self.__dict__[self.Fields[0]] = byte_array
        else:
            fieldData = structWrapper.unpack(self.FieldDefinition,
                                             byte_array[0:structWrapper.calcsize(self.FieldDefinition)])
            for i in range(0, len(self.Fields)):
                self.__dict__[self.Fields[i]] = fieldData[i]

        # Everything beyond this point for debugging purposes only
        # args = [self.FieldDefinition]
        # for field in self.Fields:
        #    args.append( self.__dict__[field] )
        # print args
        # print self.Fields

    def packData(self):
        args = [self.FieldDefinition]
        for field in self.Fields:
            args.append(self.__dict__[field])
        # print args
        # print self.Fields
        return structWrapper.pack(*args)

    def strSource(self):
        return f"0x{self.getSource():08x}"

    def strOptionBits(self):
        ret = []
        unused = []
        option_bits = self.getOptionBits()
        test_bit = lambda b: option_bits & (1 << b)
        ret.append(f'0x{option_bits:08x}')
        for i in range(0, 32):
            if self.optionBitsMap[i] is None:
                unused.append(i)
            else:
                if len(unused):
                    addUnusedBits(ret, unused)
                    unused = []
                desc, bp = self.optionBitsMap[i]
                if test_bit(i):
                    bool_str = booleanPairsMap[bp][1]
                else:
                    bool_str = booleanPairsMap[bp][0]
                ret.append('      [bit %d] %s: %s' % (i, desc, bool_str))
        if len(unused):
            addUnusedBits(ret, unused)

        return '\n'.join(ret)

    def __str__(self):
        ret = ['Instance of class %s type is %d:' % (self.__class__.__name__, self.type)]
        if self.type != 0:
            for field in self.getFields():
                strFunc = self.getStrFunction(field)
                ret.append(f'   {field}: {strFunc()}')
        return '\n'.join(ret)


def addUnusedBits(data_string, bits):
    if len(bits) > 1:
        data_string.append(f'      [bits {bits[0]:d}-{bits[-1]:d}] NOT IN USE')
    else:
        data_string.append(f'      [bit {bits[0]:d}] NOT IN USE')


bitMap128 = {
    0: ('Data Type', 'Ce'),
    1: ('Detector Packets', 'De'),
    2: ('Calibration Packets', 'De'),
    3: ('Pre-Event Buffers', 'Us'),
    4: ('Gap Override', 'Us'),
    5: ('Calibration Delay Override', 'Us'),
    6: ('Frame Count Override', 'Us'),
    7: ('FIR Multiplier Override', 'Us'),
    8: ('Averaging Parameters', 'Us'),
    9: ('Control Detector', 'Us'),
    10: ('Decimation Entry', 'Us'),
    11: ('Channel Output', 'Ed'),
    12: ('Run Detector 1', 'Ny'),
    13: ('Run Detector 2', 'Ny'),
    14: ('Run Detector 3', 'Ny'),
    15: ('Run Detector 4', 'Ny'),
    16: ('Run Detector 5', 'Ny'),
    17: ('Run Detector 6', 'Ny'),
    18: ('Run Detector 7', 'Ny'),
    19: ('Run Detector 8', 'Ny'),
    20: None,
    21: None,
    22: None,
    23: None,
    24: None,
    25: None,
    26: None,
    27: None,
    28: ('Include this LCQ when calculating frequency bitmap', 'Ny'),
    29: ('Send through DataServ', 'Ny'),
    30: ('Send trhough NetServ', 'Ny'),
    31: ('Force one CNP opaque blockette per record', 'Ny'),
}

bitMap135 = {
    0: ('Data Gaps', 'Rs'),
    1: ('Q330 Re-Boots', 'Rs'),
    2: ('Received Bps', 'Rs'),
    3: ('Sent Bps', 'Rs'),
    4: ('Communications Attempts', 'Rs'),
    5: ('Communications Successes', 'Rs'),
    6: ('Packets Received', 'Rs'),
    7: ('CommunicationsEfficiency', 'Rs'),
    8: ('POCs Received', 'Rs'),
    9: ('IP Address Changes', 'Rs'),
    10: ('Communications Duty Cycle', 'Rs'),
    11: ('Throughbut', 'Rs'),
    12: ('Missing Data', 'Rs'),
    13: ('Flood Packets Received', 'Rs'),
    14: ('Command Timeouts', 'Rs'),
    15: ('Sequence Errors', 'Rs'),
    16: ('Checksum Errors', 'Rs'),
    17: ('IO Errors', 'Rs'),
    18: ('Data Latency', 'Rs'),
    19: ('Status Latency', 'Rs'),
    20: None,
    21: None,
    22: None,
    23: None,
    24: None,
    25: None,
    26: None,
    27: None,
    28: None,
    29: None,
    30: None,
    31: None,
}

booleanPairsMap = {
    'NY': ('NO', 'YES'),
    'Ny': ('No', 'Yes'),
    'ny': ('no', 'yes'),
    'FN': ('OFF', 'ON'),
    'Fn': ('Off', 'On'),
    'fn': ('off', 'on'),
    '01': ('0', '1'),
    'De': ('Disabled', 'Enabled'),
    'Ed': ('Enabled', 'Disabled'),
    'Ce': ('Continuous', 'Event'),
    'Ap': ('Absent', 'Present'),
    'Us': ('Un-Set', 'Set'),
    'Sh': ('Show', 'Hide'),
    'Rs': ('Report', 'Supress'),
}

fixedLengthTokenMap = {
    0: [],
    1: [
        ('TokenVersionNumber', 'B', 'default')
    ],
    2: [
        ('Network', '2s', 'default'),
        ('Station', '5s', 'default')
    ],
    3: [
        ('PortNumber', 'H', 'default')
    ],
    4: [
        ('HighestPriorityPassword', '8s', 'default'),
        ('MiddlePriorityPassword', '8s', 'default'),
        ('LowestPriorityPassword', '8s', 'default'),
        ('TimeoutInSeconds', 'l', 'default'),
        ('MaximumBytesPerSecond', 'l', 'default'),
        ('Verbosity', 'B', 'default'),
        ('MaximumCPUPercentage', 'B', 'default'),
        ('UDPPortNumber', 'H', 'default'),
        ('MaximumMemoryToUseInKB', 'H', 'default'),
        ('ReservedBytes', 'H', 'default')
    ],
    5: [
        ('PortNumber', 'H', 'default')
    ],
    6: [
        ('TimezoneOffsetInSeconds', 'l', 'default'),
        ('LossInMinutesBeforeDowngradingQuality1Percent', 'H', 'default'),
        ('PLLLockedQuality', 'B', 'default'),
        ('PLLTrackingQuality', 'B', 'default'),
        ('PLLHoldQuality', 'B', 'default'),
        ('PLLOffQuality', 'B', 'default'),
        ('spare', 'B', 'default'),
        ('HighestHasBeenLockedQuality', 'B', 'default'),
        ('LowestHasBeenLockedQuality', 'B', 'default'),
        ('NeverBeenLockedQuality', 'B', 'default'),
        ('ClockQualityFilter', 'H', 'default')
    ],
    7: [
        ('MessageLogSeedLocation', '2s', 'default'),
        ('MessageLogSeedName', '3s', 'default'),
        ('TimingLogSeedLocation', '2s', 'default'),
        ('TimingLogSeedName', '3s', 'default')
    ],
    8: [
        ('ConfigurationStreamSeedLocation', '2s', 'default'),
        ('ConfigurationStreamSeedName', '3s', 'default'),
        ('Flags', 'B', 'default'),
        ('Interval', 'H', 'default')
    ],
    9: [
        ('PortNumber', 'H', 'default')
    ]
}

variableLengthTokenMap = {
    128: [
        ('LocationCode', '2s', 'default'),
        ('Seedname', '3s', 'default'),
        ('LCQReferenceNumber', 'B', 'default'),
        ('Source', 'H', 'default'),
        ('OptionBits', 'L', 'default'),
        ('Rate', 'h', 'default'),
        # The following only appear if set in OptionBits
        ('NumberOfPreEventBuffers', 'h', 'entry'),
        ('GapThreshold', 'f', 'entry'),
        ('CalibrationDelayInSeconds', 'h', 'entry'),
        ('MaximumFrameCount', 'B', 'entry'),
        ('FIRMultiplier', 'f', 'entry'),
        # Averaging Paremeters
        ('NumberOfSamplesBetweenReports', 'l', 'entry'),
        ('OptionalIIRFilterToUse', 'B', 'entry'),
        # Control Detector
        ('ControlDetectorNumber', 'B', 'entry'),
        # Decimation Entry
        ('SourceLCQNumber', 'B', 'entry'),
        ('DecimationFIRFilterNumber', 'B', 'entry'),
        # Detector to Run
        ('DetectorNumberToUseAsBase', 'B', 'entry'),
        ('DetectorNumberForThisInvocation', 'B', 'entry'),
        ('DetectorOptions', 'B', 'entry')
    ],
    129: [
        ('FilterNumber', 'B', 'default'),
        ('FilterName', 'p', 'pascal'),
        ('NumberOfSections', 'B', 'default'),
        ('Gain', 'f', 'default'),
        ('ReferenceFrequency', 'f', 'default'),
        ('CutoffFrequencyRatio', 'f', 'entry'),
        ('NumberOfPoles', 'B', 'entry')
    ],
    130: [
        ('FilterNumber', 'B', 'default'),
        ('FilterName', 'p', 'pascal')
    ],
    131: [
        ('ControlDetectorNumber', 'B', 'default'),
        ('ControlDetectorOptions', 'B', 'default'),
        ('ControlDetectorName', 'p', 'pascal'),
        ('VariableLengthDetectorEquation', 'B', 'entry')
    ],
    132: [
        ('DetectorNumber', 'B', 'default'),
        ('DetectorFilterNumber', 'B', 'default'),
        ('IwParameter', 'B', 'default'),
        ('NhtParameter', 'B', 'default'),
        ('FilhiParameter', 'l', 'default'),
        ('FilloParameter', 'l', 'default'),
        ('WaParameter', 'H', 'default'),
        ('Spare', 'H', 'default'),
        ('TcParameter', 'H', 'default'),
        ('X1ParameterDiv2', 'B', 'default'),
        ('X2ParameterDiv2', 'B', 'default'),
        ('X3ParameterDiv2', 'B', 'default'),
        ('XxParameter', 'B', 'default'),
        ('AvParameter', 'B', 'default'),
        ('DetectorName', 'p', 'pascal')
    ],
    133: [
        ('DetectorNumber', 'B', 'default'),
        ('DetectorFilterNumber', 'B', 'default'),
        ('HysterisisParameter', 'B', 'default'),
        ('WindowParameter', 'B', 'default'),
        ('HighLimitParameter', 'l', 'default'),
        ('LowLimitParameter', 'l', 'default'),
        ('TailParameter', 'H', 'default'),
        ('Spare', 'H', 'default'),
        ('DetectorName', 'p', 'pascal')
    ],
    134: [],
    135: [
        ('LocationCode', '2s', 'default'),
        ('Seedname', '2s', 'default'),
        ('LCQReferenceNumber', 'B', 'default'),
        ('Source', 'H', 'default'),
        ('OptionBits', 'L', 'default'),
        ('Rate', 'h', 'default'),
        ('GapThreshold', 'f', 'entry'),
        ('MaximumFrameCount', 'f', 'entry'),
    ],
    192: [
        ('CommEventBitNumber', 'B', 'entry'),
        ('CommEventName', 'p', 'entry')
    ],
    # OpaqueConfiguration was in a separate (duplicate) key 193 -- all of
    # these fields have been merged into a single list instead.
    193: [
        ('QueueSize', 'H', 'default'),
        ('TimeoutInSecondsBetweenSNTPSteps', 'H', 'default'),
        ('ReservedBytes', 'H', 'default'),
        ('RetrySpecification', 'p', 'pascal'),
        ('TemplateDirectory', 'p', 'pascal'),
        ('DefaultEmailSenderAddress', 'p', 'pascal'),
        ('Mailhosts', 'p', 'entry'),
        ('OpaqueConfiguration', 'R', 'default')
    ],
}
