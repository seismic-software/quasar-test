# This class represents a single chunk of tokens (the response
# from a single c1_rqmem)

import struct

from isti.utils.GetSet import GetSet

segmentHeader = '>HH'
tokenOverlayStart = '>2sH'
tokenOverlayEnd   = '>2B'


class TokenChunk(GetSet):
    def __init__(self, memChunk=None):
        if memChunk is not None:
            self.c1_mem = memChunk
            self.read_from_c1_mem()

    def read_from_c1_mem(self):
        # get the data elements preceeding the data
        byte_array = self.c1_mem.getMemoryContents()
        segmentParts = struct.unpack(segmentHeader, byte_array[0:4])
        self.setSegmentNumber(segmentParts[0])
        self.setTotalNumberOfSegments(segmentParts[1])

        self.setMemStartingAddress(self.c1_mem.getStartingAddress())
        self.setMemByteCount(self.c1_mem.getByteCount())
        self.setMemMemoryType(self.c1_mem.getMemoryType())

        # There was some logic here to get the checksums of parts of the
        # data but it was commented out.
        # The purpose of the checksum operation was not clear.

        self.setDataBytes(byte_array[4:])
