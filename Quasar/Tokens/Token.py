import struct

from isti.utils import Bits
from isti.utils.GetSet import GetSet

FIXED_TOKEN_LENGTHS = [0, 1, 7, 2, 40, 2, 16, 10, 8, 2]

TOKEN_ID_TO_NAME = {
    0: 'Ignore',
    1: 'Token Version Number',
    2: 'Network and Station ID',
    3: 'DP Netserver',
    4: 'Data Subscription Server Parameters',
    5: 'DP Webserver',
    6: 'Clock Processing Parameters',
    7: 'Log and Timing Identification',
    8: 'Configuration Identification',
    9: 'DataServer Port',
    128: 'Logical Channel Queue',
    129: 'IIR Filter Specification',
    130: 'FIR Filter Specification',
    131: 'Control Detector Specification',
    132: 'Murdock Hutt Detector Specification',
    133: 'Threshold Detector Specification',
    134: 'Non-compliant DP',
    135: 'DP Statistics Logical Channel Queue',
    192: 'Comm Event Names',
    193: 'Email Alert Configuration',
    194: 'Opaque Configuration',
}


class TokenError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)


##
# Given the first 3 bytes of a token, return the ID and the length
##
def getTokenIDAndLength(byte_array):
    tokenID = byte_array[0]
    tokenLength = 0
    if (tokenID & Bits.bitIndexToInteger(7)) == 0:
        if tokenID < len(FIXED_TOKEN_LENGTHS):
            tokenLength = FIXED_TOKEN_LENGTHS[tokenID]
        else:
            raise TokenError('Unknown Fixed Length Token: %d' % tokenID)
    else:
        # This is a variable length token
        if (tokenID & Bits.bitIndexToInteger(6)) == 0:
            # this is a one byte length
            tokenLength = byte_array[1]
        else:
            # this is a 2 byte length
            tokenLength = struct.unpack('>H', byte_array[1:3])[0]
    return tokenID, tokenLength


#
# base class for a token
#
class Token(GetSet):

    def __init__(self, byte_array=None):
        if byte_array:
            token_id, length = getTokenIDAndLength(byte_array)
            self.setTokenID(token_id)
            self.setTokenLength(length)
            self.setTokenBytes(byte_array)
            self.setTokenName(TOKEN_ID_TO_NAME[self.getTokenID()])

    def __repr__(self):
        output = self.getTokenName()
        i = 0
        for token_byte in self.getTokenBytes():
            if not i % 8:
                output += '\n'
            output += '%02x ' % token_byte
            i += 1
        return output


##
# This maps a token ID to a token class.  If more functionality than the generic
# "Token" is needed, create new classes and map them properly here
##
TOKEN_ID_TO_CLASS = {
    0: Token,
    1: Token,
    2: Token,
    3: Token,
    4: Token,
    5: Token,
    6: Token,
    7: Token,
    8: Token,
    9: Token,
    128: Token,
    129: Token,
    130: Token,
    131: Token,
    132: Token,
    133: Token,
    134: Token,
    135: Token,
    192: Token,
    193: Token,
    194: Token,
}
