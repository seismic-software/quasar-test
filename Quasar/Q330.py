import struct
import sys
from os import popen as Popen
from time import sleep

from Quasar import Status
from Quasar.CmdID import CmdID
from Quasar.Commands.c1_ctrl import c1_ctrl, FLAGS_REBOOT, FLAGS_SAVE_TO_EEPROM, \
    FLAGS_RESYNC, FLAGS_TURN_ON_GPS, FLAGS_TURN_OFF_GPS, FLAGS_COLD_START_GPS, \
    FLAGS_REBOOT_EP_S1, FLAGS_REBOOT_EP_S2
from Quasar.Commands.c1_dsrv import c1_dsrv
from Quasar.Commands.c1_erase import c1_erase
from Quasar.Commands.c1_mem import c1_mem
from Quasar.Commands.c1_ping import c1_ping
from Quasar.Commands.c1_pulse import c1_pulse
from Quasar.Commands.c1_rqfix import c1_rqfix
from Quasar.Commands.c1_rqfree import c1_rqfree
from Quasar.Commands.c1_rqlog import c1_rqlog
from Quasar.Commands.c1_rqmem import c1_rqmem
from Quasar.Commands.c1_rqmod import c1_rqmod
from Quasar.Commands.c1_rqphy import c1_rqphy
from Quasar.Commands.c1_rqsc import c1_rqsc
from Quasar.Commands.c1_rqsrv import c1_rqsrv
from Quasar.Commands.c1_rqstat import c1_rqstat
from Quasar.Commands.c1_smem import c1_smem
from Quasar.Commands.c1_sphy import c1_sphy
from Quasar.Commands.c1_srvrsp import c1_srvrsp
from Quasar.Commands.c2_rqphy import c2_rqphy
from Quasar.Commands.c2_sphy2 import c2_sphy2
from Quasar.QDPDevice import QDPDevice, QDP_C1_CERR
from Quasar.Tokens.TokenChunk import TokenChunk
from Quasar.Tokens.TokenSet import TokenSet
from Quasar.Utils.Misc import dottedIP2Num, FIRSTUSABLE, SECTOR_SIZE, \
    SECTOR_COUNT


class Q330Exception(BaseException):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


# Sensor Output Definition Exceptions
class Q330ExSODDisabled(Q330Exception):
    pass


class Q330ExSODDuplicate(Q330Exception):
    pass


class Q330ExArgs(Q330Exception):
    pass


PulseOptionMap = {'calibration': {'A': 1, 'B': 4},
                  'recenter': {'A': 2, 'B': 5},
                  'coupling': {'A': 3, 'B': 6},
                  'lock': {'A': 7, 'B': 11},
                  'unlock': {'A': 8, 'B': 12},
                  'aux1': {'A': 9, 'B': 13},
                  'aux2': {'A': 10, 'B': 14}
                  }


def get_pulse_type(sensor, definition):
    for key in list(PulseOptionMap.keys()):
        option_result = PulseOptionMap[key]
        if sensor in option_result and (option_result[sensor] == definition):
            return key



class Q330(QDPDevice):
    """
    An interface for talking to a Q330.

    This class extends the QDPDevice class to include methods that
    are specific to dealing with the Q330.
    """

    def __init__(self, serial_number, ip_address, base_port, max_tries=1,
                 device_type='Q330'):
        QDPDevice.__init__(self)
        self.platform = sys.platform

        self.setSerialNumber(serial_number)
        self.setIPAddress(ip_address)
        self.setBasePort(base_port)
        self.setRegistered(0)
        self.setAuthCode(0)
        self.setDeviceType(device_type)
        self.setMaxTries(max_tries)

    # this is not proper python formatting but needs to match the expected
    # formatting of GetSet arguments in order to function as expected
    def setSerialNumber(self, serial_number):
        # in order to allow getSerialNumber to be applied using the inherited
        # GetSet methods, we will add 'SerialNumber' to a field in the __dict__
        # manually, which will be what getSerialNumber will expect
        # however, we want this preprocessing step to happen first, where we
        # will evaluate the serial number as a hexadecimal value string
        if type(serial_number) == str:
            serial_number = eval('0x%s' % serial_number)
        self.__dict__['SerialNumber'] = serial_number

    def register(self):
        """
        Register with a Q330.

        The authentication code must be set via setAuthCode() prior
        to registration, otherwize the default value of 0 (zero) will
        be used

        After a successful registration, a c1_cack command will be returned
        """
        reg_request = c1_rqsrv()
        reg_request.setSerialNumber(self.getSerialNumber())
        challenge = self.sendCommand(reg_request)
        response = c1_srvrsp()
        response.setSerialNumber(self.getSerialNumber())
        response.setChallengeValue(challenge.getChallengeValue())
        response.setServerIPAddress(challenge.getServerIPAddress())
        response.setServerUDPPort(challenge.getServerUDPPort())
        response.setRegistrationNumber(challenge.getRegistrationNumber())

        response.createDigest(self.getAuthCode())
        ret = self.sendCommand(response)
        self.setRegistered(1)
        return ret

    def deregister(self):
        """
        Deregister from the Q330

        After a successful deregistration, a c1_cack command will be returned
        """
        dereg = c1_dsrv()
        dereg.setSerialNumber(self.getSerialNumber())
        ret = self.sendCommand(dereg)
        self.setRegistered(0)
        return ret

    def ping(self):
        """
        Send a QDP ping to the Q330.  A successful ping will cause LEDs
        to become solid on the ping'd Q330's front panel

        A c1_ping reply packet will be returned
        """
        ping = c1_ping()
        ping.setType(0)
        pong = self.sendCommand(ping)
        return pong

    def getStatus(self, whichStatuses, multiple=0):
        """
        Get one or more statuses from the Q330

        This method takes a list of strings as an argument.  Each of these
        strings should match one of the elements of Status.StatusTypes.

        If only one status is desired, that status' name may be passed by
        itself, rather than in a list.

        A c1_stat packet is returned.  The data members included depends on
        which statuses were requested.
        """
        bitmap = Status.getBitmap(whichStatuses)
        rqstat = c1_rqstat()
        rqstat.setRequestBitmap(bitmap)
        return self.sendCommand(rqstat, multiple)

    def _generateSensorControlBitmap(self, sensor, definition_codes):
        assert(isinstance(definition_codes, list))
        if not len(definition_codes):
            raise Q330ExArgs("No Action(s) Selected")

        # get the sensor status bits
        ssc_info = self.sendCommand(c1_rqsc())

        # THIS IS THE MOST CONFUSING of the Q330 commands!! See the docs:
        # Turn on high or low "active" only that bit which we care about, 
        # all others flip to the opposite state of that found.
        #
        # Each of the 8 bits in the bitmap corresponds to one of
        # the Sensor Output Definitions. If the bit in the bitmap matches
        # the state of the active high/low bit in the corresponding defintion
        # the line is pulsed.
        #
        # We find the sensor definition for which pulses are enabled 
        # for our sensor, and we make sure the corresponding
        # bit is set for each definition_code.
        #
        # For all of the other bits, we reverse the logical value of
        # the bit:
        #   So, if the definition is set to 0 for bits 0-7, we
        #   set the bit corresponding to this definition to 1.
        #   And if the definition is set to a value greater than zero
        #   for bits 0-7, we set the bit corresponding to this defintion
        #   to 0. This keeps the associated control line in the default
        #   state (high or low).
        #
        # Any control value can be associated with any of the eight control
        # lines.
        # This means that up to seven (max options) control values can be 
        # specified for an individual sensor. We can also specify a single
        # option multiple times, allowing the same operation to be associated
        # with multiple lines.
        # This is useful when we want to do something like putting a 
        # symmetric triaxial seismometer into UVW mode while calibrating.
        # By attaching the UVW enable to one of the enable lines, instead of
        # piggy-backing on the cal enables line, we can disable/enable UVW
        # calibrations by a simple modification of the Sensor Control Mapping.
        bitmap = 0
        definition_counts = {}
        for definition_code in definition_codes:
            definition_counts[definition_code] = 0
        # We need to cycle through each Sensor Output Defintion, and
        # encode each into the bitmap
        for definition_index in range(1, 9):
            key = "getSensorOutput%sDefinition" % definition_index
            value = getattr(ssc_info, key)()
            # If this is one of our target definitions, enable it in the bitmap
            if value and ((value & 0xff) in definition_codes):
                # keep track of how many times this definition has been set
                # we will verify later that it is at most once
                definition_counts[value & 0xff] += 1
                # bit-8 tells us if this is active high (1) or active low (0)
                bitmap |= (((value & 0x100) >> 8) & 0x1) << (
                        definition_index - 1)
            # Make sure to flip to the opposite of the active high/low
            # value found
            elif value and (value & 0xff):
                bitmap |= (((value & 0x100) >> 8) ^ 0x1) << (
                        definition_index - 1)

        # If the requested definition is not set, throw an exception
        for count_key in list(definition_counts.keys()):
            definition_count = definition_counts[count_key]
            if definition_count < 1:
                raise Q330ExSODDisabled("%s Pulse not Enabled for Sensor %s" % (
                    get_pulse_type(sensor, count_key).title(), sensor))

        return bitmap

    # param actions is a list or tuple of tuples of the form (sensor, action)
    def getSensorControlBitmap(self, actions):
        assert(isinstance(actions, (list, tuple)))
        valid_actions = []
        invalid_actions = []
        definition_codes = []
        for action, sensor in actions:
            if action not in PulseOptionMap:
                invalid_actions.append(action)
            else:
                valid_actions.append((action, sensor))
        if len(invalid_actions):
            raise Q330ExArgs(
                f"Invalid Action(s): {', '.join(invalid_actions)}")
        for action, sensor in valid_actions:
            definition_codes.append(PulseOptionMap[action][sensor])
        return self._generateSensorControlBitmap(sensor, definition_codes)

    """If the specified action is enabled for this sensor, 
       we pulse the appropriate control line."""

    def pulse(self, action, sensor, duration):
        option_map = PulseOptionMap[action]
        if sensor in option_map:
            definition_code = option_map[sensor]
        else:
            raise Q330ExArgs("Invalid Sensor Argument")

        bitmap = self._generateSensorControlBitmap(sensor, [definition_code])

        pulse = c1_pulse()
        pulse.setSensorControlBitmap(bitmap)
        pulse.setDuration(duration)

        return self.sendCommand(pulse)

    def recenterPulse(self, sensor, duration):
        self.pulse('recenter', sensor, duration)

    def lockPulse(self, sensor, duration):
        self.pulse('lock', sensor, duration)

    def unlockPulse(self, sensor, duration):
        self.pulse('unlock', sensor, duration)

    # For backward compatability
    def recenterSensor(self, sensor, duration):
        self.recenterPulse(sensor, duration)

    def lockSensor(self, sensor, duration):
        self.lockPulse(sensor, duration)

    def unlockSensor(self, sensor, duration):
        self.unlockPulse(sensor, duration)

    def sendControlPacket(self, flags):
        """
        Send a c1_ctrl packet with the supplied flags.  The possible flags
        are listed in the Commands.c1_ctrl module.

        The reboot, saveToEEPROM, resync, turnOnGPS, turnOffGPS and coldStartGPS
        methods are supplied as a convenience so that sendControlPacket only
        needs to be called if several of these options need to be used at once.

        A c1_cack will be returned if successful.
        """
        ctl = c1_ctrl()
        ctl.setFlags(flags)
        return self.sendCommand(ctl)

    def reboot(self):
        """
        Reboot the Q330
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_REBOOT
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_REBOOT)

    def saveToEEPROM(self):
        """
        Save the current configuration to EEPROM
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_SAVE_TO_EEPROM
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_SAVE_TO_EEPROM)

    def resync(self):
        """
        Resync
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_RESYNC
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_RESYNC)

    def turnOnGPS(self):
        """
        Turn GPS on
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_TURN_ON_GPS
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_TURN_ON_GPS)

    def turnOffGPS(self):
        """
        Turn GPS off
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_TURN_OFF_GPS
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_TURN_OFF_GPS)

    def coldStartGPS(self):
        """
        Cold start GPS
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_COLD_START_GPS
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_COLD_START_GPS)

    def rebootEPOnSerial1(self):
        """
        Reboot Environmental Processor on Serial Port 1
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_REBOOT_EP_S1
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_REBOOT_EP_S1)

    def rebootEPOnSerial2(self):
        """
        Reboot Environmental Processor on Serial Port 2
        calls sendControlPacket with Commands.c1_ctrl.FLAGS_REBOOT_EP_S2
        A c1_cack is returned if successful
        """
        return self.sendControlPacket(FLAGS_REBOOT_EP_S2)

    def getRunningModules(self):
        modRequest = c1_rqmod()
        modMap = self.sendCommand(modRequest, 0)
        return modMap

    # There was a method here that has been deleted, 'getAllModules'
    # which requested broad ranges of memory from the device and then never
    # stored them. It was not clear to me what it was supposed to do based on
    # the code, especially as the methods were unused anywhere, both elsewhere
    # in quasar and in the cnc repository (only commented-out code claiming to
    # not be fully implemented in a comment, in cnc).
    # It has been deleted.
    # There was also a method wrapping a print statement on its result,
    # called listAllModules, deleted because it could no longer be used.

    def listRunningModules(self):
        print(self.getRunningModules())

    def listAllModules(self):
        print(self.getAllModules())

    def eraseModules(self, indices):
        pass

    def installModules(self, sourceFile):
        pass

    ##
    # Get the webpage contents
    ##
    def getWebPage(self):
        least = lambda a, b: ((a > b) and [b] or [a])[0]
        memType = 5  # Web Page
        structSize = 448
        chunkSize = 442
        offset = 0
        remain = 8 * 1024  # Read up to 8Kbyte (total webpage memory)
        webPage = bytes()
        finished = False
        segment = 1
        total = 16
        while (segment < total) and (remain > 0):
            request = c1_rqmem()
            request.setStartingAddress(offset)
            request.setByteCount(least(structSize, remain))
            request.setMemoryType(memType)
            response = self.sendCommand(request, 0)
            segment, total = struct.unpack('>HH',
                                           response.getMemoryContents()[:4])
            webPage += response.getMemoryContents()[4:]
            count = int(response.getByteCount())
            # print "read %d of %d: %d bytes [%d data bytes]" % (segment, total, least(structSize, remain), count - 4)
            remain -= least(structSize, remain)
            offset += least(structSize, remain)
        return webPage

    ##
    # Set the webpage contents
    ##
    def setWebPage(self, newPage):
        least = lambda a, b: ((a > b) and [b] or [a])[0]

        max_webpage_len = 8002  # ((8192 / 448) * 438) + ((8192 % 448) - 10)
        memType = 5  # Web Page
        chunkSize = 438
        structSize = 448

        segments = []
        if len(newPage) > max_webpage_len:
            raise Q330ExArgs("Web page is too large.")
        offset = 0
        while 1:
            if len(newPage[offset:]) > chunkSize:
                segments.append(newPage[offset:offset + chunkSize])
                offset += chunkSize
            else:
                segments.append(newPage[offset:])
                break

        total_written = 0
        offset = 0
        complete = False
        index = 1
        total = len(segments)
        remain = 8192  # 8Kbyte, max webpage memory
        for segment in segments:
            buffer = struct.pack(">HH%ds" % len(segment), index, total, segment)
            request = c1_smem()
            request.setStartingAddress(offset)
            request.setByteCount(len(buffer))
            request.setMemoryType(memType)
            request.setMemoryContents(buffer)
            response = self.sendCommand(request, 0)

            remain -= least(structSize, remain)
            offset += least(structSize, remain)
            index += 1
            if (remain > 0) or (segment >= total):
                complete = True

    ##
    # Get a tokenset for one of the 4 dataports
    ##
    def getTokenSet(self, whichSet):
        """
        Return a Tokens.TokenSet instance representing the dataport number
        supplied
        """
        chunks = []
        chunkSize = 448
        req = c1_rqmem()
        currentStartingAddress = 0
        req.setStartingAddress(currentStartingAddress)
        req.setByteCount(chunkSize)
        req.setMemoryType(whichSet)
        chunks.append(TokenChunk(self.sendCommand(req)))
        while (chunks[-1].getSegmentNumber() < chunks[
            -1].getTotalNumberOfSegments()):
            req = c1_rqmem()
            currentStartingAddress += chunks[-1].getMemByteCount() + 6
            req.setStartingAddress(currentStartingAddress)
            req.setByteCount(chunkSize)
            req.setMemoryType(whichSet)

            mem = self.sendCommand(req)
            chunks.append(TokenChunk(mem))

        thisTokenSet = TokenSet(chunks)

        return thisTokenSet

    ##
    # Inject a new tokenset into a dataport
    ##
    def setTokenSet(self, newSet, whichSet):
        newSet.rebuildChunks()
        chunks = newSet.getChunkList()
        for chunk in chunks:
            mem = chunk.c1_mem
            smem = c1_smem(mem.getPacketBytes())
            smem.setMemoryType(whichSet)
            self.sendCommand(smem)


class RobustQ330(Q330):
    """
    Q330 class with added functionality from PASSCAL's Hocus
    """

    def __init__(self, serial_number=None, ip_address=0, base_port=0,
                 device_type='Q330'):
        Q330.__init__(self, serial_number, ip_address, base_port,
                      device_type=device_type)

    def realRegister(self, lck=None):
        """
        To obtain the stable real IP registration, we will use the MAC number.
        We bind the MAC with the IP in OS. The first step is to obtain
        the necessary infomation (real info) through broadcasting registration.
        Then we try the real registration.

        The by product is general physical interfaces!!!
        """
        phyResponse = self.broadcastPhysicalInterfaces()

        if phyResponse.getQDPCommand() != CmdID.C1_PHY:
            print("broadcasting error.")
            return

        realIP = phyResponse.strEthernetIPAddress()
        basePort = phyResponse.strBasePort()
        mac = phyResponse.strEthernetMACAddressHigh32() + ':' + \
              phyResponse.strEthernetMACAddressLow16()

        if lck:
            lck.acquire()  # for multi-threads

        if self.platform == 'win32':
            Popen("arp -s " + realIP + ' ' + mac.replace(':', '-', 5))
        elif self.platform[:-1] == 'linux':
            # delete the host whose IP is the same as me.
            Popen("/sbin/arp -s " + realIP + ' ' + mac)
        elif self.platform == 'darwin':
            Popen("/usr/sbin/arp -s %s %s" % (realIP, mac))
        else:
            print('This OS is not supported.')

        if lck:
            lck.release()

        # Real IP registration to GUARANTEE 'saveTOEEPROM' and 'reboot' work
        self.setIPAddress(realIP)
        self.setAuthCode(self.getAuthCode())
        self.setBasePort(int(basePort))
        self.setSerialNumber(self.getSerialNumber())

        # Not clear which exceptions can be thrown here, so no typing of
        # exceptions to fix warnings about the exception clause being too broad
        try:
            self.register()
        except:
            print("Error: can't register using real IP.")
            return None

        return phyResponse

    def getFirmwareVersion(self, requireRegister=1):
        if requireRegister:
            self.realRegister()
        fix = self.sendCommand(c1_rqfix())
        if requireRegister:
            self.deregister()
        try:  # for old firmware version
            v = int(fix.strSystemVersion())
            v1 = v / 256
            v2 = v % 256
            return str(v1) + str(v2)
        except:
            return fix.strSystemVersion()

    def getGeneralStatus(self):
        """
        Get eight general status by calling c1_rqstat and c1_rqfix

        Currently, we do not include eight parameters of Mics Input which are
        included in Willard.
        """
        self.realRegister()
        stat = self.getStatus('Global')
        fix = self.sendCommand(c1_rqfix())
        self.deregister()
        gstatus = {}
        gstatus['Total Hours'] = stat.strTotalTimeInSeconds()
        gstatus['Power On Time'] = stat.strPowerOnTimeInSeconds()
        gstatus['Time of Last Re-Syncs'] = stat.strTimeOfLastResync()
        gstatus['Total Number of Re-Syncs'] = stat.strTotalNumberOfResyncs()
        gstatus['Status Inputs'] = stat.strStatusInputs()
        gstatus['AMB Voltage Control'] = stat.strAnalogVoltageControlValue()
        gstatus['Time of Last Boot'] = fix.strTimeOfLastReboot()
        gstatus['Total Number of Boots'] = fix.strTotalNumberOfReboots()
        # get current time since 2000-01-01 00:00:00
        curr = int(stat.strSecondsOffset()) + \
               int(stat.strUSecOffset()) + \
               int(stat.strCurrentDataSequenceNumber())
        gstatus['Timestamp of Status'] = str(curr)
        return gstatus

    def getGPSStatus(self):
        """
        Get GPS Status
        """

        self.realRegister()
        stat = self.getStatus('Gps')
        time = self.getStatus('Global')
        self.deregister()
        gstatus = {}
        gstatus['GPS Time'] = stat.strGPSTimeString()
        gstatus['GPS Date'] = stat.strGPSDateString()
        gstatus['GPS Fix Type'] = stat.strGPSFixString()
        gstatus['GPS Height'] = stat.strGPSHeightString()
        gstatus['GPS Latitude'] = stat.strGPSLatitudeString()
        gstatus['GPS Longitude'] = stat.strGPSLongitudeString()
        gstatus['GPS Run Time'] = stat.strGPSPowerOnOffTime()
        gstatus['Satellites Used'] = stat.strNumberOfSatellitesUsed()
        gstatus['Satellites in View'] = stat.strNumberOfSatellitesInView()
        gstatus['Checksum'] = stat.strTotalChecksumErrors()
        # get current time since 2000-01-01 00:00:00
        curr = int(time.strSecondsOffset()) + \
               int(time.strUSecOffset()) + \
               int(time.strCurrentDataSequenceNumber())
        gstatus['Timestamp of Status'] = str(curr)
        return gstatus

    def setEthernetIP(self, newIP):
        """
        Set/Change an ethernet IP
        Serial Number should be already set.
        After change, the new IP will be saved and the q330 will reboot.
        """
        return self.setPhysicalInterfaces(5, newIP)

    def setEthernetMACHigh32(self, newMacH32):
        """
        Set/Change a mac high 32 bits

        Should not happen usually but protocal supports
        """
        return self.setPhysicalInterfaces(6, newMacH32)

    def setEthernetMACLow16(self, newMacL16):
        """
        Set/Change a mac low 16 bits

        Should not happen usually but protocal supports too
        """
        return self.setPhysicalInterfaces(7, newMacL16)

    def setPhysicalInterfaces(self, which, newOne):
        """
        Set/Change an physical iterfaces
        Basically call c1_sphy
        After changed, the q330 may or may not reboot.
        """
        phyResponse = self.broadcastPhysicalInterfaces()

        if phyResponse.getQDPCommand() != CmdID.C1_PHY:
            print("broadcast error.")
            return

        oldIP = phyResponse.strEthernetIPAddress()
        basePort = phyResponse.strBasePort()
        mac = phyResponse.strEthernetMACAddressHigh32() + ':' + \
              phyResponse.strEthernetMACAddressLow16()
        # keep original parameters
        setRequest = c1_sphy(phyResponse.getPacketBytes())

        setRequest.setSerialNumber(self.getSerialNumber())
        if which == 1:  # Set serial number (not work probably)
            setRequest.setSerialNumber(newOne)
        elif which == 2:  # Set Interface 1 IP Address
            setRequest.setSerialInterface1IPAddress(dottedIP2Num(newOne))
        elif which == 3:  # Set Interface 2 IP Address
            setRequest.setSerialInterface2IPAddress(dottedIP2Num(newOne))
        elif which == 4:  # Set Interface 3 IP Address
            setRequest.setSerialInterface3IPAddress(dottedIP2Num(newOne))
        elif which == 5:  # Set Ethernet IP Address
            setRequest.setEthernetIPAddress(dottedIP2Num(newOne))
        elif which == 6:  # Set MAC high 32
            setRequest.setEthernetMACAddressHigh32(eval("0x%s" % newOne))
        elif which == 7:  # Set MAC low 32
            setRequest.setEthernetMACAddressLow16(eval("0x%s" % newOne))
        elif which == 8:  # Set Base Port
            setRequest.setBasePort(newOne)
        elif which == 9:  # Set Serial Interface 1 Baud
            setRequest.setSerialInterface1Baud(newOne)
        elif which == 10:  # Set Serial Interface 1 Flags
            setRequest.setSerialInterface1Flags(newOne)
        elif which == 11:  # Set Serial Interface 2 Baud
            setRequest.setSerialInterface2Baud(newOne)
        elif which == 12:  # Set Serial Interface 2 Flags
            setRequest.setSerialInterface2Flags(newOne)
        elif which == 13:  # Set Serial Interface 3 Baud
            setRequest.setSerialInterface3Baud(newOne)
        elif which == 14:  # Set Serial Interface 3 Flags
            setRequest.setSerialInterface3Flags(newOne)
        elif which == 15:  # Set Ethernet Flags
            setRequest.setEthernetFlags(newOne)
        elif which == 16:  # Set Serial Interface 1 Throttle
            setRequest.setSerialInterface1Throttle(newOne)
        elif which == 17:  # Set Serial Interface 2 Throttle
            setRequest.setSerialInterface2Throttle(newOne)
        elif which == 18:  # Set Serial Interface 3 Throttle
            setRequest.setSerialInterface3Throttle(newOne)

        if self.platform == 'win32':
            Popen("arp -d")
            Popen("arp -s " + oldIP + ' ' + mac.replace(':', '-', 5))
        elif self.platform[:-1] == 'linux':
            # delete the host whose IP is the same as mine.
            Popen("/sbin/arp -s " + oldIP + ' ' + mac)
        elif self.platform == 'darwin':
            Popen("/usr/sbin/arp -s %s %s" % (oldIP, mac))
        else:
            print('This OS is not supported')

        # Real IP registration to GUARANTEE 'saveTOEEPROM' and 'reboot' work
        self.setIPAddress(oldIP)
        self.setAuthCode(self.getAuthCode())
        self.setBasePort(int(basePort))
        self.setSerialNumber(self.getSerialNumber())

        try:
            self.register()
        except:
            print("Error: can't register using real IP.")
            return 0

        # now updated
        try:
            # moved from broadcast registering. In this way, even
            # if seting IP fails, we still have chances to try again
            ret = self.sendCommand(setRequest, receiveMultiple=1)
            self.saveToEEPROM()
            sleep(1)
            self.reboot()
            sleep(2)
            return 1
        except QDP_C1_CERR as e:
            print(e)
            self.deregister()
            return 0

    def broadcastPhysicalInterfaces(self):
        """
        Broadcast the physical interfaces
        """

        if self.getSerialNumber() is None:
            print("can't set/change IP without serial number.")
            return

        if not self.getIPAddress():
            self.setIPAddress("255.255.255.255")
            self.setReceiveTimeout(5)
            self.setBasePort(65535)

        self.register()
        phyResponses = self.sendCommand(c1_rqphy(), receiveMultiple=1)
        try:
            self.deregister()
        except:
            pass

        for phyResponse in phyResponses:  # return a list (c1_err and c1_phy)
            if phyResponse.getQDPCommand() == CmdID.C1_PHY:
                break
        return phyResponse

    def setAllGeneralPhysicalInterfaces(self, aQ330SettingData):
        """
        Set/Change an all general physical iterfaces
        After changes, the q330 may or may not reboot.
        """

        phyResponse = self.broadcastPhysicalInterfaces()

        if phyResponse.getQDPCommand() != CmdID.C1_PHY:
            print("broadcast error.")
            return

        oldIP = phyResponse.strEthernetIPAddress()
        basePort = phyResponse.strBasePort()
        mac = phyResponse.strEthernetMACAddressHigh32() + ':' + \
              phyResponse.strEthernetMACAddressLow16()
        # keep original parameters
        setRequest = c1_sphy(phyResponse.getPacketBytes())

        # setRequest.setSerialNumber(self.getSerialNumber())
        setRequest.setSerialInterface1IPAddress(
            dottedIP2Num(aQ330SettingData.serialInterface1IPAddress))
        setRequest.setSerialInterface2IPAddress(
            dottedIP2Num(aQ330SettingData.serialInterface2IPAddress))
        setRequest.setBasePort(int(aQ330SettingData.basePort))
        setRequest.setSerialInterface1Baud(int(
            aQ330SettingData.serialInterface1Baud))
        setRequest.setSerialInterface2Baud(int(
            aQ330SettingData.serialInterface2Baud))
        setRequest.setSerialInterface1Flags(int(
            aQ330SettingData.serialInterface1Flags))
        setRequest.setSerialInterface2Flags(int(
            aQ330SettingData.serialInterface2Flags))
        setRequest.setEthernetFlags(int(aQ330SettingData.ethernetFlags))
        setRequest.setSerialInterface1Throttle(int(
            aQ330SettingData.serialInterface1Throttle))
        setRequest.setSerialInterface2Throttle(int(
            aQ330SettingData.serialInterface2Throttle))

        if self.platform == 'win32':
            Popen("arp -d")
            Popen("arp -s " + oldIP + ' ' + mac.replace(':', '-', 5))
        elif self.platform[:-1] == 'linux':
            # delete the host whose IP is the same as mine.
            Popen("/sbin/arp -s " + oldIP + ' ' + mac)
        elif self.platform == 'darwin':
            Popen("/usr/sbin/arp -s %s %s" % (oldIP, mac))
        else:
            print('This OS is not supported.')

        # Real IP registration to GUARANTEE 'saveTOEEPROM' and 'reboot' work
        self.setIPAddress(oldIP)
        self.setAuthCode(self.getAuthCode())
        self.setBasePort(int(basePort))
        self.setSerialNumber(self.getSerialNumber())

        try:
            self.register()
        except:
            print("Error: can't register using real IP.")
            return 0

        # now updated
        try:
            # moved from broadcast registering. In this way, even
            # if setting IP fails, we still have chances to try again
            ret = self.sendCommand(setRequest, receiveMultiple=1)
            self.saveToEEPROM()
            sleep(1)
            self.reboot()
            sleep(2)
            return 1
        except QDP_C1_CERR as e:
            print(e)
            self.deregister()
            return 0

    def getADVPhysicalInterfaces(self, phyIfsNum, requireRegister=1):
        """
        Real Register for the advance physical interfaces
        """
        if requireRegister:
            self.realRegister()
        c2rqphy = c2_rqphy()
        c2rqphy.setPhysicalInterfaceNumber(phyIfsNum)
        phyResponses = self.sendCommand(c2rqphy, receiveMultiple=1)
        if requireRegister:
            self.deregister()

        for phyResponse in phyResponses:  # return a list (c1_err and c1_phy)
            if phyResponse.getQDPCommand() == CmdID.C2_PHY:
                break
        return phyResponse

    def getAllPhysicalInterfaces(self,
                                 requireRegister=1,
                                 reqMemSize=1,
                                 phyResponse=None):
        """
        Get all physical Interfaces including 3 advanced. Register once to
        save running time. Return a tuple
        """
        if requireRegister:
            phyResponse = self.realRegister()

        advPhyResponses = []
        phyIfsNums = (0, 1, 3)
        for i in phyIfsNums:
            advPhyResponse = self.getADVPhysicalInterfaces(
                phyIfsNum=i, requireRegister=0)
            advPhyResponses.append(advPhyResponse)

        dataPortMemorySizes = None
        if reqMemSize:
            fix = self.sendCommand(c1_rqfix())
            dataPortMemorySizes = []
            dataPortMemorySizes.append(fix.strDataPort1PacketMemorySize())
            dataPortMemorySizes.append(fix.strDataPort2PacketMemorySize())
            dataPortMemorySizes.append(fix.strDataPort3PacketMemorySize())
            dataPortMemorySizes.append(fix.strDataPort4PacketMemorySize())

        if requireRegister:
            self.deregister()
        return (phyResponse,
                advPhyResponses[0],
                advPhyResponses[1],
                advPhyResponses[2],
                dataPortMemorySizes)

    def setAllPhysicalInterfaces(self,
                                 aQ330SettingData,
                                 aQ330AdvSettingData,
                                 allPhy=None):
        """
        Set/Change an all physical iterfaces
        After changed, the q330 may or may not reboot.
        """
        phyRes = self.realRegister()
        if not allPhy:
            allPhy = self.getAllPhysicalInterfaces(
                requireRegister=0, reqMemSize=0, phyResponse=phyRes)

        # keep original parameters
        setRequest = c1_sphy(allPhy[0].getPacketBytes())

        # setRequest.setSerialNumber(self.getSerialNumber())
        setRequest.setSerialInterface1IPAddress(
            dottedIP2Num(aQ330SettingData.serialInterface1IPAddress))
        setRequest.setSerialInterface2IPAddress(
            dottedIP2Num(aQ330SettingData.serialInterface2IPAddress))
        setRequest.setBasePort(int(aQ330SettingData.basePort))
        setRequest.setSerialInterface1Baud(int(
            aQ330SettingData.serialInterface1Baud))
        setRequest.setSerialInterface2Baud(int(
            aQ330SettingData.serialInterface2Baud))
        setRequest.setSerialInterface1Flags(int(
            aQ330SettingData.serialInterface1Flags))
        setRequest.setSerialInterface2Flags(int(
            aQ330SettingData.serialInterface2Flags))
        setRequest.setEthernetFlags(int(aQ330SettingData.ethernetFlags))
        setRequest.setSerialInterface1Throttle(int(
            aQ330SettingData.serialInterface1Throttle))
        setRequest.setSerialInterface2Throttle(int(
            aQ330SettingData.serialInterface2Throttle))

        setAdvRequest = [None, None, None]
        for i in range(3):
            setAdvRequest[i] = c2_sphy2(
                allPhy[i + 1].getPacketBytes())  # c2_sphy?
            setAdvRequest[i].setPhysicalInterfaceNumber(
                aQ330AdvSettingData.physicalInterfaceNumber[i])
            setAdvRequest[i].setDataPortNumber(
                int(aQ330AdvSettingData.dataPortNumber[i]))
            setAdvRequest[i].setModemInitialization(
                aQ330AdvSettingData.modemInitialization[i])
            setAdvRequest[i].setPhoneNumber(
                aQ330AdvSettingData.phoneNumber[i])
            setAdvRequest[i].setDialOutUserName(
                aQ330AdvSettingData.userName[i])  # DialIn/OutUserName?
            setAdvRequest[i].setDialOutPassword(
                aQ330AdvSettingData.password[i])  # DialIn/OutPassword?
            setAdvRequest[i].setMemoryTriggerLevel(
                aQ330AdvSettingData.MemoryTriggerLevel[i])
            setAdvRequest[i].setFlags(
                aQ330AdvSettingData.flags[i])
            setAdvRequest[i].setRetryIntervalOrPowerOffTime(
                int(aQ330AdvSettingData.retryIntervalOrPowerOffTime[i]))
            setAdvRequest[i].setInterval(
                int(aQ330AdvSettingData.interval[i]))
            setAdvRequest[i].setWebServerBPSLimit(
                int(aQ330AdvSettingData.webServerBPSLimit[i]))
            setAdvRequest[i].setPointOfContactOrBalerIPAddress(
                dottedIP2Num(
                    aQ330AdvSettingData.pointOfContactOrBalerIPAddress[i]))
            setAdvRequest[i].setBalerSecondaryIPAddress(
                dottedIP2Num(aQ330AdvSettingData.balerSecondaryIPAddress[i]))
            setAdvRequest[i].setPointOfContactPortNumber(
                int(aQ330AdvSettingData.pointOfContactPortNumber[i]))
            setAdvRequest[i].setBalerRetries(
                int(aQ330AdvSettingData.balerRetries[i]))
            setAdvRequest[i].setBalerRegistrationTimeout(
                int(aQ330AdvSettingData.balerRegistrationTimeout[i]))
            setAdvRequest[i].setSerialBaudRate(
                int(aQ330AdvSettingData.serialBaudRate[i]))

        # now updated
        try:
            # moved from broadcast registering. In this way, even
            # if seting IP fails, we still have chances to try again
            ret = self.sendCommand(setRequest, receiveMultiple=1)
            for i in range(3):
                self.sendCommand(setAdvRequest[i], receiveMultiple=1)
            self.saveToEEPROM()
            sleep(1)
            self.reboot()
            sleep(2)
            return 1
        except QDP_C1_CERR as e:
            print(e)
            self.deregister()
            return 0

    def getDataPort(self, portnumber):
        self.realRegister()
        try:
            return self.sendCommand(c1_rqlog(portnumber), receiveMultiple=1)
        finally:
            self.deregister()

    def getTokenSet(self, whichSet, requireRegister=None):
        if requireRegister:
            self.realRegister()
        try:
            # was called like Q330.getTokenSet(self, whichSet)
            # which appears to be incorrect. I've made the param explicitly
            # assigned, as the above (wrong) construction seems to assign self
            # as the whichSet input param, which is incorrect.
            return super().getTokenSet(whichSet=whichSet)
        finally:
            if requireRegister:
                self.deregister()
            else:
                pass

    def setTokenSet(self, tokens, whichSet, requireRegister=None):
        if requireRegister:
            self.realRegister()
        try:
            chunks = self.rebuidChunks(tokens, whichSet)
            for chunk in chunks:
                mem = chunk.c1_mem
                smem = c1_smem(mem.getPacketBytes())
                smem.setMemoryType(whichSet)
                self.sendCommand(smem)

            if requireRegister:
                self.saveToEEPROM()
                sleep(1)
                self.reboot()
                sleep(2)
        except QDP_C1_CERR as e:
            print(e)
            if requireRegister: self.deregister()

    def setTokenSet4Bin(self, newSet, whichSet, requireRegister=None):
        """
        just use hal's quaser setTokenSet which is a little different
        from my setTokenSet in HCSQ330.
        """
        if requireRegister:
            self.realRegister()
        try:
            # another case where super method was probably called incorrectly
            super().setTokenSet(newSet=newSet, whichSet=whichSet)
        finally:
            if requireRegister:
                self.deregister()
            else:
                pass

    def rebuidChunks(self, tokens, whichSet):
        if not tokens:
            return ''

        tokenBytes = ''
        for token in tokens:
            tokenBytes += token.getTokenBytes()

        chunkDataSize = 438
        startingPoint = 0
        # we have all the bytes, lets break them up into
        # chunks
        chunkBytes = []
        while startingPoint < len(tokenBytes):
            chunkBytes.append(
                tokenBytes[startingPoint:startingPoint + chunkDataSize])
            startingPoint += chunkDataSize

        chunks = []
        curAddress = 0
        currentSegmentNumber = 1
        for chunkData in chunkBytes:
            # we need to create the segment header
            segHeader = struct.pack(">HH", currentSegmentNumber,
                                    len(chunkBytes))
            mem = c1_mem()
            mem.setMemoryContents(segHeader + chunkData)
            mem.setStartingAddress(448 * (currentSegmentNumber - 1))
            mem.setByteCount(len(chunkData) + 4)
            mem.setMemoryType(whichSet)
            currentSegmentNumber += 1
            chunks.append(TokenChunk(mem))

        return chunks

    def eraseModSectors(self, first, last, requireRegistration=None):
        if first < FIRSTUSABLE or last < FIRSTUSABLE or first > last:
            return

        if requireRegistration:
            self.realRegister()
        c1erase = c1_erase()
        i = last - 1
        while i > first or i == first:
            c1erase.setFirstSector(i)
            c1erase.setLastSector(i)
            self.sendCommand(c1erase)
            i -= 1
        if requireRegistration: self.deregister()

    def eraseModules(self, startAddr4FreeFlash, requireRegistration=None):
        if requireRegistration:
            self.realRegister()
            c1free = self.sendCommand(c1_rqfree())  # may not called twice
            startAddr4FreeFlash = c1free.getStartingByteAddressForFreeFlash()
        cursector = startAddr4FreeFlash / SECTOR_SIZE
        if cursector > (SECTOR_COUNT - 1):
            cursector = SECTOR_COUNT - 1
        erasecount = cursector - FIRSTUSABLE + 1
        first = FIRSTUSABLE
        last = cursector + 1
        self.eraseModSectors(first, last)
        if requireRegistration:
            self.deregister()
