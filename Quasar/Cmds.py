# A single module that imports all of the command packet classes into
# it's namespace.  This is a convenient module to import * from, when
# doing a lot of back and forth communication with the Q330.
from Quasar.CmdID import CmdID
from Quasar.Commands.c1_cack import c1_cack
from Quasar.Commands.c1_cerr import c1_cerr
from Quasar.Commands.c1_com import c1_com
from Quasar.Commands.c1_ctrl import c1_ctrl
from Quasar.Commands.c1_dcp import c1_dcp
from Quasar.Commands.c1_dev import c1_dev
from Quasar.Commands.c1_dsrv import c1_dsrv
from Quasar.Commands.c1_erase import c1_erase
from Quasar.Commands.c1_fix import c1_fix
from Quasar.Commands.c1_flgs import c1_flgs
from Quasar.Commands.c1_free import c1_free
from Quasar.Commands.c1_gid import c1_gid
from Quasar.Commands.c1_glob import c1_glob
from Quasar.Commands.c1_log import c1_log
from Quasar.Commands.c1_man import c1_man, c1_sman
from Quasar.Commands.c1_mem import c1_mem, c1_smem
from Quasar.Commands.c1_mod import c1_mod
from Quasar.Commands.c1_mrt import c1_mrt
from Quasar.Commands.c1_mysn import c1_mysn
from Quasar.Commands.c1_phy import c1_phy
from Quasar.Commands.c1_ping import c1_ping
from Quasar.Commands.c1_pollsn import c1_pollsn
from Quasar.Commands.c1_pulse import c1_pulse
from Quasar.Commands.c1_pwr import c1_pwr
from Quasar.Commands.c1_qcal import c1_qcal
from Quasar.Commands.c1_rcnp import c1_rcnp
from Quasar.Commands.c1_rqcom import c1_rqcom
from Quasar.Commands.c1_rqdcp import c1_rqdcp
from Quasar.Commands.c1_rqdev import c1_rqdev
from Quasar.Commands.c1_rqfix import c1_rqfix
from Quasar.Commands.c1_rqflgs import c1_rqflgs
from Quasar.Commands.c1_rqfree import c1_rqfree
from Quasar.Commands.c1_rqgid import c1_rqgid
from Quasar.Commands.c1_rqglob import c1_rqglob
from Quasar.Commands.c1_rqlog import c1_rqlog
from Quasar.Commands.c1_rqman import c1_rqman
from Quasar.Commands.c1_rqmem import c1_rqmem
from Quasar.Commands.c1_rqmod import c1_rqmod
from Quasar.Commands.c1_rqphy import c1_rqphy
from Quasar.Commands.c1_rqpwr import c1_rqpwr
from Quasar.Commands.c1_rqrt import c1_rqrt
from Quasar.Commands.c1_rqsc import c1_rqsc
from Quasar.Commands.c1_rqspp import c1_rqspp
from Quasar.Commands.c1_rqsrv import c1_rqsrv
from Quasar.Commands.c1_rqstat import c1_rqstat
from Quasar.Commands.c1_rqthn import c1_rqthn
from Quasar.Commands.c1_rt import c1_rt
# added by JDEdwards
from Quasar.Commands.c1_sauth import c1_sauth
from Quasar.Commands.c1_sc import c1_sc, c1_ssc
from Quasar.Commands.c1_scnp import c1_scnp
from Quasar.Commands.c1_scom import c1_scom
from Quasar.Commands.c1_sdev import c1_sdev
from Quasar.Commands.c1_sglob import c1_sglob
from Quasar.Commands.c1_slog import c1_slog
from Quasar.Commands.c1_sout import c1_sout
from Quasar.Commands.c1_sphy import c1_sphy
from Quasar.Commands.c1_spp import c1_spp, c1_sspp
from Quasar.Commands.c1_spwr import c1_spwr
from Quasar.Commands.c1_srtc import c1_srtc
from Quasar.Commands.c1_srvch import c1_srvch
from Quasar.Commands.c1_srvrsp import c1_srvrsp
from Quasar.Commands.c1_stat import c1_stat
from Quasar.Commands.c1_stop import c1_stop
from Quasar.Commands.c1_thn import c1_thn
from Quasar.Commands.c1_umsg import c1_umsg
from Quasar.Commands.c1_vco import c1_vco
from Quasar.Commands.c1_web import c1_web
from Quasar.Commands.c1_wstat import c1_wstat
from Quasar.Commands.c2_amass import c2_amass, c2_samass
from Quasar.Commands.c2_back import c2_back
from Quasar.Commands.c2_bcmd import c2_bcmd
from Quasar.Commands.c2_boff import c2_boff
from Quasar.Commands.c2_brdy import c2_brdy
from Quasar.Commands.c2_bresp import c2_bresp
from Quasar.Commands.c2_epcfg import c2_epcfg
from Quasar.Commands.c2_epd import c2_epd
from Quasar.Commands.c2_epresp import c2_epresp
from Quasar.Commands.c2_eptun import c2_eptun
from Quasar.Commands.c2_gps import c2_gps, c2_sgps
from Quasar.Commands.c2_inst import c2_inst
from Quasar.Commands.c2_md5 import c2_md5
from Quasar.Commands.c2_phy import c2_phy
from Quasar.Commands.c2_poc import c2_poc
from Quasar.Commands.c2_qv import c2_qv
from Quasar.Commands.c2_regchk import c2_regchk
from Quasar.Commands.c2_regresp import c2_regresp
from Quasar.Commands.c2_rqamass import c2_rqamass
from Quasar.Commands.c2_rqepcfg import c2_rqepcfg
from Quasar.Commands.c2_rqepd import c2_rqepd
from Quasar.Commands.c2_rqgps import c2_rqgps
from Quasar.Commands.c2_rqmd5 import c2_rqmd5
from Quasar.Commands.c2_rqphy import c2_rqphy
from Quasar.Commands.c2_rqqv import c2_rqqv
from Quasar.Commands.c2_rqumsgs import c2_rqumsgs
from Quasar.Commands.c2_rqwin import c2_rqwin
from Quasar.Commands.c2_sbpwr import c2_sbpwr
from Quasar.Commands.c2_sepcfg import c2_sepcfg
from Quasar.Commands.c2_snapt import c2_snapt
from Quasar.Commands.c2_sphy import c2_sphy
from Quasar.Commands.c2_terr import c2_terr, c3_annc
from Quasar.Commands.c2_umsgs import c2_umsgs
from Quasar.Commands.c2_vack import c2_vack
from Quasar.Commands.c2_win import c2_win, c2_swin
from Quasar.Commands.c3_bcfg import c3_bcfg
# added by Kanglin
from Quasar.Commands.c3_rqannc import c3_rqannc, c2_terc
from Quasar.Commands.c3_rqbcfg import c3_rqbcfg
from Quasar.Commands.c3_sannc import c3_sannc
from Quasar.Commands.c3_sbcfg import c3_sbcfg
# c2_dep and c2_depr are deprecated
# from Quasar.Commands.c2_dep import *
# from Quasar.Commands.c2_depr import *
from Quasar.Commands.rl_cmd import rl_cmd

# A mapping of QDP Command IDs to the classes that represent
# them.  This is used during communication, to dynamically
# create packet instances
cmdToClass = {
    CmdID.C1_POLLSN : c1_pollsn,
    CmdID.C1_MYSN   : c1_mysn,
    CmdID.C1_RQSRV  : c1_rqsrv,
    CmdID.C1_SRVCH  : c1_srvch,
    CmdID.C1_SRVRSP : c1_srvrsp,
    CmdID.C1_CERR   : c1_cerr,
    CmdID.C1_PING   : c1_ping,
    CmdID.C1_CACK   : c1_cack,
    CmdID.C1_DSRV   : c1_dsrv,
    CmdID.C1_STAT   : c1_stat,
    CmdID.C1_RQSTAT : c1_rqstat,
    CmdID.C1_PHY    : c1_phy,
    CmdID.C1_RQPHY  : c1_rqphy,
    CmdID.C1_SPHY   : c1_sphy,
    CmdID.C1_LOG    : c1_log,
    CmdID.C1_SLOG   : c1_slog,
    CmdID.C1_RQLOG  : c1_rqlog,
    CmdID.C1_CTRL   : c1_ctrl,
    CmdID.C1_SGLOB  : c1_sglob,
    CmdID.C1_GLOB   : c1_glob,
    CmdID.C1_RQGLOB : c1_rqglob,
    CmdID.C1_FIX    : c1_fix,
    CmdID.C1_RQFIX  : c1_rqfix,
    CmdID.C1_MAN    : c1_man,
    CmdID.C1_RQMAN  : c1_rqman,
    CmdID.C1_SMAN   : c1_sman,
    CmdID.C1_WSTAT  : c1_wstat,
    CmdID.C1_VCO    : c1_vco,
    CmdID.C1_PULSE  : c1_pulse,
    CmdID.C1_QCAL   : c1_qcal,
    CmdID.C1_STOP   : c1_stop,
    CmdID.C1_RQRT   : c1_rqrt,
    CmdID.C1_RT     : c1_rt,
    CmdID.C1_MRT    : c1_mrt,
    CmdID.C1_RQTHN  : c1_rqthn,
    CmdID.C1_THN    : c1_thn,
    CmdID.C1_RQGID  : c1_rqgid,
    CmdID.C1_GID    : c1_gid,
    CmdID.C1_SCNP   : c1_scnp,
    CmdID.C1_RCNP   : c1_rcnp,
    CmdID.C1_SRTC   : c1_srtc,
    CmdID.C1_SOUT   : c1_sout,
    CmdID.C1_SSPP   : c1_sspp,
    CmdID.C1_RQSPP  : c1_rqspp,
    CmdID.C1_SPP    : c1_spp,
    CmdID.C1_SSC    : c1_ssc,
    CmdID.C1_SC     : c1_sc,
    CmdID.C1_RQSC   : c1_rqsc,
    CmdID.C1_UMSG   : c1_umsg,
    CmdID.C1_PWR    : c1_pwr,
    CmdID.C1_SPWR   : c1_spwr,
    CmdID.C1_RQPWR  : c1_rqpwr,
    CmdID.C1_WEB    : c1_web,
    CmdID.C1_RQFLGS : c1_rqflgs,
    CmdID.C1_FLGS   : c1_flgs,
    CmdID.C1_RQDCP  : c1_rqdcp,
    CmdID.C1_DCP    : c1_dcp,
    CmdID.C1_DEV    : c1_dev,
    CmdID.C1_RQDEV  : c1_rqdev,
    CmdID.C1_SDEV   : c1_sdev,
    CmdID.C1_MOD    : c1_mod,
    CmdID.C1_RQMOD  : c1_rqmod,
    CmdID.C1_ERASE  : c1_erase,
    CmdID.C1_MEM    : c1_mem,
    CmdID.C1_RQMEM  : c1_rqmem,
    CmdID.C1_SMEM   : c1_smem,
    CmdID.C1_RQFREE : c1_rqfree,
    CmdID.C1_FREE   : c1_free,
    CmdID.C2_SPHY   : c2_sphy,
    CmdID.C2_RQPHY  : c2_rqphy,
    CmdID.C2_PHY    : c2_phy,
    CmdID.C2_SGPS   : c2_sgps,
    CmdID.C2_RQGPS  : c2_rqgps,
    CmdID.C2_GPS    : c2_gps,
    CmdID.C2_SWIN   : c2_swin,
    CmdID.C2_RQWIN  : c2_rqwin,
    CmdID.C2_WIN    : c2_win,
    CmdID.C2_AMASS  : c2_amass,
    CmdID.C2_RQAMASS: c2_rqamass,
    CmdID.C2_SAMASS : c2_samass,
    CmdID.C2_SBPWR  : c2_sbpwr,
    CmdID.C2_POC    : c2_poc,
    CmdID.C2_BACK   : c2_back,
    CmdID.C2_VACK   : c2_vack,
    CmdID.C2_BRDY   : c2_brdy,
    CmdID.C2_BOFF   : c2_boff,
    CmdID.C2_BCMD   : c2_bcmd,
    CmdID.C2_BRESP  : c2_bresp,
    CmdID.C2_REGCHK : c2_regchk,
    CmdID.C2_REGRESP: c2_regresp,
    CmdID.C2_INST   : c2_inst,
    CmdID.C2_RQQV   : c2_rqqv,
    CmdID.C2_QV     : c2_qv,
    CmdID.C2_RQMD5  : c2_rqmd5,
    CmdID.C2_SNAPT  : c2_snapt,
    CmdID.C2_MD5    : c2_md5,
    #CmdID.C2_DEP    : c2_dep, # Overwritten by TERC
    #CmdID.C2_DEPR   : c2_depr, # Overwritten by TERR
    CmdID.RL_CMD    : rl_cmd,
    #added by kanglin
    CmdID.C2_TERR   : c2_terr,
    CmdID.C2_TERC   : c2_terc,
    CmdID.C3_ANNC   : c3_annc,
    CmdID.C3_RQANNC : c3_rqannc,
    CmdID.C3_SANNC  : c3_sannc,
    #added by JDEdwards
    CmdID.C1_SAUTH  : c1_sauth,
    CmdID.C2_RQEPD  : c2_rqepd,
    CmdID.C2_EPD    : c2_epd,
    CmdID.C2_RQEPCFG: c2_rqepcfg,
    CmdID.C2_EPCFG  : c2_epcfg,
    CmdID.C2_SEPCFG : c2_sepcfg,
    CmdID.C2_EPTUN  : c2_eptun,
    CmdID.C2_EPRESP : c2_epresp,
    CmdID.C2_RQUMSGS: c2_rqumsgs,
    CmdID.C2_UMSGS  : c2_umsgs,
    CmdID.C3_BCFG   : c3_bcfg,
    CmdID.C3_RQBCFG : c3_rqbcfg,
    CmdID.C3_SBCFG  : c3_sbcfg,
    CmdID.C1_COM    : c1_com,
    CmdID.C1_RQCOM  : c1_rqcom,
    CmdID.C1_SCOM   : c1_scom,
}
