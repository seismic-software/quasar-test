#! /usr/bin/env python
from time import sleep
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

from Quasar.Cmds import c1_rqfix, c1_rqglob, c1_rqlog, c1_rqsc, c1_sglob, \
    c1_slog, c1_sphy, c1_ssc, c2_rqamass, c2_rqgps, c2_rqphy, c2_samass, \
    c2_sgps, c3_rqannc, c3_sannc
from Quasar.Commands.c2_sphy2 import c2_sphy2

from Quasar.Q330 import RobustQ330
from Quasar.QDPDevice import QDP_C1_CERR
from Quasar.Utils import Misc
from Quasar.XML import SaveXML
from Quasar.XML.Token import tags, t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, \
    t128, t129, t130, t131, t132, t133, t192


# global variable may have problem for 1-4 token loop

class XMLHandler(ContentHandler):
    def __init__(self):
        super().__init__()
        self.elts = ('Q330_Configuration', 'tokens1', 'tokens2', 'tokens3',
                     'tokens4', 'interfaces', 'data1', 'data2', 'data3',
                     'data4', 'global', 'sensctrl', 'slave', 'advser1',
                     'advser2', 'advether', 'gps', 'pll', 'automass',
                     'announce', 'others')

        self.curIndex = 0  # go down tag index to save time
        self.maxIndex = len(tags)
        self.tokens = []  # for original output
        self.tokenLists = ([], [], [], [])  # for rearrange token order
        self.configs = []  # for configuration

        self.firFilterNumber = 0
        self.firFilterName2Number = {'DEC10': 0}
        self.iirFilterName2Number = {}
        self.lcqNumber = 0
        self.sourceLCQName2Number = {}  # for t128-10
        self.commNumber = 0
        self.commName2Number = {}
        self.iirFilterNumber = 0
        self.iirFilterName2Number = {}
        self.detectorNumber = 0  # for both t132, t133
        self.detectorName2Number = {}
        self.controlDetNumber = 0
        self.controlDetName2Number = {}
        self.lcqDetectorNumber4ThisInvocation = 0  # for tokens1, 2, 3, or 4
        self.seedAndName2Number = {}
        self.seedAndNameNumber = 0

        # hold self.values for LCQToken unitl the following Tokne 131 done
        # so that we have the 'name' of <controldet> vs the 'controlDetectorNumber' of
        # <lcq>
        self.values4LCQToken = []
        self.ptr4lcq = 0

        # Some different Tag group may have the same sub tag, using the following
        # for tracking
        self.curTagGroup = 0

    def startDocument(self):
        pass

    def endDocument(self):
        totalLen = 0
        for i in range(len(self.tokens)):
            # print self.tokens[i]
            # print '\n'
            totalLen += len(self.tokens[i].getTokenBytes())  # for debug

    def startElement(self, name, attrs):
        self.value = None
        curIndex = self.getCurrentIndex(name)
        if curIndex == 2:
            self.values = {}
        elif curIndex in (8, 141, 142, 145):
            self.values = {}
            self.optionFields = {}
            self.firFilterNumber = 0
            self.iirFilterNumber = 0
            self.controlDetNumber = 0
            self.commNumber = 0
            self.iirFilterName2Number = {}
            self.commName2Number = {}
            self.sourceLCQName2Number = {}
            self.lcqDetectorNumber4ThisInvocation = 0
            self.detectorNumber = 0
            self.detectorName2Number = {}
            self.lcqNumber = 0
            self.seedAndName2Number = {}
            self.seedAndNameNumber = 0
        elif curIndex == 10:
            self.values = {}
        elif curIndex == 15:
            self.values = {}
        elif curIndex == 19:
            self.values = {}
        elif curIndex == 25:
            self.values = {}
            self.curTagGroup = 25
        elif curIndex == 35:
            self.values = {}
        elif curIndex == 46:
            self.values = []
        elif curIndex == 78:
            self.values = {}
        elif curIndex == 79:
            self.values = {}
            self.curTagGroup = 79
        elif curIndex == 84:
            self.subValues = {}
        elif curIndex == 89:
            self.values = {}
            self.curTagGroup = 89
        elif curIndex == 108:
            self.subValues = {}
            self.values = {}
            self.values['OptionBits'] = 0
            self.optionFields = {}
            self.numberOfDet2Run = 0  # for setting option bits of particular t128.

        elif curIndex == 135:
            self.subValues = []
            self.values = {}
            self.values['DetectorEquations'] = []
        elif curIndex == 146:
            self.values = {}
            self.values['SerialInterface1Flags'] = 0
            self.values['SerialInterface2Flags'] = 0
            self.values['SerialInterface3Flags'] = 0
            self.values['EthernetFlags'] = 0
            self.curTagGroup = 146
        elif curIndex in (182, 217, 218, 219):
            self.values = {}
            self.values['Flags'] = 0
            self.curTagGroup = 182  # data1, data2, data3 or data4
        elif curIndex in (198, 206, 207, 208, 209, 210):
            self.freq_map = 0
        elif curIndex == 220:
            self.values = {}
            self.subValues = {}
            self.values['AuxAndStatusSamplingRates'] = 0
            self.values['GainBitmap'] = 0
            self.values['FilterBitmap'] = 0
            self.values['InputBitmap'] = 0
            self.curTagGroup = 220
        elif curIndex == 270:
            self.values = {}
            self.subValues = {}
            self.curTagGroup = 270
        elif curIndex == 280:
            self.values = {}
        elif curIndex == 290:
            self.values = {}
            self.values['Flags'] = 0
            self.curTagGroup = 290
        elif curIndex == 320:
            self.values = {}
            self.values['Flags'] = 0
            self.curTagGroup = 320
        elif curIndex == 321:
            self.values = {}
            self.values['Flags'] = 0
            self.curTagGroup = 321
        elif curIndex == 323:
            self.values = {}
            self.values['TimingMode'] = 0
        elif curIndex == 333:
            self.values = {}
            self.values['PLLFlags'] = 0
            self.curTagGroup = 333
            # <pll>, <advser1>, <advser2>, <advether> have <interval>
        elif curIndex == 346:
            self.values = {}
        elif curIndex == 347:
            self.curTagGroup = 347
        elif curIndex == 356:
            self.curTagGroup = 356
        elif curIndex == 357:
            self.values = {}
            self.values['UnlockFlags'] = 0
            self.values['Flags1'] = 0
            self.values['Flags2'] = 0
            self.values['Flags3'] = 0
            self.values['Flags4'] = 0
            self.values['Flags5'] = 0
            self.values['Flags6'] = 0
            # both <announce> and <interfaces> have <eth_unlock>,<ser1_unlock>
            # and <ser2_unlock>
            self.curTagGroup = 357
        elif curIndex == 359:
            self.curTagGroup = 359
        elif curIndex == 372:
            self.curTagGroup = 372
        elif curIndex == 373:
            self.curTagGroup = 373
        elif curIndex == 374:
            self.curTagGroup = 374
        elif curIndex == 375:
            self.curTagGroup = 375
        elif curIndex == 376:
            self.curTagGroup = 376

    def endElement(self, name):
        name = Misc.killWhitespace(name)
        t = None  # for tokens
        c = None  # for config
        curIndex = self.getCurrentIndex(name)

        if curIndex == 1:
            pass
        elif curIndex in (
        8, 141, 142, 145):  # for controlDetNumber of token 128
            tokensLength = 0
            tnum = {8: 0, 141: 1, 142: 2, 145: 3}
            for i in range(self.ptr4lcq, len(self.tokens)):
                token = self.tokens[i]
                if token.getTokenID() == 128:
                    optionFields = token.getOptionFields()
                    if 'ControlDetector' in optionFields:
                        optionFields['ControlDetector'] = \
                        self.controlDetName2Number[
                            optionFields['ControlDetector']]
                        token.setOptionFields(optionFields)
                    token.createBinToken()
                tokensLength += len(token.getTokenBytes())

            # add to self.tokenLists
            self.rearrangeOrder(
                tnum[curIndex], self.ptr4lcq, len(self.tokens))
            self.ptr4lcq = len(self.tokens)
            if (tokensLength % 2) != 0:  # storage odd length
                t = t0()
                t.createBinToken()
                # yes need to update self.tokenLists for output
                self.tokenLists[tnum[curIndex]].append(t)
                self.ptr4lcq += 1  # yes need to update pointer
        elif curIndex == 9:
            t = t1()
            t.createBinToken(int(self.value))
        elif curIndex == 10:
            self.values['Network'] = str(self.value[1:-1])
        elif curIndex == 11:
            # change station ID/code here to make things easy
            self.values['StationID'] = str(self.value[1:-1])
            if len(self.values['StationID']) < 5:
                for i in range(5 - len(self.values['StationID'])):
                    self.values['StationID'] += " "
            t = t2()  # here for 'Network'
            t.createBinToken(self.values)
        elif curIndex == 12:
            t = t3()
            t.createBinToken(int(self.value))
        elif curIndex == 13:
            t = t5()
            t.createBinToken(int(self.value))
        elif curIndex == 14:
            t = t9()
            t.createBinToken(int(self.value))
        elif curIndex == 15:
            self.values['MessageLogSeedLocation'] = str(self.value[1:-1])
        elif curIndex == 16:
            self.values['MessageLogSeedName'] = str(self.value[1:-1])
        elif curIndex == 17:
            self.values['TimingLogSeedLocation'] = str(self.value[1:-1])
        elif curIndex == 18:
            self.values['TimingLogSeedName'] = str(self.value[1:-1])
            t = t7()
            t.createBinToken(self.values)
        elif curIndex == 19:
            self.values['ConfigurationStreamSeedLocation'] = str(
                self.value[1:-1])
        elif curIndex == 20:
            self.values['ConfigurationStreamSeedName'] = str(self.value[1:-1])
        elif curIndex == 21:
            self.values['Interval'] = int(self.value)
        elif curIndex == 22:
            self.values['Flags'] = Misc.bit2Int(0, int(self.value), 0)
        elif curIndex == 23:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 1)
        elif curIndex == 24:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 2)
            t = t8()
            t.createBinToken(self.values)
        elif curIndex == 25:
            t = t4()
            t.createBinToken(self.values)
        elif curIndex == 26:
            self.values['HighestPriorityPassword'] = str(self.value[1:-1])
        elif curIndex == 27:
            self.values['MiddlePriorityPassword'] = str(self.value[1:-1])
        elif curIndex == 28:
            self.values['LowestPriorityPassword'] = str(self.value[1:-1])
        elif curIndex == 29:
            if self.curTagGroup == 25:
                self.values['TimeoutInSeconds'] = int(self.value)
            elif self.curTagGroup in (359, 372, 373, 374, 375, 376):
                whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                           376: '6'}
                iname = 'TimeoutInMinutes' + whichDP[self.curTagGroup]
                self.values[iname] = int(self.value)
        elif curIndex == 30:
            self.values['MaximumBytesPerSecond'] = int(self.value)
        elif curIndex == 31:
            self.values['Verbosity'] = int(self.value)
        elif curIndex == 32:
            self.values['MaximumCPUPercentage'] = int(self.value)
        elif curIndex == 33:
            self.values['UDPPortNumber'] = int(self.value)
        elif curIndex == 34:
            self.values['MaximumMemoryToUseInKB'] = int(self.value)
            self.values['ReservedBytes'] = 0
        elif curIndex == 35:
            self.values['Spare'] = 0
            t = t6()
            t.createBinToken(self.values)
        elif curIndex == 36:
            self.values['TimezoneOffsetInSeconds'] = int(self.value)
        elif curIndex == 37:
            self.values['LossInMinuts'] = int(self.value)
        elif curIndex == 38:
            self.values['PLLLockedQuality'] = int(self.value)
        elif curIndex == 39:
            self.values['PLLTrackingQuality'] = int(self.value)
        elif curIndex == 40:
            self.values['PLLHoldQuality'] = int(self.value)
        elif curIndex == 41:
            self.values['PLLOffQuality'] = int(self.value)
        elif curIndex == 42:
            self.values['HighestHasBeenLockedQuality'] = int(self.value)
        elif curIndex == 43:
            self.values['LowestHasBeenLockedQuality'] = int(self.value)
        elif curIndex == 44:
            self.values['NeverBeenLockedQuality'] = int(self.value)
        elif curIndex == 45:
            self.values['ClockQualityFilter'] = int(self.value)
        elif curIndex in (46, 47, 48, 49, 50, 51, 52, 53,
                          54, 55, 56, 57, 58, 59, 60, 61,
                          62, 63, 64, 65, 66, 67, 68, 69,
                          70, 71, 72, 73, 74, 75, 76, 77):  # for token 192
            bitNumber = int(name[9:])  # 'commname_1' for example
            eventName = str(self.value[1:-1])
            en = Misc.killWhitespace(eventName)
            self.commName2Number[eventName] = self.commNumber
            self.commNumber += 1
            if en[:5] != 'COMM:':
                self.values[bitNumber] = eventName
            if curIndex == 77:  # suppose there is <commname_32>
                t = t192()
                t.createBinToken(self.values)
        elif curIndex == 78:
            self.firFilterNumber += 1
            self.values['FilterNumber'] = self.firFilterNumber
            self.values['FilterName'] = str(self.value[1:-1])
            self.firFilterName2Number[
                self.values['FilterName']] = self.firFilterNumber
            t = t130()
            t.createBinToken(self.values)
        elif curIndex == 79:
            self.values['FilterNumber'] = self.iirFilterNumber
            self.values['FilterName'] = self.values['name']
            self.iirFilterName2Number[
                self.values['FilterName']] = self.iirFilterNumber
            self.iirFilterNumber += 1
            t = t129()
            t.createBinToken(self.values)
        elif curIndex in (4, 80, 128):  # all are <name>s.
            self.values['name'] = str(self.value[1:-1])
        elif curIndex == 81:
            self.values['NumberOfSections'] = int(self.value)
        elif curIndex == 82:
            self.values['Gain'] = float(self.value)
        elif curIndex == 83:
            self.values['ReferenceFrequency'] = float(self.value)
        elif curIndex == 84:  # 1st section of iir ended
            self.values['Sections'] = []
            self.values['Sections'].append(self.subValues)
            self.subValues = {}
        elif curIndex == 85:
            self.subValues['cutratio'] = float(self.value)
        elif curIndex == 86:
            self.subValues['poles'] = int(self.value)
        elif curIndex == 87:
            if self.curTagGroup == 79:  # for <iir>
                self.subValues['high'] = int(self.value)
            elif self.curTagGroup == 89:  # for <threshold>
                self.values['HighLimitParameter'] = int(self.value)
            elif self.curTagGroup == 270:  # for <sensctrl>
                self.subValues['high'] = int(self.value)
        elif curIndex == 88:  # 2nd section of iir ended
            self.values['Sections'].append(self.subValues)
            self.subValues = {}
        elif curIndex == 89:
            self.values['DetectorNumber'] = self.detectorNumber
            self.values['Spare'] = 0
            self.values['DetectorName'] = self.values['name']
            self.detectorName2Number[self.values['name']] = self.detectorNumber
            self.detectorNumber += 1
            t = t133()
            t.createBinToken(self.values)
        elif curIndex == 90:
            try:
                self.values['DetectorFilterNumber'] = self.iirFilterName2Number[
                    self.value[1:-1]]
            except:
                self.values['DetectorFilterNumber'] = 255
        elif curIndex == 91:
            self.values['HysterisisParameter'] = int(self.value)
        elif curIndex == 92:
            self.values['WindowParameter'] = int(self.value)
        elif curIndex == 93:
            if self.curTagGroup == 89:  # for <threshold>
                self.values['HighLimitParameter'] = int(self.value)
            elif self.curTagGroup == 79:
                self.subValues['high'] = int(self.value)
            elif self.curTagGroup == 270:
                self.subValues['high'] = int(self.value)
        elif curIndex == 94:
            self.values['LowLimitParameter'] = int(self.value)
        elif curIndex == 95:
            self.values['TailParameter'] = int(self.value)
        elif curIndex == 96:
            self.values['DetectorNumber'] = self.detectorNumber
            self.values['Spare'] = 0
            self.values['DetectorName'] = self.values['name']
            self.detectorName2Number[self.values['name']] = self.detectorNumber
            self.detectorNumber += 1
            t = t132()
            t.createBinToken(self.values)
        elif curIndex == 97:
            self.values['FilhiParameter'] = int(self.value)
        elif curIndex == 98:
            self.values['FilloParameter'] = int(self.value)
        elif curIndex == 99:
            self.values['IwParameter'] = int(self.value)
        elif curIndex == 100:
            self.values['NhtParameter'] = int(self.value)
        elif curIndex == 101:
            self.values['X1ParameterOver2'] = int(self.value)
        elif curIndex == 102:
            self.values['X2ParameterOver2'] = int(self.value)
        elif curIndex == 103:
            self.values['X3ParameterOver2'] = int(self.value)
        elif curIndex == 104:
            self.values['XxParameter'] = int(self.value)
        elif curIndex == 105:
            self.values['TcParameter'] = int(self.value)
        elif curIndex == 106:
            self.values['WaParameter'] = int(self.value)
        elif curIndex == 107:
            self.values['AvParameter'] = int(self.value)
        elif curIndex == 108:  # lcq end.
            self.values['LCQReferenceNumber'] = self.lcqNumber
            self.lcqNumber += 1
            self.values['OptionFields'] = self.optionFields
            t = t128()
            t.setTokenID(128)
            t.setFields(self.values)
            # not 't.createBinToken(self.values)' until <controldet> done 
        elif curIndex == 109:
            self.values['LocationCode'] = str(self.value[1:-1])
        elif curIndex == 110:
            self.values['Seedname'] = str(self.value[1:-1])
            self.sourceLCQName2Number[self.values['Seedname']] = self.lcqNumber
        elif curIndex == 111:
            s = str(self.value)  # special, e.g. 10100000
            b2i = (128, 64, 32, 16, 8, 4, 2, 1)
            v = 0
            for i in range(len(s)):  # 10100000 -> a0 -> 160
                if int(s[i]):
                    v += b2i[i]
            self.values['Source'] = v * 256  # high 8 bits
        elif curIndex == 112:  # self.value is a int in low 8 bits in xml file
            self.values['Source'] += int(self.value)
        elif curIndex == 113:
            self.values['Rate'] = int(self.value)
        elif curIndex == 114:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 0)
        elif curIndex == 115:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 1)
        elif curIndex == 116:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 2)
        elif curIndex == 117:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 11)
        elif curIndex == 118:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 28)
        elif curIndex == 119:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 29)
        elif curIndex == 120:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 30)
        elif curIndex == 121:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], int(self.value), 31)
        elif curIndex == 122:
            self.optionFields['NumberOfPre-EventBuffers'] = int(self.value)
            # only if pre-eventbuffer is not zero, we set the bit
            if self.optionFields['NumberOfPre-EventBuffers']:
                self.values['OptionBits'] = Misc.bit2Int(
                    self.values['OptionBits'], 1, 3)
        elif curIndex == 123:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, 4)
            self.optionFields['GapThreshold'] = float(self.value)
        elif curIndex == 124:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, 5)
            self.optionFields['CalibrationDelayInSeconds'] = int(self.value)
        elif curIndex == 125:
            if int(self.value) != 255:
                self.values['OptionBits'] = Misc.bit2Int(
                    self.values['OptionBits'], 1, 6)
            self.optionFields['MaximumFrameCount'] = int(self.value)
        elif curIndex == 126:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, 9)
            # hold the name of 'controlDetector' and will be converted
            # to 'controlDetector' number after <controldet> done
            self.optionFields['ControlDetector'] = str(self.value[1:-1])
        elif curIndex == 127:
            bits = (12, 13, 14, 15, 16, 17, 18, 19)
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, bits[self.numberOfDet2Run])
            self.seedAndName2Number[self.values['Seedname'] +
                                    ':' + self.values[
                                        'name']] = self.seedAndNameNumber
            self.numberOfDet2Run += 1
            self.seedAndNameNumber += 1
            detOpt = Misc.bit2Int(0, self.subValues['run'], 0)
            detOpt = Misc.bit2Int(detOpt, self.subValues['log'], 1)
            detOpt = Misc.bit2Int(detOpt, self.subValues['msg'], 3)
            self.optionFields['DetectorToRun%d' % self.numberOfDet2Run] = (
                self.detectorName2Number[self.values['name']],
                self.lcqDetectorNumber4ThisInvocation,
                detOpt)
            self.lcqDetectorNumber4ThisInvocation += 1  # for this(token1) invocation
        elif curIndex == 129:
            self.subValues['run'] = int(self.value)
        elif curIndex == 130:
            self.subValues['log'] = int(self.value)
        elif curIndex == 131:
            self.subValues['msg'] = int(self.value)
        elif curIndex == 132:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, 7)
            self.optionFields['FIRMultiplier'] = float(self.value)
        elif curIndex == 133:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, 10)
            s = str(self.value[1:-1])
            self.optionFields['SourceLCQNumber'] = self.sourceLCQName2Number[s]
        elif curIndex == 134:
            s = str(self.value[1:-1])
            self.optionFields['DecimationFIRFilterNumber'] = \
            self.firFilterName2Number[s]
        elif curIndex == 135:
            self.values['ControlDetectorNumber'] = self.controlDetNumber
            self.values['ControlDetectorName'] = self.values['name']
            self.controlDetName2Number[
                self.values['name']] = self.controlDetNumber
            self.controlDetNumber += 1
            ms2bits = {'comm': 0, 'det': 64, 'cal': 128, 'op': 192}
            ops = {'leftpar': 0,
                   'rightpar': 1,
                   'not': 2,
                   'and': 3,
                   'or': 4,
                   'eor': 5,
                   'endoflist': 6}
            for i in range(len(self.subValues)):
                eq = self.subValues[i]
                if eq[0] == 'comm':
                    byte = ms2bits[eq[0]] + self.commName2Number[eq[1]]
                elif eq[0] == 'det':
                    byte = ms2bits[eq[0]] + self.seedAndName2Number[eq[1]]
                elif eq[0] == 'cal':
                    byte = ms2bits[eq[0]] + self.sourceLCQName2Number[eq[1]]
                else:
                    byte = ms2bits[eq[0]] + ops[eq[1]]
                self.values['DetectorEquations'].append(byte)
            t = t131()
            t.createBinToken(self.values)
        elif curIndex == 136:
            self.values['ControlDetectorOptions'] = int(self.value)
        elif curIndex == 137:
            self.subValues['op'] = str(self.value[1:-1])
        elif curIndex == 138:
            self.subValues['det']  = str(self.value[1:-1])
        elif curIndex == 139:
            self.subValues['comm'] = str(self.value[1:-1])
        elif curIndex == 140:
            self.subValues['cal'] = str(self.value[1:-1])
        elif curIndex == 141:
            pass
        elif curIndex == 142:
            pass
        elif curIndex == 143:
            self.values['OptionBits'] = Misc.bit2Int(
                self.values['OptionBits'], 1, 8)
            self.optionFields['NumberOfSamplesBetweenReports'] = int(self.value)
        elif curIndex == 144:
            s = str(self.value[1:-1])
            self.optionFields['OptionalIIRFilterToUse'] = \
            self.iirFilterName2Number[s]
        elif curIndex == 146:
            baud2i = {0: 0, 1200: 1, 2400: 2, 4800: 3, 9600: 4, 19200: 5,
                      38400: 6, 57600: 7, 115200: 8}
            self.values['SerialInterface1Baud'] = baud2i[
                self.values['SerialInterface1Baud']]
            self.values['SerialInterface2Baud'] = baud2i[
                self.values['SerialInterface2Baud']]
            c = SaveXML.SaveInterfaces(self.values)
        elif curIndex == 147:
            self.values['SerialInterface1IPAddress'] = Misc.dottedIP2Num(
                str(self.value))
        elif curIndex == 148:
            self.values['SerialInterface2IPAddress'] = Misc.dottedIP2Num(
                str(self.value))
        elif curIndex == 149:
            pass  # not set IP please
            # self.values['EthernetIPAddress'] = Misc.dottedIP2Num(str(self.value))
        elif curIndex == 150:
            self.values['BasePort'] = int(self.value)
        elif curIndex == 151:
            self.values['SerialInterface1Baud'] = int(self.value)
        elif curIndex == 152:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 0)
        elif curIndex == 153:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 2)
        elif curIndex == 154:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 3)
        elif curIndex == 155:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 4)
        elif curIndex == 156:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 5)
        elif curIndex == 157:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 6)
        elif curIndex == 158:
            self.values['SerialInterface1Flags'] = Misc.bit2Int(
                self.values['SerialInterface1Flags'], int(self.value), 7)
        elif curIndex == 159:
            v = int(self.value)
            if v == 1:
                self.values['SerialInterface1Flags'] = Misc.bit2Int(
                    self.values['SerialInterface1Flags'], int(self.value), 8)
            elif v == 2:
                self.values['SerialInterface1Flags'] = Misc.bit2Int(
                    self.values['SerialInterface1Flags'], int(self.value), 9)
        elif curIndex == 160:
            if self.curTagGroup == 146:  # both<interf...> and <announ...> have it
                self.values['SerialInterface1Flags'] = Misc.bit2Int(
                    self.values['SerialInterface1Flags'], int(self.value), 10)
            elif self.curTagGroup in (357, 359, 372, 373, 374, 375, 376):
                self.values['UnlockFlags'] = Misc.bit2Int(
                    self.values['UnlockFlags'], int(self.value), 8)
        elif curIndex == 161:
            self.values['SerialInterface2Baud'] = int(self.value)
        elif curIndex == 162:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 0)
        elif curIndex == 163:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 2)
        elif curIndex == 164:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 3)
        elif curIndex == 165:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 4)
        elif curIndex == 166:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 5)
        elif curIndex == 167:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 6)
        elif curIndex == 168:
            self.values['SerialInterface2Flags'] = Misc.bit2Int(
                self.values['SerialInterface2Flags'], int(self.value), 7)
        elif curIndex == 169:
            v = int(self.value)
            if v == 1:
                self.values['SerialInterface2Flags'] = Misc.bit2Int(
                    self.values['SerialInterface2Flags'], 1, 8)
            elif v == 2:
                self.values['SerialInterface2Flags'] = Misc.bit2Int(
                    self.values['SerialInterface2Flags'], 1, 9)
        elif curIndex == 170:
            if self.curTagGroup == 146:  # both <inter...> and <ann...> have it
                self.values['SerialInterface2Flags'] = Misc.bit2Int(
                    self.values['SerialInterface2Flags'], int(self.value), 10)
            elif self.curTagGroup in (357, 359, 372, 373, 374, 375, 376):
                self.values['UnlockFlags'] = Misc.bit2Int(
                    self.values['UnlockFlags'], int(self.value), 9)
        elif curIndex == 171:
            self.values['SerialInterface3Flags'] = Misc.bit2Int(
                self.values['SerialInterface3Flags'], int(self.value), 0)
        elif curIndex == 172:
            self.values['EthernetFlags'] = Misc.bit2Int(
                self.values['EthernetFlags'], int(self.value), 0)
        elif curIndex == 173:
            self.values['EthernetFlags'] = Misc.bit2Int(
                self.values['EthernetFlags'], int(self.value), 1)
        elif curIndex == 174:
            self.values['EthernetFlags'] = Misc.bit2Int(
                self.values['EthernetFlags'], int(self.value), 4)
        elif curIndex == 175:
            self.values['EthernetFlags'] = Misc.bit2Int(
                self.values['EthernetFlags'], int(self.value), 5)
        elif curIndex == 176:
            self.values['EthernetFlags'] = Misc.bit2Int(
                self.values['EthernetFlags'], int(self.value), 6)
        elif curIndex == 177:
            self.values['EthernetFlags'] = Misc.bit2Int(
                self.values['EthernetFlags'], int(self.value), 7)
        elif curIndex == 178:
            v = int(self.value)
            if v == 1:
                self.values['EthernetFlags'] = Misc.bit2Int(
                    self.values['EthernetFlags'], 1, 8)
            elif v == 2:
                self.values['EthernetFlags'] = Misc.bit2Int(
                    self.values['EthernetFlags'], 1, 9)
        elif curIndex == 179:
            if self.curTagGroup == 146:  # both <announce> and <interfaces> have it
                self.values['EthernetFlags'] = Misc.bit2Int(
                    self.values['EthernetFlags'], int(self.value), 10)
            elif self.curTagGroup in (357, 359, 372, 373, 374, 375, 376):
                self.values['UnlockFlags'] = Misc.bit2Int(
                    self.values['UnlockFlags'], int(self.value), 11)
        elif curIndex == 180:
            self.values['SerialInterface1Throttle'] = int(self.value)
        elif curIndex == 181:
            self.values['SerialInterface2Throttle'] = int(self.value)
        elif curIndex in (182, 217, 218, 219):
            c = SaveXML.SaveDataPort(self.values)
        elif curIndex == 183:
            self.values['DataPortNumber'] = int(self.value)
        elif curIndex == 184:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 0)
        elif curIndex == 185:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 2)
        elif curIndex == 186:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 3)
        elif curIndex == 187:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 4)
        elif curIndex == 188:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 8)
        elif curIndex == 189:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 9)
        elif curIndex == 190:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 10)
        elif curIndex == 191:
            self.values['PercentOfPacketBuffer'] = int(float(
                self.value) * 2.56 + 0.005)
        elif curIndex == 192:
            self.values['MTU'] = int(self.value)
        elif curIndex == 193:
            self.values['GroupCount'] = int(self.value)
        elif curIndex == 194:
            self.values['MaximumResendTimeout'] = int(float(self.value) * 10)
        elif curIndex == 195:
            self.values['GroupTimeout'] = int(float(self.value) * 10)
        elif curIndex == 196:
            self.values['MinimumResendTimeout'] = int(float(self.value) * 10)
        elif curIndex == 197:
            self.values['WindowSize'] = int(self.value)
        elif curIndex == 198:
            self.values['Channel1Freqs'] = self.freq_map
        elif curIndex == 199:
            if self.curTagGroup == 182:
                # affected by <scaling> <Hz>1.0<Hz>,
                # so using int(float(self.value))
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 0)
            elif self.curTagGroup == 220:
                self.subValues['Hz1'] = float(self.value)
        elif curIndex == 200:
            if self.curTagGroup == 182:
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 1)
            elif self.curTagGroup == 220:
                self.subValues['Hz10'] = float(self.value)
        elif curIndex == 201:
            if self.curTagGroup == 182:
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 2)
            elif self.curTagGroup == 220:
                self.subValues['Hz20'] = float(self.value)
        elif curIndex == 202:
            if self.curTagGroup == 182:
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 3)
            elif self.curTagGroup == 220:
                self.subValues['Hz40'] = float(self.value)
        elif curIndex == 203:
            if self.curTagGroup == 182:
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 4)
            elif self.curTagGroup == 220:
                self.subValues['Hz50'] = float(self.value)
        elif curIndex == 204:
            if self.curTagGroup == 182:
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 5)
            elif self.curTagGroup == 220:
                self.subValues['Hz100'] = float(self.value)
        elif curIndex == 205:
            if self.curTagGroup == 182:
                self.freq_map = Misc.bit2Int(
                    self.freq_map, int(float(self.value)), 6)
            elif self.curTagGroup == 220:
                self.subValues['Hz200'] = float(self.value)
        elif curIndex == 206:
            self.values['Channel2Freqs'] = self.freq_map
        elif curIndex == 207:
            self.values['Channel3Freqs'] = self.freq_map
        elif curIndex == 208:
            self.values['Channel4Freqs'] = self.freq_map
        elif curIndex == 209:
            self.values['Channel5Freqs'] = self.freq_map
        elif curIndex == 210:
            self.values['Channel6Freqs'] = self.freq_map
        elif curIndex == 211:
            self.values['AcknowledgeCount'] = int(self.value)
        elif curIndex == 212:
            self.values['AcknowledgeTimeout'] = int(float(self.value) * 10)
        elif curIndex == 213:
            try:
                self.values['EthernetThrottle'] = int(
                    1024000.0 / float(self.value) + 0.5)
            except:  # defeat
                self.values['EthernetThrottle'] = 0
        elif curIndex == 214:
            self.values['AlarmPercent'] = int(float(
                self.value) * 2.56 + 0.005)
        elif curIndex == 215:
            self.values['AutomaticFilters'] = int(self.value)
        elif curIndex == 216:
            self.values['ManualFilters'] = int(self.value)
        elif curIndex == 220:
            c = SaveXML.SaveGlobal(self.values)
        elif curIndex == 221:
            self.values['ClockTimeout'] = int(self.value)
        elif curIndex == 222:
            self.values['InitialVCO'] = int(self.value)
        elif curIndex == 223:
            self.values['GPSBackupPower'] = int(self.value)
        elif curIndex == 224:
            self.values['AuxAndStatusSamplingRates'] = Misc.bit2Int(
                self.values['AuxAndStatusSamplingRates'], int(self.value), 0)
        elif curIndex == 225:
            self.values['AuxAndStatusSamplingRates'] = Misc.bit2Int(
                self.values['AuxAndStatusSamplingRates'], int(self.value), 8)
        elif curIndex == 226:
            v = int(self.value)
            if v == 1:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 0)
            elif v == 2:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 1)
        elif curIndex == 227:
            v = int(self.value)
            if v == 1:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 2)
            elif v == 2:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 3)
        elif curIndex == 228:
            v = int(self.value)
            if v == 1:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 4)
            elif v == 2:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 5)
        elif curIndex == 229:
            v = int(self.value)
            if v == 1:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 6)
            elif v == 2:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 7)
        elif curIndex == 230:
            v = int(self.value)
            if v == 1:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 8)
            elif v == 2:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 9)
        elif curIndex == 231:
            v = int(self.value)
            if v == 1:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 10)
            elif v == 2:
                self.values['GainBitmap'] = Misc.bit2Int(
                    self.values['GainBitmap'], 1, 11)
        elif curIndex == 232:
            v = int(self.value)
            if v == 1:
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 0)
            elif v == 2:
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 1)
            elif v == 3:
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 0)
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 1)
        elif curIndex == 233:
            v = int(self.value)
            if v == 1:
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 2)
            elif v == 2:
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 3)
            elif v == 3:
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 2)
                self.values['FilterBitmap'] = Misc.bit2Int(
                    self.values['FilterBitmap'], 1, 3)
        elif curIndex == 234:
            self.values['InputBitmap'] = Misc.bit2Int(
                self.values['InputBitmap'], int(self.value), 0)
        elif curIndex == 235:
            self.values['InputBitmap'] = Misc.bit2Int(
                self.values['InputBitmap'], int(self.value), 1)
        elif curIndex == 236:
            self.values['InputBitmap'] = Misc.bit2Int(
                self.values['InputBitmap'], int(self.value), 2)
        elif curIndex == 237:
            self.values['InputBitmap'] = Misc.bit2Int(
                self.values['InputBitmap'], int(self.value), 3)
        elif curIndex == 238:
            self.values['InputBitmap'] = Misc.bit2Int(
                self.values['InputBitmap'], int(self.value), 4)
        elif curIndex == 239:
            self.values['InputBitmap'] = Misc.bit2Int(
                self.values['InputBitmap'], int(self.value), 5)
        elif curIndex == 240:
            v = int(self.value)
            if v == 1:
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 8)
            elif v == 2:
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 9)
            elif v == 3:
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 8)
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 9)
        elif curIndex == 241:
            v = int(self.value)
            if v == 1:
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 10)
            elif v == 2:
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 11)
            elif v == 3:
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 10)
                self.values['InputBitmap'] = Misc.bit2Int(
                    self.values['InputBitmap'], 1, 11)
        elif curIndex == 242:
            self.values['WebPort'] = int(self.value)
        elif curIndex == 243:
            self.values['ServerTimeout'] = int(self.value)
        elif curIndex == 244:
            self.values['DriftTolerance'] = int(self.value)
        elif curIndex == 245:
            self.values['JumpFilter'] = int(self.value)
        elif curIndex == 246:
            self.values['JumpThresholdInUSec'] = int(self.value)
        elif curIndex == 247:
            self.values['CalibratorOffset'] = int(self.value)
        elif curIndex == 248:
            self.values['SensorControlBitmap'] = int(self.value)
        elif curIndex == 249:
            self.values['SamplingPhase'] = int(self.value)
        elif curIndex == 250:
            self.values['GPSColdStartSeconds'] = int(self.value)
        elif curIndex == 251:
            self.values['UserTag'] = int(self.value)
        elif curIndex == 252:
            self.values['Channel1FreqBit0Scale'] = 1024 * self.subValues['Hz1']
            self.values['Channel1FreqBit1Scale'] = 1024 * self.subValues['Hz10']
            self.values['Channel1FreqBit2Scale'] = 1024 * self.subValues['Hz20']
            self.values['Channel1FreqBit3Scale'] = 1024 * self.subValues['Hz40']
            self.values['Channel1FreqBit4Scale'] = 1024 * self.subValues['Hz50']
            self.values['Channel1FreqBit5Scale'] = 1024 * self.subValues[
                'Hz100']
            self.values['Channel1FreqBit6Scale'] = 1024 * self.subValues[
                'Hz200']
        elif curIndex == 253:
            self.values['Channel2FreqBit0Scale'] = 1024 * self.subValues['Hz1']
            self.values['Channel2FreqBit1Scale'] = 1024 * self.subValues['Hz10']
            self.values['Channel2FreqBit2Scale'] = 1024 * self.subValues['Hz20']
            self.values['Channel2FreqBit3Scale'] = 1024 * self.subValues['Hz40']
            self.values['Channel2FreqBit4Scale'] = 1024 * self.subValues['Hz50']
            self.values['Channel2FreqBit5Scale'] = 1024 * self.subValues[
                'Hz100']
            self.values['Channel2FreqBit6Scale'] = 1024 * self.subValues[
                'Hz200']
        elif curIndex == 254:
            self.values['Channel3FreqBit0Scale'] = 1024 * self.subValues['Hz1']
            self.values['Channel3FreqBit1Scale'] = 1024 * self.subValues['Hz10']
            self.values['Channel3FreqBit2Scale'] = 1024 * self.subValues['Hz20']
            self.values['Channel3FreqBit3Scale'] = 1024 * self.subValues['Hz40']
            self.values['Channel3FreqBit4Scale'] = 1024 * self.subValues['Hz50']
            self.values['Channel3FreqBit5Scale'] = 1024 * self.subValues[
                'Hz100']
            self.values['Channel3FreqBit6Scale'] = 1024 * self.subValues[
                'Hz200']
        elif curIndex == 255:
            self.values['Channel4FreqBit0Scale'] = 1024 * self.subValues['Hz1']
            self.values['Channel4FreqBit1Scale'] = 1024 * self.subValues['Hz10']
            self.values['Channel4FreqBit2Scale'] = 1024 * self.subValues['Hz20']
            self.values['Channel4FreqBit3Scale'] = 1024 * self.subValues['Hz40']
            self.values['Channel4FreqBit4Scale'] = 1024 * self.subValues['Hz50']
            self.values['Channel4FreqBit5Scale'] = 1024 * self.subValues[
                'Hz100']
            self.values['Channel4FreqBit6Scale'] = 1024 * self.subValues[
                'Hz200']
        elif curIndex == 256:
            self.values['Channel5FreqBit0Scale'] = 1024 * self.subValues['Hz1']
            self.values['Channel5FreqBit1Scale'] = 1024 * self.subValues['Hz10']
            self.values['Channel5FreqBit2Scale'] = 1024 * self.subValues['Hz20']
            self.values['Channel5FreqBit3Scale'] = 1024 * self.subValues['Hz40']
            self.values['Channel5FreqBit4Scale'] = 1024 * self.subValues['Hz50']
            self.values['Channel5FreqBit5Scale'] = 1024 * self.subValues[
                'Hz100']
            self.values['Channel5FreqBit6Scale'] = 1024 * self.subValues[
                'Hz200']
        elif curIndex == 257:
            self.values['Channel6FreqBit0Scale'] = 1024 * self.subValues['Hz1']
            self.values['Channel6FreqBit1Scale'] = 1024 * self.subValues['Hz10']
            self.values['Channel6FreqBit2Scale'] = 1024 * self.subValues['Hz20']
            self.values['Channel6FreqBit3Scale'] = 1024 * self.subValues['Hz40']
            self.values['Channel6FreqBit4Scale'] = 1024 * self.subValues['Hz50']
            self.values['Channel6FreqBit5Scale'] = 1024 * self.subValues[
                'Hz100']
            self.values['Channel6FreqBit6Scale'] = 1024 * self.subValues[
                'Hz200']
        elif curIndex == 258:
            self.values['Channel1Offset'] = int(self.value)
        elif curIndex == 259:
            self.values['Channel2Offset'] = int(self.value)
        elif curIndex == 260:
            self.values['Channel3Offset'] = int(self.value)
        elif curIndex == 261:
            self.values['Channel4Offset'] = int(self.value)
        elif curIndex == 262:
            self.values['Channel5Offset'] = int(self.value)
        elif curIndex == 263:
            self.values['Channel6Offset'] = int(self.value)
        elif curIndex == 264:
            self.values['Channel1Gain'] = 1024 * float(self.value)
        elif curIndex == 265:
            self.values['Channel2Gain'] = 1024 * float(self.value)
        elif curIndex == 266:
            self.values['Channel3Gain'] = 1024 * float(self.value)
        elif curIndex == 267:
            self.values['Channel4Gain'] = 1024 * float(self.value)
        elif curIndex == 268:
            self.values['Channel5Gain'] = 1024 * float(self.value)
        elif curIndex == 269:
            self.values['Channel6Gain'] = 1024 * float(self.value)
        elif curIndex == 270:
            c = SaveXML.SaveSensor(self.values)
        elif curIndex == 271:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput1Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 272:
            self.subValues['function'] = int(self.value)
        elif curIndex == 273:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput2Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 274:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput3Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 275:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput4Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 276:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput5Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 277:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput6Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 278:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput7Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 279:
            self.subValues['high'] = Misc.bit2Int(0, self.subValues['high'], 8)
            self.values['SensorOutput8Definition'] = self.subValues[
                                                         'function'] + \
                                                     self.subValues['high']
        elif curIndex == 280:
            c = SaveXML.SaveSlave(self.values)
        elif curIndex == 281:
            v = int(self.value)
            if v == 0 or (v > 125 and v < 250):
                self.values['MaximumMainCurrent'] = v
            else:  # using default
                self.values['MaximumMainCurrent'] = 200
        elif curIndex == 282:
            v = int(self.value)
            if v > 1 and v < 6500:
                self.values['MinimumOffTime'] = v
            else:
                self.values['MinimumOffTime'] = 60
        elif curIndex == 283:  # stop here, not working
            pass  # so dangerous !!!
            # v = int(float(self.value) / 0.15 + 0.0005)
            # if v > 50 and v < 255:
            #    self.values['MinimumPSVoltage'] = v
            # else:
            #    self.values['MinimumPSVoltage'] = int(9.0 / 0.15)
        elif curIndex == 284:
            v = int(self.value)
            if v > 5 and v < 128:
                self.values['MaximumAntennaCurrent'] = v
            else:
                self.values['MaximumAntennaCurrent'] = 34
        elif curIndex == 285:
            v = int(self.value)
            if v > -40 and v < 0:
                self.values['MinimumTemperature'] = v
            else:
                self.values['MinimumTemperature'] = -28
        elif curIndex == 286:
            v = int(self.value)
            if v > 50 and v < 85:
                self.values['MaximumTemperature'] = v
            else:
                self.values['MaximumTemperature'] = 55
        elif curIndex == 287:
            v = int(self.value)
            if v > 1 and v < 20:
                self.values['TemperatureHysteresis'] = v
            else:
                self.values['TemperatureHysteresis'] = 5
        elif curIndex == 288:  # stop here
            pass  # so dangerous!!!
            # v = int(float(self.value) / 0.15 + 0.0005)
            # if v > 1 and v < 20:
            #    self.values['VoltageHysteresis'] = v
            # else:
            #    self.values['VoltageHysteresis'] = int(1.95/0.15)
        elif curIndex == 289:
            v = int(self.value)
            if v > 0 and v < 4095:
                self.values['DefaultVCOValue'] = int(self.value)
            else:
                self.values['DefaultVCOValue'] = 2048
        elif curIndex == 290:
            c = SaveXML.SaveAdvanced(0, self.values)
        elif curIndex == 291:
            self.values['PhysicalInterfaceNumber'] = int(self.value)
        elif curIndex == 292:
            self.values['DataPortNumber'] = int(self.value)
        elif curIndex == 293:
            self.values['ModemInitialization'] = str(self.value[1:-1])
        elif curIndex == 294:
            self.values['PhoneNumber'] = str(self.value[1:-1])
        elif curIndex == 295:
            self.values['DialOutUserName'] = str(self.value[1:-1])
        elif curIndex == 296:
            self.values['DialOutPassword'] = str(self.value[1:-1])
        elif curIndex == 297:
            self.values['DialInUserName'] = str(self.value[1:-1])
        elif curIndex == 298:
            self.values['DialInPassword'] = str(self.value[1:-1])
        elif curIndex == 299:
            self.values['MemoryTriggerLevel'] = int(self.value)
        elif curIndex == 300:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 0)
        elif curIndex == 301:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 1)
        elif curIndex == 302:
            v = int(self.value)
            if v == 1:
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 2)
            elif v == 2:
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 3)
            elif v == 3:
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 2)
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 3)
        elif curIndex == 303:
            v = int(self.value)
            if v == 1:
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 4)
            elif v == 2:
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 5)
            elif v == 3:
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 4)
                self.values['Flags'] = Misc.bit2Int(
                    self.values['Flags'], 1, 5)
        elif curIndex == 304:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 6)
        elif curIndex == 305:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 7)
        elif curIndex == 306:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 8)
        elif curIndex == 307:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 9)
        elif curIndex == 308:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 10)
        elif curIndex == 309:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 11)
        elif curIndex == 310:
            self.values['Flags'] = Misc.bit2Int(
                self.values['Flags'], int(self.value), 15)
        elif curIndex == 311:
            self.values['RetryIntervalOrPowerOffTime'] = int(self.value)
        elif curIndex == 312:
            if self.curTagGroup in (290, 320, 321):
                self.values['Interval'] = int(self.value)
            elif self.curTagGroup == 333:
                self.values['PLLUpdateIntervalInSeconds'] = int(self.value)
        elif curIndex == 313:
            self.values['WebServerBPSLimit'] = int(int(self.value) / 10.0 + 0.5)
        elif curIndex == 314:
            self.values['PointOfContactOrBalerIPAddress'] = Misc.dottedIP2Num(
                str(self.value))
        elif curIndex == 315:
            self.values['BalerSecondaryIPAddress'] = Misc.dottedIP2Num(
                str(self.value))
        elif curIndex == 316:
            self.values['PointOfContactPortNumber'] = int(self.value)
        elif curIndex == 317:
            self.values['BalerRetries'] = int(self.value)
        elif curIndex == 318:
            self.values['BalerRegistrationTimeout'] = int(self.value)
        elif curIndex == 319:
            self.values['RoutedPacketsTimeout'] = int(self.value)
        elif curIndex == 320:
            c = SaveXML.SaveAdvanced(1, self.values)
        elif curIndex == 321:
            baud2i = {0: 0, 1200: 1, 2400: 2, 4800: 3, 9600: 4, 19200: 5,
                      38400: 6, 57600: 7, 115200: 8}
            self.values['SerialBaudRate'] = baud2i[
                self.values['SerialBaudRate']]
            c = SaveXML.SaveAdvanced(3, self.values)
        elif curIndex == 322:
            self.values['SerialBaudRate'] = int(self.value)
        elif curIndex == 323:
            c = SaveXML.SaveGPSPLL('gps', self.values)
        elif curIndex == 324:
            v = int(self.value)
            if v == 1:
                self.values['TimingMode'] = Misc.bit2Int(
                    self.values['TimingMode'], 1, 0)
            elif v == 2:
                self.values['TimingMode'] = Misc.bit2Int(
                    self.values['TimingMode'], 1, 1)
            elif v == 3:
                self.values['TimingMode'] = Misc.bit2Int(
                    self.values['TimingMode'], 1, 0)
                self.values['TimingMode'] = Misc.bit2Int(
                    self.values['TimingMode'], 1, 1)
            elif v == 4:
                self.values['TimingMode'] = Misc.bit2Int(
                    self.values['TimingMode'], 1, 2)
        elif curIndex == 325:
            self.values['TimingMode'] = Misc.bit2Int(
                self.values['TimingMode'], int(self.value), 3)
        elif curIndex == 326:
            self.values['TimingMode'] = Misc.bit2Int(
                self.values['TimingMode'], int(self.value), 4)
        elif curIndex == 327:
            self.values['TimingMode'] = Misc.bit2Int(
                self.values['TimingMode'], int(self.value), 5)
        elif curIndex == 328:
            self.values['TimingMode'] = Misc.bit2Int(
                self.values['TimingMode'], int(self.value), 6)
        elif curIndex == 329:
            self.values['PowerCyclingMode'] = int(self.value)
        elif curIndex == 330:
            self.values['GPSOffTime'] = int(self.value)
        elif curIndex == 331:
            self.values['GPSResyncHour'] = int(self.value)
        elif curIndex == 332:
            self.values['GPSMaximumOnTime'] = int(self.value)
        elif curIndex == 333:
            c = SaveXML.SaveGPSPLL('pll', self.values)
        elif curIndex == 334:
            self.values['PLLLockUSecs'] = int(self.value)
        elif curIndex == 335:
            self.values['PLLFlags'] = Misc.bit2Int(
                self.values['PLLFlags'], int(self.value), 0)
        elif curIndex == 336:
            self.values['PLLFlags'] = Misc.bit2Int(
                self.values['PLLFlags'], int(self.value), 1)
        elif curIndex == 337:
            self.values['PLLFlags'] = Misc.bit2Int(
                self.values['PLLFlags'], int(self.value), 2)
        elif curIndex == 338:
            self.values['Pfrac'] = float(self.value)
        elif curIndex == 339:
            self.values['VCOSlope'] = float(self.value)
        elif curIndex == 340:
            self.values['VCOIntercept'] = float(self.value)
        elif curIndex == 341:
            self.values['MaxInitialKMRMS'] = float(self.value)
        elif curIndex == 342:
            self.values['InitialKMWeight'] = float(self.value)
        elif curIndex == 343:
            self.values['KMWeight'] = float(self.value)
        elif curIndex == 344:
            self.values['BestVCOWeight'] = float(self.value)
        elif curIndex == 345:
            self.values['KMDelta'] = float(self.value)
        elif curIndex == 346:
            c = SaveXML.SaveAutoMass(self.values)
        elif curIndex == 347:
            pass
        elif curIndex == 348:
            if self.curTagGroup == 347:
                self.values['Tolerance1A'] = int(self.value)
            else:
                self.values['Tolerance2A'] = int(self.value)
        elif curIndex == 349:
            if self.curTagGroup == 347:
                self.values['Tolerance1B'] = int(self.value)
            else:
                self.values['Tolerance2B'] = int(self.value)
        elif curIndex == 350:
            if self.curTagGroup == 347:
                self.values['Tolerance1C'] = int(self.value)
            else:
                self.values['Tolerance2C'] = int(self.value)
        elif curIndex == 351:
            if self.curTagGroup == 347:
                self.values['MaximumTries1'] = int(self.value)
            else:
                self.values['MaximumTries2'] = int(self.value)
        elif curIndex == 352:
            if self.curTagGroup == 347:
                self.values['NormalInterval1'] = int(self.value)
            else:
                self.values['NormalInterval2'] = int(self.value)
        elif curIndex == 353:
            if self.curTagGroup == 347:
                self.values['SquelchInterval1'] = int(self.value)
            else:
                self.values['SquelchInterval2'] = int(self.value)
        elif curIndex == 354:
            if self.curTagGroup == 347:
                self.values['SensorControlBitmap1'] = int(self.value)
            else:
                self.values['SensorControlBitmap2'] = int(self.value)
        elif curIndex == 355:
            if self.curTagGroup == 347:
                self.values['Duration1'] = int(float(self.value) * 100)
            else:
                self.values['Duration2'] = int(float(self.value) * 100)
        elif curIndex == 356:  # <group2>
            pass
        elif curIndex == 357:
            c = SaveXML.SaveAnnouncement(self.values)
        elif curIndex == 358:
            self.values['NumberOfActiveEntries'] = int(self.value)
        elif curIndex == 359:  # <dp1>
            pass
        elif curIndex == 360:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'DPIPAddress' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.dottedIP2Num(str(self.value))
        elif curIndex == 361:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'RouterIPAddress' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.dottedIP2Num(str(self.value))
        elif curIndex == 362:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'ResumeTimeInMinutes' + whichDP[self.curTagGroup]
            self.values[iname] = int(self.value)
        elif curIndex == 363:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'DPUPDPort' + whichDP[self.curTagGroup]
            self.values[iname] = int(self.value)
        elif curIndex == 364:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 0)
        elif curIndex == 365:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 1)
        elif curIndex == 366:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 2)
        elif curIndex == 367:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 3)
        elif curIndex == 368:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 4)
        elif curIndex == 369:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 5)
        elif curIndex == 370:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 7)
        elif curIndex == 371:
            whichDP = {359: '1', 372: '2', 373: '3', 374: '4', 375: '5',
                       376: '6'}
            iname = 'Flags' + whichDP[self.curTagGroup]
            self.values[iname] = Misc.bit2Int(
                self.values[iname], int(self.value), 8)

        elif curIndex == -999:
            if name[:-1] == 'sect':  # section of iir
                self.values['Sections'].append(self.subValues)
                self.subValues = {}

        if t: self.tokens.append(t)
        if c: self.configs.append(c)

    def characters(self, value):
        self.value = value

    def getCurrentIndex(self, name):
        limit = 0
        while 1:
            limit += 1
            if limit > self.maxIndex:  # sect8 is good but not in tags.
                return -999  # do not set self.curIndex to -999
            if name == tags[self.curIndex]['value']:
                break
            else:
                self.curIndex += 1
                if self.curIndex == self.maxIndex: self.curIndex = 0
        return self.curIndex

    def rearrangeOrder(self, index, startPtr, endPtr):
        """
        rearrange the token order to prepare to save to memory
        based on the output the token order should be
        'token1, token2, token3, token4, token5, token9 ...
        """
        order = (1, 2, 3, 4, 5, 9, 6, 7, 8, 192, 129, 130, 132,
                 128, 131, 193)  # 132 and 133 are the same order. 0 is optional

        for i in range(len(order)):
            for j in range(startPtr, endPtr):
                token = self.tokens[j]
                tokenID = token.getTokenID()
                if order[i] in (132, 133):
                    if tokenID in (132, 133):
                        self.tokenLists[index].append(token)
                else:
                    if tokenID == order[i]:
                        self.tokenLists[index].append(token)
        # for i in range(len(self.tokenLists[index])):
        #    print self.tokenLists[index][i]
        #    print '\n'

    def doOthers(self):
        pass


def doParser(xmlfile):
    parser = make_parser()
    handler = XMLHandler()
    parser.setContentHandler(handler)
    parser.parse(xmlfile)
    return handler


if __name__ == '__main__':
    q330 = RobustQ330(serial_number=eval("0x10000044dd6fb96"))  # 802
    xmlfile = open('config.xml', 'r')
    handler = doParser(xmlfile)
    xmlfile.close()

    InterfaceRes = q330.broadcastPhysicalInterfaces()
    q330.realRegister()
    try:
        c1fix = q330.sendCommand(c1_rqfix())  # for advanced interfaces
        dataPortMemorySizes = []
        dataPortMemorySizes.append(c1fix.getDataPort1PacketMemorySize())
        dataPortMemorySizes.append(c1fix.getDataPort2PacketMemorySize())
        dataPortMemorySizes.append(c1fix.getDataPort3PacketMemorySize())
        dataPortMemorySizes.append(c1fix.getDataPort4PacketMemorySize())
        for i in range(len(handler.configs)):
            config = handler.configs[i]
            if config.getName() == 'Interfaces':
                c1sphy = c1_sphy(InterfaceRes.getPacketBytes())  # keep MAC,...
                config.setConfigCommand(c1sphy)
                q330.sendCommand(config.getCommand())
            elif config.getName() == 'Data':
                # get the old data
                c1rqlog = c1_rqlog()
                c1rqlog.setDataPortNumber(config.getDataPortNumber())
                c1log = q330.sendCommand(c1rqlog)
                # set the new data and 'some' old data
                c1slog = c1_slog(c1log.getPacketBytes())
                config.setConfigCommand(c1slog)
                q330.sendCommand(config.getCommand())
            elif config.getName() == 'Global':
                c1glob = q330.sendCommand(c1_rqglob())
                c1sglob = c1_sglob(c1glob.getPacketBytes())
                config.setConfigCommand(c1sglob)
                q330.sendCommand(config.getCommand())
            elif config.getName() == 'Sensor':
                c1sc = q330.sendCommand(c1_rqsc())
                c1ssc = c1_ssc(c1sc.getPacketBytes())
                config.setConfigCommand(c1ssc)
                q330.sendCommand(config.getCommand())
            elif config.getName() == 'Slave':  # stop here
                pass
                # c1spp = q330.sendCommand(c1_rqspp())
                # c1sspp = c1_sspp(c1spp.getPacketBytes())
                # config.setConfigCommand(c1sspp)
                # q330.sendCommand(config.getCommand())
            elif config.getName() == 'Advanced':
                c2rqphy = c2_rqphy()
                serNum = config.getSerial()
                c2rqphy.setPhysicalInterfaceNumber(serNum)
                c2phy = q330.sendCommand(c2rqphy)
                c2sphy = c2_sphy2(c2phy.getPackageBytes())
                if c2sphy.getDialOutPassword() == 0:
                    c2sphy.setDialOutPassword("")
                if c2sphy.getDialInPassword() == 0:
                    c2sphy.setDialInPassword("")
                config.setConfigCommand(c2sphy)
                new_c2sphy = config.getCommand()
                mtl = new_c2sphy.getMemoryTriggerLevel()
                lognum = new_c2sphy.getDataPortNumber()
                memsize = dataPortMemorySizes[lognum]
                dpm = int(memsize)
                mtl = int(dpm * mtl / 100.0)
                new_c2sphy.setMemoryTriggerLevel(mtl)
                q330.sendCommand(new_c2sphy)
            elif config.getName() == 'gps':
                c2gps = q330.sendCommand(c2_rqgps())
                c2sgps = c2_sgps(c2gps.getPacketBytes())
                config.setConfigCommand(c2sgps)
                q330.sendCommand(config.getCommand())
            elif config.getName() == 'pll':
                # The same as 'gps' but not in Willard Config menu
                pass
            elif config.getName() == 'AutoMass':
                c2amass = q330.sendCommand(c2_rqamass())
                c2samass = c2_samass(c2amass.getPacketBytes())
                config.setConfigCommand(c2samass)
                q330.sendCommand(config.getCommand())
            elif config.getName() == 'Announce':
                try:  # for old firmware without announce yet
                    c3annc = q330.sendCommand(c3_rqannc())
                    c3sannc = c3_sannc(c3annc.getPacketBytes())
                except:
                    c3sannc = c3_sannc()
                    c3sannc.setC3_rqannc(2)  # Why? by comparing with above try

                config.setConfigCommand(c3sannc)
                q330.sendCommand(config.getCommand())
            else:
                pass

        q330.saveToEEPROM()
        sleep(1)
        q330.reboot()
        sleep(2)
    except QDP_C1_CERR as e:
        q330.deregister()
        print(e)
