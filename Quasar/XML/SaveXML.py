from isti.utils.GetSet import GetSet


class SaveXML(GetSet):
    def __init__(self, values = None): #values from parser
        self.values = values
        
    def setConfigCommand(self, cmd): # cmd like c3_sannc
        if self.values:
            for field in cmd.Fields:
                if field in self.values:
                    setFunction = cmd.setFunction(field)
                    setFunction(self.values[field])
        self.setCommand(cmd)


class SaveInterfaces(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('Interfaces')

class SaveDataPort(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('Data')
        self.setDataPortNumber(values['DataPortNumber'])

class SaveGlobal(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('Global')

class SaveSensor(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('Sensor')

class SaveSlave(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('Slave')

class SaveAdvanced(SaveXML):
    def __init__(self, serNum,  values):
        SaveXML.__init__(self, values)
        self.setName('Advanced')
        self.setSerial(serNum)

class SaveGPSPLL(SaveXML):
    def __init__(self, name, values):
        SaveXML.__init__(self, values)
        self.setName(name) #GPS, or PLL

class SaveAutoMass(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('AutoMass')

class SaveAnnouncement(SaveXML):
    def __init__(self, values):
        SaveXML.__init__(self, values)
        self.setName('Announce')
