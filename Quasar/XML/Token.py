from isti.utils.Bits import bitIndexToInteger
import Quasar.Tokens.Token
from Quasar.Q330 import RobustQ330
from Quasar.XML.Tags import *
import struct

t0Ignore = ''
t1TokenVersionNumber = '>B'
t2NetworkAndStationID = '>2s5s'
t3DPNetserver = '>H'
t4DataSubscriptionServerParameters = '>8p8p8pLLBBHHH'
t5DPWebserver = '>H'
t6ClockProcessingParameters = '>LHBBBBBBBBH'
t7LogAndTimingIdentification = '>2s3s2s3s'
t8ConfigurationIdentification = '>2s3sBH'
t9DataServerPort = '>H'

t128LogicalChannelQueue = '>2s3sBHLh'  # no options
t193EmailAlertConfiguration = 'HHHsss10s'

# global variable may have problem for 1-4 token loop
IIRFilters = {}
FIRFilters = {0: 'DEC10'}
Detectors = {}
ControlDetectors = {}
LCQs = {}
CommEvents = {}
LCQDetectors = []


class Token(Quasar.Tokens.Token.Token):
    def __init__(self, byte_array=None):
        Quasar.Tokens.Token.Token.__init__(self, byte_array)

    def packToken(self):
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[self.getTokenID()])
        head = struct.pack('>B', self.getTokenID())
        length = self.getTokenLength()
        if length > 191:
            head += struct.pack('>H', length)
        elif length > 127:
            head += struct.pack('>B', length)

        args = [self.FieldDefinition]
        for field in self.Fields:
            args.append(self.__dict__[field])
        return head + struct.pack(*args)

    def setFields(self, values):
        self.__dict__ |= values

    def unpackToken(self, byte_array):
        tokenData = struct.unpack(
            self.FieldDefinition, byte_array[0:struct.calcsize(self.FieldDefinition)])
        # since tokenData appears to be indexed, we presumably
        # can't replace this with a simple merge operation
        for i in range(0, len(self.Fields)):
            self.__dict__[self.Fields[i]] = tokenData[i]

    def prt2XMLFile(self, xmlfile):
        for i in range(0, len(self.tagid2field)):
            tag_id = self.tagid2field[i][0]
            field_id = self.tagid2field[i][1]
            strFunction = self.getStrFunction(self.Fields[field_id])
            eltval = strFunction()
            q = 1  # for string
            if len(eltval) > 0:
                if eltval[0] in ('-', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'):
                    q = 0
            xmlfile.write(self.createElement(tag_id, eltval, q))

    def createElement(self, tagid, eltval, q):
        s = self.createBegin(tagid)
        if q:
            s = s + '\"' + eltval + '\"'
        else:
            s += eltval
        s = s + self.createEnd(tagid)
        return s

    def createBegin(self, tagid):
        s = self.createSpace(tagid)
        s = s + '<' + tags[tagid]['value'] + '>'
        return s

    def createEnd(self, tagid):
        return '</' + tags[tagid]['value'] + '>\n'

    def createSpace(self, tagid):
        s = ''
        for i in range(tags[tagid]['space']):
            s += ' '
        return s


class t0(Token):
    def __init__(self, byte_array=None):
        self.Fields = []
        self.FieldDefinition = t0Ignore
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        pass  # ignore

    def createBinToken(self):
        self.setTokenID(0)
        self.setTokenBytes(self.packToken())


class t1(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'TokenVersionNumber'
        ]
        self.FieldDefinition = t1TokenVersionNumber
        self.tagid2field = ((9, 0),)
        Token.__init__(self, byte_array)

    def createBinToken(self, ver):
        self.setTokenID(1)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[1])
        self.setTokenVersionNumber(ver)
        self.setTokenBytes(self.packToken())


class t2(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'Network',
            'StationID'
        ]
        self.FieldDefinition = t2NetworkAndStationID
        self.tagid2field = ((10, 0), (11, 1))
        Token.__init__(self, byte_array)

    def createBinToken(self, values):
        self.setTokenID(2)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[2])
        self.setFields(values)
        self.setTokenBytes(self.packToken())


class t3(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'PortNumber'
        ]
        self.FieldDefinition = t3DPNetserver
        self.tagid2field = ((12, 0),)
        Token.__init__(self, byte_array)

    def createBinToken(self, value):
        self.setTokenID(3)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[3])
        self.setPortNumber(value)
        self.setTokenBytes(self.packToken())


class t4(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'HighestPriorityPassword',
            'MiddlePriorityPassword',
            'LowestPriorityPassword',
            'TimeoutInSeconds',
            'MaximumBytesPerSecond',
            'Verbosity',
            'MaximumCPUPercentage',
            'UDPPortNumber',
            'MaximumMemoryToUseInKB',
            'ReservedBytes'
        ]
        self.FieldDefinition = t4DataSubscriptionServerParameters
        self.tagid2field = ((26, 0), (27, 1), (28, 2), (29, 3), (30, 4),
                            (31, 5), (32, 6), (33, 7), (34, 8))
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(25) + '\n')
        Token.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createSpace(25) + self.createEnd(25))

    def createBinToken(self, values):
        self.setTokenID(4)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[4])
        self.setFields(values)
        self.setTokenBytes(self.packToken())


class t5(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'PortNumber'
        ]
        self.FieldDefinition = t5DPWebserver
        self.tagid2field = ((13, 0),)
        Token.__init__(self, byte_array)

    def createBinToken(self, value):
        self.setTokenID(5)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[5])
        self.setPortNumber(value)
        self.setTokenBytes(self.packToken())


class t6(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'TimezoneOffsetInSeconds',
            'LossInMinuts',
            'PLLLockedQuality',
            'PLLTrackingQuality',
            'PLLHoldQuality',
            'PLLOffQuality',
            'Spare',
            'HighestHasBeenLockedQuality',
            'LowestHasBeenLockedQuality',
            'NeverBeenLockedQuality',
            'ClockQualityFilter'
        ]
        self.FieldDefinition = t6ClockProcessingParameters
        self.tagid2field = ((36, 0), (37, 1), (38, 2), (39, 3), (40, 4),
                            (41, 5), (42, 7), (43, 8), (44, 9), (45, 10))
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(35) + '\n')
        Token.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createSpace(35) + self.createEnd(35))

    def createBinToken(self, values):
        self.setTokenID(6)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[6])
        self.setFields(values)
        self.setTokenBytes(self.packToken())


class t7(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'MessageLogSeedLocation',
            'MessageLogSeedName',
            'TimingLogSeedLocation',
            'TimingLogSeedName'
        ]
        self.FieldDefinition = t7LogAndTimingIdentification
        self.tagid2field = ((15, 0), (16, 1), (17, 2), (18, 3))
        Token.__init__(self, byte_array)

    def createBinToken(self, values):
        self.setTokenID(7)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[7])
        self.setFields(values)
        self.setTokenBytes(self.packToken())


class t8(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'ConfigurationStreamSeedLocation',
            'ConfigurationStreamSeedName',
            'Flags',
            'Interval'
        ]
        self.FieldDefinition = t8ConfigurationIdentification
        self.tagid2field = ((19, 0), (20, 1), (21, 3))
        Token.__init__(self, byte_array)

    def createBinToken(self, values):
        self.setTokenID(8)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[8])
        self.setFields(values)
        self.setTokenBytes(self.packToken())

    def prt2XMLFile(self, xmlfile):
        Token.prt2XMLFile(self, xmlfile)
        flags = self.getFlags()
        bits = [0, 1, 2]
        tagids = (22, 23, 24)
        for i in range(3):
            value = '0'
            if flags & bitIndexToInteger(bits[i]):
                value = '1'
            xmlfile.write(self.createElement(tagids[i], value, 0))


class t9(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'PortNumber'
        ]
        self.FieldDefinition = t9DataServerPort
        self.tagid2field = ((14, 0),)
        Token.__init__(self, byte_array)

    def createBinToken(self, value):
        self.setTokenID(9)
        self.setTokenLength(Quasar.Tokens.Token.FIXED_TOKEN_LENGTHS[9])
        self.setPortNumber(value)
        self.setTokenBytes(self.packToken())


class t192(Token):
    def __init__(self, byte_array=None):
        self.Fields = ''
        self.FieldDefinition = ''
        self.CommEventNames = {}
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        for i in range(len(self.CommEventNames)):
            xmlfile.write(self.createElement(46 + i, self.CommEventNames[i], 1))
        if len(self.CommEventNames) < 32:
            for i in range(len(self.CommEventNames), 32):
                xmlfile.write(self.createElement(46 + i, 'COMM:' + str(i + 1), 1))

    def createBinToken(self, values):
        self.TokenID = 192
        self.TokenName = Quasar.Tokens.Token.TOKEN_ID_TO_NAME[192]
        tokenLength = 2  # include 2 byte_array for token length
        byte_array = ''
        for i in range(len(values)):
            bitNumber = values[i][0] - 1
            nameLen = len(values[i][1])
            evtName = values[i][1]
            byte = struct.pack('>BB%ds' % nameLen, bitNumber, nameLen, evtName)
            tokenLength += struct.calcsize('>BB%ds' % nameLen)
            byte_array += byte
        self.TokenLength = tokenLength
        self.TokenBytes = struct.pack('>BH', 192, tokenLength) + byte_array


class t129(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'FilterNumber',
            'FilterName',
            'NumberOfSections',
            'Gain',
            'ReferenceFrequency',
            'Sections'
        ]
        self.FieldDefinition = ''
        self.tagid2field = ((80, 1), (81, 2), (82, 3), (83, 4))
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(79) + '\n')
        Token.prt2XMLFile(self, xmlfile)
        secs = self.getSections()
        for i in range(self.getNumberOfSections()):
            xmlfile.write(self.createSpace(84) + '<sect' + str(i) + '>\n')
            cutratio = "%f" % secs[i]['cutoffFrequencyRatio']
            xmlfile.write(
                self.createElement(85, cutratio.strip(), 0))
            xmlfile.write(
                self.createElement(86, str(secs[i]['numberOfPoles']), 0))
            xmlfile.write(
                self.createElement(87, str(secs[i]['high']), 0))
            xmlfile.write(self.createSpace(84) + '</sect' + str(i) + '>\n')
        xmlfile.write(self.createSpace(79) + self.createEnd(79))

    def createBinToken(self, values):
        self.setTokenID(129)
        for i in range(len(self.Fields)):
            self.__dict__[self.Fields[i]] = values[self.Fields[i]]
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[129])
        byte_array = struct.pack('>BB%dsBff' % len(self.getFilterName()),
                            self.getFilterNumber(),
                            len(self.getFilterName()),
                            self.getFilterName(),
                            self.getNumberOfSections(),
                            self.getGain(),
                            self.getReferenceFrequency())
        secs = ''
        for i in range(self.getNumberOfSections()):
            poles = self.getSections()[i]['poles']
            if self.getSections()[i]['high']:
                b = 1
                poles += (b << 7)
            secs += struct.pack('>fB',
                                self.getSections()[i]['cutratio'],
                                poles)
        self.setTokenLength(1 + len(byte_array + secs))  # token Length needs '+1'
        self.setTokenBytes(
            struct.pack('>BB', self.getTokenID(), self.getTokenLength())
            + byte_array + secs)


class t130(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'FilterNumber',
            'FilterName'
        ]
        self.tagid2field = ((78, 1),)
        Token.__init__(self, byte_array)

    def createBinToken(self, values):
        self.setTokenID(130)
        self.setTokenLength(3 + len(values['FilterName']))
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[130])
        self.setFilterNumber(values['FilterNumber'])
        self.setFilterName(values['FilterName'])
        self.setTokenBytes(struct.pack(
            '>BBBB%ds' % len(values['FilterName']),
            self.getTokenID(),
            self.getTokenLength(),
            self.getFilterNumber(),
            len(values['FilterName']),
            self.getFilterName()))


class t133(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'DetectorNumber',
            'DetectorFilterNumber',
            'HysterisisParameter',
            'WindowParameter',
            'HighLimitParameter',
            'LowLimitParameter',
            'TailParameter',
            'Spare',
            'DetectorName'
        ]
        self.FieldDefinition = ''
        self.tagid2field = ((91, 2), (92, 3), (93, 4), (94, 5), (95, 6), (80, 8))
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(89) + '\n')
        try:
            xmlfile.write(
                self.createElement(
                    90, IIRFilters[self.getDetectorFilterNumber()], 1))
        except:
            xmlfile.write(self.createElement(90, '', 1))
        Token.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createSpace(96) + self.createEnd(89))

    def createBinToken(self, values):
        self.setTokenID(133)
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[133])
        self.setFields(values)
        byte_array = struct.pack('>BBBBllHHB%ds' % len(self.getDetectorName()),
                            self.getDetectorNumber(),
                            self.getDetectorFilterNumber(),
                            self.getHysterisisParameter(),
                            self.getWindowParameter(),
                            self.getHighLimitParameter(),
                            self.getLowLimitParameter(),
                            self.getTailParameter(),
                            self.getSpare(),
                            len(self.getDetectorName()),
                            self.getDetectorName())
        self.setTokenLength(1 + len(byte_array))
        self.setTokenBytes(
            struct.pack('>BB', self.getTokenID(), self.getTokenLength()) + byte_array)


class t132(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'DetectorNumber',
            'DetectorFilterNumber',
            'IwParameter',
            'NhtParameter',
            'FilhiParameter',
            'FilloParameter',
            'WaParameter',
            'Spare',
            'TcParameter',
            'X1ParameterOver2',
            'X2ParameterOver2',
            'X3ParameterOver2',
            'XxParameter',
            'AvParameter',
            'DetectorName'
        ]
        self.FieldDefinition = ''
        self.tagid2field = ((97, 4), (98, 5), (99, 2), (100, 3),)
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(96) + '\n')
        try:
            xmlfile.write(
                self.createElement(
                    90, IIRFilters[self.getDetectorFilterNumber()], 1))
        except:
            xmlfile.write(self.createElement(90, '', 1))
        Token.prt2XMLFile(self, xmlfile)
        xmlfile.write(
            self.createElement(101, str(self.getX1ParameterOver2() * 2), 0))
        xmlfile.write(
            self.createElement(102, str(self.getX2ParameterOver2() * 2), 0))
        xmlfile.write(
            self.createElement(103, str(self.getX3ParameterOver2() * 2), 0))
        xmlfile.write(self.createElement(104, self.strXxParameter(), 0))
        xmlfile.write(self.createElement(105, self.strTcParameter(), 0))
        xmlfile.write(self.createElement(106, self.strWaParameter(), 0))
        xmlfile.write(self.createElement(107, self.strAvParameter(), 0))
        xmlfile.write(self.createElement(80, self.strDetectorName(), 1))
        xmlfile.write(self.createSpace(96) + self.createEnd(96))

    def createBinToken(self, values):
        self.setTokenID(132)
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[132])
        self.setFields(values)
        byte_array = struct.pack('>BBBBllHHHBBBBBB%ds' % len(self.getDetectorName()),
                            self.getDetectorNumber(),
                            self.getDetectorFilterNumber(),
                            self.getIwParameter(),
                            self.getNhtParameter(),
                            self.getFilhiParameter(),
                            self.getFilloParameter(),
                            self.getWaParameter(),
                            self.getSpare(),
                            self.getTcParameter(),
                            self.getX1ParameterOver2() / 2,
                            self.getX2ParameterOver2() / 2,
                            self.getX3ParameterOver2() / 2,
                            self.getXxParameter(),
                            self.getAvParameter(),
                            len(self.getDetectorName()),
                            self.getDetectorName())
        self.setTokenLength(1 + len(byte_array))
        self.setTokenBytes(
            struct.pack('>BB', self.getTokenID(), self.getTokenLength()) + byte_array)


class t131(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'ControlDetectorNumber',
            'ControlDetectorOptions',
            'ControlDetectorName',
            'DetectorEquations'
        ]
        self.FieldDefinition = ''
        self.tagid2field = ((80, 2),)
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(135) + '\n')
        v = '0'
        if self.getControlDetectorOptions() & bitIndexToInteger(0):
            v = '1'
        xmlfile.write(self.createElement(136, v, 0))
        Token.prt2XMLFile(self, xmlfile)
        eqns = self.getDetectorEquations()
        for eqn in eqns:
            name, op = eqn.split(' ', 1)
            if name == 'op':
                xmlfile.write(self.createElement(137, op, 1))
            elif name == 'det':
                try:
                    (lcqref, detref) = LCQDetectors[int(op)]
                    lcq = LCQs[lcqref]
                    det = Detectors[detref]
                except:
                    (lcq, det) = ('', '')
                xmlfile.write(self.createElement(138, lcq + ':' + det, 1))
            elif name == 'LCQ':
                try:
                    lcqname = LCQs[int(op)]
                except:
                    lcqname = ''
                xmlfile.write(self.createElement(140, lcqname, 1))
            else:
                try:
                    evtname = CommEvents[int(op)]
                except:
                    evtname = ''
                xmlfile.write(self.createElement(139, evtname, 1))
        xmlfile.write(self.createSpace(135) + self.createEnd(135))

    def createBinToken(self, values):
        self.setTokenID(131)
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[131])
        self.setFields(values)
        byte_array = struct.pack('>BBB%ds' % len(self.getControlDetectorName()),
                            self.getControlDetectorNumber(),
                            self.getControlDetectorOptions(),
                            len(self.getControlDetectorName()),
                            self.getControlDetectorName())
        eqs = self.getDetectorEquations()
        for i in range(len(eqs)):
            byte_array += struct.pack('>B', eqs[i])

        self.setTokenLength(2 + len(byte_array))  # last ff type and length
        self.setTokenBytes(
            struct.pack('>BB', self.getTokenID(), self.getTokenLength())
            + byte_array + struct.pack('>B', 255))


class t128(Token):
    def __init__(self, byte_array=None):
        self.Fields = [
            'LocationCode',
            'Seedname',
            'LCQReferenceNumber',
            'Source',
            'OptionBits',
            'Rate',
            'OptionFields'  # corressponding OptionBits when it sets 1
        ]
        self.FieldDefinition = ''
        self.tagid2field = ((109, 0), (110, 1),)
        Token.__init__(self, byte_array)

    def prt2XMLFile(self, xmlfile):
        b2b = ('0000', '0001', '0010', '0011',
               '0100', '0101', '0110', '0111',
               '1000', '1001', '1010', '1011',
               '1100', '1101', '1110', '1111')

        xmlfile.write(self.createBegin(108) + '\n')
        Token.prt2XMLFile(self, xmlfile)
        chan = self.getSource() >> 8  # 8 right shift for the 1st byte
        s = '%x' % chan
        if s == '0':
            s = '00'
        chan = ''
        for i in range(len(s)):
            chan += b2b[eval('0x%s' % s[i])]
        subchan = self.getSource() & eval('0xff')
        xmlfile.write(self.createElement(111, chan, 0))
        xmlfile.write(self.createElement(112, str(subchan), 0))
        xmlfile.write(self.createElement(113, self.strRate(), 0))
        optionFields = self.getOptionFields()
        if 'EventData' in optionFields:
            xmlfile.write(
                self.createElement(114, str(optionFields['EventData']), 0))
        if 'WritesDetectorPackets' in optionFields:
            xmlfile.write(
                self.createElement(115, str(
                    optionFields['WritesDetectorPackets']), 0))
        if 'WritesCalibrationPackets' in optionFields:
            xmlfile.write(
                self.createElement(116, str(
                    optionFields['WritesCalibrationPackets']), 0))
        if 'NoOutput' in optionFields:
            xmlfile.write(
                self.createElement(117, str(optionFields['NoOutput']), 0))
        if 'Disable' in optionFields:
            xmlfile.write(
                self.createElement(118, str(optionFields['Disable']), 0))
        if 'DataServ' in optionFields:
            xmlfile.write(
                self.createElement(119, str(optionFields['DataServ']), 0))
        if 'NetServ' in optionFields:
            xmlfile.write(
                self.createElement(120, str(optionFields['NetServ']), 0))
        if 'CNPForce' in optionFields:
            xmlfile.write(
                self.createElement(121, str(optionFields['CNPForce']), 0))
        if 'NumberOfPre-EventBuffers' in optionFields:
            xmlfile.write(
                self.createElement(
                    122, str(optionFields['NumberOfPre-EventBuffers']), 0))
        if 'GapThreshold' in optionFields:
            xmlfile.write(
                self.createElement(123, str(optionFields['GapThreshold']), 0))
        if 'CalibrationDelayInSeconds' in optionFields:
            xmlfile.write(
                self.createElement(
                    124, str(optionFields['CalibrationDelayInSeconds']), 0))
        if 'MaximumFrameCount' in optionFields:
            xmlfile.write(
                self.createElement(
                    125, str(optionFields['MaximumFrameCount']), 0))
        if 'ControlDetector' in optionFields:
            try:
                decname = ControlDetectors[optionFields['ControlDetector']]
            except:
                decname = ''
            xmlfile.write(self.createElement(126, decname, 1))
        if 'NumberOfSamplesBetweenReports' in optionFields:
            xmlfile.write(
                self.createElement(143, str(optionFields[
                                                'NumberOfSamplesBetweenReports']), 0))
        if 'OptionalIIRFilterToUse' in optionFields:
            try:
                optfil = IIRFilters[optionFields['OptionalIIRFilterToUse']]
            except:
                optfil = ''
            xmlfile.write(self.createElement(144, optfil, 1))
        for i in range(8):
            k = 'DetectorToRun%d' % (i + 1)
            if k in optionFields:
                detNum4Base = optionFields[k][0]
                detNum4ThisInvocation = optionFields[k][1]
                detOpt = optionFields[k][2]
                xmlfile.write(self.createBegin(127) + '\n')
                try:
                    detector = Detectors[int(detNum4Base)]
                except:
                    detector = ''
                xmlfile.write(
                    self.createElement(128, detector, 1))
                if detOpt & bitIndexToInteger(0):
                    v = '1'
                else:
                    v = '0'
                xmlfile.write(self.createElement(129, v, 0))
                if detOpt & bitIndexToInteger(1):
                    v = '1'
                else:
                    v = '0'
                xmlfile.write(self.createElement(130, v, 0))
                if detOpt & bitIndexToInteger(3):
                    v = '1'
                else:
                    v = '0'
                xmlfile.write(self.createElement(131, v, 0))
                xmlfile.write(self.createSpace(127) + self.createEnd(127))
        if 'FIRMultiplier' in optionFields:
            xmlfile.write(
                self.createElement(132, str(optionFields['FIRMultiplier']), 0))
        if 'SourceLCQNumber' in optionFields:
            try:
                lcqname = LCQs[int(optionFields['SourceLCQNumber'])]
            except:
                lcqname = ''
            xmlfile.write(self.createElement(133, lcqname, 1))
        if 'DecimationFIRFilterNumber' in optionFields:
            try:
                filname = FIRFilters[int(
                    optionFields['DecimationFIRFilterNumber'])]
            except:
                filname = ''
            xmlfile.write(self.createElement(134, filname, 1))

        xmlfile.write(self.createSpace(108) + self.createEnd(108))

    def createBinToken(self):
        self.setTokenID(128)
        self.setTokenName(Quasar.Tokens.Token.TOKEN_ID_TO_NAME[128])

        byte_array = struct.pack(t128LogicalChannelQueue,
                            self.getLocationCode(),
                            self.getSeedname(),
                            self.getLCQReferenceNumber(),
                            self.getSource(),
                            self.getOptionBits(),
                            self.getRate())

        optionBits = self.getOptionBits()
        optionFields = self.getOptionFields()

        if optionBits & bitIndexToInteger(3):
            byte_array += struct.pack(
                '>H', optionFields['NumberOfPre-EventBuffers'])
        if optionBits & bitIndexToInteger(4):
            byte_array += struct.pack('>f', optionFields['GapThreshold'])
        if optionBits & bitIndexToInteger(5):
            byte_array += struct.pack(
                '>H', optionFields['CalibrationDelayInSeconds'])
        if optionBits & bitIndexToInteger(6):
            byte_array += struct.pack('>B', optionFields['MaximumFrameCount'])
        if optionBits & bitIndexToInteger(7):
            byte_array += struct.pack('>f', optionFields['FIRMultiplier'])
        if optionBits & bitIndexToInteger(8):
            byte_array += struct.pack('>LB',
                                 optionFields['NumberOfSamplesBetweenReports'],
                                 optionFields['OptionalIIRFilterToUse'])
        if optionBits & bitIndexToInteger(9):
            byte_array += struct.pack('>B', optionFields['ControlDetector'])
        if optionBits & bitIndexToInteger(10):
            byte_array += struct.pack('>BB',
                                 optionFields['SourceLCQNumber'],
                                 optionFields['DecimationFIRFilterNumber'])
        optionFieldsKeys = list(optionFields.keys())
        for i in range(1, 9):
            index = 'DetectorToRun%d' % i
            if index in optionFieldsKeys:
                byte_array += struct.pack('>BBB',
                                     optionFields[index][0],
                                     optionFields[index][1],
                                     optionFields[index][2])

        self.setTokenLength(1 + len(byte_array))
        self.setTokenBytes(
            struct.pack('>BB', self.getTokenID(), self.getTokenLength()) + byte_array)


def unpackTokens(tokens):
    decodedTokens = []
    for token in tokens:
        token_id = token.getTokenID()
        byte_array = token.getTokenBytes()
        if token_id == 1:
            t = t1(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 2:
            t = t2(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 3:
            t = t3(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 4:
            t = t4(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 5:
            t = t5(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 6:
            t = t6(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 7:
            t = t7(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 8:
            t = t8(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 9:
            t = t9(byte_array)
            t.unpackToken(byte_array[1:])
        elif token_id == 192:
            t = t192(byte_array)
            current = 3  # 0:id, 1-2:token length
            tokenLength = token.getTokenLength()
            while current < tokenLength:
                bitNumber = struct.unpack('>B', byte_array[current])[0]
                current += 1
                strLength = struct.unpack('>B', byte_array[current])[0]
                current += 1
                eventName = struct.unpack('>%ds' % strLength,
                                          byte_array[current:current + strLength])[0]
                current += strLength
                t.CommEventNames[bitNumber] = eventName
                CommEvents[bitNumber] = eventName
        elif token_id == 129:
            t = t129(byte_array)
            tokenData = []
            current = 2  # 0:id, 1:token length
            tokenData.append(struct.unpack('>B', byte_array[current])[0])
            current += 1
            strLength = struct.unpack('>B', byte_array[current])[0]
            current += 1
            tokenData.append(struct.unpack('>%ds' % strLength,
                                           byte_array[current:current + strLength])[0])
            current += strLength
            td = struct.unpack('>Bff', byte_array[current:current + 9])
            current += 9
            for i in range(3):
                tokenData.append(td[i])
            numOfSec = td[0]
            secs = []
            for i in range(numOfSec):
                sec = {}
                td = struct.unpack('>fB', byte_array[current:current + 5])
                current += 5
                sec['cutoffFrequencyRatio'] = td[0]
                sec['numberOfPoles'] = td[1]
                if sec['numberOfPoles'] & 128:  # most significant bit set
                    sec['numberOfPoles'] -= 128
                    sec['high'] = 1
                else:
                    sec['high'] = 0
                secs.append(sec)
            tokenData.append(secs)
            for i in range(len(t.Fields)):
                t.set_attr(t.Fields[i], tokenData[i])
            IIRFilters[t.getFilterNumber()] = t.strFilterName()
        elif token_id == 130:
            t = t130(byte_array)
            current = 2
            td = []
            td.append(struct.unpack('>B', byte_array[current])[0])
            current += 1
            strLength = struct.unpack('>B', byte_array[current])[0]
            current += 1
            td.append(struct.unpack('>%ds' % strLength, byte_array[current:])[0])
            for i in range(len(t.Fields)):
                t.set_attr(t.Fields[i], td[i])
            FIRFilters[t.getFilterNumber()] = t.strFilterName()
        elif token_id == 133:
            t = t133(byte_array)
            current = 2
            tokenData = struct.unpack('>BBBBllHH', byte_array[current:current + 16])
            current += 16
            strLength = struct.unpack('>B', byte_array[current])
            current += 1
            td = struct.unpack('>%ds' % strLength, byte_array[current:])
            for i in range(len(t.Fields) - 1):
                t.set_attr(t.Fields[i], tokenData[i])
            t.set_attr(t.Fields[-1], td[0])
            Detectors[t.getDetectorNumber()] = t.strDetectorName()
        elif token_id == 132:
            t = t132(byte_array)
            current = 2
            tokenData = struct.unpack('>BBBBllHHHBBBBB',
                                      byte_array[current:current + 23])
            current += 23
            strLength = struct.unpack('>B', byte_array[current])
            current += 1
            td = struct.unpack('>%ds' % strLength, byte_array[current:])
            for i in range(len(t.Fields) - 1):
                t.set_attr(t.Fields[i], tokenData[i])
            t.set_attr(t.Fields[-1], td[0])
            Detectors[t.getDetectorNumber()] = t.strDetectorName()
        elif token_id == 131:
            t = t131(byte_array)
            current = 2
            tokenData = []
            tokenData.append(struct.unpack('>B', byte_array[current])[0])
            current += 1
            tokenData.append(struct.unpack('>B', byte_array[current])[0])
            current += 1
            strLength = struct.unpack('>B', byte_array[current])[0]
            current += 1
            tokenData.append(struct.unpack(f'>{strLength:d}s', byte_array[current:current + strLength])[0])
            current += strLength
            tokenLength = t.getTokenLength()
            td = []
            while 1:
                byte = struct.unpack('>B', byte_array[current])[0]
                if byte == 255:
                    break
                current += 1
                if (byte & 128) and (byte & 64):
                    op = ('leftpar',
                          'rightpar',
                          'not',
                          'and',
                          'or',
                          'exclusiveor',
                          'endoflist')
                    byte = byte - 128 - 64
                    td.append(f'op {op[byte]}')
                elif (byte & 128) and (not (byte & 64)):
                    td.append(f'LCQ {byte & 63:d}')  # 63 = 0x3f
                elif (not (byte & 128)) and (byte & 64):
                    td.append(f'det {byte & 63:d}')  # 63 = 0x3f
                else:
                    td.append(f'CommEvent {byte & 31:d}')  # 31 = 0xif
            tokenData.append(td)
            for i in range(len(t.Fields)):
                t.set_attr(t.Fields[i], tokenData[i])
            ControlDetectors[t.getControlDetectorNumber()] = t.strControlDetectorName()
        elif token_id == 128:
            t = t128(byte_array)
            current = 2
            length = struct.calcsize(t128LogicalChannelQueue)
            tokenData = struct.unpack(t128LogicalChannelQueue,
                                      byte_array[current:current + length])
            current += length
            optionBits = tokenData[-2]
            td = {}
            lcqRefNum = tokenData[2]
            if optionBits & bitIndexToInteger(0):
                td['EventData'] = 1
            else:
                td['EventData'] = 0

            if optionBits & bitIndexToInteger(1):
                td['WritesDetectorPackets'] = 1
            else:
                td['WritesDetectorPackets'] = 0

            if optionBits & bitIndexToInteger(2):
                td['WritesCalibrationPackets'] = 1
            else:
                td['WritesCalibrationPackets'] = 0

            if optionBits & bitIndexToInteger(3):
                nopb = struct.unpack('>H', byte_array[current:current + 2])[0]
                current += 2
                td['NumberOfPre-EventBuffers'] = nopb
            else:
                td['NumberOfPre-EventBuffers'] = 0

            if optionBits & bitIndexToInteger(4):
                gap = struct.unpack('>f', byte_array[current:current + 4])[0]
                current += 4
                td['GapThreshold'] = gap
            else:
                td['GapThreshold'] = 0

            if optionBits & bitIndexToInteger(5):
                caldly = struct.unpack('>H', byte_array[current:current + 2])[0]
                current += 2
                td['CalibrationDelayInSeconds'] = caldly
            else:
                td['CalibrationDelayInSeconds'] = 0

            if optionBits & bitIndexToInteger(6):
                mfc = struct.unpack('>B', byte_array[current])[0]
                current += 1
                td['MaximumFrameCount'] = mfc
            else:
                td['MaximumFrameCount'] = 255

            if optionBits & bitIndexToInteger(7):
                fm = struct.unpack('>f', byte_array[current:current + 4])[0]
                current += 4
                td['FIRMultiplier'] = fm

            if optionBits & bitIndexToInteger(8):
                avgsamps = struct.unpack('>L', byte_array[current:current + 4])[0]
                current += 4
                avgfilt = struct.unpack('>B', byte_array[current])[0]
                current += 1
                td['NumberOfSamplesBetweenReports'] = avgsamps
                td['OptionalIIRFilterToUse'] = avgfilt

            if optionBits & bitIndexToInteger(9):
                ctrldet = struct.unpack('>B', byte_array[current])[0]
                current += 1
                td['ControlDetector'] = ctrldet

            if optionBits & bitIndexToInteger(10):
                td['SourceLCQNumber'] = struct.unpack('>B', byte_array[current])[0]
                current += 1
                td['DecimationFIRFilterNumber'] = struct.unpack(
                    '>B', byte_array[current])[0]
                current += 1

            if optionBits & bitIndexToInteger(11):
                td['NoOutput'] = 1
            else:
                td['NoOutput'] = 0

            if optionBits & (2 ** 28):
                td['Disable'] = 1
            else:
                td['Disable'] = 0

            if optionBits & (2 ** 29):
                td['DataServ'] = 1
            else:
                td['DataServ'] = 0

            if optionBits & (2 ** 30):
                td['NetServ'] = 1
            else:
                td['NetServ'] = 0

            if optionBits & (2 ** 31):
                td['CNPForce'] = 1
            else:
                td['CNPForce'] = 0

            BITS = (
                (2 ** 12),
                (2 ** 13),
                (2 ** 14),
                (2 ** 15),
                (2 ** 16),
                (2 ** 17),
                (2 ** 18),
                (2 ** 19)
            )
            number = 1
            for b in BITS:
                if optionBits & b:
                    tdd = struct.unpack('>BBB', byte_array[current:current + 3])
                    current += 3
                    td['DetectorToRun%d' % number] = tdd
                    LCQDetectors.append((lcqRefNum, tdd[0]))  # for controldet
                    number += 1

            # set all fileds except 'OptionFields'   
            for i in range(len(t.Fields) - 1):
                t.set_attr(t.Fields[i], tokenData[i])
            # sef the last field that is 'OptionFields'
            t.set_attr(t.Fields[-1], td)
            LCQs[t.getLCQReferenceNumber()] = t.strSeedname()
        else:
            t = t0(byte_array)
        decodedTokens.append(t)
    return decodedTokens


def printToXMLFile(xmlfile, newTokens):
    for token in newTokens:
        token.prt2XMLFile(xmlfile)


def print2XMLFile(q330, xmlfile, requireRegister=None):
    """
    call by external function
    """
    global IIRFilters
    global FIRFilters
    global Detectors
    global ControlDetectors
    global LCQs
    global CommEvents
    global LCQDetectors

    for i in range(1, 5):
        xmlfile.write('<tokens%d>\n' % i)
        tokenSet = q330.getTokenSet(i, requireRegister)
        print(tokenSet)  # print hex tokens
        tokens = tokenSet.getTokens()
        decodedTokens = unpackTokens(tokens)
        printToXMLFile(xmlfile, decodedTokens)
        xmlfile.write('</tokens%d>\n' % i)
        # changed here
        IIRFilters.clear()
        FIRFilters.clear()
        FIRFilters = {0: 'DEC10'}
        Detectors.clear()
        ControlDetectors.clear()
        LCQs.clear()
        CommEvents.clear()
        l = len(LCQDetectors)
        del LCQDetectors[0:l]


if __name__ == '__main__':
    xmlfile = open('out.xml', 'w')
    q330 = RobustQ330(serial_number=eval("0x100000a27991462"))  # 799
    print2XMLFile(q330, xmlfile, 1)
    xmlfile.close()
