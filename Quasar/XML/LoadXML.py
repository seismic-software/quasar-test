from time import strftime, localtime

from Quasar.Commands.c1_rqfix import c1_rqfix
from Quasar.Commands.c1_rqglob import c1_rqglob
from Quasar.Commands.c1_rqlog import c1_rqlog
from Quasar.Commands.c1_rqsc import c1_rqsc
from Quasar.Commands.c1_rqspp import c1_rqspp
from Quasar.Commands.c2_rqamass import c2_rqamass
from Quasar.Commands.c2_rqgps import c2_rqgps
from Quasar.Commands.c2_rqphy import c2_rqphy
from Quasar.Commands.c3_rqannc import c3_rqannc
from Quasar.Q330 import RobustQ330
from isti.utils.Bits import bitIndexToInteger
from isti.utils.GetSet import GetSet
from Quasar.Utils.Misc import num2DottedIP
from Quasar.XML import Token4Load
from Quasar.XML.Tags import tags



class LoadXML(GetSet):
    def __init__(self, response=None):
        self.response = response

    def prt2XMLFile(self, xmlfile):
        for i in range(0, len(self.tagid2field)):
            tagid = self.tagid2field[i][0]
            fieldid = self.tagid2field[i][1]
            strFunction = self.response.getStrFunction(
                self.response.Fields[fieldid])
            eltval = strFunction()
            q = 1  # for string
            if len(eltval) > 0:  # may have problem if the string is '1'
                if eltval[0] in (
                '-', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'):
                    q = 0
            xmlfile.write(self.createElement(tagid, eltval, q))

    def createElement(self, tagid, eltval, q):
        s = self.createBegin(tagid)
        if q:
            s = s + '\"' + eltval + '\"'
        else:
            s += eltval
        s = s + self.createEnd(tagid)
        return s

    def createBegin(self, tagid):
        s = self.createSpace(tagid)
        s = s + '<' + tags[tagid]['value'] + '>'
        return s

    def createEnd(self, tagid):
        return '</' + tags[tagid]['value'] + '>\n'

    def createSpace(self, tagid):
        s = ''
        for i in range(tags[tagid]['space']):
            s += ' '
        return s


class LoadInterfaces(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((147, 1), (148, 2), (149, 4), (150, 7),)
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(146) + '\n')
        LoadXML.prt2XMLFile(self, xmlfile)
        fflags = (self.response.getSerialInterface1Flags(),
                  self.response.getSerialInterface2Flags())
        bauds = (self.response.strSerialInterface1Baud(),
                 self.response.strSerialInterface2Baud())
        offset = (0, 10)
        for i in range(2):
            xmlfile.write(self.createElement(151 + offset[i], bauds[i], 0))
            flags = fflags[i]
            v = '0'
            if flags & bitIndexToInteger(0): v = '1'
            xmlfile.write(self.createElement(152 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(2): v = '1'
            xmlfile.write(self.createElement(153 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(3): v = '1'
            xmlfile.write(self.createElement(154 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(4): v = '1'
            xmlfile.write(self.createElement(155 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(5): v = '1'
            xmlfile.write(self.createElement(156 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(6): v = '1'
            xmlfile.write(self.createElement(157 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(7): v = '1'
            xmlfile.write(self.createElement(158 + offset[i], v, 0))
            v = '0'
            if (flags & bitIndexToInteger(8)) and (
            not (flags & bitIndexToInteger(9))): v = '1'
            xmlfile.write(self.createElement(159 + offset[i], v, 0))
            v = '0'
            if flags & bitIndexToInteger(10): v = '1'
            xmlfile.write(self.createElement(160 + offset[i], v, 0))
        flags = self.response.getSerialInterface3Flags()
        v = '0'
        if flags & bitIndexToInteger(0): v = '1'
        xmlfile.write(self.createElement(171, v, 0))
        flags = self.response.getEthernetFlags()
        v = '0'
        if flags & bitIndexToInteger(0): v = '1'
        xmlfile.write(self.createElement(172, v, 0))
        v = '0'
        if flags & bitIndexToInteger(1): v = '1'
        xmlfile.write(self.createElement(173, v, 0))
        v = '0'
        if flags & bitIndexToInteger(4): v = '1'
        xmlfile.write(self.createElement(174, v, 0))
        v = '0'
        if flags & bitIndexToInteger(5): v = '1'
        xmlfile.write(self.createElement(175, v, 0))
        v = '0'
        if flags & bitIndexToInteger(6): v = '1'
        xmlfile.write(self.createElement(176, v, 0))
        v = '0'
        if flags & bitIndexToInteger(7): v = '1'
        xmlfile.write(self.createElement(177, v, 0))
        v = '0'
        if (flags & bitIndexToInteger(8)) and (
        not (flags & bitIndexToInteger(9))): v = '1'
        xmlfile.write(self.createElement(178, v, 0))
        v = '0'
        if flags & bitIndexToInteger(10): v = '1'
        xmlfile.write(self.createElement(179, v, 0))
        # ser1_throt and ser2_throt are old standard?
        # we comment them out as follows
        # xmlfile.write(self.createElement(
        #    180, self.response.strSerialInterface1Throttle(), 0))
        # xmlfile.write(self.createElement(
        #    181, self.response.strSerialInterface2Throttle(), 0))
        xmlfile.write(self.createSpace(146) + self.createEnd(146))


class LoadDataPort(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((192, 3), (193, 4))
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        tagids = (182, 217, 218, 219)
        portnumber = self.response.getDataPortNumber()
        xmlfile.write(self.createBegin(tagids[portnumber]) + '\n')
        xmlfile.write(self.createElement(183, str(portnumber), 0))
        flags = self.response.getFlags()
        xmlfile.write(
            self.createElement(184, bitcal(flags, bitIndexToInteger(0)), 0))
        xmlfile.write(
            self.createElement(185, bitcal(flags, bitIndexToInteger(2)), 0))
        xmlfile.write(
            self.createElement(186, bitcal(flags, bitIndexToInteger(3)), 0))
        xmlfile.write(
            self.createElement(187, bitcal(flags, bitIndexToInteger(4)), 0))
        xmlfile.write(
            self.createElement(188, bitcal(flags, bitIndexToInteger(8)), 0))
        xmlfile.write(
            self.createElement(189, bitcal(flags, bitIndexToInteger(9)), 0))
        xmlfile.write(
            self.createElement(190, bitcal(flags, bitIndexToInteger(10)), 0))
        xmlfile.write(self.createElement(
            191,
            str(int(self.response.getPercentOfPacketBuffer() / 2.56 + 0.5)), 0))
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            194, str(self.response.getMaximumResendTimeout() / 10.0), 0))
        xmlfile.write(self.createElement(
            195, str(self.response.getGroupTimeout() / 10.0), 0))
        xmlfile.write(self.createElement(
            196, str(self.response.getMinimumResendTimeout() / 10.0), 0))
        xmlfile.write(self.createElement(
            197, self.response.strWindowSize(), 0))
        ids = (198, 206, 207, 208, 209, 210)
        fflags = (self.response.getChannel1Freqs(),
                  self.response.getChannel2Freqs(),
                  self.response.getChannel3Freqs(),
                  self.response.getChannel4Freqs(),
                  self.response.getChannel5Freqs(),
                  self.response.getChannel6Freqs(),
                  )
        for i in range(len(ids)):
            xmlfile.write(self.createBegin(ids[i]) + '\n')
            xmlfile.write(self.createElement(
                199, bitcal(fflags[i], bitIndexToInteger(0)), 0))
            xmlfile.write(self.createElement(
                200, bitcal(fflags[i], bitIndexToInteger(1)), 0))
            xmlfile.write(self.createElement(
                201, bitcal(fflags[i], bitIndexToInteger(2)), 0))
            xmlfile.write(self.createElement(
                202, bitcal(fflags[i], bitIndexToInteger(3)), 0))
            xmlfile.write(self.createElement(
                203, bitcal(fflags[i], bitIndexToInteger(4)), 0))
            xmlfile.write(self.createElement(
                204, bitcal(fflags[i], bitIndexToInteger(5)), 0))
            xmlfile.write(self.createElement(
                205, bitcal(fflags[i], bitIndexToInteger(6)), 0))
            xmlfile.write(self.createSpace(ids[i]) + self.createEnd(ids[i]))
        xmlfile.write(self.createElement(
            211, self.response.strAcknowledgeCount(), 0))
        xmlfile.write(self.createElement(
            212, str(self.response.getAcknowledgeTimeout() / 10.0), 0))
        try:
            xmlfile.write(self.createElement(
                213, str(int(
                    1024000.0 / self.response.getEthernetThrottle() + 0.5)), 0))
        except:  # 0 defeat
            pass
        xmlfile.write(self.createElement(
            214, str(int(self.response.getAlarmPercent() * 100 / 256.0 + 0.5)),
            0))
        xmlfile.write(self.createElement(
            215, self.response.strAutomaticFilters(), 0))
        xmlfile.write(self.createElement(
            216, self.response.strManualFilters(), 0))

        xmlfile.write(self.createSpace(tagids[portnumber]) +
                      self.createEnd(tagids[portnumber]))


class LoadGlobal(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((221, 0), (222, 1), (223, 2))
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(220) + '\n')
        LoadXML.prt2XMLFile(self, xmlfile)
        auxstat = self.response.getAuxAndStatusSamplingRates()
        xmlfile.write(
            self.createElement(224, bitcal(auxstat, bitIndexToInteger(0)), 0))
        xmlfile.write(
            self.createElement(225, bitcal(auxstat, bitIndexToInteger(8)), 0))
        gain = self.response.getGainBitmap()
        tagids = (226, 227, 228, 229, 230, 231)
        bits = ((bitIndexToInteger(0), bitIndexToInteger(1)),
                (bitIndexToInteger(2), bitIndexToInteger(3)),
                (bitIndexToInteger(4), bitIndexToInteger(5)),
                (bitIndexToInteger(6), bitIndexToInteger(7)),
                (bitIndexToInteger(8), bitIndexToInteger(9)),
                (bitIndexToInteger(10), bitIndexToInteger(11)))
        for i in range(6):
            if (bits[i][0] & gain) == 0 and (bits[i][1] & gain) == 0:
                v = '0'
            elif (bits[i][0] & gain) != 0 and (bits[i][1] & gain) == 0:
                v = '1'
            else:
                v = '2'
            xmlfile.write(self.createElement(tagids[i], v, 0))
        fil = self.response.getFilterBitmap()
        if (fil & bitIndexToInteger(0)) == 0 and (
                fil & bitIndexToInteger(1)) == 0:
            v = '0'
        elif (fil & bitIndexToInteger(0)) != 0 and (
                fil & bitIndexToInteger(1)) == 0:
            v = '1'
        elif (fil & bitIndexToInteger(0)) == 0 and (
                fil & bitIndexToInteger(1)) != 0:
            v = '2'
        else:
            v = '3'
        xmlfile.write(self.createElement(232, v, 0))
        if (fil & bitIndexToInteger(2)) == 0 and (
                fil & bitIndexToInteger(3)) == 0:
            v = '0'
        else:
            v = '1'
        xmlfile.write(self.createElement(233, v, 0))
        tagids = (234, 235, 236, 237, 238, 239)
        bits = (
        bitIndexToInteger(0), bitIndexToInteger(1), bitIndexToInteger(2),
        bitIndexToInteger(3), bitIndexToInteger(4), bitIndexToInteger(5))
        inpt = self.response.getInputBitmap()
        for i in range(6):
            xmlfile.write(self.createElement(
                tagids[i], bitcal(inpt, bits[i]), 0))
        if (inpt & bitIndexToInteger(8)) == 0 and (
                inpt & bitIndexToInteger(9)) == 0:
            v = '0'
        elif (inpt & bitIndexToInteger(8)) != 0 and (
                inpt & bitIndexToInteger(9)) == 0:
            v = '1'
        elif (inpt & bitIndexToInteger(8)) == 0 and (
                inpt & bitIndexToInteger(9)) != 0:
            v = '2'
        else:
            v = '3'
        xmlfile.write(self.createElement(240, v, 0))
        if (inpt & bitIndexToInteger(10)) == 0 and (
                inpt & bitIndexToInteger(11)) == 0:
            v = '0'
        elif (inpt & bitIndexToInteger(10)) != 0 and (
                inpt & bitIndexToInteger(11)) == 0:
            v = '1'
        elif (inpt & bitIndexToInteger(10)) == 0 and (
                inpt & bitIndexToInteger(11)) != 0:
            v = '2'
        else:
            v = '3'
        xmlfile.write(self.createElement(241, v, 0))
        self.tagid2field = ((242, 7), (243, 8), (244, 9), (245, 10), (246, 11),
                            (247, 12), (248, 13), (249, 14), (250, 15),
                            (251, 16))
        LoadXML.prt2XMLFile(self, xmlfile)
        tagids = (252, 253, 254, 255, 256, 257)
        hzids = (199, 200, 201, 202, 203, 204, 205)
        for i in range(6):
            xmlfile.write(self.createBegin(tagids[i]) + '\n')
            for j in range(7):
                getFunction = self.response.get_attr(
                    self.response.Fields[8 * i + j + 17])  # orl [8*i+j+18] ?
                v = getFunction() / 1024.0
                xmlfile.write(self.createElement(hzids[j], str(v), 0))
            xmlfile.write(
                self.createSpace(tagids[i]) + self.createEnd(tagids[i]))
        self.tagid2field = []
        for i in range(6):
            self.tagid2field.append((258 + i, 65 + i))
        LoadXML.prt2XMLFile(self, xmlfile)
        for i in range(6):
            getFunction = self.response.get_attr(
                self.response.Fields[71 + i])
            v = getFunction() / 1024.0
            xmlfile.write(self.createElement(264 + i, str(v), 0))
        xmlfile.write(self.createSpace(220) + self.createEnd(220))


class LoadSensorControlMapping(LoadXML):
    def __init__(self, response):
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(270) + '\n')
        tagids = (271, 273, 274, 275, 276, 277, 278, 279)
        for i in range(8):
            xmlfile.write(self.createBegin(tagids[i]) + '\n')
            getFunction = self.response.get_attr(
                self.response.Fields[i])
            val = getFunction()
            f = val & 255
            xmlfile.write(self.createElement(272, str(f), 0))
            xmlfile.write(
                self.createElement(93, bitcal(val, bitIndexToInteger(8)), 0))
            xmlfile.write(
                self.createSpace(tagids[i]) + self.createEnd(tagids[i]))

        xmlfile.write(self.createSpace(270) + self.createEnd(270))


class LoadSlave(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((281, 0), (282, 1))
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(280) + '\n')
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            283, str(self.response.getMinimumPSVoltage() * .15), 0))
        self.tagid2field = ((284, 3), (285, 4), (286, 5), (287, 6))
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            288, str(self.response.getVoltageHysteresis() * .15), 0))
        xmlfile.write(self.createElement(
            289, self.response.strDefaultVCOValue(), 0))
        xmlfile.write(self.createSpace(280) + self.createEnd(280))


class LoadAdvancedPhysicalInterface(LoadXML):
    def __init__(self, response):
        self.tagid2field = (
        (291, 0), (292, 1))  # ,(293,2),(294,3),(295,4),(297,5))
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile, dataPortMemorySizes):
        begintags = (290, 320, 0, 321)
        phynum = self.response.getPhysicalInterfaceNumber()
        xmlfile.write(self.createBegin(begintags[phynum]) + '\n')
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            293, self.response.strModemInitialization(), 1))
        xmlfile.write(self.createElement(
            294, self.response.strPhoneNumber(), 1))
        xmlfile.write(self.createElement(
            295, self.response.strDialOutUserName(), 1))
        xmlfile.write(self.createElement(
            297, self.response.strDialInUserName(), 1))
        xmlfile.write(self.createElement(
            299, self.getBufferFillPercent(dataPortMemorySizes), 0))
        flags = self.response.getFlags()
        if phynum != 3:
            xmlfile.write(
                self.createElement(300, bitcal(flags, bitIndexToInteger(0)), 0))
            xmlfile.write(
                self.createElement(301, bitcal(flags, bitIndexToInteger(1)), 0))
            if (flags & bitIndexToInteger(2)) == 0 and (
                    flags & bitIndexToInteger(3)) == 0:
                v = '0'
            elif (flags & bitIndexToInteger(2)) != 0 and (
                    flags & bitIndexToInteger(3)) == 0:
                v = '1'
            elif (flags & bitIndexToInteger(2)) == 0 and (
                    flags & bitIndexToInteger(3)) != 0:
                v = '2'
            else:
                v = '3'
            xmlfile.write(self.createElement(302, v, 0))
        if (flags & bitIndexToInteger(4)) == 0 and (
                flags & bitIndexToInteger(5)) == 0:
            v = '0'
        elif (flags & bitIndexToInteger(4)) != 0 and (
                flags & bitIndexToInteger(5)) == 0:
            v = '1'
        elif (flags & bitIndexToInteger(4)) == 0 and (
                flags & bitIndexToInteger(5)) != 0:
            v = '2'
        else:
            v = '3'
        xmlfile.write(self.createElement(303, v, 0))
        xmlfile.write(
            self.createElement(304, bitcal(flags, bitIndexToInteger(6)), 0))
        xmlfile.write(
            self.createElement(305, bitcal(flags, bitIndexToInteger(7)), 0))
        xmlfile.write(
            self.createElement(306, bitcal(flags, bitIndexToInteger(8)), 0))
        xmlfile.write(
            self.createElement(307, bitcal(flags, bitIndexToInteger(9)), 0))
        if phynum != 3:
            xmlfile.write(
                self.createElement(308, bitcal(flags, bitIndexToInteger(10)),
                                   0))
        xmlfile.write(
            self.createElement(309, bitcal(flags, bitIndexToInteger(11)), 0))
        xmlfile.write(
            self.createElement(310, bitcal(flags, bitIndexToInteger(15)), 0))
        self.tagid2field = ((311, 12), (312, 13))
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            313, str(self.response.getWebServerBPSLimit() * 10), 0))
        xmlfile.write(self.createElement(
            314, num2DottedIP(
                self.response.getPointOfContactOrBalerIPAddress()), 0))
        xmlfile.write(self.createElement(
            315, num2DottedIP(
                self.response.getBalerSecondaryIPAddress()), 0))
        self.tagid2field = ((316, 17), (317, 18), (318, 19))
        LoadXML.prt2XMLFile(self, xmlfile)

        if phynum == 3:
            bauds = ('Reserved', '1200', '2400', '4800', '9600',
                     '19200', '38400', '57600', '115200')
            xmlfile.write(self.createElement(
                322, bauds[self.response.getSerialBaudRate()], 0))
        else:
            xmlfile.write(self.createElement(
                319, self.response.strRoutedPacketsTimeout(), 0))
        xmlfile.write(self.createSpace(
            begintags[phynum]) + self.createEnd(begintags[phynum]))

    def getBufferFillPercent(self, dataPortMemorySizes):
        lognum = self.response.getDataPortNumber()
        memsize = dataPortMemorySizes[lognum]
        dpm = int(memsize)
        if dpm:
            bufferFillPercent = float(
                self.response.getMemoryTriggerLevel()) / dpm
        else:
            bufferFillPercent = 0
        bufferFillPercent += 0.005
        return str(int(bufferFillPercent * 100))


class GPSParameters(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((329, 1), (330, 2), (331, 3), (332, 4))
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(323) + '\n')
        timingmode = self.response.getTimingMode()
        xmlfile.write(self.createElement(
            324, str(timingmode & 7), 0))  # mode & 0x7 (111)
        xmlfile.write(
            self.createElement(325, bitcal(timingmode, bitIndexToInteger(3)),
                               0))
        xmlfile.write(
            self.createElement(326, bitcal(timingmode, bitIndexToInteger(4)),
                               0))
        xmlfile.write(
            self.createElement(327, bitcal(timingmode, bitIndexToInteger(5)),
                               0))
        xmlfile.write(
            self.createElement(328, bitcal(timingmode, bitIndexToInteger(6)),
                               0))
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createSpace(323) + self.createEnd(323))
        xmlfile.write(self.createBegin(333) + '\n')
        xmlfile.write(self.createElement(
            334, self.response.strPLLLockUSecs(), 0))
        xmlfile.write(self.createElement(
            312, self.response.strPLLUpdateIntervalInSeconds(), 0))
        flags = self.response.getPLLFlags()
        xmlfile.write(
            self.createElement(335, bitcal(flags, bitIndexToInteger(0)), 0))
        xmlfile.write(
            self.createElement(336, bitcal(flags, bitIndexToInteger(1)), 0))
        xmlfile.write(
            self.createElement(337, bitcal(flags, bitIndexToInteger(2)), 0))
        self.tagid2field = (
        (338, 9), (339, 10), (340, 11), (341, 12), (342, 13),
        (343, 14), (344, 15), (345, 16))
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createSpace(333) + self.createEnd(333))


class AutoMassRecentering(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((348, 0), (349, 1), (350, 2), (351, 3), (352, 4),
                            (353, 5), (354, 6))
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(346) + '\n')
        xmlfile.write(self.createBegin(347) + '\n')
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            355, str(self.response.getDuration1() / 100.0), 0))
        xmlfile.write(self.createSpace(347) + self.createEnd(347))
        xmlfile.write(self.createBegin(356) + '\n')
        self.tagid2field = ((348, 8), (349, 9), (350, 10), (351, 11), (352, 12),
                            (353, 13), (354, 14))
        LoadXML.prt2XMLFile(self, xmlfile)
        xmlfile.write(self.createElement(
            355, str(self.response.getDuration2() / 100.0), 0))
        xmlfile.write(self.createSpace(356) + self.createEnd(356))
        xmlfile.write(self.createSpace(346) + self.createEnd(346))


class Announce(LoadXML):
    def __init__(self, response):
        self.tagid2field = ((358, 2),)
        LoadXML.__init__(self, response)

    def prt2XMLFile(self, xmlfile):
        xmlfile.write(self.createBegin(357) + '\n')
        LoadXML.prt2XMLFile(self, xmlfile)
        flags = self.response.getUnlockFlags()
        xmlfile.write(
            self.createElement(179, bitcal(flags, bitIndexToInteger(11)), 0))
        xmlfile.write(
            self.createElement(160, bitcal(flags, bitIndexToInteger(8)), 0))
        xmlfile.write(
            self.createElement(170, bitcal(flags, bitIndexToInteger(9)), 0))
        dps = (359, 372, 373, 374, 375, 376)
        for i in range(6):
            xmlfile.write(self.createBegin(dps[i]) + '\n')
            func = self.response.get_attr(self.response.Fields[6 * i + 4])
            xmlfile.write(self.createElement(360, num2DottedIP(func()), 0))
            func = self.response.get_attr(self.response.Fields[6 * i + 5])
            xmlfile.write(self.createElement(361, num2DottedIP(func()), 0))
            func = self.response.getStrFunction(self.response.Fields[6 * i + 6])
            xmlfile.write(self.createElement(29, func(), 0))
            func = self.response.getStrFunction(self.response.Fields[6 * i + 7])
            xmlfile.write(self.createElement(362, func(), 0))
            func = self.response.getStrFunction(self.response.Fields[6 * i + 9])
            xmlfile.write(self.createElement(363, func(), 0))
            func = self.response.get_attr(self.response.Fields[6 * i + 8])
            flags = func()
            xmlfile.write(
                self.createElement(364, bitcal(flags, bitIndexToInteger(0)), 0))
            xmlfile.write(
                self.createElement(365, bitcal(flags, bitIndexToInteger(1)), 0))
            xmlfile.write(
                self.createElement(366, bitcal(flags, bitIndexToInteger(2)), 0))
            xmlfile.write(
                self.createElement(367, bitcal(flags, bitIndexToInteger(3)), 0))
            xmlfile.write(
                self.createElement(368, bitcal(flags, bitIndexToInteger(4)), 0))
            xmlfile.write(
                self.createElement(369, bitcal(flags, bitIndexToInteger(5)), 0))
            xmlfile.write(
                self.createElement(370, bitcal(flags, bitIndexToInteger(7)), 0))
            xmlfile.write(
                self.createElement(371, bitcal(flags, bitIndexToInteger(8)), 0))
            xmlfile.write(self.createSpace(dps[i]) + self.createEnd(dps[i]))
        xmlfile.write(self.createSpace(357) + self.createEnd(357))


def bitcal(flags, bit):
    if flags & bit:
        return '1'
    else:
        return '0'


def printToXMLFile(xmlfile, q330):
    # header

    hd = LoadXML()
    xmlfile.write(hd.createBegin(0) + '\n')
    xmlfile.write('\n')
    xmlfile.write(hd.createBegin(1) + '\n')
    xmlfile.write(hd.createBegin(2) + '\n')
    xmlfile.write(hd.createElement(3, '1', 0))
    xmlfile.write(hd.createElement(4, 'Hocus v 0.1', 1))
    currtime = strftime('%a, %d %b %Y %H:%M:%S GMT', localtime())
    xmlfile.write(hd.createElement(6, currtime, 1))
    xmlfile.write(hd.createSpace(2) + hd.createEnd(2))

    # broadcast for physical interfaces for future use, maybe not needed
    InterfaceRes = q330.broadcastPhysicalInterfaces()

    # Real Register
    try:
        q330.realRegister()
        # for tokens
        try:
            Token4Load.print2XMLFile(q330, xmlfile)
        except:
            pass
        # for interfaces
        inter = LoadInterfaces(InterfaceRes)
        inter.prt2XMLFile(xmlfile)

        # for 4 data port
        for i in range(4):
            rqlog = c1_rqlog()
            rqlog.setDataPortNumber(i)
            dataPortRes = q330.sendCommand(rqlog)
            dp = LoadDataPort(dataPortRes)
            dp.prt2XMLFile(xmlfile)

        # for global
        globalRes = q330.sendCommand(c1_rqglob())
        glb = LoadGlobal(globalRes)
        glb.prt2XMLFile(xmlfile)

        # for sensor control
        senCtrlRes = q330.sendCommand(c1_rqsc())
        sc = LoadSensorControlMapping(senCtrlRes)
        sc.prt2XMLFile(xmlfile)

        # for slave
        slaveRes = q330.sendCommand(c1_rqspp())
        svl = LoadSlave(slaveRes)
        svl.prt2XMLFile(xmlfile)

        # for adv interfaces
        interfaceNumbers = (0, 1, 3)
        fix = q330.sendCommand(c1_rqfix())
        dataPortMemorySizes = []
        dataPortMemorySizes.append(fix.getDataPort1PacketMemorySize())
        dataPortMemorySizes.append(fix.getDataPort2PacketMemorySize())
        dataPortMemorySizes.append(fix.getDataPort3PacketMemorySize())
        dataPortMemorySizes.append(fix.getDataPort4PacketMemorySize())
        for i in range(3):
            c2rqphy = c2_rqphy()
            c2rqphy.setPhysicalInterfaceNumber(interfaceNumbers[i])
            advInterfsRes = q330.sendCommand(c2rqphy)
            api = LoadAdvancedPhysicalInterface(advInterfsRes)
            api.prt2XMLFile(xmlfile, dataPortMemorySizes)

        # for GPS
        gpsRes = q330.sendCommand(c2_rqgps())
        gps = GPSParameters(gpsRes)
        gps.prt2XMLFile(xmlfile)

        # for auto mass
        amassRes = q330.sendCommand(c2_rqamass())
        amass = AutoMassRecentering(amassRes)
        amass.prt2XMLFile(xmlfile)

        # for announcement
        try:
            anncRes = q330.sendCommand(c3_rqannc())
            annc = Announce(anncRes)
            annc.prt2XMLFile(xmlfile)
        except:
            pass  # for the q330s with old firmware and xml config
        xmlfile.write(hd.createSpace(1) + hd.createEnd(1))
    finally:
        q330.deregister()


if __name__ == '__main__':
    xmlfile = open('outpara.xml', 'w')
    q330 = RobustQ330(serial_number=eval("0x100000a27991462"))  # 799
    printToXMLFile(xmlfile, q330)
    xmlfile.close()
