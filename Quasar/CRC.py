"""
Implement the Quanterra CRC algorithm.
"""

from isti.utils import convertUtils

global crcTable
crcTable = None


# Nothing fancy here, we're dealing with 1 byte numbers,
# so we can ignore byte order completely
def __setup_crc_table():
    """Set up the CRC table, so we can refer to it later to calculate CRCs"""
    global crcTable

    if crcTable is not None:
        return

    crcTable = [None] * 256
    tdata = 0
    accum = 0

    for count in range(0, 256):
        #        tdata = count << 24
        tdata = convertUtils.shiftLeftPreserving32Bits(count, 24)
        accum = 0
        for bits in range(1, 9):
            if ((tdata ^ accum) & 0x80000000) != 0:
                #                accum = (accum << 1) ^ 1443300200
                accum = convertUtils.shiftLeftPreserving32Bits(accum, 1) ^ 1443300200
            else:
                #                accum = (accum << 1)
                accum = convertUtils.shiftLeftPreserving32Bits(accum, 1)
            #            tdata = tdata << 1
            tdata = convertUtils.shiftLeftPreserving32Bits(tdata, 1)
        crcTable[count] = accum


# Here, we have to deal with byte orders.
def computeCRC(dataBytes, length):
    """Compute a CRC for dataBytes, which has a length of length"""
    global crcTable
    if not crcTable:
        __setup_crc_table()

    localcrcTable = crcTable

    # is there a reason why newCrc isn't derived from this value? It's unused
    # and if it were we'd at least be able to produce a value on empty data
    newCrc = '\x00\x00\x00\x00'
    lng = 0
    firstByte = 0

    dataPtr = 0
    dataLen = length

    while dataLen > 0:
        # thisByte = ord(dataBytes[dataPtr])
        thisByte = dataBytes[dataPtr]
        # newCrc = (lng << 8) ^ localcrcTable[(firstByte ^ thisByte) & 255]
        newCrc = convertUtils.shiftLeftPreserving32Bits(lng, 8) ^ localcrcTable[(firstByte ^ thisByte) & 255]
        firstByte = newCrc >> 24
        lng = newCrc
        dataPtr += 1
        dataLen -= 1

    # return struct.unpack(asLongStr, crc)
    return newCrc


# Here, we have to deal with byte orders.
# def computeCRC(dataBytes, length):
#    """Compute a CRC for dataBytes, which has a length of length"""
#    global crcTable
#    setupCRCTable()
#    asByteStr = "BBBB"
#    asLongStr = ">i"
#    oneChar   = "B"

#    crc = struct.pack(asLongStr, 0)
#    dataPtr = 0
#    dataLen = length

#    while dataLen > 0:
#        crcBytes = struct.unpack(asByteStr, crc)
#        lng   = struct.unpack(asLongStr, crc)[0]
#        thisByte = struct.unpack(oneChar, dataBytes[dataPtr])[0]
#        newCrc = (lng << 8) ^ crcTable[(crcBytes[0] ^ thisByte) & 255]
#        crc = struct.pack(asLongStr, newCrc)
#        dataPtr += 1
#        dataLen -= 1

#    #return struct.unpack(asLongStr, crc)
#    return newCrc

# ignore the << futurewarnings.  If we're not in 2.4 or higher, futurewarnings
# arent in existance, so the try/except will just except to a pass

try:
    import warnings

    warnings.filterwarnings("ignore", r'.*', FutureWarning, 'Quasar.CRC', 0)
except:
    pass
