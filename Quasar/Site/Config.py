"""
Utilities and classes related to sitewide configuration
"""

import copy

from isti.utils import ConfigFile


class Config:
    """
    A class representing a sitewide configuration
    """

    def __init__(self, configFileName=None):
        if configFileName is not None:
            cfg = ConfigFile.ConfigFile(configFileName)
            self.DeviceMap = cfg._configuration
        else:
            self.DeviceMap = {'Q330': {},
                              'Q335': {}}

    def addDevice(self, tagNumber, serialNumber, ipAddress, port,
                  deviceType='Q330'):
        self.DeviceMap[deviceType][f'{tagNumber}'] = {
            'Serial': f'{serialNumber:x}',
            'IP': ipAddress,
            'BasePort': port
        }

    def getDeviceMap(self, deviceType='Q330'):
        """
        return a copy of the internal device map
        """
        return copy.deepcopy(self.DeviceMap[deviceType])

    def getDeviceMaps(self):
        return copy.deepcopy(self.DeviceMap)

    def getDeviceInfoByTag(self, tag, deviceType='Q330'):
        for key in list(self.DeviceMap[deviceType].keys()):
            if key in (tag, f'{tag}'):
                return self.DeviceMap[deviceType][key]

    def getDeviceInfoByIP(self, ip):
        return self.searchForDevice('IP', ip)

    def getDeviceInfoBySerial(self, rawSerial):
        serial = rawSerial
        if isinstance(serial, int):
            serial = f'{serial:x}'
        return self.searchForDevice('Serial', serial)

    def searchForDevice(self, searchBy, searchFor, deviceType='Q330'):
        for key in self.DeviceMap[deviceType]:
            device = self.DeviceMap[deviceType][key]
            if device[searchBy] in (searchFor, f'{searchFor}'):
                return device

    def writeToFile(self, filename):
        string = ConfigFile.configDictToString(self.DeviceMap)
        outfile = open(filename, 'w')
        outfile.write(string)
        outfile.close()

    def __repr__(self):
        return self.DeviceMap.__repr__()
