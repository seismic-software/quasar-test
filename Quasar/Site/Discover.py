from Quasar.Commands.c1_pollsn import c1_pollsn
from Quasar.Commands.c1_rqphy import c1_rqphy
from Quasar.Site.Config import Config
from Quasar.CmdID import CmdID
from Quasar.Q330 import Q330
from Quasar.Utils.Misc import num2DottedDouble, num2DottedIP, isIP, \
    killWhitespace


def searchQ330s():
    q330 = Q330(0, 0, 0)
    q330.setIPAddress('255.255.255.255')
    q330.setAuthCode(0)
    q330.setBasePort(65535)
    q330.setEthernetMACAddressHigh32("FF:FF:FF:FF")  # fly work?
    q330.setEthernetMACAddressLow16("FF:FF")  # fly? work?
    mysnResponses = q330.sendCommand(c1_pollsn(), receiveMultiple=1)

    c = Config()
    for response in mysnResponses:
        ip, port = response.getSourceIPAddressInfo()
        c.addDevice(response.getKMIPropertyTag(),
                    response.getSerialNumber(),
                    ip,
                    port)
    return c


def discover(defaultAuthCode=0, authCodeMap=None, receiveTimeout=20):
    """
    discover all Q330s reachable, and build a site configuration

    defaultAuthCode can be passed in to define a default authentication
    code for all registration attempts.  This defaults to zero.

    authCodeMap is a Python dict type, that maps either a Q330's tag or it's
    serial number to an auth code.  This can be used for exceptions to
    the default.
    """

    if authCodeMap is None:
        authCodeMap = {}
    q330 = Q330(0, '255.255.255.255', 5330)
    q330.setReceiveTimeout(receiveTimeout)
    mysnResponses = q330.sendCommand(c1_pollsn(), receiveMultiple=1)

    c = Config()

    for response in mysnResponses:
        # 10.172.53.1  if on serial1
        # 10.172.53.2  if on serial2
        # 10.172.53.3  if on serial3 (console)
        # 10.172.53.4  if on ethernet
        ip, port = response.getSourceIPAddressInfo()
        tag = response.getKMIPropertyTag()
        serial = response.getSerialNumber()

        if ip in ('10.172.53.1', '10.172.53.2', '10.172.53.3', '10.172.53.4'):
            # this is an "old style" broadcast response, we need to do some work
            # to determine the usable IP
            authcode = defaultAuthCode
            if tag in list(authCodeMap.keys()):
                authcode = authCodeMap[tag]
            elif f'{tag}' in list(authCodeMap.keys()):
                authcode = authCodeMap[f'{tag}']
            elif serial in list(authCodeMap.keys()):
                authcode = authCodeMap[serial]
            elif f'{serial}' in list(authCodeMap.keys()):
                authcode = authCodeMap[f'{serial}']

            q330.setSerialNumber(serial)
            q330.setAuthCode(authcode)
            q330.setBasePort(65535)
            q330.register()

            rqphy = c1_rqphy()
            phyResponses = q330.sendCommand(rqphy, receiveMultiple=1)
            phy = None
            for phyResponse in phyResponses:
                if phyResponse.getQDPCommand() == CmdID.C1_PHY and serial == phyResponse.getSerialNumber():
                    phy = phyResponse

            if phy:
                if ip == '10.172.53.1':
                    ip = phy.strSerialInterface1IPAddress()
                elif ip == '10.172.53.2':
                    ip = phy.strSerialInterface1IPAddress()
                elif ip == '10.172.53.3':
                    ip = phy.strSerialInterface3IPAddress()
                elif ip == '10.172.53.4':
                    ip = phy.strEthernetIPAddress()
                q330.setIPAddress(ip)
                q330.setBasePort(phy.getBasePort())

            try:
                q330.deregister()
            except:
                pass

            q330.setSerialNumber(0)
            q330.setIPAddress('255.255.255.255')

        c.addDevice(response.getKMIPropertyTag(), response.getSerialNumber(),
                    ip, port)

    return c


def discoverQ330s():
    """slow but can figure out the no-updated ip prob in old firmware"""
    c = discover(0)  # Quasar.Site.Config.discover
    return c


if __name__ == '__main__':
    print(num2DottedDouble(129, 129, 288))
    print(num2DottedIP(288))

    config = searchQ330s()
    deviceMap = config.getDeviceMap()
    for tagNum in list(deviceMap.keys()):
        thisDevice = deviceMap[tagNum]
        print(f'KMI Tag: {tagNum}')
        print(f'   IP: {thisDevice["IP"]}')
        print(f'   Serial: {thisDevice["Serial"]}')
        print(f'   Base Port: {thisDevice["BasePort"]:d}')
        print()

    print(isIP('0.0.0.0'))
    print(isIP('0.255.255.255'))
    print(isIP('0.0.0.256'))
    print(killWhitespace(""" first  line
    second line  """))
