#! /usr/bin/env python

import socket
import struct
import sys

FLASH_SIZE = 2097152  # Total 2M flash memory size
SECTOR_SIZE = 65536  # Each sector is 64K
SECTOR_COUNT = FLASH_SIZE / SECTOR_SIZE  # Total 32 Sectors
BOOTBLOCKS = 6  # Start from the 6th sector
FIRSTUSABLE = BOOTBLOCKS


def myplatform():
    return sys.platform


def dottedIP2Num(ip):
    """convert decimal dotted quad string to long integer"""
    try:
        return struct.unpack('>L', socket.inet_aton(ip))[0]
    except:
        return 0


def num2DottedIP(n):
    """convert long int to dotted quad string"""
    return socket.inet_ntoa(struct.pack('>L', n))


def makeMask(n):
    """return a mask of n bits as a long integer"""
    return (2 << n - 1) - 1


def ipToNetAndHost(ip, maskbits):
    """returns tuple (network, host) dotted-quad addresses given
    IP and mask size"""
    # (by Greg Jorgensen)

    n = dottedIP2Num(ip)
    m = makeMask(maskbits)

    host = n & m
    net = n - host

    return num2DottedIP(net), num2DottedIP(host)


def num2DottedDouble(ip1, ip2, n):
    if int(n) > 65025:
        print("Only handle the integer not larger than 65025")
        return '0.0.0.0'

    if int(ip1) < 0 or int(ip1) > 255:
        ip1 = 192
    if int(ip2) < 0 or int(ip2) > 255:
        ip2 = 168
    ip = str(ip1) + '.' + str(ip2) + '.0.0'
    ipn = dottedIP2Num(ip)
    ipn = ipn + int(n)

    return num2DottedIP(ipn)


def isIP(ip):
    try:
        dottedIP2Num(ip)
    except:
        return 0
    return 1


def isPosInteger(i):  # include 0
    try:
        i = int(i)
    except:
        return 0
    if i < 0: return 0
    return 1


def isPosFloat(f):  # include 0.0
    try:
        f = float(f)
    except:
        return 0
    if f < 0.0: return 0
    return 1


class SimpleCallback:
    """Create a callback shim. Based on code by Scott David Daniels
    (which also handles keyword arguments).
    so callback can have arguments.
    """

    def __init__(self, callback, *firstArgs):
        """
        create an object here
        """
        self.__callback = callback
        self.__firstArgs = firstArgs

    def __call__(self, *args):
        """
        create a callable object by defining __call__ method
        It seems that it usually does not need to give *args to this
        method.
        """
        return self.__callback(*(self.__firstArgs + args))


def killWhitespace(s):
    """
    remove redundant whitespace from a string
    """
    return ''.join(s.split())


def bit2Int(base, bit, loc):
    b = 1
    if bit:
        base += (b << loc)
    return base


def sortDictValues(adict):
    keys = list(adict.keys())
    keys.sort()
    return [adict[key] for key in keys]
