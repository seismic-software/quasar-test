import calendar
import copy
import datetime
import os
import struct
import sys
import time

import isti.utils.convertUtils
from isti.utils.GetSet import GetSet

MSEED_FIXED_HEADER = ">6scb5s2s3s2s10sHhhBBBBiHH"
MSEED_FIXED_HEADER_SIZE = struct.calcsize(MSEED_FIXED_HEADER)

BLOCKETTE_1000_FORMAT = ">HHBBBB"
BLOCKETTE_1001_FORMAT = ">HHBBBB"
BLOCKETTE_500_FORMAT = ">HHL10sBBL16s32s128s"

BLOCKETTE_1000_FORMAT_SIZE = struct.calcsize(BLOCKETTE_1000_FORMAT)
BLOCKETTE_1001_FORMAT_SIZE = struct.calcsize(BLOCKETTE_1001_FORMAT)
BLOCKETTE_500_FORMAT_SIZE = struct.calcsize(BLOCKETTE_500_FORMAT)


class NotEnoughDataException(Exception):
    pass


class UnknownBlocketteException(Exception):
    def __init__(self, blocketteNumber):
        self.blocketteNumber = blocketteNumber
        Exception.__init__(self)

    def getBlocketteNumber(self):
        return self.getBlocketteNumber()

    def __repr__(self):
        return 'UnknownBlocketteException: %s' % self.getBlocketteNumber()


class CorruptedMiniseedRecordException(Exception):
    pass


class Blockette(GetSet):
    def __init__(self):
        pass

    def setBlocketteType(self, t):
        self._blocketteType = t

    def getBlocketteType(self):
        return self._blocketteType

    def setNextBlocketteByteNumber(self, bn):
        self._nextBlocketteByteNumber = bn

    def getNextBlocketteByteNumber(self):
        return self._nextBlocketteByteNumber

    def setEncodingFormat(self, t):
        self._encodingFormat = t

    def getEncodingFormat(self):
        return self._encodingFormat

    def setWordOrder(self, t):
        self._wordOrder = t

    def getWordOrder(self):
        return self._wordOrder

    def setDataRecordLength(self, t):
        self._dataRecordLength = t

    def getDataRecordLength(self):
        return self._dataRecordLength

    def setHeaderSize(self, t):
        self._headerSize = t

    def getHeaderSize(self):
        return self._headerSize


class Packet(GetSet):
    def __init__(self, fixedHeader=None, blockettes=None, data=None):
        self.setFixedHeader(fixedHeader)
        self.setBlockettes(blockettes)
        self.setData(data)
        pass

    def getNumberOfDataBytes(self):
        numDataBytes = 0
        blockettes = self.getBlockettes()
        if blockettes:
            for blockette in blockettes:
                if blockette.getBlocketteType() == 1000:
                    numDataBytes += pow(2, blockette.getDataRecordLength())
        return numDataBytes

    def getPacketSize(self):
        global MSEED_FIXED_HEADER_SIZE
        global BLOCKETTE_1000_FORMAT_SIZE
        global BLOCKETTE_1001_FORMAT_SIZE

        numBytes = 0
        blockettes = self.getBlockettes()
        if blockettes:
            for blockette in blockettes:
                if blockette.getBlocketteType() == 1000:
                    numBytes += BLOCKETTE_1000_FORMAT_SIZE + \
                                pow(2, blockette.getDataRecordLength())
                elif blockette.getBlocketteType() == 1001:
                    numBytes += BLOCKETTE_1001_FORMAT_SIZE

        return numBytes  # + MSEED_FIXED_HEADER_SIZE

    def getEndTimeAsEpoch(self):
        return getRecordEndTimeAsEpoch(self.getFixedHeader())


FILENAME_TEMPLATE_MAP = {
    '$Station': 'stationIdentifierCode',
    '$Channel': 'channel',
    '$Network': 'network',
    '$Location': 'location'
}


class mseedFileWriter:
    def __init__(self, baseFilename=None, splitChannels=False):
        self.dataBuffer = ''
        self.currentPacketDataLength = None
        self.fileMap = {}
        self.splitChannels = splitChannels
        self.baseFilename = baseFilename
        self.allowDataOverwrite = False
        self.filenameTemplate = None
        self.requiredFieldsForFilename = None
        self.auxFilenameReplacements = None
        self.currentRecordStartTime = None
        self.channelStats = {}
        self.fileToSequenceNumberMap = {}
        self.fileToLastRecordMap = {}
        self.channelToLastRecordMap = {}
        self.forcedDatafileName = None
        self.allowedDatafiles = None
        self.sequenceNumbersToSkip = []
        self.maxFileSize = None
        self.logStatus = None
        self.ignoreDataStartingBefore = None
        self.alreadyIgnoredFirstRecord = False
        self.debug = 0
        self.previousPacket = None
        self.dataDirectory = None

    def setDataDirectory(self, v):
        self.dataDirectory = v

    def getDataDirectory(self):
        return self.dataDirectory

    def setIgnoreDataStartingBefore(self, v):
        self.ignoreDataStartingBefore = v

    def setForcedDatafileName(self, n):
        self.forcedDatafileName = n

    def setAllowedDatafileNames(self, l):
        self.allowedDatafiles = l

    def setMaxFileSizeInBytes(self, val):
        self.maxFileSize = val

    def setSequenceNumbersToSkip(self, skiplist):
        self.sequenceNumbersToSkip = skiplist

    def setAuxFilenameReplacements(self, replacementMap):
        """
        add new replacements to our replacement map.  These can either
        resolve to a string, or a function call.  These will be common to
        all files created.
        """
        self.auxFilenameReplacements = replacementMap

    def incrementStatsForChannel(self, channel):
        if channel not in self.channelStats:
            self.channelStats[channel] = 0

        self.channelStats[channel] += 1

    def getChannelStats(self):
        return self.channelStats

    def setFilenameTemplate(self, filenameTemplate):
        global FILENAME_TEMPLATE_MAP
        # first, we'll set the template
        self.filenameTemplate = filenameTemplate
        # now lets do some basic parsing, so we can cache which items are
        # needed
        self.requiredFieldsForFilename = []

        for fieldName in list(FILENAME_TEMPLATE_MAP.keys()):
            if fieldName in self.filenameTemplate:
                self.requiredFieldsForFilename.append(
                    FILENAME_TEMPLATE_MAP[fieldName])

    def getNextSequenceNumberForFile(self, file):
        self.fileToSequenceNumberMap[file] += 1
        return self.fileToSequenceNumberMap[file]

    def getFileForData(self, fixedHeader):
        if not self.filenameTemplate:
            return self.getFileForChannel(fixedHeader['channel'])
        else:
            idTuple = self.makeTupleForHeader(fixedHeader)
            if idTuple not in self.fileMap:
                thisFileName = self.filenameTemplate
                if self.forcedDatafileName is not None:
                    thisFileName = self.forcedDatafileName
                for key, value in list(FILENAME_TEMPLATE_MAP.items()):
                    if key in thisFileName:
                        val = fixedHeader[value].strip()
                        if val == '':
                            val = '_'
                        thisFileName = thisFileName.replace(key, val)
                if self.auxFilenameReplacements:
                    for key, value in list(
                            self.auxFilenameReplacements.items()):
                        thisFileName = thisFileName.replace(key, value)

                if not self.forcedDatafileName and not self.allowDataOverwrite \
                        and os.path.isfile(thisFileName) and \
                        (not self.allowedDatafiles or
                         thisFileName not in self.allowedDatafiles):
                    tmpName = thisFileName
                    index = 1
                    while os.path.isfile(tmpName):
                        tmpName = "%s_%d" % (thisFileName, index)
                        index += 1
                    thisFileName = tmpName
                if callable(self.logStatus):
                    self.logStatus('Opening datafile: %s' % thisFileName)

                thisFileName = os.path.join(self.getDataDirectory(),
                                            thisFileName)
                dirPart = os.sep.join(thisFileName.split(os.sep)[:-1])
                if not os.path.isdir(dirPart):
                    os.makedirs(dirPart)

                thisFile = open(thisFileName, 'ab')
                if self.forcedDatafileName or \
                        (self.allowedDatafiles and
                         thisFileName in self.allowedDatafiles):
                    seq = getLastSequenceNumberInVolume(thisFileName)
                    self.fileToSequenceNumberMap[thisFile] = seq
                else:
                    self.fileToSequenceNumberMap[thisFile] = 0
                self.fileToLastRecordMap[thisFile] = None
                self.channelStats[fixedHeader['channel']] = 0
                self.fileMap[idTuple] = thisFile
            return self.fileMap[idTuple]

    def closeFile(self, f):
        del self.fileToLastRecordMap[f]
        del self.fileToSequenceNumberMap[f]
        for key in list(self.fileMap.keys()):
            val = self.fileMap[key]
            if val == f:
                del self.fileMap[key]

        datafileName = f.name
        if callable(self.logStatus):
            self.logStatus('Closing datafile: %s...' % datafileName)
        f.close()
        if callable(self.logStatus):
            self.logStatus('%s closed successfully' % datafileName)

    def setStatusLoggingFunction(self, f):
        self.logStatus = f

    def makeTupleForHeader(self, header):
        l = []
        for key in self.requiredFieldsForFilename:
            l.append(header[key])
        return tuple(l)

    def setAllowDataOverwrite(self, val):
        """ Should we overwrite datafiles? Default is false """
        self.allowDataOverwrite = val

    def getFileForChannel(self, channel):
        if not self.splitChannels:
            channel = 'All'

        if channel not in self.fileMap:
            if self.forcedDatafileName:
                fname = self.forcedDatafileName
            elif channel == 'All':
                fname = self.baseFilename
            else:
                fname = '%s_%s' % (self.baseFilename, channel)

            # force a rename, if a datafile exists with this name
            if not self.forcedDatafileName and not \
                    self.allowDataOverwrite and os.path.isfile(fname) and \
                    (not self.allowedDatafiles or
                     fname not in self.allowedDatafiles):
                tmpName = fname
                index = 1
                while os.path.isfile(tmpName):
                    tmpName = '%s_%d' % (fname, index)
                    index += 1
                fname = tmpName

            if callable(self.logStatus):
                self.logStatus('Opening datafile: %s' % fname)

            self.fileMap[channel] = open(fname, 'ab')
            if self.forcedDatafileName or \
                    (self.allowedDatafiles and fname in self.allowedDatafiles):
                seq = getLastSequenceNumberInVolume(fname)
                self.fileToSequenceNumberMap[self.fileMap[channel]] = seq
            else:
                self.fileToSequenceNumberMap[self.fileMap[channel]] = 0
            self.fileToLastRecordMap[self.fileMap[channel]] = None
            self.channelStats[channel] = 0

        return self.fileMap[channel]

    def write(self, byte_array):
        self.dataBuffer += byte_array
        self.processBuffer()

    def close(self):
        closedFiles = []
        for channel, fileOb in list(self.fileMap.items()):
            if fileOb not in closedFiles:
                # file.close()
                self.closeFile(fileOb)
            closedFiles.append(fileOb)

    def getEarliestLastRecordEndTime(self):
        files = list(self.fileToLastRecordMap.keys())

        earliest = self.fileToLastRecordMap[files[0]].getEndTimeAsEpoch()

        for file in files:
            thisEnd = self.fileToLastRecordMap[file].getEndTimeAsEpoch()
            if not thisEnd:
                continue

            if thisEnd < earliest or earliest is None:
                earliest = thisEnd

        return earliest

    def getChannelsFilesSamplerateAndLastRecordEndTime(self):
        ret = {}

        for channel in list(self.channelToLastRecordMap.keys()):
            record = self.channelToLastRecordMap[channel]
            fixedHeader = record.getFixedHeader()
            startTime = convertBTIMEToEpochTime(
                decodeBTIME(fixedHeader['recordStartTime']))
            endTime = record.getEndTimeAsEpoch()
            datafile = self.getFileForData(fixedHeader)
            sampleRate = getSampleRate(fixedHeader)
            if len(fixedHeader['location'].strip()):
                ret[f'{fixedHeader["location"]}-{fixedHeader["channel"]}'] = \
                    (startTime, endTime, datafile.name, sampleRate)
            else:
                ret[f'{fixedHeader["channel"]}'] = \
                    (startTime, endTime, datafile.name, sampleRate)

        return ret

    def processBuffer(self):
        if self.currentPacketDataLength and len(
                self.dataBuffer) < self.currentPacketDataLength:
            return

        while len(self.dataBuffer) > MSEED_FIXED_HEADER_SIZE:
            headerItems = getFixedHeader(self.dataBuffer, 0)
            self.currentRecordStartTime = headerItems['recordStartTime']
            thisPacket = Packet()

            blockettes = []
            try:
                nextBlocketteStart = 48
                while headerItems['numberOfBlockettesThatFollow'] > \
                        len(blockettes) and nextBlocketteStart != 0:
                    blockette, nextBlocketteStart = \
                        getNextBlocketteAndStartingOffset(
                            self.dataBuffer, nextBlocketteStart, self.debug)
                    # set up some lookahead optimizations.
                    # We memorize the size of this data record, and
                    # then we can stop checking for packet completeness
                    # until we have at least that many bytes in the buffer
                    if blockette.getBlocketteType() == 1000:
                        self.currentPacketDataLength = \
                            pow(2, blockette.getDataRecordLength())
                        if len(self.dataBuffer) < self.currentPacketDataLength:
                            return
                        blockettes.append(blockette)
                        break
                thisPacket.setFixedHeader(headerItems)
                thisPacket.setBlockettes(blockettes)

                packetSize = thisPacket.getNumberOfDataBytes()
                if len(self.dataBuffer) < packetSize:
                    break
                dataFile = self.getFileForData(headerItems)
                if self.maxFileSize and \
                        (dataFile.tell() + packetSize > self.maxFileSize):
                    # once we hit the limit, time to close and start a new file
                    self.closeFile(dataFile)
                    self.forcedDatafileName = None
                    dataFile = self.getFileForData(headerItems)

                recordEpochStartTime = convertBTIMEToEpochTime(
                    decodeBTIME(headerItems['recordStartTime']))
                recordEpochEndTime = getRecordEndTimeAsEpoch(headerItems)
                if (headerItems['sequenceNumber'] not in
                    self.sequenceNumbersToSkip) and \
                        (recordEpochStartTime >= self.ignoreDataStartingBefore
                         or (recordEpochStartTime <
                             self.ignoreDataStartingBefore and
                             self.ignoreDataStartingBefore -
                             recordEpochStartTime < 1)):
                    newSeq = \
                        f'{self.getNextSequenceNumberForFile(dataFile):06d}'
                    dataToWrite = f'{newSeq}{self.dataBuffer[6:packetSize]}'
                    dataFile.write(dataToWrite)
                    self.incrementStatsForChannel(headerItems['channel'])
                self.fileToLastRecordMap[dataFile] = thisPacket
                self.channelToLastRecordMap[headerItems['channel']] = thisPacket
                self.dataBuffer = self.dataBuffer[packetSize:]
                self.currentPacketDataLength = None
                self.previousPacket = thisPacket
            except NotEnoughDataException as e:
                if self.debug:
                    exceptionInfo = isti.utils.convertUtils.formatExceptionInfo()
                    errMsg = f"Exception: {exceptionInfo[0]}\nMessage: " \
                             f"{exceptionInfo[1]}\n Traceback: " \
                             f"{' '.join(exceptionInfo[2])}"
                    print('Got a NotEnoughDataException')
                    print(errMsg)
                break


def getSampleRate(hdr):
    # if there's no sampleRateFactor, then the
    # end time of the record is meaningless
    if hdr['sampleRateFactor'] == 0:
        return None

    factor = abs(hdr['sampleRateFactor'])
    multiplier = abs(hdr['sampleRateMultiplier'])

    if hdr['sampleRateFactor'] < 0:
        tmpResult = float(multiplier) / factor
    else:
        tmpResult = factor * multiplier

    return tmpResult


def getRecordEndTimeAsEpoch(hdr):
    sampleRate = getSampleRate(hdr)
    if not sampleRate:
        return None

    lengthInSeconds = float(hdr['numberOfSamples']) / sampleRate

    epochStart = convertBTIMEToEpochTime(decodeBTIME(hdr['recordStartTime']))
    epochEnd = epochStart + lengthInSeconds
    return epochEnd


def getFixedHeader(data, startingOffset):
    global MSEED_FIXED_HEADER
    fixedHeaderElements = [
        'sequenceNumber',
        'dataHeaderQualityIndicator',
        'reservedByte',
        'stationIdentifierCode',
        'location',
        'channel',
        'network',
        'recordStartTime',
        'numberOfSamples',
        'sampleRateFactor',
        'sampleRateMultiplier',
        'activityFlags',
        'ioAndClockFlags',
        'DataQualityFlags',
        'numberOfBlockettesThatFollow',
        'timeCorrection',
        'beginningOfData',
        'firstBlockette'
    ]
    parts = struct.unpack(MSEED_FIXED_HEADER,
                          data[startingOffset:startingOffset + 48])
    partMap = {}
    for element in fixedHeaderElements:
        partMap[element] = parts[fixedHeaderElements.index(element)]

    try:
        int(partMap['sequenceNumber'])
    except:
        print(
            f'Data length: {len(data):d}, starting offset: {startingOffset:d}')
        print('-----')
        print(f'Got a bogus sequence of {repr(partMap["sequenceNumber"])}. ')
    return partMap


def getNextBlocketteAndStartingOffset(data, startingOffset, debug=1):
    if len(data) - startingOffset < 4:
        if debug:
            print(
                f'data length - startingOffset < 4 '
                f'({len(data):d} - {startingOffset:d} < 4)')
        raise NotEnoughDataException()
    try:
        (blocketteType, nextStartingOffset) = \
            struct.unpack(">HH", data[startingOffset:startingOffset + 4])
    except:
        print(
            f"Got an error: datalen: {len(data):d}, offset: {startingOffset:d}")

    nextBlockette = None
    global BLOCKETTE_1000_FORMAT
    global BLOCKETTE_1001_FORMAT
    global BLOCKETTE_500_FORMAT
    global BLOCKETTE_1000_FORMAT_SIZE
    global BLOCKETTE_1001_FORMAT_SIZE
    global BLOCKETTE_500_FORMAT_SIZE
    if blocketteType == 1000:

        if (len(data) - startingOffset) < BLOCKETTE_1000_FORMAT_SIZE:
            if debug:
                print(
                    f'data length - startingOffset < format size ('
                    f'{len(data):d} - {startingOffset:d} < '
                    f'{BLOCKETTE_1000_FORMAT_SIZE:d})')
            raise NotEnoughDataException()
        (blocketteType, nextBlocketteByteNumber, encodingFormat, wordOrder,
         dataRecordLength, reserved) = struct.unpack(
            BLOCKETTE_1000_FORMAT, data[startingOffset:startingOffset + 8])
        nextStartingOffset = nextBlocketteByteNumber
        nextBlockette = Blockette()
        nextBlockette.setBlocketteType(blocketteType)
        nextBlockette.setNextBlocketteByteNumber(nextBlocketteByteNumber)
        nextBlockette.setEncodingFormat(encodingFormat)
        nextBlockette.setWordOrder(wordOrder)
        nextBlockette.setDataRecordLength(dataRecordLength)
        nextBlockette.setHeaderSize(8)
    else:
        nextBlockette = Blockette()
        nextBlockette.setBlocketteType(blocketteType)
    return nextBlockette, nextStartingOffset


def readMseedData(dataBytes, ignoreData=False, onlyGetBlockette1000=False):
    packets = []
    curPos = 0

    while curPos < len(dataBytes):

        # get the next header, advance the pointer
        fixedHeader = getFixedHeader(dataBytes, curPos)
        curPos = curPos + fixedHeader['firstBlockette']

        # spin, getting all blockettes
        blockettes = []
        blockette, nextBlocketteStart = getNextBlocketteAndStartingOffset(
            dataBytes, curPos)
        blockettes.append(blockette)

        if not onlyGetBlockette1000 or (
                onlyGetBlockette1000 and blockette.getBlocketteType() != 1000):
            while fixedHeader['numberOfBlockettesThatFollow'] > len(
                    blockettes):  # nextBlocketteStart != 0:
                blockette, nextBlocketteStart = \
                    getNextBlocketteAndStartingOffset(
                        dataBytes, nextBlocketteStart)
                blockettes.append(blockette)
                if onlyGetBlockette1000 and blockette.getBlocketteType() == 1000:
                    break
        thisPacket = Packet()
        thisPacket.setFixedHeader(fixedHeader)
        thisPacket.setBlockettes(blockettes)

        packetDataLength = (thisPacket.getNumberOfDataBytes()) - 48
        if ignoreData:
            thisPacket.setData([])
        else:
            thisPacket.setData(dataBytes[curPos:curPos + packetDataLength])

        packets.append(thisPacket)
        curPos += packetDataLength

    return packets


def rewriteMseedFile(infilename, outfilename, recordsToSkip=None):
    """
    Rewrite an mseed file, skipping certian records and assigning new sequence
    numbers as we go
    """

    if recordsToSkip is None:
        recordsToSkip = []
    outfile = mseedFileWriter()
    outfile.setForcedDatafileName(outfilename)
    outfile.setSequenceNumbersToSkip(recordsToSkip)

    infile = open(infilename, 'rb')

    data = infile.read(4096 * 4)
    while data:
        outfile.write(data)
        data = infile.read(4096 * 4)

    infile.close()
    outfile.close()


def readMseedFile(filename, ignoreData=False, lastNBytes=0,
                  onlyGetBlockette1000=False):
    dataFile = open(filename, 'rb')
    if lastNBytes:
        dataFile.seek(0 - lastNBytes, 2)
    dataBytes = dataFile.read()
    return readMseedData(dataBytes, ignoreData, onlyGetBlockette1000)


def getSignatureFromHeader(hdr):
    thisSignature = (
        hdr['stationIdentifierCode'],
        hdr['channel'],
        hdr['network'],
        hdr['location'],
        hdr['recordStartTime'],
        hdr['numberOfSamples']
    )
    return thisSignature


def findDuplicateRecordsInMseedFile(filename):
    # open the file, seek to the end, determine the file size, and
    # go back to the start
    mseedFile = open(filename, 'rb')
    mseedFile.seek(0, 2)
    fileSize = mseedFile.tell()
    mseedFile.seek(0, 0)

    recordSignatures = {}
    duplicatedRecords = []
    dataChunk = ''
    checkpointTime = time.time()
    while mseedFile.tell() < fileSize:
        dataChunk += mseedFile.read(4096)

        try:
            records = readMseedData(dataChunk, ignoreData=True,
                                    onlyGetBlockette1000=True)
        except NotEnoughDataException:
            if mseedFile.tell() < fileSize:
                continue
            else:
                break
        dataChunk = ''
        for record in records:
            hdr = record.getFixedHeader()
            thisSignature = getSignatureFromHeader(hdr)
            if thisSignature in recordSignatures:
                duplicatedRecords.append(hdr['sequenceNumber'])
            else:
                recordSignatures[thisSignature] = 1

    return duplicatedRecords


def decodeBTIME(byte_array):
    (year, dayOfYear, hoursOfDay, minutesOfDay, secondsOfDay, Spare,
     precision) = struct.unpack('>HHBBBBh', byte_array)
    return {
        'year': year,
        'dayOfYear': dayOfYear,
        'hoursOfDay': hoursOfDay,
        'minutesOfDay': minutesOfDay,
        'secondsOfDay': secondsOfDay,
        'precision': precision
    }


def convertBTIMEToEpochTime(btime):
    startDay = datetime.date(btime['year'], 1, 1)
    startDay = startDay.fromordinal(
        startDay.toordinal() + (btime['dayOfYear'] - 1))
    ret = calendar.timegm(
        (btime['year'], startDay.month, startDay.day, btime['hoursOfDay'],
         btime['minutesOfDay'], btime['secondsOfDay'],
         0, 0, -1)
    )
    ret += float('0.%4.4d' % (btime['precision']))
    return ret


def getSequenceNumbersInVolume(filename, lastNBytes=0):
    records = readMseedFile(filename, ignoreData=True, lastNBytes=lastNBytes)
    ret = []

    for record in records:
        hdr = record.getFixedHeader()
        ret.append(int(hdr['sequenceNumber']))

    return ret


def getLastSequenceNumberInVolume(filename):
    try:
        seqs = getSequenceNumbersInVolume(filename, 16384)
        if len(seqs) > 0:
            return seqs[-1]
        else:
            return 0
    except:
        return 0


def gapCheckPackets(packets, gapThresholdInSeconds=None):
    gaps = []
    expectedNextStart = None
    previousPacketHeader = None
    previousPacketQuestionableTime = 1
    for packet in packets:
        clockLocked = 0
        fixedHeader = packet.getFixedHeader()
        questionableTime = fixedHeader['DataQualityFlags'] & 128
        btime = decodeBTIME(fixedHeader['recordStartTime'])
        startDay = datetime.date(btime['year'], 1, 1)
        startDay = startDay.fromordinal(
            startDay.toordinal() + (btime['dayOfYear'] - 1))
        packetStarted = time.mktime(
            (btime['year'], startDay.month, startDay.day, btime['hoursOfDay'],
             btime['minutesOfDay'],
             btime['secondsOfDay'], 0, 0, -1)
        )
        packetStarted += float('0.%d' % (btime['precision']))
        sampleRate = fixedHeader['sampleRateFactor'] * fixedHeader[
            'sampleRateMultiplier']
        packetEnded = packetStarted + (
                    float(fixedHeader['numberOfSamples']) / float(sampleRate))
        if expectedNextStart:
            delta = packetStarted - expectedNextStart
            if not gapThresholdInSeconds:
                gapThresholdInSeconds = .5 * (1.0 / float(sampleRate))
            if delta > gapThresholdInSeconds:
                reason = ''
                if not questionableTime and previousPacketQuestionableTime:
                    reason = 'GPS resync'
                gaps.append({
                    'reason': reason,
                    'lengthInSeconds': delta,
                    'packetBefore': previousPacketHeader['sequenceNumber'],
                    'packetAfter': fixedHeader['sequenceNumber']
                })

        expectedNextStart = packetEnded
        previousPacketHeader = copy.deepcopy(fixedHeader)
        previousPacketQuestionableTime = questionableTime
    return gaps


if __name__ == '__main__':
    packets = readMseedFile(sys.argv[1])
    gaps = gapCheckPackets(packets)
    print('found %d gaps' % (len(gaps)))
    for gap in gaps:
        print('Gap of %f seconds between sequence %s and %s (%s)' % (
        gap['lengthInSeconds'], gap['packetBefore'], gap['packetAfter'],
        gap['reason']))
