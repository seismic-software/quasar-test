
import time


def makeEpochTime(timeStr):
    """
    Take a time in the form of yyyy-mm-dd hh:mm:ss and convert to epoch time
     """
    datePart, timePart = timeStr.strip().split(' ')
    year, month, day = datePart.split('-')
    hour, minute, second = timePart.split(':')

    timeTup = (int(year), int(month), int(day), int(hour), int(minute),
               int(second), 0, 0, -1)
    return time.mktime(timeTup)
