# Matches some urllib functionality that has been deprecated for public
# use and is no longer intended for import in python
from urllib.parse import urlparse


def unwrap(url):
    """Transform a string like '<URL:scheme://host/path>' into
    'scheme://host/path'.

    The string is returned unchanged if it's not a wrapped URL.
    """
    # Copied directly from the urllib.parse function unwrap
    # which was not able to be imported directly into pycharm for some reason
    url = str(url).strip()
    if url[:1] == '<' and url[-1:] == '>':
        url = url[1:-1].strip()
    if url[:4] == 'URL:':
        url = url[4:].strip()
    return url



def splithost(url):
    """splithost('//host[:port]/path') --> 'host[:port]', '/path'."""
    parse_tuple = urlparse(url)
    host = parse_tuple.netloc
    if parse_tuple.port is not None:
        host += ':' + str(parse_tuple.port)
    return host, parse_tuple.path


def splittype(url):
    """splittype('type:opaquestring') --> 'type', 'opaquestring'."""
    parse_tuple = urlparse(url)
    scheme = parse_tuple.scheme
    # returns the url scheme (i.e., 'https', 'file') and then the rest of the
    # url with the colon separator removed
    return scheme, url[len(scheme) + 1:]
