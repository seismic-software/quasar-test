def channelMatch(specificChannel, channels):
    """
    Is [specificChannel] represented in the channel mask [channels]
    This takes into account wildcarding and other valid channel mask
    operations, as specified in the baler's web interface
    """
    # first, lowercase everything, to make matching easier
    specificChannel = specificChannel.lower()
    channels = channels.lower()

    # handle the case where they're the same
    if specificChannel == channels:
        return 1

    # handle the case where this channel has been excluded
    if '~%s' % specificChannel in channels:
        return 0

    if len(specificChannel) == len(channels):
        for i in range(0, len(specificChannel)):
            if channels[i] != '?' and channels[i] != specificChannel[i]:
                return 0
        return 1

    # there could be more than one channel in channels
    if ' ' in channels:
        chanList = channels.split(' ')
        for channel in chanList:
            if channelMatch(specificChannel, channel):
                return 1
    return 0
