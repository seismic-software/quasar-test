"""
Several convenience functions for dealing with Statuses.

Also included here are several lists, represeting the different
types of status replies, and the data members that are included
in each.
"""

from isti.utils import Bits

# Generic
structBitmap = '>L'
structUserMessage = 'L79p'
structSatelliteEntry = 'HHHH'
structARPEntry = 'LLHH'

# Q330
structThreadEntry = 'LHBBLHH'
structAuxBoardEntry = 'L'
structSerialSensorEntry = '8HLL'
structEnvironmentalProcessorSDI = 'BBBB6s13sB3sB'  # x 4
structEnvironmentalProcessorADC = 'LLBBHLLLL'  # x 1

# Q335
structFrontEndEntry = 'LLLLLHHLLLLHHHHQBBBB'

StatusDefinitions = {
    "Q330": {
        'Global': 'HHHHLLLLLLHHHHHHHHL',
        'Gps': 'HHHH10p12p6p12p14p14pLL',
        'PowerSupply': 'HhHHHHhHHBB',
        'BoomPosition': '6h8HL',
        'Thread': 'HHLHH',
        'PLL': 'ffffLLHH',
        'Satellites': 'HH',
        'ARP': 'HH',
        'DataPort1': 'LLLLLLHHHH',
        'DataPort2': 'LLLLLLHHHH',
        'DataPort3': 'LLLLLLHHHH',
        'DataPort4': 'LLLLLLHHHH',
        'Serial1': 'LLHHLLLLLL',
        'Serial2': 'LLHHLLLLLL',
        'Serial3': 'LLHHLLLLLL',
        'Ethernet': 'LLHH14LHHL',
        'Baler': 'LLHHLLHHLLHHLLHH',
        'DynamicIP': '4L',
        'AuxiliaryBoard': '4H',
        'SerialSensor': 'HH',
        'EnvironmentalProcessor': 'fffLLHHLLLLLLLLHHHHHBBLHHLLLLLL',
    },
    "Q335": {
        'Global': 'HHHBBLLLLLLHHHHHHHHL',
        'Gps': 'HHHH10p12p6p12p14p14pLL',
        'BoomPosition': '6h10H',
        'PLL': 'ffffLLHH',
        'Satellites': 'HH',
        'ARP': 'HH',
        'DataPort1': 'LLLLLLHHHH',
        'DataPort2': 'LLLLLLHHHH',
        'DataPort3': 'LLLLLLHHHH',
        'DataPort4': 'LLLLLLHHHH',
        'Communications': 'LLHHLLLLBBBB',
        'FrontEnd': 'HH',
    },
}

GlobalFields = {
    "Q330": [
        'AcquisitionControl',
        'ClockQuality',
        'MinutesSinceLoss',
        'AnalogVoltageControlValue',
        'SecondsOffset',
        'USecOffset',
        'TotalTimeInSeconds',
        'PowerOnTimeInSeconds',
        'TimeOfLastResync',
        'TotalNumberOfResyncs',
        'GPSStatus',
        'CalibratorStatus',
        'SensorControlBitmap',
        'CurrentVCO',
        'DataSequenceNumber',
        'PLLRunningIfSet',
        'StatusInputs',
        'MiscInputs',
        'CurrentDataSequenceNumber',
    ],
    "Q335": [
        'AcquisitionControl',
        'ClockQuality',
        'MinutesSinceLoss',
        'FaultCode',
        'Spare1',
        'SecondsOffset',
        'USecOffset',
        'TotalTimeInSeconds',
        'PowerOnTimeInSeconds',
        'TimeOfLastResync',
        'TotalNumberOfResyncs',
        'GPSStatus',
        'CalibratorStatus',
        'SensorControlBitmap',
        'CurrentCoreProcessorVCO',
        'DataSequenceNumber',
        'PLLRunningIfSet',
        'StatusInputs',
        'MiscInputs',
        'CurrentDataSequenceNumber',
    ],
}

_GpsFields = [
    'GPSPowerOnOffTime',
    'GPSOnIfNotZero',
    'NumberOfSatellitesUsed',
    'NumberOfSatellitesInView',
    'GPSTimeString',
    'GPSDateString',
    'GPSFixString',
    'GPSHeightString',
    'GPSLatitudeString',
    'GPSLongitudeString',
    'TimeOfLastGood1PPS',
    'TotalChecksumErrors',
]
GpsFields = {
    "Q330": _GpsFields,
    "Q335": _GpsFields,
}

PowerSupplyFields = {
    "Q330": [
        'ChargingPhase',
        'BatteryTemperature',
        'BatteryCapacity',
        'DepthOfDischarge',
        'BatteryVoltage',
        'InputVoltage',
        'BatteryCurrent',
        'AbsorptionSetPoint',
        'FloatSetPoint',
        'Alerts',
        'LoadsOff',
    ],
}

BoomFields = {
    "Q330": [
        'Channel1Boom',
        'Channel2Boom',
        'Channel3Boom',
        'Channel4Boom',
        'Channel5Boom',
        'Channel6Boom',
        'AnalogPositiveSupply',
        'AnalogNegativeSupply',
        'InputPower',
        'SystemTemperature',
        'MainCurrent',
        'GPSAntennaCurrent',
        'Seismometer1Temperature',
        'Seismometer2Temperature',
        'SlaveProcessorTimeouts',
    ],
    "Q335": [
        'Channel1Boom',
        'Channel2Boom',
        'Channel3Boom',
        'Channel4Boom',
        'Channel5Boom',
        'Channel6Boom',
        'AnalogPositiveSupply',
        'SuperCapBackupVoltage',
        'InputVoltage',
        'SystemTemperature',
        'MainCurrent',
        'GPSAntennaCurrent',
        'SensorATemperature',
        'SensorBTemperature',
        'SensorACurrent',
        'SensorBCurrent',
    ],
}

ThreadFields = {
    "Q330": [
        'NumberOfEntries',
        'SizeOfThisBlock',
        'TotalSystemTimeInHigh32',
        'TotalSystemTimeInLow16',
        'Spare1',
    ],
}

ThreadEntryFields = {
    "Q330": [
        'TotalRunningTimeHigh32',
        'TotalRunningTimeLow16',
        'Priority',
        'PriorityCounter',
        'TimeSinceLastRunHigh32',
        'TimeSinceLastRunLow16',
        'Flags',
    ],
}

_PLLFields = [
    'InitialVCO',
    'TimeError',
    'RMSVCO',
    'BestVCO',
    'Spare1',
    'TicksSinceLastUpdate',
    'Km',
    'State',
]
PLLFields = {
    "Q330": _PLLFields,
    "Q335": _PLLFields,
}

_SatelliteFields = [
    'NumberOfEntries',
    'LengthOfThisBlock',
]
SatelliteFields = {
    "Q330": _SatelliteFields,
    "Q335": _SatelliteFields,
}

_SatelliteEntryFields = [
    'SatelliteNumber',
    'Elevation',
    'Azimuth',
    'SNR',
]
SatelliteEntryFields = {
    "Q330": _SatelliteEntryFields,
    "Q335": _SatelliteEntryFields,
}

_ARPFields = [
    'NumberOfEntries',
    'LengthOfThisBlock',
]
ARPFields = {
    "Q330": _ARPFields,
    "Q335": _ARPFields,
}

_ARPEntryFields = [
    'IPAddress',
    'MACAddressHigh32',
    'MACAddressLow16',
    'TimeoutInSeconds',
]
ARPEntryFields = {
    "Q330": _ARPEntryFields,
    "Q335": _ARPEntryFields,
}

_DataPortFields = [
    'TotalDataPacketsSent',
    'TotalPacketsResent',
    'TotalFillPacketsSent',
    'ReceiveSequenceErrors',
    'BytesOfPacketCurrentlyUsed',
    'TimeOfLastDataPacketAcked',
    'PhysicalInterfaceNumber',
    'DataPortNumber',
    'RetransmissionTimeout',
    'DataPortFlags',
]
DataPortFields = {
    "Q330": _DataPortFields,
    "Q335": _DataPortFields,
}

SerialFields = {
    "Q330": [
        'ReceiveChecksumErrors',
        'TotalIOErrors',
        'PhysicalInterfaceNumber',
        'Spare1',
        'DestinationUnreachableICMPPacketsReceived',
        'SourceQuenchICMPPacketsReceived',
        'EchoRequestICMPPacketsReceived',
        'RedirectICMPPacketsReceived',
        'CharacterOverruns',
        'FramingErrors',
    ],
}

CommunicationsFields = {
    "Q335": [
        'LastTime',
        'TotalNumberOfCycles',
        'StatusAndTimeouts',
        'OnMinutes',
        'Spare1',
        'BalerCommunicationsStatus',
        'Special',
        'EthernetCommunicationsStatus',
        'Data1Hold',
        'Data2Hold',
        'Data3Hold',
        'Spare2',
    ]
}

EthernetFields = {
    "Q330": [
        'ReceiveChecksumErrors',
        'TotalIOErrors',
        'PhysicalInterfaceNumber',
        'Spare1',
        'DestinationUnreachableICMPPacketsReceived',
        'SourceQuenchICMPPacketsReceived',
        'EchoRequestICMPPacketsReceived',
        'RedirectICMPPacketsReceived',
        'RuntFrames',
        'CRCErrors',
        'BroadcastFrames',
        'UnicastFrames',
        'TotalGoodFrames',
        'JabberErrors',
        'OutOfWindow',
        'TransmittedOK',
        'ReceivePacketsMissed',
        'TransmitCollisions',
        'CurrentLinkStatus',
        'Spare2',
        'Spare3',
    ],
}

FrontEndEntryFields = {
    "Q335": [
        'InitialVCO',
        'TimeError',
        'BestVCO',
        'TicksSinceLastUpdate',
        'Km',
        'State',
        'Flags',
        'SecondsSinceReSync',
        'NumberOfReSyncs',
        'SecondsSinceBoot',
        'CPCommunicationsErrors',
        'InputVoltageToFE',
        'SensorControlBitmap',
        'GainAndCalibratorStatus',
        'SensorTemperature',
        'SensorSerialNumber',
        'Channel1Boom',
        'Channel2Boom',
        'Channel3Boom',
        'Spare1',
    ],
}
FrontEndFields = {
    "Q335": [
        'NumberOfEntries',
        'LengthOfThisBlock',
    ]
}

BalerFields = {
    "Q330": [
        'Baler1LastTime',
        'Baler1TotalNumberOfCycles',
        'Baler1StatusAndTimeouts',
        'Baler1OnOffMinutes',
        'Baler2LastTime',
        'Baler2TotalNumberOfCycles',
        'Baler2StatusAndTimeouts',
        'Baler2OnOffMinutes',
        'Baler3LastTime',
        'Baler3TotalNumberOfCycles',
        'Baler3StatusAndTimeouts',
        'Baler3OnOffMinutes',
        'Baler4LastTime',
        'Baler4TotalNumberOfCycles',
        'Baler4StatusAndTimeouts',
        'Baler4OnOffMinutes',
    ],
}

DynamicIPFields = {
    "Q330": [
        'Serial1IPAddress',
        'Serial2IPAddress',
        'Reserved',
        'EthernetIPAddress',
    ],
}

AuxBoardEntryFields = {
    "Q330": [
        'Conversion',
    ],
}

AuxBoardFields = {
    "Q330": [
        'SizeOfThisBlock',
        'PacketVersion',
        'AuxType',
        'AuxVersion',
    ],
}

SerialSensorEntryFields = {
    "Q330": [
        (2, 'H', 'SizeOfThisSubBlock'),
        (2, 'H', 'SensorType'),
        (2, 'H', 'SerialInterface'),
        (2, 'H', 'SecondsPerSample'),
        (2, 'H', 'Units'),
        (2, 'H', 'IntegrationTime'),
        (2, 'H', 'FractionalDigits'),
        (2, 'H', 'ValidFields'),
        (4, 'L', 'PressureMeasurement'),
        (4, 'L', 'InternalTemperatureMeasurement'),
        (4, 'L', 'HumidityMeasurement'),
        (4, 'L', 'ExternalTemperatureMeasurement'),
    ],
}

SerialSensorFields = {
    "Q330": [
        'TotalSizeOfThisBlock',
        'NumberOfSubBlocks',
    ],
}

EPSDIDeviceEntry = {
    "Q330": [
        'SDIAddress',
        'Phase',
        'DriverID',
        'SDISpare1',
        'SensorModel',
        'SerialNumber',
        'SDISpare2',
        'SensorVersionNumber',
        'SDISpare3',
    ],
}

EPADCDeviceEntry = {
    "Q330": [
        'ADCSerialNumberHigh32',
        'ADCSerialNumberLow32',
        'ADCModel',
        'ADCRevision',
        'ADCSpare1',
        'ADCSpare2',
        'ADCSpare3',
        'ADCSpare4',
        'ADCSpare5',
    ],
}

EnvironmentalProcessorFields = {
    "Q330": [
        'InitialVCO',
        'TimeError',
        'BestVCO',
        'TicksSinceLastUpdate',
        'Km',
        'State',
        'Spare1',
        'EPSerialNumberHigh32',
        'EPSerialNumberLow32',
        'ProcessorID',
        'SecondsSinceBoot',
        'SecondsSinceReSync',
        'NumberOfReSyncs',
        'Q330CommunicationsErrors',
        'EPCommunicationsErrors',
        'Spare2',
        'SDIDevicesActive',
        'FirmwareVersion',
        'Flags',
        'AnalogChannels',
        'EPModel',
        'EPRevision',
        'Gains',
        'InputVoltageToEP',  # 0.1V increments
        'InternalHumidity',  # 1% increments
        'BuiltInPressure',  # ubar
        'InternalTemperature',  # 0.1C increments
        'ADInputChannel1Counts',
        'ADInputChannel2Counts',
        'ADInputChannel3Counts',
        'Spare3',
    ],
}

_UserMessageFields = [
    'SendersIPAddress',
    'UserMessage',
]
UserMessageFields = {
    "Q330": _UserMessageFields,
    "Q335": _UserMessageFields,
}

StatusBits = {
    "Q330": {
        'Global': 0,
        'Gps': 1,
        'PowerSupply': 2,
        'BoomPosition': 3,
        'Thread': 4,
        'PLL': 5,
        'Satellites': 6,
        'ARP': 7,
        'DataPort1': 8,
        'DataPort2': 9,
        'DataPort3': 10,
        'DataPort4': 11,
        'Serial1': 12,
        'Serial2': 13,
        'Serial3': 14,
        'Ethernet': 15,
        'Baler': 16,
        'DynamicIP': 17,
        'AuxiliaryBoard': 18,
        'SerialSensor': 19,
        'EnvironmentalProcessor': 20,
    },
    "Q335": {
        'Global': 0,
        'Gps': 1,
        'BoomPosition': 3,
        'PLL': 5,
        'Satellites': 6,
        'ARP': 7,
        'DataPort1': 8,
        'DataPort2': 9,
        'DataPort3': 10,
        'DataPort4': 11,
        'Communications': 16,
        'FrontEnd': 21,
    },
}

StatusFields = {
    "Q330": {
        'Global': GlobalFields,
        'Gps': GpsFields,
        'PowerSupply': PowerSupplyFields,
        'BoomPosition': BoomFields,
        'Thread': ThreadFields,
        'PLL': PLLFields,
        'Satellites': SatelliteFields,
        'ARP': ARPFields,
        'DataPort1': DataPortFields,
        'DataPort2': DataPortFields,
        'DataPort3': DataPortFields,
        'DataPort4': DataPortFields,
        'Serial1': SerialFields,
        'Serial2': SerialFields,
        'Serial3': SerialFields,
        'Ethernet': EthernetFields,
        'Baler': BalerFields,
        'DynamicIP': DynamicIPFields,
        'AuxiliaryBoard': AuxBoardFields,
        'SerialSensor': SerialSensorFields,
        'EnvironmentalProcessor': EnvironmentalProcessorFields,
    },
    "Q335": {
        'Global': GlobalFields,
        'Gps': GpsFields,
        'BoomPosition': BoomFields,
        'PLL': PLLFields,
        'Satellites': SatelliteFields,
        'ARP': ARPFields,
        'DataPort1': DataPortFields,
        'DataPort2': DataPortFields,
        'DataPort3': DataPortFields,
        'DataPort4': DataPortFields,
        'Communications': CommunicationsFields,
        'FrontEnd': FrontEndFields,
    },
}

StatusStrings = {
    "Q330": {
        'Global': 'Global Status',
        'Gps': 'GPS Status',
        'PowerSupply': 'Power Supply Status',
        'BoomPosition': 'Boom Positions, Temperatures, and Voltages',
        'Thread': 'Thread Status',
        'PLL': 'PLL Status',
        'Satellites': 'GPS Satellites',
        'ARP': 'ARP Status',
        'DataPort1': 'Data Port 1 Status',
        'DataPort2': 'Data Port 2 Status',
        'DataPort3': 'Data Port 3 Status',
        'DataPort4': 'Data Port 4 Status',
        'Serial1': 'Serial Interface 1 Status',
        'Serial2': 'Serial Interface 2 Status',
        'Serial3': 'Serial Interface 3 Status',
        'Ethernet': 'Ethernet Status',
        'Baler': 'Baler Status',
        'DynamicIP': 'Dynamic IP Address',
        'AuxiliaryBoard': 'Auxiliary Board Status',
        'SerialSensor': 'Serial Sensor Status',
        'EnvironmentalProcessor': 'Environmental Processor Status',
    },
    "Q335": {
        'Global': 'Global Status',
        'Gps': 'GPS Status',
        'BoomPosition': 'Boom Positions, Temperatures, and Voltages',
        'PLL': 'PLL Status',
        'Satellites': 'GPS Satellites',
        'ARP': 'ARP Status',
        'DataPort1': 'Data Port 1 Status',
        'DataPort2': 'Data Port 2 Status',
        'DataPort3': 'Data Port 3 Status',
        'DataPort4': 'Data Port 4 Status',
        'Communications': 'Communication Status',
        'FrontEnd': 'Front-End Status',
    },
}

StatusKeys = {
    "Q330": {
        0: 'Global',
        1: 'Gps',
        2: 'PowerSupply',
        3: 'BoomPosition',
        4: 'Thread',
        5: 'PLL',
        6: 'Satellites',
        7: 'ARP',
        8: 'DataPort1',
        9: 'DataPort2',
        10: 'DataPort3',
        11: 'DataPort4',
        12: 'Serial1',
        13: 'Serial2',
        14: 'Serial3',
        15: 'Ethernet',
        16: 'Baler',
        17: 'DynamicIP',
        18: 'AuxiliaryBoard',
        19: 'SerialSensor',
        20: 'EnvironmentalProcessor',
    },
    "Q335": {
        0: 'Global',
        1: 'Gps',
        3: 'BoomPosition',
        5: 'PLL',
        6: 'Satellites',
        7: 'ARP',
        8: 'DataPort1',
        9: 'DataPort2',
        10: 'DataPort3',
        11: 'DataPort4',
        16: 'Communications',
        21: 'FrontEnd',
    },
}


def getBitmap(whichStatus, deviceType='Q330'):
    return getBitmapFromKeys(whichStatus, deviceType)


def getBitmapFromKeys(whichStatus, deviceType='Q330'):
    """
    Return a bitmap field usable in a c1_rqstat, based on a list of
    strings.  These strings should come from the StatusTypes list in
    this module.

    If only one status is desired, it may be passed in as a string, or a
    one element list, 
    """
    bitmap = 0
    if type(whichStatus) == str:
        whichStatus = [whichStatus]
    if type(whichStatus) in (tuple, list):
        for status in whichStatus:
            bitmap |= 1 << StatusBits[deviceType][status]
    return bitmap


def getKeysFromBitmap(bitmap, deviceType='Q330'):
    values = []
    statusKeys = StatusKeys[deviceType]
    for i in range(0, 32):
        test_bit = 0x0000001 << i
        if i in statusKeys and (bitmap & test_bit):
            values.append(statusKeys[i])
    return values


def bitmapToString(b, deviceType='Q330'):
    """
    Return a string representing a c1_stat status bitmap
    """
    values = []
    statusKeys = StatusKeys[deviceType]
    statusBits = StatusBits[deviceType]
    statusStrings = StatusStrings[deviceType]
    for i in range(0, 32):
        if i in statusKeys:
            values.append(statusStrings[statusKeys[i]])
        else:
            values.append('')
    return Bits.bitsToStrings(b, values)


# A class which can be used to wrap a dictionary which may or may not contain
# keys with the specified prefix and postfix.
#
# Inserts automatically append and prepend the supplied prefixes and 
# postfixes respectively
#
# Deletions and retrievals search for the key with the prefix and postfix
# first, and revert to the supplied key if the modified key was not found.
# The key fallback behavior can be switched off 
class StatusDict():
    def __init__(self, hashtable, prefix='', postfix='', key_fallback=True):
        self._hashtable = hashtable
        self._prefix = prefix
        self._postfix = postfix
        self._key_fallback = key_fallback

    def _get_key(self, key):
        return self._prefix + key + self._postfix

    # Python 2 to 3 requirement to handle if x "in" y
    def __contains__(self, item):
        return self.__getitem__(item)

    def __getitem__(self, key):
        vkey = self._get_key(key)
        if vkey in self._hashtable:
            return self._hashtable[vkey]
        elif self._key_fallback:
            return self._hashtable[key]
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        vkey = self._get_key(key)
        self._hashtable[vkey] = value

    def __delitem__(self, key):
        vkey = self._get_key(key)
        if vkey in self._hashtable:
            del self._hashtable[vkey]
        elif self._key_fallback:
            del self._hashtable[key]
        else:
            raise KeyError(key)

    def has_key(self, key):
        vkey = self._get_key(key)
        if vkey in self._hashtable:
            return True
        elif self._key_fallback and key in self._hashtable:
            return True
        return False

    def keys(self):
        return list(self._hashtable.keys())

    def values(self):
        return list(self._hashtable.values())

    def items(self):
        return list(self._hashtable.items())
