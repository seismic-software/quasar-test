License
=======

Unless otherwise noted, This project is in the public domain in the United
States because it contains materials that originally came from the United
States Geological Survey, an agency of the United States Department of
Interior. For more information, see the official USGS copyright policy at
https://www.usgs.gov/information-policies-and-instructions/copyrights-and-credits

Additionally, we waive copyright and related rights in the work
worldwide through the CC0 1.0 Universal public domain dedication.


CC0 1.0 Universal Summary
-------------------------

This is a human-readable summary of the
[Legal Code (read the full text)][1].


### No Copyright

The person who associated a work with this deed has dedicated the work to
the public domain by waiving all of his or her rights to the work worldwide
under copyright law, including all related and neighboring rights, to the
extent allowed by law.

You can copy, modify, distribute and perform the work, even for commercial
purposes, all without asking permission.


### Other Information

In no way are the patent or trademark rights of any person affected by CC0,
nor are the rights that other persons may have in the work or in how the
work is used, such as publicity or privacy rights.

Unless expressly stated otherwise, the person who associated a work with
this deed makes no warranties about the work, and disclaims liability for
all uses of the work, to the fullest extent permitted by applicable law.
When using or citing the work, you should not imply endorsement by the
author or the affirmer.



[1]: https://creativecommons.org/publicdomain/zero/1.0/legalcode  


### TERMS AND CONDITIONS FOR ACCESSING OR OTHERWISE USING ISTI QU@SAR  

#### ISTI COMMUNITY LICENSE AGREEMENT

1. This LICENSE AGREEMENT is between the Instrumental Software
Technologies, Inc.  ("ISTI"), and the Individual or Organization
("Licensee") accessing and otherwise using QU@SAR software in source
or binary form and its associated documentation.

2. Subject to the terms and conditions of this License Agreement, ISTI
hereby grants Licensee an exclusive, royalty-free, world-wide license
to develop with, analyze, test, perform and/or display publicly,
QU@SAR or any prepare derivative works of QU@SAR, and otherwise use
QU@SAR alone or in any derivative version, provided, however, that
ISTI's License Agreement (this text) and ISTI's notice of copyright, i.e.,
"Copyright (c) 2003 Instrumental Software Technologies, Inc; All
Rights Reserved" are retained in QU@SAR alone or in any derivative
version prepared by Licensee.

3. In the event Licensee prepares a derivative work that is based on
or incorporates QU@SAR or any part thereof, and wants to make the
derivative work available to others as provided herein, then Licensee
hereby agrees to include in any such work a brief summary of the
changes made to QU@SAR. Furthermore, the Licensee will only distribute
the derivative work to other valid license holders of the QU@SAR
product or any other parties who maybe be covered under any community
aggreements with ISTI. ISTI will provide a list of valid licensed
users of QU@SAR. (See section 9 below).

4. ISTI is making QU@SAR available to Licensee on an "AS IS" basis.
ISTI MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED.  BY
WAY OF EXAMPLE, BUT NOT LIMITATION, ISTI MAKES NO AND DISCLAIMS ANY
REPRESENTATION OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE
OF QU@SAR WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.

5. ISTI SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF QU@SAR
FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS A
RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING QU@SAR OR ANY
DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.

6. This License Agreement will automatically terminate upon a material
breach of its terms and conditions.

7. Nothing in this License Agreement shall be deemed to create any
relationship of agency, partnership, or joint venture between ISTI and
Licensee.  This License Agreement does not grant permission to use
ISTI trademarks or trade name in a trademark sense to endorse or
promote products or services of Licensee, or any third party.

8. By copying, installing or otherwise using QU@SAR, Licensee agrees
to be bound by the terms and conditions of this License Agreement.

9. At the time that ISTI has recovered its development costs, we will
permit an exception to clause 3. Distribution of any derivative work
to any academic user of QU@SAR will be allowed regardless of license
holders. This notification will be made via electronic email to the
signer of the license. At this time ISTI will open up the QU@SAR
product for free to academic use only.

10. ISTI will be the arbitrar of all disputes as to who is
authorised to used QU@SAR in either binary or source format, and by
extension all derivitive works.

11. If the software is provided with, or as part of a commercial
product, or is used in other commercial software products the customer
must be informed that "This product includes software developed by
Instrumental Software Technologies, Inc.  (http://www.isti.com)"

12.  If the software is provided with, or as part of a commercial
product, all advertising materials mentioning features or use of this
software must display the following acknowledgment: "This product
includes software developed by Instrumental Software Technologies,
Inc. (http://www.isti.com)"