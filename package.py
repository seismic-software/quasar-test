#!/usr/bin/python -O
import os
import shutil
import sys

import clean
import compile_tools
from build import utils

clean_dirs = [
    'Quasar',
    'isti',
    'build'
]

compile_dirs = []

copy_dirs_compiled = []

copy_dirs_uncompiled = [
    ('Quasar', 'Quasar', 'build/Quasar/Quasar'),
    ('Quasar', 'isti', 'build/Quasar/isti'),
]

copy_dirs = [
    # ('Example', 'util/Example/bin' , 'build/Example')
]

copy_files = [
    ('Quasar', 'build/install_Quasar.py', 'build/Quasar/install_Quasar.py'),
    ('Quasar', 'build/install.py', 'build/Quasar/install.py'),
    ('Quasar', 'build/utils.py', 'build/Quasar/utils.py'),
]

archives = [
    ('Quasar', 'Quasar.tar.bz2', 'build', 'Quasar'),
]

version_file = 'build/Quasar/Quasar/__version__.py'


class ExMessage(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


def ignore_uncompiled(directory, file_list):
    ignore = []
    for file_name in file_list:
        full_name = "%s/%s" % (directory, file_name)
        if (len(file_name) > 3) and (file_name[-3:] in ['.py']):
            continue
        elif (file_name != '.svn') and os.path.isdir(full_name):
            continue
        ignore.append(file_name)
    return ignore


def ignore_compiled(directory, file_list):
    ignore = []
    for file_name in file_list:
        full_name = "%s/%s" % (directory, file_name)
        if file_name[0:11] == '__init__.py':
            if file_name[-4:] not in ['.pyc', '.pyo']:
                continue
        elif (len(file_name) > 4) and (file_name[-4:] in ['.pyc', '.pyo']):
            continue
        elif (file_name != '.svn') and os.path.isdir(full_name):
            continue
        ignore.append(file_name)
    return ignore


def ignore_svn(directory, file_list):
    ignore = []
    for file_name in file_list:
        full_name = f"{directory}/{file_name}"
        if file_name != '.svn':
            continue
        ignore.append(file_name)
    return ignore


if __name__ == "__main__":

    try:
        package_all = False
        package_ids = {}
        if len(sys.argv) < 2:
            package_all = True
        else:
            for arg_id in sys.argv[1:]:
                if arg_id == 'all':
                    package_all = True
                    break
                else:
                    package_ids[arg_id] = True

        master_dir = os.getcwd()

        try:
            print("Cleaning...")
            for directory in clean_dirs:
                clean.clean_dir(directory)
        except IOError as e:
            raise ExMessage(f"Cleaning failed. {e}") from e

        try:
            print("Compiling...")
            for directory in compile_dirs:
                compile_tools.compile_dir(directory)
        except IOError as e:
            raise ExMessage(f"Compile failed. {e}") from e

        try:
            print("Copying compiled directories...")
            for (arg_id, source, destination) in copy_dirs_compiled:
                if (not package_all) and (arg_id not in package_ids):
                    continue
                if os.path.exists(destination):
                    shutil.rmtree(destination)
                utils.copy_tree(source, destination, symlinks=True,
                                ignore=ignore_compiled)
        except IOError as e:
            raise ExMessage(f"Directory Copy failed. {e}") from e

        try:
            print("Copying uncompiled directories...")
            for (arg_id, source, destination) in copy_dirs_uncompiled:
                if (not package_all) and (arg_id not in package_ids):
                    continue
                if os.path.exists(destination):
                    shutil.rmtree(destination)
                utils.copy_tree(source, destination, symlinks=True,
                                ignore=ignore_uncompiled)
        except IOError as e:
            raise ExMessage(f"Directory Copy failed. {e}") from e

        try:
            print("Copying directories...")
            for (arg_id, source, destination) in copy_dirs:
                if (not package_all) and (arg_id not in package_ids):
                    continue
                if os.path.exists(destination):
                    shutil.rmtree(destination)
                utils.copy_tree(source, destination, symlinks=True,
                                ignore=ignore_svn)
        except IOError as e:
            raise ExMessage(f"Directory Copy failed. {e}") from e

        try:
            print("Copying individual files...")
            for (arg_id, source, destination) in copy_files:
                if (not package_all) and (arg_id not in package_ids):
                    continue
                shutil.copy(source, destination)
        except IOError as e:
            raise ExMessage(f"File Copy failed. {e}") from e

        try:
            version_string = \
            os.popen("git describe --tags --always").readlines()[-1].strip()
            print("Creating version file... (%s)")
            open(version_file, 'w+').write(f"version=\"{version_string}\"\n")
        except IOError as e:
            raise ExMessage(f"Could not create version file. {e}") from e

        try:
            print("Generating Package(s)...")
            for (arg_id, pkg, location, source) in archives:
                if (not package_all) and (arg_id not in package_ids):
                    continue
                package = master_dir + '/' + pkg
                directory = master_dir + '/' + location
                package_command = "cd %s; tar cvjf %s %s" % (
                directory, package, source)
                md5_command = "md5sum %s" % package

                result = os.popen(package_command).read()
                if len(result):
                    print(result)
                result = os.popen(md5_command).read()
                if len(result):
                    print(result)

        except IOError as e:
            raise ExMessage(f"Copy failed. {e}") from e

        print("Done.")
    except ExMessage as e:
        print("E:", str(e))

