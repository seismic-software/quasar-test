"""
This module is intended as a wrapper to the struct module.  The goal
here is to provide 'Q' and 'q' support to versions of the struct module
prior to version 2.2.

This module was initially written to allow for Jython use (Jython smells
quite a bit like Python2.1) and a nice side effect was the ability for
CPython to work with pre-2.2 as well.

We rely on the library supplied struct module as much as we can, and
in cases where the library supplied module has support for 'q' and 'Q',
we let it do all the work.  In the cases where it does not, we do an
on-the-fly conversion (long long -> two longs, and vice versa) and hand
that off to the struct module.
"""

import struct

STRUCT_SUPPORTS_Q = 0

error = struct.error
HIGH_32_BIT_MASK = 0xFFFFFFFF00000000
LOW_32_BIT_MASK = 0x00000000FFFFFFFF
FULL_64_BIT_MASK = 0xFFFFFFFFFFFFFFFF


##
# do we even need to bother dealing with this ourselves
##
def fmtNeedsAttention(fmt):
    if STRUCT_SUPPORTS_Q == 1:
        return 0
    elif 'q' in fmt:
        return 1
    elif 'Q' in fmt:
        return 1
    else:
        return 0


def getNativeByteOrder():
    myLong = struct.pack('=L', 0xFF00)
    if myLong == '\x00\xff\x00\x00':
        return '<'
    return '>'


def splitLongLong(longLong, byteOrder):
    """
    take a longlong, and create a 2 element tuple.
    byteOrder should be one of the following: <, >, =, signifying
    little endian, big endian, or native, signifying which order
    the tuple is in
    """

    longLong = 1 * longLong

    if longLong != (longLong & FULL_64_BIT_MASK):
        raise error("Number too large for 64 bits")

    high32 = (longLong & HIGH_32_BIT_MASK) >> 32
    low32 = longLong & LOW_32_BIT_MASK

    bo = byteOrder
    if bo == '=':
        bo = getNativeByteOrder()

    if bo == '<':
        return low32, high32
    return high32, low32


def joinLongLong(longTuple, byteOrder):
    """
    take a 2 element tuple, and create a single longlong from it.
    byteOrder should be one of the following: <, >, =, signifying
    little endian, big endian, or native, signifying which order
    the tuple is in
    """
    if longTuple[0] != (longTuple[0] & LOW_32_BIT_MASK):
        raise error("Number too large for 32 bits")

    if longTuple[1] != (longTuple[1] & LOW_32_BIT_MASK):
        raise error("Number too large for 32 bits")

    bo = byteOrder
    if bo == '=':
        bo = getNativeByteOrder()

    if bo == '<':
        low32 = longTuple[0]
        high32 = longTuple[1]
    else:
        low32 = longTuple[1]
        high32 = longTuple[0]

    result = (1 * high32) << 32
    result += low32
    return result


##
# calculate the number of elements in a struct format
##
def numElementsInFormat(fmt):
    currentMultiplier = ''
    currentNumberOfElements = 0
    for ch in fmt:
        # these are byte order markers
        if ch in '=<>@!':
            continue
        # handle numeric multipliers
        elif ch in '0123456789':
            currentMultiplier += ch
        # handle element markers that use mutipliers
        elif ch in 'xcbBhHiIlLqQfdP':
            if currentMultiplier == '':
                currentNumberOfElements += 1
            else:
                currentNumberOfElements += int(currentMultiplier)
                currentMultiplier = ''
        # handle element markers that do not use multipliers
        elif ch in 'ps':
            currentNumberOfElements += 1
            currentMultiplier = ''
        else:
            raise error("Invalid character in format string '%s'" % ch)
    return currentNumberOfElements


##
# split a format into sections, giving each 'Q' or 'q' it's own section
##
def splitFormat(fmt):
    ret = []
    currentFmtChunk = ''
    if fmt[0] in '<>!@=':
        byteOrderMarker = fmt[0]
    for ch in fmt:
        if ch in '<>!@=':
            continue
        if ch in 'qQ':
            if currentFmtChunk != '':
                ret.append(byteOrderMarker + currentFmtChunk)
                currentFmtChunk = ''
            ret.append(byteOrderMarker + ch)
        else:
            currentFmtChunk += ch
    if currentFmtChunk != '':
        ret.append(byteOrderMarker + currentFmtChunk)
    return ret


##
# Below are functions that are proxied to the struct module.  These functions
# handle 'q' and 'Q' functionality locally, if we are running on a python that
# does not handle these itself.  The Python2.3 documentation for the struct
# module should be refered to.
##


def _pack(fmt, *args):
    if fmt == '':
        return
    if STRUCT_SUPPORTS_Q or (not fmtNeedsAttention(fmt)):
        newArgs = (fmt,) + args
        return struct.pack(*newArgs)
    else:
        byteOrderMarker = fmt[0]
        if fmt[0] not in '=<>':
            byteOrderMarker = '='
        ret = ''
        argStart = 0
        # now we need to split the fmt/args into chunks, and pack each individually
        formatParts = splitFormat(fmt)
        for formatChunk in formatParts:
            longToTuple = 0
            numElements = numElementsInFormat(formatChunk)
            if 'q' in formatChunk:
                formatChunk = formatChunk.replace('q', 'll')
                longToTuple = 1
            elif 'Q' in formatChunk:
                formatChunk = formatChunk.replace('Q', 'LL')
                longToTuple = 1
            argChunk = args[argStart:argStart + numElements]
            argStart += numElements
            if longToTuple == 1:
                argChunk = splitLongLong(argChunk[0], byteOrderMarker)
            ret += struct.pack(*(formatChunk,) + argChunk)
        return ret


def _unpack(fmt, string):
    if fmt == '':
        return
    if STRUCT_SUPPORTS_Q or (not fmtNeedsAttention(fmt)):
        return struct.unpack(fmt, string)
    else:
        byteOrderMarker = fmt[0]
        if fmt[0] not in '=<>':
            byteOrderMarker = '='
        ret = []
        strStart = 0
        formatParts = splitFormat(fmt)
        # split, parse, join
        for formatChunk in formatParts:
            numBytes = calcsize(formatChunk)
            strChunk = string[strStart:strStart + numBytes]
            strStart += numBytes
            tupleToLong = 0
            if 'q' in formatChunk:
                formatChunk = formatChunk.replace('q', 'll')
                tupleToLong = 1
            elif 'Q' in formatChunk:
                formatChunk = formatChunk.replace('Q', 'LL')
                tupleToLong = 1
            result = struct.unpack(*(formatChunk, strChunk))
            if tupleToLong:
                ret += [joinLongLong(result, byteOrderMarker)]
            else:
                ret += result
        return ret


def _calcsize(fmt):
    # Jython's struct module does _not_ handle this correctly
    if fmt == '':
        return 0
    if STRUCT_SUPPORTS_Q or (not fmtNeedsAttention(fmt)):
        return struct.calcsize(fmt)
    else:
        newfmt = fmt.replace('Q', 'LL')
        newfmt = newfmt.replace('q', 'LL')
        return struct.calcsize(newfmt)


#  here we check to see if we even need to do anything
try:
    struct.pack('Q', 0)
    STRUCT_SUPPORTS_Q = 1
    pack = struct.pack
    unpack = struct.unpack
    calcsize = struct.calcsize
except:
    STRUCT_SUPPORTS_Q = 0
    pack = _pack
    unpack = _unpack
    calcsize = _calcsize

if __name__ == '__main__':
    import sys

    l = int(sys.argv[1])
    spl = splitLongLong(l, '=')
    print(l)
    print(spl)
    print(joinLongLong(spl, '='))
