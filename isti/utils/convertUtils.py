
import calendar
import struct
import sys
import time
import traceback


class IllegalByteFormatException(Exception):
    pass


class InvalidDateException(Exception):
    def __init__(self, msg):
        self.msg = msg
        Exception.__init__(self)

    def getMsg(self):
        return self.msg


def reformatTime(rawTime):
    """
    turn a time, with some delimiters, into yyyy/mm/d hh:mm:ss format.  
    Missing values are inserted, defaults being
    2000/1/1 0:0:0
    """

    if not rawTime:
        return ''
    parts = [2000, 1, 1, 0, 0, 0]
    numbers = '0123456789'
    validSeperators = [' ', '/', '-', '.', ':']
    numBuf = ''
    currentPart = 0
    numSeps = 0
    foundSpace = False
    for ch in rawTime:
        if ch in numbers:
            numBuf += ch
        elif ch not in validSeperators:
            raise InvalidDateException("Invalid character in date: '%s'" % ch)
        else:
            numSeps += 1
            if ch == ' ':
                foundSpace = True
            if numSeps > 2 and not foundSpace:
                raise InvalidDateException("Malformed Date")
            if numBuf != '':
                parts[currentPart] = int(numBuf)
                currentPart += 1
                numBuf = ''

    # if we've gotten to the end, and there is data in the buffer...
    if numBuf:
        parts[currentPart] = int(numBuf)
    if parts[0] < 2000:
        raise InvalidDateException("Invalid Year: %d" % parts[0])
    if (parts[1] < 1) or (parts[1] > 12):
        raise InvalidDateException("Invalid Month: %d" % parts[1])
    if (parts[2] < 1) or (parts[2] > 31):
        raise InvalidDateException("Invalid Day: %d" % parts[2])
    if (parts[3] < 0) or (parts[3] > 24):
        raise InvalidDateException("Invalid Hour: %d" % parts[3])
    if (parts[4] < 0) or (parts[4] > 59):
        raise InvalidDateException("Invalid Minute: %d" % parts[4])
    if (parts[5] < 0) or (parts[5] > 59):
        raise InvalidDateException("Invalid Second: %d" % parts[5])

    ret = '%4d/%02d/%02d %02d:%02d:%02d' % tuple(parts)
    return ret


def makeEpochTime(timeStr):
    """
    Take a time in the form of yyyy-mm-dd hh:mm:ss and convert to epoch time
     """
    datePart, timePart = timeStr.strip().split(' ')
    year, month, day = datePart.split('-')
    hour, minute, second = timePart.split(':')

    timeTup = (int(year), int(month), int(day), int(hour), int(minute), int(second), 0, 0, -1)
    return time.mktime(timeTup)


def shiftLeftPreserving32Bits(num, shiftBy):
    val = 0xFFFFFFFF & (num << shiftBy)
    if type(val) == int:
        byteStr = struct.pack("<Q", val)
        valAsInt = struct.unpack("<l", byteStr[0:4])[0]
        return valAsInt

    return val


def formatExceptionInfo(maxTBlevel=10, infoTuple=None):
    if not infoTuple:
        cla, exc, trbk = sys.exc_info()
    else:
        cla, exc, trbk = infoTuple

    try:
        excName = cla.__name__
    except:
        excName = cla

    try:
        excArgs = exc.__dict__["args"]
    except KeyError:
        excArgs = "<no args>"
    excTb = traceback.format_tb(trbk, maxTBlevel)
    return excName, excArgs, excTb


def formatBytes(numBytes, bias="M"):
    numKilos = 0
    numMegs = 0
    bytesLeft = numBytes
    if bias in ('m', 'M'):
        numMegs = bytesLeft / (1024 * 1024)
        bytesLeft = bytesLeft - (numMegs * (1024 * 1024))

    if bias in ('M', 'm', 'k', 'K'):
        numKilos = bytesLeft / 1024
        bytesLeft = bytesLeft - (numKilos * 1024)

    if numMegs:
        return '%.02fM' % (numMegs + (numKilos / 1000.0))
    if numKilos:
        return '%.2fK' % (numKilos + (bytesLeft / 1000.0))
    return '%dB' % bytesLeft


def unformatBytes(byteStr):
    byteStr = byteStr.strip()

    label = byteStr[-1].lower()

    bases = {
        'b': 1,
        'k': 1024,
        'm': 1024 * 1024,
        'g': 1024 * 1024 * 1024
    }

    try:
        num = float(byteStr[:-1])
    except:
        raise IllegalByteFormatException(byteStr)

    if label not in list(bases.keys()):
        raise IllegalByteFormatException(byteStr)

    return int(num * bases[label])


def convertYearAndDayOfYearToEpoch(year, dayOfYear):
    year = int(year)
    dayOfYear = int(dayOfYear)

    firstOfYearAsEpoch = calendar.timegm((year, 1, 1, 0, 0, 0, 0, 0, 0))
    return firstOfYearAsEpoch + ((dayOfYear - 1) * (60 * 60 * 24))


def makeDuration(seconds):
    days = 0
    hours = 0
    minutes = 0
    ret = ''

    while seconds >= (60 * 60 * 24):
        days += 1
        seconds -= (60 * 60 * 24)
    if days:
        ret += "%dd " % days
    while seconds >= (60 * 60):
        hours += 1
        seconds -= (60 * 60)
    if hours:
        ret += "%dh " % hours
    while seconds >= 60:
        minutes += 1
        seconds -= 60
    if minutes:
        ret += "%dm " % minutes
    if seconds:
        ret += "%ds" % seconds

    return ret.strip()


def secondsToDuration(seconds):
    return makeDuration(seconds)


def durationToSeconds(duration):
    days = hours = minutes = seconds = 0
    buffer = ''
    for ch in duration:
        if ch in 'dhms':
            num = int(buffer)
            if ch == 'd':
                days = num
            elif ch == 'h':
                hours = num
            elif ch == 'm':
                minutes = num
            elif ch == 's':
                seconds = num
            buffer = ''
        else:
            buffer += ch

    return seconds + (60 * minutes) + (60 * 60 * hours) + (60 * 60 * 24 * days)


# ignore the << futurewarnings.  If we're not in 2.4 or higher, futurewarnings
# arent in existance, so the try/except will just except to a pass


try:
    import warnings

    warnings.filterwarnings("ignore", r'.*', FutureWarning, 'isti.utils.convertUtils', 0)
except:
    pass

if __name__ == '__main__':
    print('24 << 3 = %d' % shiftLeftPreserving32Bits(24, 3))
