from functools import partial


class GetSet:
    """
    An 'ability' type mixin/class that allows for
    'on the fly' creation of get/set functions as
    well as str functions
    """

    def __getattr__(self, attr):
        """
        Handle the cases where the code is looking for getSomething
        or setSomething.  We'll create these functions on the fly.
        """
        # expects a function name like getBasePort. In that case it will
        # establish a getting function for the field BasePort in the
        # inheriting class's __dict__ structure
        # similarly, for setBasePort, if there is no BasePort field in the
        # dict, an entry for 'BasePort' will be assigned the value to set

        # this has been modified to make it a bit more coherent with python 3
        if len(attr) >= 3:
            if attr[0:3] == 'set':
                return partial(self.set_attr, attr[3:])
            if attr[0:3] == 'get':
                return partial(self.get_attr, attr[3:])
            if attr[0:3] == 'str':
                return self.getStrFunction(attr[3:])
            else:
                raise AttributeError(attr)
        raise AttributeError(attr)

    def get_attr(self, attr):
        """
        Get the data member
        """
        if attr in self.__dict__:
            return self.__dict__[attr]
        return None

    def set_attr(self, attr, x):
        """ Get the 'set' function for a data member """
        self.__dict__[attr] = x

    def getStrFunction(self, rawField):
        """
        Get the string output function for the field if one is present.
        If not, return an anonymous function that converts the requested
        field to a string
        """
        # experimental support for indexing
        indexable = 0
        index = 0
        if '_' in rawField:
            try:
                parts = rawField.split('_')
                index = int(parts[-1])
                field = '_'.join(parts[:-1])
                indexable = True
            except ValueError:
                field = rawField
        else:
            field = rawField

        strMethod = f'str{field}'

        # make a list of all places we can get functions from
        classes = [self.__class__]
        classes += self.__class__.__bases__

        # reverse the class list, so we traverse it so that the root base
        # classes are in the beginning (so inheritance works as intended)
        # we're adding values to a dict, so the last values added with the
        # same name, will be the ones that get called.
        classes.reverse()

        # now a list of all attributes
        methods = {}
        for classOb in classes:
            methods.update(classOb.__dict__)

        if strMethod in list(methods):
            strFunc = getattr(self, strMethod)
            if indexable == 1:
                strFunc = partial(strFunc, index)
            return strFunc
        if field in self.__dict__ and indexable:
            return partial(str, self.get_attr(field)[index])
        else:
            stringify_this = self.get_attr(rawField)
            if stringify_this is None:
                def get_none():
                    return None
                return get_none
            return partial(str, stringify_this)

    def asDictionary(self):
        return self.__dict__
