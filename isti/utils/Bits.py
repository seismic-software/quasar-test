"""
Some items that make dealing with bitmaps easier.

Included are names for the powers of two (BIT0..BIT16)
and a function for decoding a bitfield into a series
of strings
"""


def bitIndexToInteger(index):
    return 2 ** index


def bitIndexListToInteger(indices):
    accum = 0
    for index in indices:
        accum += bitIndexToInteger(index)
    return accum


def bitsToStrings(bitmap, strValues, joinWith='\n      '):
    """
    Go through <bitmap>.  For each bit in <bitmap> there
    should be an element in <strValues>.  This element
    should either be a string, or a 2 element tuple.

    In the case of the string, that string is appended to the
    return value when it's coresponding bit is set, and nothing
    is added if the bit is not set.  In the case of a tuple, the
    first value of the tuple is used for a set bit, and the
    second value for an unset bit.

    Before returning, the various strings are joined together
    by inserting <joinWith> between each one
    """

    ret = ['']
    strBothValues = []
    for value in strValues:
        if type(value) in (type((1, 2)), type([1, 2])):
            strBothValues.append(value)
        else:
            strBothValues.append((value, None))

    bitmap = 1 * bitmap
    for power in range(0, 32):
        bitValue = pow(2, power)
        if len(strBothValues) > power:
            if bitmap & bitValue:
                if strBothValues[power][0] is not None:
                    ret.append(strBothValues[power][0])
            else:
                if strBothValues[power][1] is not None:
                    ret.append(strBothValues[power][1])

    return joinWith.join(ret)
