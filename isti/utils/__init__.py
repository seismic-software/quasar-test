# placeholder in order to make "Utils" a package

import sys

if sys.platform == 'win32':
    lineTerminator = '\r\n'
else:
    lineTerminator = '\n'
