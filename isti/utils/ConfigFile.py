# ======================================================================
# Copyright (C) 2000-2003 Instrumental Software Technologies, Inc. (ISTI)
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. If modifications are performed to this code, please enter your own
# copyright, name and organization after that of ISTI.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in
# the documentation and/or other materials provided with the
# distribution.
# 3. All advertising materials mentioning features or use of this
# software must display the following acknowledgment:
# "This product includes software developed by Instrumental
# Software Technologies, Inc. (http:#www.isti.com)"
# 4. If the software is provided with, or as part of a commercial
# product, or is used in other commercial software products the
# customer must be informed that "This product includes software
# developed by Instrumental Software Technologies, Inc.
# (http:#www.isti.com)"
# 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
# must not be used to endorse or promote products derived from
# this software without prior written permission. For written
# permission, please contact "info@isti.com".
# 6. Products derived from this software may not be called "ISTI"
# nor may "ISTI" appear in their names without prior written
# permission of Instrumental Software Technologies, Inc.
# 7. Redistributions of any form whatsoever must retain the following
# acknowledgment:
# "This product includes software developed by Instrumental
# Software Technologies, Inc. (http:#www.isti.com/)."
# 8. Redistributions of source code, or portions of this source code,
# must retain the above copyright notice, this list of conditions
# and the following disclaimer.
# THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
# TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
# INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import pprint
import sys
import xml.sax.handler
import xml.sax.saxutils

from defusedxml.sax import parseString


class MalformedXMLException(Exception):
    """Custom exception for when there's an XML error"""


def configDictToString(config_dict, prefix=''):
    lines = []
    for item in list(config_dict.keys()):
        currentPrefix = '%s%s' % (prefix, item)
        if item == 'ConfigFileName' and isinstance(config_dict[item], str):
            continue
        if isinstance(config_dict[item], dict):
            lines.append(configDictToString(config_dict[item], currentPrefix + '.'))
            lines.append('')
        elif isinstance(config_dict[item], list):
            for val in config_dict[item]:
                if isinstance(val, dict):
                    lines.append(configDictToString(val, currentPrefix + '.'))
                else:
                    lines.append('%s = %s' % (currentPrefix, val))
        else:
            lines.append('%s = %s' % (currentPrefix, config_dict[item]))
    return '\n'.join(lines)


class Config:
    def __init__(self, config=None, requiredParent=None, forceString=False):

        self._configuration = {}
        self.forceString = forceString
        self.requiredParent = requiredParent
        self.configProfile = ''

        if config:
            if '\r' in config:
                configLines = config.split('\r')
            else:
                configLines = config.split('\n')

            for line in configLines:
                self.parseConfigLine(line)

    def __repr__(self):
        return pprint.pformat(self._configuration)

    def __len__(self):
        return len(self._configuration) - 1

    def __getitem__(self, item):
        if len(self.configProfile) and not item.lower().startswith('global.'):
            item = '%s.%s' % (self.configProfile, item)
        parts = item.split('.')
        currentLevel = self._configuration
        for part in parts:
            try:
                currentLevel = currentLevel[part]
            except:
                return None
        return currentLevel

    def __str__(self):
        return configDictToString(self._configuration)

    def __setitem__(self, varName, varValue):
        parts = varName.split('.')
        if self.requiredParent and parts[0] != self.requiredParent:
            parts.insert(0, self.requiredParent)

        if len(self.configProfile) and not varName.lower().startswith('global.'):
            parts.insert(0, self.configProfile)

        parentMap = self._configuration
        for i in range(0, len(parts)):
            part = parts[i]
            if i == len(parts) - 1:
                ## ability to remove a config item
                if varValue == None:
                    if part in parentMap:
                        del parentMap[part]
                else:
                    if part in parentMap:
                        tmpVal = parentMap[part]
                        if type(tmpVal) != type([]):
                            parentMap[part] = []
                            parentMap[part].append(tmpVal)
                        parentMap[part].append(varValue)
                    else:
                        parentMap[part] = varValue
            else:
                if part in parentMap:
                    if type(parentMap[part]) != type({}):
                        tmpVal = parentMap[part]
                        parentMap[part] = {}
                        parentMap[part]['default_value'] = tmpVal
                    parentMap = parentMap[part]
                else:
                    parentMap[part] = {}
                    parentMap = parentMap[part]

    def hasProfile(self, profileName):
        if self[profileName]:
            return True
        return False

    def getProfiles(self):
        profiles = list(self._configuration.keys())
        ret = []
        for profileName in profiles:
            if profileName.lower() not in ('global', 'configfilename'):
                ret.append(profileName)
        return ret

    def setProfile(self, profileName):
        self.configProfile = profileName
        self['Global.CurrentProfile'] = None
        self['Global.CurrentProfile'] = profileName

    def getProfile(self):
        return self.configProfile

    def parseConfigLine(self, line):
        # skip empty lines:
        if not line or not len(line):
            return
        # skip comment lines
        if line[0] in '#\r\n\'*':
            return
        # skip lines without an '='
        if '=' not in line:
            return
        while line[-1] in ('\r', '\n'):
            line = line[:-1]

        varName = line.split('=')[0].strip()
        varValue = self.getLikelyValueFromString('='.join(line.split('=')[1:]))
        if self.requiredParent and not varName.startswith('%s.' % self.requiredParent):
            varName = '%s.%s' % (self.requiredParent, varName)

        if type(varName) == str:
            varName = str(varName)
        if type(varValue) == str:
            varValue = str(varValue)

        self[varName] = varValue

    def getConfigMap(self):
        return self._configuration

    def setConfigMap(self, newConfigMap):
        if 'ConfigFileName' in self._configuration:
            newConfigMap['ConfigFileName'] = self._configuration['ConfigFileName']
        self._configuration = newConfigMap

    def getLikelyValueFromString(self, strVal):
        strVal = strVal.strip()
        if self.forceString or 'D' in strVal or 'E' in strVal:
            return strVal
        try:
            ret = int(strVal)
            return ret
        except:
            try:
                ret = int(strVal)
                return ret
            except:
                return strVal


class ConfigFile(Config):

    def __init__(self, configFile):
        Config.__init__(self)
        self._configuration['ConfigFileName'] = configFile
        self.readConfigFile(configFile)
        if self['Global.CurrentProfile']:
            self.configProfile = self['Global.CurrentProfile']

    def readConfigFile(self, fileName):
        if not os.path.isfile(fileName):
            return
        with open(fileName, encoding='utf-8').readlines() as lines:
            for line in lines:
                self.parseConfigLine(line)

    def writeToFile(self, filename=None):
        if not filename:
            filename = self._configuration['ConfigFileName']
        asStr = configDictToString(self._configuration)
        with open(filename, 'w', encoding='utf-8') as outfile:
            outfile.write(asStr)


# This method is missing a definition for MalformedXMLException, which
# does not appear to be available anywhere. This entire class is not used
# and can probably be safely removed.
class XMLConfig(Config, xml.sax.handler.ContentHandler):

    def __init__(self, config, *args, **kwargs):
        Config.__init__(self, None, *args, **kwargs)
        if 'forceString' in kwargs:
            del kwargs['forceString']

        xml.sax.handler.ContentHandler.__init__(self)
        self.currentElementFamilyTree = []
        self.currentValue = ''

        if config:
            parseString(str(config), self)

    def getCurrentElementName(self):
        return '.'.join(self.currentElementFamilyTree)

    def startElement(self, elementName, elementAttrs):
        self.currentElementFamilyTree.append(elementName)
        self.currentValue = ''

    def endElement(self, elementName):
        closing = self.currentElementFamilyTree.pop()
        if closing != elementName:
            self.currentElementFamilyTree.append(closing)
            raise MalformedXMLException("Unexpected close of %s" % elementName)

        tmpVal = self.currentValue
        tmpVal = tmpVal.replace('\n', '')
        tmpVal = tmpVal.replace('\r', '')
        tmpVal = tmpVal.replace('\t', '')
        tmpVal = tmpVal.replace(' ', '')
        if len(tmpVal):
            configLine = '%s.%s = %s' % (self.getCurrentElementName(), closing, self.currentValue)
            self.parseConfigLine(configLine)
        self.currentValue = ''

    def characters(self, content):
        self.currentValue += content

    def makeXMLFromConfigMap(self, configMap=None, parentPath=''):
        if not configMap:
            configMap = self._configuration
        configKeys = list(configMap.keys())
        ret = ""
        indent = ''
        if parentPath:
            indent = '   ' * (parentPath.count('.') + 1)

        for key in configKeys:

            ret += "%s<%s>" % (indent, key)
            value = configMap[key]

            if type(value) == dict:
                if parentPath:
                    parent = '%s.%s' % (parentPath, key)
                else:
                    parent = key
                ret += '\r\n'
                ret += self.makeXMLFromConfigMap(value, parent)
                ret += indent

            else:
                ret += '%s' % xml.sax.saxutils.escape(value)
            ret += "</%s>\r\n" % key

        ret = ret.replace('\r\r', '\r')

        return ret

    def writeToFile(self, fileName=None):
        with open(fileName, 'w', encoding='utf-8') as outFile:
            outFile.write(self.makeXMLFromConfigMap())


class XMLConfigFile(XMLConfig):

    def __init__(self, configFile=None):
        with open(configFile, 'r').read() as xmlData:
            self.configFileName = configFile
            XMLConfig.__init__(self, xmlData)

    def writeToFile(self, fileName=None):

        if fileName:
            fileToOpen = fileName
        else:
            fileToOpen = self.configFileName

        with open(fileToOpen, 'w', encoding='utf-8') as outFile:
            outFile.write(self.makeXMLFromConfigMap())


if __name__ == '__main__':
    f = Config(sys.argv[1])
    print(f)
