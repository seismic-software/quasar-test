# Quasar for Python 3

This is a library used by cnc-for-quasar to interface with remote Q330 Digitizers. 
Some more details can be found in the docs subdirectory;
quasar.txt includes some info about components while qdp-packets enumerates what each QDP command packet represents.

Quasar was original developed by ISTI in collaboration with Kinemetrics. Since then the USGS has kept the software up-to-date and made improvements and modifications.

Contact ISTI if you'd like to engage ISTI to write additional software or make changes.
